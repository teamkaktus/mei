<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1200px, maximum-scale=1.0, user-scalable=yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" href="web/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>



<div class="wrap">
    <div class="header cf">
        <div class="left_side cf">
            <div class="socials">
                <span>Мы в соцсетях:</span>
                <div>
                    <a href="#3"><i class="mi_icon fb_icon"></i></a>
                    <a href="#3"><i class="mi_icon tw_icon"></i></a>
                    <a href="#3"><i class="mi_icon yt_icon"></i></a>
                    <a href="http://vk.com/ckmei"><i class="mi_icon vk_icon"></i></a>
                </div>
            </div>
            <a href="/" class="main_logo">
                <img src="/img/logo.png" alt="">
            </a>
        </div>
        <div class="right_side cf">
            <div class="btns_create">
            <?php if (\Yii::$app->user->isGuest) {?>
                <a href="#" class="big_white_btn login_opener">Создать резюме</a>
                <a href="#" class="big_white_btn login_opener">Разместить вакансию</a>
            <?php }else {?>
                <?php if (Yii::$app->session['user']['role']=='соискатель'){?>
                    <a href="/updaterezume" class="big_white_btn">Создать резюме</a>
                    <a onclick="$('#error_vak_modal').arcticmodal();" class="big_white_btn">Разместить вакансию</a>
                <?php }else {?>
                    <a onclick="$('#error_ruzume_modal').arcticmodal();" class="big_white_btn">Создать резюме </a>
                    <a class="big_white_btn vacancy_opener">Разместить вакансию</a>
                <?php }?>
            <?php }?>
            </div>
            <div class="authorization">
                <?php if (\Yii::$app->user->isGuest) {
                    ?>
                <a href="#" class="login_link login_opener"><i class="mi_icon login_icon"></i>Вход</a>
                <a href="#" class="reg_link registration_opener"><i class="mi_icon reg_icon"></i>Регистрация</a>
                <?php
                } else {?>
                    <?php if (Yii::$app->session['user']['role']=='соискатель'){?>
                    <a href="/worker" class="login_link"><i class="mi_icon reg_icon"></i>Личный кабинет</a>
        <?php }else {?>
                        <a href="/employer" class="login_link"><i class="mi_icon reg_icon"></i>Личный кабинет</a>
        <?php }?>
                    <a href="/logout" class="login_link"><i class="mi_icon login_icon"></i>Выход</a>
    <?php }?>
            </div>
            <div class="nav">
                <ul class="cf">
                    <li><a href="/">Главная</a></li>
                    <li class="">
                        <a href="#3">Наши проекты</a>
                    </li>
                    <li><a href="<?=Url::to(['/site/vacancies'])?>">Вакансии</a></li>
                    <li><a href="/information">Полезная информация</a></li>
                    <li><a href="/contact">Контакты</a></li>
                </ul>
            </div>
            <div class="phone_section">
                <a href="+7 (495) 345-78-09" class="call_to_us">+7 (495) 345-78-09</a>
                <a class="call_to_us_btn review_opener">Обратная связь</a>
            </div>
            <div class="search">
                <ul class="tabs-menu cf">
                    <li class="current"><a href="#for_job">Ищу вакансии</a></li>
                    <li><a href="#for_resume">Ищу резюме</a></li>
                </ul>
                <div class="tab">
                    <div id="for_job" class="tab-content">

                        <?php $form = ActiveForm::begin([
                            'action' => '/vacancies',
                        ]) ?>
                            <div class="visible_part">
                                <a class="open_search" style="position: absolute; margin-left: 577px; margin-top: 13px; z-index: 1;"><i class="mi_icon open_search_icon"></i><span><b>Развернуть</b> расширенный поиск</span></a>
                                <?php echo \kartik\widgets\Typeahead::widget([
                                    'name'=>'name',
                                    'options' => ['placeholder' => 'Например: Программист','class'=>'main_search_input'],
                                    'pluginOptions' => [
                                        'highlight'=>true,
                                        'minLength'=>3
                                    ],
                                    'dataset' => [
                                        [
                                            'remote' => [
                                                'url' => urldecode(Url::to(['api/vacancyname','q'=>'%QUERY'])),
                                                'wildcard' => '%QUERY'
                                            ],
                                            'limit' => 10
                                        ]
                                    ]
                                ]);?>

                            </div>

                            <div class="hidden_part_title">Режим расширенного поиска:</div>
                            <div class="hidden_part cf">
                                <div class="input_section">
                                    <label for="by_work_condition1">Режим работы</label>
                                    <?=Html::dropDownList('daily_work',[],[
                                        'Пн - Пт'=>'Пн - Пт',
                                        'Вт - Сб'=>'Вт - Сб',
                                        'Свободный'=>'Свободный',
                                        'Удаленный'=>'Удаленный',
                                        'Сессионный'=>'Сессионный',
                                        '1 через 2'=>'1 через 2'
                                    ],['prompt'=>''])?>
                                </div>
                                <div class="input_section">
                                    <label for="by_work_department1">По факультету</label>
                                    <input type="text" name="facultet" style="height: 35px; width: 215px"/>
                                </div>
                                <div class="input_section">
                                    <label for="by_job_title1">По названию вакансии</label>
                                    <input type="text" name="name2" style="height: 35px; width: 215px"/>
                                </div>
                                <div class="input_section">
                                    <label for="by_job_payment1">Зарплата(руб.)</label>
                                    <input type="text" name="salary_b" style="height: 35px; width: 107px" placeholder="ОТ">
                                    <input type="text" name="salary_e" style="height: 35px; width: 107px" placeholder="ДО">
                                </div>
                                <div class="input_section">
                                    <label for="by_hot1">Образование</label>
                                    <?= Html::dropDownList('education', [], ['Среднее'=>'Среднее','Высшее'=>'Высшее'],['prompt'=>'']) ?>

                                </div>
                                <div class="input_section">
                                    <input class="submit_btn" type="submit" value="Искать!">
                                    <i class="mi_icon mi_search"></i>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div id="for_resume" class="tab-content">
                        <form action="">
                            <div class="visible_part">
                                <input type="search" class="main_search_input" placeholder="Например: Программист"/>
                                <a href="#3" class="open_search"><i class="mi_icon open_search_icon"></i><span><b>Развернуть</b> расширенный поиск</span></a>
                            </div>

                            <div class="hidden_part_title">Режим расширенного поиска:</div>
                            <div class="hidden_part cf">
                                <div class="input_section">
                                    <label for="by_work_condition2">Режим работы</label>
                                    <select name="" id="by_work_condition2">
                                        <option value="">Свободный</option>
                                        <option value="">Удаленный</option>
                                        <option value="">Сессионный</option>
                                    </select>
                                </div>
                                <div class="input_section">
                                    <label for="by_work_department2">По факультету</label>
                                    <select name="" id="by_work_department2">
                                        <option value="">Гуманитарный</option>
                                        <option value="">Нормальный</option>
                                    </select>
                                </div>
                                <div class="input_section">
                                    <label for="by_job_title2">По названию вакансии</label>
                                    <select name="" id="by_job_title2">
                                        <option value="">Програмист</option>
                                        <option value="">Дизайнео</option>
                                        <option value="">Верстальщик</option>
                                        <option value="">SEO-шник</option>
                                    </select>
                                </div>
                                <div class="input_section">
                                    <label for="by_job_payment2">Зарплата</label>
                                    <select name="" id="by_job_payment2">
                                        <option value="">до 500 у.е.</option>
                                        <option value="">500-1500 у.е</option>
                                        <option value="">1500+ у.е.</option>
                                    </select>
                                </div>
                                <div class="input_section">
                                    <label for="by_hot2">Горящая / Негорящая</label>
                                    <select name="" id="by_hot2">
                                        <option value="">Горящая</option>
                                        <option value="">Негорящая</option>
                                    </select>
                                </div>
                                <div class="input_section">
                                    <input class="submit_btn" type="submit" value="Искать!">
                                    <i class="mi_icon mi_search"></i>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $content ?>

<?= \app\widgets\PartnersWidget::widget()?>

</div>
<div class="footer_wr cf">
    <div class="footer">
        <div class="left_part">
            <p>График работы:<br />
                с9.00 до 19.00 ежедневно, кроме Вс.</p>
            <p class="extra_padding">г. Москва, ул. Маяковского, 34, офис 567<br />
                3-ий этаж.</p>
            <p>© 2010 - 2015. МЭИ. Все права защищены.</p>
            <div class="socials">
                <a href="#3"><i class="mi_icon fb_icon"></i></a>
                <a href="#3"><i class="mi_icon tw_icon"></i></a>
                <a href="#3"><i class="mi_icon yt_icon"></i></a>
                <a href="http://vk.com/ckmei"><i class="mi_icon vk_icon"></i></a>
            </div>
        </div>
        <div class="center_part">
            <nav class="footer_nav">
                <ul>
                    <li><a href="#3">Наши проекты</a></li>
                    <li><a href="#3">Поиск резюме</a></li>
                    <li><a href="/hotvacancies">Горящие вакансии</a></li>
                    <li><a href="/vacancies">Поиск вакансий</a></li>
                    <li><a href="/information">Полезная информация</a></li>
                    <li><a href="/posts">Статьи</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="#3">Условия использования</a></li>
                    <li><a href="/contact">Контакты</a></li>
                </ul>
            </nav>
        </div>
        <div class="right_part">
            <div class="phone_section">
                <a href="+7 (495) 345-78-09" class="call_to_us">+7 (495) 345-78-09</a>
                <a href="#3" class="call_to_us_mail">Эл. почта: mei@gmail.com</a>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="logon_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div>
            <div class="modal_title_1">
                вход в личный кабинет
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'login1-form',
                'action' => '/signin',
            ]) ?>
                <input type="text" placeholder="Ваш логин:" name="login" required/>
                <input type="password" placeholder="Введите пароль:" name="password" required/>
            <a class="cancel_btn arcticmodal-close" href="#">Забыли пароль?</a>
            <div class="row" style="margin: 0px">
                <input type="submit" class="btn btn-success" value="Войти" style="margin-right: 10px"/>
                <input type="submit" class="btn btn-warning arcticmodal-close registration_opener" value="Регистрация"/>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="review_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div class="modal_title_1">
            обратная связь
        </div>
        <form action="">
            <input type="text" placeholder="Ваше имя:" required/>
            <input type="email" placeholder="Ваша электронная почта:" required/>
            <textarea name=""  >
            </textarea>
            <input type="submit" value="Перезвонить"/>
        </form>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="registration_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div class="modal_wrap">
            <div class="modal_title_2">
                <span>Регистрация</span>
            </div>
            <form action="" id="ls_reg_form">
                <div class="cf">
                    <div class="input_holder"><label for="reg_name">Ваше имя:</label><input id="reg_name" type="text" name="username" required/></div>
                    <div class="input_holder"><label for="reg_mail">Ваша электронная почта:</label><input id="reg_mail" type="email" name="email" required/></div>
                    <div class="input_holder"><label for="reg_login">Ваш логин:</label><input id="reg_login" type="text" name="login" required/></div>
                    <div class="input_holder"><label for="reg_pass">Ваш пароль:</label><input id="reg_pass" type="password" name="password" required/></div>
                </div>
                <div class="cf">
                    <input type="radio" id="worker" name="worker" value="1" checked="checked"/><label class="radio_label" for="worker">- я соискатель</label>
                    <input type="radio" id="worker2" name="worker" value="2"/><label class="radio_label" for="employer">- я работодатель</label>
                </div>

                <div class="cf">
                    <!--                    arcticmodal-close - это класс закрывающий модалку  arcticmodal-close rules_opener-->
                    <input type="submit" class="ls-reg-form" value="Ок"/>
                    <a href="#3" class="cancel_btn arcticmodal-close">Отмена</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="rules">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div class="modal_wrap">
            <div class="modal_title_3">
                Ознакомьтесь с правилами регистрации
            </div>
            <div class="text_part">
                <p>- Регистрация на сайте бесплатная и свободная.
                <p>- При регистрации на сайте пользователь соглашается выполнять данные Правила безусловно.
                <p>- Для регистрации на сайте пользователь должен предоставить действующий адрес электронной почты на него будет выслано письмо со ссылкой подтверждения регистрации.
                <p>- Выбор имени пользователя (nickname) является вашим исключительным правом.
                <p>- Администрация оставляет за собой право принять меры к прекращению использования nickname, если его использование нарушает общепринятые моральные и этические нормы или является оскорбительным для других пользователей сайта.
                <p>- Запрещена регистрация nickname, настолько схожего с уже существующими, что оно может ввести в заблуждение других пользователей сайта.
                <p>- Созданный вами аккаунт из сайта не удаляется
                <p>- Заполнив все обязательные поля регистрации Вы получаете возможность: Если Вы соискатель - откликаться на вакансии и регистрироваться на мероприятия, создавать / редактировать / сохранять / распечатывать резюме. Если Вы работодатель - размещать вакансии и подавать заявки на участие в мероприятиях.
                <p>- Заполняя формы регистрации Вы соглашаетесь на обработку персональных данных
            </div>
            <form action="">
                <div class="cf">
                    <input type="checkbox" checked id="license" value="1"/><label class="radio_label" for="license">- я согласен</label>
                </div>
                <div class="cf">
                    <input type="submit" id="ls_red_end" class="" value="Ок"/>
                    <a href="#" class="cancel_btn arcticmodal-close">Отмена</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="vacancy_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <?php if(!\Yii::$app->user->isGuest) echo \app\widgets\VacancyWidget::widget();?>
    </div>
</div>

<div style="display: none;">
    <div class="box-modal" id="error_ruzume_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div>
            <div class="modal_title_1">
                <div class="modal_title_1">
                    ОШЫБКА!
                </div>
                <p style='color: #ffffff; font-family: "BebasNeueBold","helvetica","arial",sans-serif; font-size: 30px; padding-bottom: 30px; text-transform: uppercase;'>Данная опция доступна для соискатель. Хотите авторизоваться как соискатель?</p>
                <a href="/out" class="btn btn-success">ДА</a>
                <a class="btn btn-danger arcticmodal-close">НЕТ</a>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <div class="box-modal" id="error_vak_modal">
        <div class="box-modal_close arcticmodal-close" style="margin-top: -20px;"><i class="mi_icon mi_close"></i></div>
        <div>
            <div class="modal_title_1">
                ОШЫБКА!
            </div>
            <p style='color: #ffffff; font-family: "BebasNeueBold","helvetica","arial",sans-serif; font-size: 30px; padding-bottom: 30px; text-transform: uppercase;'>Данная опция доступна для работодателей. Хотите авторизоваться как работодатель?</p>
            <a href="/out" class="btn btn-success">ДА</a>
            <a class="btn btn-danger arcticmodal-close">НЕТ</a>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>