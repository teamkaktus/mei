<?php
/**
 *
 * @var PsiWhiteSpace $model
 */
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_key
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_desc
]);
?>

<div class="middle_full anns cf">
    <div class="middle_sbr cf">
        <div class="title mb34"><span><?=$model->name?></span></div>

        <div class="middle_content">
            <div class="news_item shadow_box cf open_anons">
                <div class="img_wrapper">
                    <img src="<?=$model->img?>" alt="<?=$model->img_alt?>">
                </div>
                <div content="info_section cf">
                    <div class="small_img_wrap">
                        <img src="<?=$model->img?>" alt="<?=$model->img_alt?>">
                    </div>
                    <div class="content_section">
                        <div class="title_5"><?php //echo $model->name?></div>
                        <div class="text_section">
                            <?=$model->desc?>
                        </div>

                        <div class="vis_when_open cf">
                            <div class="left_side">
                                <?php if (\Yii::$app->user->isGuest) {
                                    ?>
                                    <a class="small_btn registration_opener">Зарегистрироваться</a>
                                    <?php
                                } else {
                                    if (Yii::$app->session['user']['role'] == 'соискатель') {
                                        ?>
                                        <a class="small_btn" onclick="events_reg(<?=$model->id?>)">Зарегистрироваться</a>
                                        <?php
                                    }
                                }?>
                                <div class="news_data"><i class="mi_icon mi_calendar"></i><span><?=$model->date_for?></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a href="/" class="all_news_btn">На главную</a>
        </div>
    </div>

    <div class="sidebar_right sidebar">
        <a href="" class="sidebar_section mb53 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>

        <?=\app\widgets\NewsWidget::widget()?>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>
