<?php
/**
 *
 * @var PsiWhiteSpace $model
 */
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_key
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_desc
]);
?>

<div class="middle_full anns cf">
    <div class="middle_sbr cf">
        <div class="title mb34"><span><?=$model->name?></span></div>

        <div class="middle_content">
            <div class="news_item shadow_box cf open_anons">
                <div class="img_wrapper">
                    <img src="<?=$model->img?>" alt="<?=$model->img_alt?>">
                </div>
                <div content="info_section cf">
                    <div class="small_img_wrap">
                        <img src="<?=$model->img?>" alt="<?=$model->img_alt?>">
                    </div>
                    <div class="content_section">
                        <div class="title_5"><?php //echo $model->name?></div>
                        <div class="text_section">
                            <?=$model->desc?>
                        </div>

                        <div class="vis_when_open cf">
                            <div class="left_side">
                                <div class="news_data"><i class="mi_icon mi_calendar"></i><span><?=Yii::$app->formatter->asDate($model->updated_at)?></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a href="/" class="all_news_btn">На главную</a>
        </div>
    </div>

    <div class="sidebar_right sidebar">
        <a href="" class="sidebar_section mb53 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <div class="title"><span>Анонс мероприятий</span></div>
        <div class="sidebar_one_post mb70">
            <a class="img_wrapper">
                <img src="img/content/004.jpg" alt="">
            </a>
            <span class="title_3">Презентация компании шлюмберже, которая состоится 7 октября!</span>
            <p>Посетите презентацию компании шлюмберже, которая состоится 7 октября в г. Москва,на бульваре Маяковского 89, зал 3.Будет интересно и незабываемо! Напиткии еда как бесплатное угощение!</p>
            <a href="#3" class="all_news_btn">Подробнее</a>
        </div>
        <?=\app\widgets\PostsWidget::widget()?>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>
