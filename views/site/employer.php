<?php
/**
 *
 */?>
<div class="content cf">
    <?= \app\widgets\UserWidget::widget()?>

    <div class="middle_sbl mb15 mbs">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="middle_full cf">
    <div class="middle_sbr cf">
        <div class="employer_content">
            <div class="title_8">Мои данные:</div>
            <div class="info_part">
                <p><?= $user->surname.' '.$user->username.' '.$user->lastname?></p>
                <p><?= $user->tel_m?>
                    <br /><?= $user->tel_d?></p>
                <p><?= $user->email?></p>
                <p><?=$kompany->name?>
                    <br /><?=$kompany->industry?></p>

                <p><?=$kompany->type_activity?><p/>
            </div>
            <div class="title_8">
                Реквизиты компании:
            </div>
            <a class="update_info" href="/employerupdate">Редактировать данные</a>
            <div class="company_info">
                <div class="primary_section">
                    <div class="company_info_line"><?=$kompany->name?></div>
                    <div class="company_info_line"><b><span>Юридический адрес</span></b><?=$kompany->u_adres?></div>
                    <div class="company_info_line"><b><span>Фактичесикй адрес</span></b><?=$kompany->f_adres?></div>
                </div>
                <div class="company_info_line"><b><span>ИНН</span></b><?=$kompany->inn?></div>
                <div class="company_info_line"><b><span>КПП</span></b><?=$kompany->kpp?></div>
                <div class="company_info_line"><b><span>р/с</span></b><?=$kompany->country?></div>
                <div class="company_info_line"><b><span>Банк</span></b><?=$kompany->name_bank?></div>
                <div class="company_info_line"><b><span>к/с</span></b><?=$kompany->country_bank?></div>
                <div class="company_info_line"><b><span>БИК</span></b><?=$kompany->bik?></div>
                <div class="company_info_line"><b><span>КОД ОКПО, ОКОНХ / ОКВЭД</span></b><?=$kompany->kod?></div>
                <div class="company_info_line"><b><span>Подписант</span></b><?=$kompany->fio_podpysanta?></div>
                <div class="company_info_line"><b><span>Должность подписанта</span></b><?=$kompany->industry_podpysanta?></div>
                <div class="company_info_line"><b><span>Основание подписи</span></b><?=$kompany->osnovanie?></div>
            </div>
            <?= \app\widgets\PlacementVacanciesWidget::widget()?>
            <?= \app\widgets\ArchivVacanciesWidget::widget()?>
            <div class="user_events">
                <div class="title_6"><span>Мои мероприятия</span></div>
                <div class="cf user_events_list">
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/001.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/002.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/003.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="cf">
                        <a href="#3" class="near_info">Посмотреть ближайшие</a>
                    </div>
                </div>
            </div>
        </div>    </div>
    <div class="sidebar_right sidebar">
        <?=\app\widgets\HotVacanciesWidget::widget()?>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <?=\app\widgets\EventsbarWidget::widget()?>
        <?=\app\widgets\PostsWidget::widget()?>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>
