<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
?>
<div class="site-contact">
    <div class="middle_full cf">
        <div class="cf">
            <div class="contacts_left">
                <div class="title"><span>Контакты</span></div>
                <div class="contact_text">
                    <div class="contact_title">Как добраться</div>
                    <p>Голливудский актер Крис Рок не собирается отказываться от проведения церемонии «Оскар» из-за скандала с расистской подоплекой. Об этом изданию Entertainment Tonight сообщил продюсер шоу Реджинальд Худлин. По его словам, артист решил переписать свои монологи так, чтобы прокомментировать со сцены ситуацию, сложившуюся вокруг премии.</p>
                    <p>Как пояснил Худлин, Рок закончил работу над своей программой около недели назад, но после того, как Уилл Смит и его жена Джада объявили «Оскару» бойкот, актер взялся за новый сценарий. Голливудский актер Крис Рок не собирается отказываться от проведения церемонии «Оскар» из-за скандала с расистской подоплекой. Об этом изданию Entertainment Tonight сообщил продюсер шоу Реджинальд Худлин. По его словам, артист решил переписать свои монологи так, чтобы прокомментировать со сцены ситуацию, сложившуюся вокруг премии.</p>
                    <p>Как пояснил Худлин, Рок закончил работу над своей программой около недели назад, но после того, как Уилл Смит и его жена Джада объявили «Оскару» бойкот, актер взялся за новый сценарий.</p>
                </div>
            </div>
            <div class="contacts_map shadow_box">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5080.99181897548!2d30.477784409378035!3d50.45048948066465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1461263811627" frameborder="0"  allowfullscreen></iframe>
                <a href="#3" class="btn_for_dnld">Скачать карту с описанием проезда</a>
            </div>
            <div class="contacts_right">
                <p><i class="mi_icon mi_location"></i>г. Москва, проспект Пушкина,
                    23, оф. 845</p>
                <p><i class="mi_icon mi_phone"></i>+7 (495) 435-96-09<br/>
                    +7 (495) 325-69-45</p>
                <p><i class="mi_icon mi_mail"></i>infomei@gmail.com</p>
                <p><i class="mi_icon mi_skype"></i>skype: mei2015</p>
                <div class="socials">
                    <span>Мы в соцсетях:</span>
                    <div>
                        <a href="#3"><i class="mi_icon fb_icon"></i></a>
                        <a href="#3"><i class="mi_icon tw_icon"></i></a>
                        <a href="#3"><i class="mi_icon yt_icon"></i></a>
                        <a href="#3"><i class="mi_icon vk_icon"></i></a>
                    </div>
                </div>
            </div>
        </div></div>
</div>
