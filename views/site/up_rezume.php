<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\Pages */
/* @var $form yii\widgets\ActiveForm */

$yers = [];
for($i=1985;$i<2010;$i++){
    $yers[$i]=$i;
}

$mouth = [
    'Янваль'=>'Янваль',
    'Февраль'=>'Февраль',
    'Март'=>'Март',
    'Апрель'=>'Апрель',
    'Май'=>'Май',
    'Июнь'=>'Июнь',
    'Август'=>'Август',
    'Сентябрь'=>'Сентябрь',
    'Октябрь'=>'Октябрь',
    'Ноябрь'=>'Ноябрь',
    'Декабрь'=>'Декабрь'
]
?>
<div class="content cf">
    <?= \app\widgets\UserWidget::widget()?>

    <div class="middle_sbl mbs">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>    </div>
</div>
<div class="middle_full cf">
    <div class="middle_sbr cf">
        <div class="user_content update_page">
            <form method="post">
            <div class="updt_section_1 updt_section cf">
                <div class="title_8">Редактировать данные:</div>
                <p>Если вы хотите сохранить или распечатать готовое резюме, необходимо заполнить все поля, если Вы хотите откликаться на вакансии и регистрироваться на мероприятии, но готовое резюме Вам не нужно, достаточно заполнить поля со вездочкой</p>
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_input1">Ваш логин:</label>
                        <input type="text" id="name_input1" name="login" required value="<?=$user->login?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_1">Ваш пароль:</label>
                        <input type="password" id="name_password1_1" name="password" required value="<?=$user->password?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Повторить пароль:</label>
                        <input type="password" id="name_password1_2" name="repit_password" required value="<?=$user->password?>"/>
                    </div>
                </div>
                <a class="update_info" href="#3">Редактировать данные</a>
            </div>
            <div class="updt_section_2 updt_section cf">
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_input2" >Фамилия:</label>
                        <input type="text"  name="surname" required value="<?=$user->surname?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_1">Имя:</label>
                        <input type="text"  name="username" required value="<?=$user->username?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2"  >Отчество:</label>
                        <input type="text"  name="lastname" required value="<?=$user->lastname?>"/>
                    </div>
                </div>
                <div class="cf">
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="">Ваша дата рождения:</label>
                        <?php     echo \yii\widgets\MaskedInput::widget([
                            'name' => 'date',
                            'value' => $user->date,
                            'clientOptions' => ['alias' =>  'dd/mm/yyyy']
                        ]);?>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_password1_2">Ваш пол:</label>
                        <?= Html::dropDownList('sex', $user->sex, ['М'=>'М','Ж'=>'Ж']) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_phone1">Ваш номер телефона:</label>

                        <?php     echo \yii\widgets\MaskedInput::widget([
                            'name' => 'tel_m',
                            'value' => $user->tel_m,
                            'mask' => '(999) 999-9999'
                        ]);?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Ваша электронная почта:</label>
                        <input type="email" id="name_mail1" name="email" required value="<?=$user->email?>"/>
                    </div>
                </div>
            </div>
            <div class="updt_section_3 updt_section cf">
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_phone1">Ваше место жительства:</label>
                    <input type="text" id="name_phone1" name="sity" required value="<?=$rezume->sity?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label" for="name_mail1">Станция метро:</label>
                    <input type="text" id="name_phone1" name="st_metro" value="<?=$rezume->st_metro?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Ваше гражданство:</label>
                    <input type="text" id="name_phone1" name="citizenship" required value="<?=$rezume->citizenship?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Переезд:</label>

                    <?= Html::dropDownList('crossing', $rezume->crossing, ['Согласен'=>'Согласен','Не согласен'=>'Не согласен','Возможно обсуждение'=>'Возможно обсуждение']) ?>
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Родной язык:</label>
                    <?= Html::input('text', 'native_lang', $rezume->native_lang) ?>
                </div>

            </div>
                <div class="updt_section_4 updt_section cf" >
                    <div class="cf" id="langs">

                        <?php
                        $k=1;
                        foreach($langs as $lang){
                            ?>
                        <div class="row" style="margin-left: 10px;">
                            <div class="input_holder_3">
                                <label class="main_label important_input" for="name_mail1">Дополнительный язык:</label>
                                <input type="text" id="name_phone1" name="lang<?=$k;?>" value="<?=$lang->name?>" />
                            </div>
                            <div class="input_holder_3">
                                <label class="main_label important_input" for="name_mail1">
                                    Уровень владения языками:</label>
                               <select name="level<?=$k;?>" >
                                   <?php if($lang->level=='Базовые знания') {?>
                                        <option value="Базовые знания" selected>Базовые знания</option>
                                    <?php }else {?>
                                        <option value="Базовые знания">Базовые знания</option>
                                   <?php }?>
                                    <?php if($lang->level=='Среднее') {?>
                                        <option value="Среднее" selected>Среднее</option>
                                    <?php } else {?>
                                        <option value="Среднее">Среднее</option>
                                    <?php }?>
                                    <?php if($lang->level=='Высшее') {?>
                                        <option value="Высшее" selected>Высшее</option>
                                    <?php }else {?>
                                        <option value="Высшее">Высшее</option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <?php $k++; }?>
                        <input type="hidden" value="<?=$k;?>" id="count_lang" name="count_lang"/>

                    </div>
                    <a class="small_red_btn" onclick="add_lang()">Добавить еще</a>
                </div>
            <div class="updt_section_4 updt_section cf">
                <div class="cf">
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_mail1">Отрасль в которой Вы хотели бы работать:</label>
                        <input type="text" id="name_phone1" name="sector_job" required value="<?=$rezume->sector_job?>" />
                    </div>

                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_mail1">Примерная зарплата(руб.)</label>
                        <?php     echo \yii\widgets\MaskedInput::widget([
                            'name' => 'salary',
                            'value' => $rezume->salary,
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);?>
                    </div>
                </div>

            </div>
            <div class="updt_section_4 updt_section cf">
                <div class="cf" id="vuzs">
                    <div class="row" style="margin-left: 10px">
                        <div class="input_holder_3">
                            <label class="main_label important_input" for="name_mail1">Ваше образование:</label>
                            <?= Html::dropDownList('education', $rezume->education, ['Среднее'=>'Среднее','Высшее'=>'Высшее']) ?>
                        </div>
                    </div>
                    <?php
                    $k=1;
                    foreach($vuzs as $vuz){
                        ?>
                    <div class="row" style="margin-left: 10px">
                        <div class="input_holder_3">
                            <label class="main_label important_input" for="name_mail1">ВУЗ:</label>
                            <input type="text" name="vuz_name<?=$k;?>" value="<?=$vuz->name?>"/>
                        </div>
                        <div class="input_holder_3 reg_icon">
                            <label class="main_label important_input" for="name_mail1">Факультет/институт:</label>
                            <input type="text" id="name_mail1" name="vuz_faculty<?=$k;?>" value="<?=$vuz->faculty?>"/>
                        </div>
                        <div class="input_holder_3 reg_icon">
                            <label class="main_label important_input" for="name_mail1">Специальность:</label>
                            <input type="text" id="name_mail1" name="vuz_specialty<?=$k;?>" value="<?=$vuz->specialty?>"/>
                        </div>
                        <div class="row  " style="margin-left: 0px">
                            <label class="main_label important_input" for="name_mail1">Годы обучения (магистратура):</label>
                            <div class="small_slct smallest_slct">
                                <label class="small_label">
                                    Месяц поступления
                                </label>
                                <?= Html::dropDownList('m_b_m'.$k, $vuz->month_entrance_m, $mouth, ['prompt'=>'']) ?>
                                <label class="small_label">
                                    Месяц окончания
                                </label>
                                <?= Html::dropDownList('m_e_m'.$k, $vuz->month_ending_m, $mouth,['prompt'=>'']) ?>
                                <label class="small_label">
                                    Год поступления
                                </label>
                                <?= Html::dropDownList('y_b_m'.$k, $vuz->year_entrance_m, $yers,['prompt'=>'']) ?>
                                <label class="small_label">
                                    Год окончания
                                </label>
                                <?= Html::dropDownList('y_e_m'.$k, $vuz->year_ending_m, $yers,['prompt'=>'']) ?>
                            </div>
                        </div>
                        <div class="row "  style="margin-left: 0px">
                            <label class="main_label important_input" for="name_mail1">Годы обучения (бакалавриат):</label>
                            <div class="small_slct smallest_slct">
                                <label class="small_label">
                                    Месяц поступления
                                </label>
                                <?= Html::dropDownList('m_b_b'.$k, $vuz->month_entrance_b, $mouth, ['prompt'=>'']) ?>
                                <label class="small_label">
                                    Месяц окончания
                                </label>
                                <?= Html::dropDownList('m_e_b'.$k, $vuz->month_ending_b, $mouth,['prompt'=>'']) ?>
                                <label class="small_label">
                                    Год поступления
                                </label>
                                <?= Html::dropDownList('y_b_b'.$k, $vuz->year_entrance_b, $yers,['prompt'=>'']) ?>
                                <label class="small_label">
                                    Год окончания
                                </label>
                                <?= Html::dropDownList('y_e_b'.$k, $vuz->year_ending_b, $yers,['prompt'=>'']) ?>
                            </div>
                        </div>
                    </div>
                    <?php  $k++; }?>

                    <input type="hidden" value="<?=$k?>" id="count_vuz" name="count_vuz"/>
                </div>
                <a href="#3" class="small_red_btn" onclick="add_vuz()">Добавить еще ВУЗ</a>
            </div>
            <div class="updt_section_5 updt_section cf">
                <div class="input_holder_2 reg_icon">
                    <label class="main_label important_input" for="name_mail1">Дополнительное образование:</label>
                    <textarea name="additional_education"  cols="30" rows="10"><?=$rezume->additional_education?></textarea>
                </div>
                <div class="input_holder_2 reg_icon">
                    <label class="main_label important_input" for="name_mail1">Курсы:</label>
                    <textarea name="courses"   cols="30" rows="10"><?=$rezume->courses?></textarea>
                </div>
            </div>
            <div class="updt_section_6 updt_section cf">
                <div class="cf" id="experience">
                    <div class="row" style="margin-left: 0;">
                        <div class="input_holder_5">
                            <label class="main_label important_input" for="name_mail1">Опыт работы:</label>
                            <?= Html::dropDownList('experience', $rezume->experience, ['Есть'=>'Есть','Нет'=>'Нет'],['prompt'=>'']) ?>
                        </div>
                    </div>

                <?php $k=1; foreach($experiences as $experience){?>
                <div class="cf">
                    <div class="input_holder_1">
                        <label class="main_label important_input" for="name_mail1">Годы работы:</label>
                        <div class="small_slct smallest_slct">
                            <label class="small_label">
                                С
                            </label>
                            <?= Html::dropDownList('m_w_b'.$k, $experience->month_work_b, $mouth,['prompt'=>'']) ?>

                            <?= Html::dropDownList('y_w_b'.$k, $experience->year_work_b, $yers,['prompt'=>'']) ?>

                            <label class="small_label">
                                По
                            </label>
                            <?= Html::dropDownList('m_w_e'.$k, $experience->month_work_e, $mouth,['prompt'=>'']) ?>

                            <?= Html::dropDownList('y_w_e'.$k, $experience->year_work_e, $yers,['prompt'=>'']) ?>
                        </div>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Место работы (страна, город, место):</label>
                        <textarea name="sity_work<?=$k?>"  cols="30" rows="10"><?=$experience->sity_work?></textarea>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Должность:</label>
                        <textarea name="situation<?=$k?>"  cols="30" rows="10"><?=$experience->situation?></textarea>
                    </div>
                </div>
                    <?php $k++; }?>
                    <input type="hidden" value="<?=$k?>" id="count_experience" name="count_experience"/>
                </div>
                <a class="small_red_btn add_univer" onclick="add_experience()">Добавить еще </a>
            </div>
            <div class="updt_section_6 updt_section cf">
                <div class="cf">
                    <div class="input_holder_2">
                        <label class="main_label important_input" for="vac_pc_skills">Владение компьютером:</label>

                            <div class="cb_wrap">
                                <?= Html::checkbox('ps_internet', $rezume->ps_internet, ['label' => '- интернет']);?>
                            </div>
                        <div class="cb_wrap">
                            <?= Html::checkbox('ps_office', $rezume->ps_office, ['label' => '- MS Office']);?>
                        </div>
                        <div class="cb_wrap">
                            <?= Html::checkbox('ps_photoshop', $rezume->ps_photoshop, ['label' => '- Photoshop']);?>
                        </div>
                        <div class="cb_wrap">
                            <?= Html::checkbox('ps_autocad', $rezume->ps_autocad, ['label' => '- Autocad']);?>
                        </div>
                        <div class="cb_wrap">
                            <?= Html::checkbox('ps_corel', $rezume->ps_corel, ['label' => '- Corel Draw']);?>
                        </div>
                        <div class="cb_wrap">
                            <?= Html::checkbox('ps_1c', $rezume->ps_1c, ['label' => '- 1С']);?>
                        </div>

                    </div>

                    <div class="input_holder_2 reg_icon">
                        <label class="main_label " for="name_mail1">Рекомендации:</label>
                        <textarea name="recommendations"  cols="30" rows="10"><?=$rezume->recommendations?></textarea>
                    </div>
                    <div class="input_holder_2">
                        <label class="main_label" for="vac_driver_skills">Водительское удостоверение:</label>
                        <div class="cb_line cf" id="vac_driver_skills">
                            <div class="cb_wrap">
                                <?= Html::checkbox('auto_cat_a', $rezume->auto_cat_a, ['label' => '- категория А']);?>
                            </div>
                            <div class="cb_wrap">
                                <?= Html::checkbox('auto_cat_b', $rezume->auto_cat_b, ['label' => '- категория В']);?>
                            </div>
                            <div class="cb_wrap">
                                <?= Html::checkbox('auto_cat_c', $rezume->auto_cat_c, ['label' => '- категория С']);?>
                            </div>
                            <div class="cb_wrap">
                                <?= Html::checkbox('auto_cat_d', $rezume->auto_cat_d, ['label' => '- категория D']);?>
                            </div>
                            <div class="cb_wrap">
                                <?= Html::checkbox('auto_cat_e', $rezume->auto_cat_e, ['label' => '- категория E']);?>
                            </div>
                        </div>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Увлечения:</label>
                        <textarea name="hobbies"  cols="30" rows="10"><?=$rezume->hobbies?></textarea>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Ключевые навыки / доп информация:</label>
                        <textarea name="information"  cols="30" rows="10"><?=$rezume->information?></textarea>
                    </div>
                </div>
                <div class="btns_line cf">
                    <button type="submit" class="btn_1">Сохранить все изменения</button>
<!--                    <a href="#3" class="btn_1">Сохранить все изменения</a>-->
                    <a href="#3" class="btn_3">Скачать резюме</a>
                    <a href="#3" class="btn_2 summary_opener">Посмотреть резюме</a>
                </div>
            </div>
            </form>
        </div><div class="user_content update_page">













            <div class="user_events">
                <div class="title_6"><span>Мои мероприятия</span></div>
                <div class="cf user_events_list">
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/001.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/002.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/003.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="cf">
                        <a href="#3" class="near_info">Посмотреть ближайшие</a>
                    </div>
                </div>
            </div>
            <div class="user_vacancies_list">
                <div class="title_6"><span>Мои вакансии:</span></div>
                <a href="#3" class="archive_btn">Архив вакансий</a>
                <div class="history_of_vac">
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Бухгалтер (девушка),</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Системный администратор,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Менеджер по продажам,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Флорист,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Футболист,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Маркетолог,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Консультант ЗОЖ,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Тренер,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="read_more"><a href="" class="">Посмотреть еще</a></div>
                </div>
            </div>
        </div>





    </div>
    <div class="sidebar_right sidebar">
        <div class="sidebar_section mb15 sb_hot_vacancies">
            <div><img src="img/rocket.png" alt=""></div>
            <div class="sb_title_4">горящие вакансии!</div>
            <p>Обратите внимание на горящие
                вакансии! Спешите найти работу,
                которая подходит именно вам!
            </p>
            <a href="#3" class="sb_orenge_btn">Смотреть горящие вакансии</a>
        </div>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <div class="title"><span>Анонс мероприятий</span></div>
        <div class="sidebar_one_post mb70">
            <a class="img_wrapper">
                <img src="img/content/004.jpg" alt="">
            </a>
            <span class="title_3">Презентация компании шлюмберже, которая состоится 7 октября!</span>
            <p>Посетите презентацию компании шлюмберже, которая состоится 7 октября в г. Москва,на бульваре Маяковского 89, зал 3.Будет интересно и незабываемо! Напиткии еда как бесплатное угощение!</p>
            <a href="#3" class="all_news_btn">Подробнее</a>
        </div>
        <div class="title"><span>полезная информация</span></div>
        <div class="sidebar_news">
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья. Правила работы, которые приведут
                        к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский палеонтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 2. Для многих карьера и зарплата являются чуть ли не синонимами.</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 3. Правила работы, которые приведут к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <a href="#3" class="all_news_btn green">Все новости</a>
        </div>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>

<div style="display: none;">
    <div class="box-modal" id="summary_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div class="modal_title_3">
            просмотр вашего резюме
        </div>
        <div class="modal_wrap">
            <div class="sum_create_date">Дата создания <?php echo Yii::$app->formatter->asDate( $rezume->created_at);?></div>
            <div class="modal_title_5"><?=$user->surname?> <?=$user->username?> <?=$user->lastname?></div>
            <div class="some"><?=$rezume->sector_job?></div>
            <div class="sum_info_table">
                <div class="sum_info_row cf">
                    <div class="sum_param">Мобильный телефон</div>
                    <div class="sum_value"><?=$user->tel_m?></div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Примерная зарплата</div>
                    <div class="sum_value"><?=$rezume->salary?> руб.</div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Электронная почта</div>
                    <div class="sum_value"><?=$user->email?></div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Возраст</div>
                    <div class="sum_value">22 года (<?=$user->date?>)</div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Проживание</div>
                    <div class="sum_value"><?=$rezume->sity?> <?php if ($rezume->st_metro) echo'(ст. м. '.$rezume->st_metro.')';?></div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Переезд</div>
                    <div class="sum_value">
                        <?=$rezume->crossing?>
                        <div class="sum_sub_info">Предпочтительное время в пути до будущей работы не является критерием для выбора работы</div>
                    </div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Гражданство</div>
                    <div class="sum_value"><?=$rezume->citizenship?></div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param">Языки</div>
                    <div class="sum_value">
                        <p><?=$rezume->native_lang?> (родной)</p>
                        <p>Английский (базовые знания)</p>
                    </div>
                </div>

                <div class="sum_info_row cf">
                    <div class="sum_param sub_title">Образование</div>
                    <div class="sum_value">
                        <p><?=$rezume->education?></p>
                        <b class="addition_param">Строительство, недвижимость</b>
                        <p>Инженер. Строительство. Начальный уровень / Мало опыта</p>
                    </div>
                </div>

                <div class="sum_info_row cf">
                    <div class="sum_param sub_title">Основное</div>
                    <div class="sum_value">
                        <b class="addition_param">Отрасль в которой Вы хотели бы работать</b>
                        <p><?=$rezume->sector_job?></p>
                        <b class="addition_param">Повышение квалификации, курсы</b>
                        <p><?=$rezume->courses?></p>
                    </div>
                </div>
                <div class="sum_info_row cf">
                    <div class="sum_param sub_title">Опыт работы</div>
                    <div class="sum_value">
                        <p>Июль 2011 - продолжаю работать ТермоИзоляция, ООО (Москва).  Строительство
                            / архитектура/недвижимость. Мастер СМР
                        </p>
                    </div>
                </div>





                <div class="sum_info_row cf">
                    <div class="sum_param sub_title">Ключевые навыки</div>
                    <div class="sum_value">
                        <b class="addition_param">Владение компьютером:</b>
                        <ul>
                            <?php if($rezume->ps_internet){?>
                                <li>- интернет,</li>
                            <?php }?>
                            <?php if($rezume->ps_office){?>
                                <li>- MS Office,</li>
                            <?php }?>
                            <?php if($rezume->ps_photoshop){?>
                                <li>- Photoshop,</li>
                            <?php }?>
                            <?php if($rezume->ps_autocad){?>
                                <li>- Autocad,</li>
                            <?php }?>
                            <?php if($rezume->ps_corel){?>
                                <li>- Corel Draw,</li>
                            <?php }?>
                            <?php if($rezume->ps_1c){?>
                                <li>- 1С,</li>
                            <?php }?>
                        </ul>
                        <br>
                        <b class="addition_param">Водительское удостоверение:</b>
                        <ul>
                            <?php if($rezume->auto_cat_a){?>
                                <li>- категория А,</li>
                            <?php }?>
                            <?php if($rezume->auto_cat_b){?>
                                <li>- категория B,</li>
                            <?php }?>
                            <?php if($rezume->auto_cat_c){?>
                                <li>- категория C,</li>
                            <?php }?>
                            <?php if($rezume->auto_cat_d){?>
                                <li>- категория D,</li>
                            <?php }?>
                            <?php if($rezume->auto_cat_e){?>
                                <li>- категория E,</li>
                            <?php }?>
                        </ul>
                        <br>
                        <?php if($rezume->recommendations){?>
                            <b class="addition_param">Рекомендации:</b>
                            <p>
                                <?=$rezume->recommendations?>
                            </p>
                        <?php }?>
                        <?php if($rezume->hobbies){?>
                            <b class="addition_param">Увлечения:</b>
                            <p>
                                <?=$rezume->hobbies?>
                            </p>
                        <?php }?>
                        <?php if($rezume->information){?>
                            <b class="addition_param">Дополнительная информация:</b>
                            <p>
                                <?=$rezume->information?>
                            </p>
                        <?php }?>

                    </div>
                </div>
                <div class="title_6"><span><?=$user->surname?> <?=$user->username?> <?=$user->lastname?></span></div>
            </div>
        </div>
    </div>
</div>
<script>
    function add_lang(){
       // alert($('#count_lang').val());
        var i =$('#count_lang').val();
        var elem = '<div class="row" style="margin-left: 10px;"> ' +
            '<div class="input_holder_3"> ' +
            '<label class="main_label important_input" for="name_mail1">Дополнительный язык:</label> ' +
            '<input type="text" id="name_phone1" name="lang'+i+'" value="" /> ' +
            '</div> ' +
            '<div class="input_holder_3"> ' +
            '<label class="main_label important_input" for="name_mail1">Уровень владения языками:</label> ' +
            '<div class="styled-select slate" style="float: left; width:268px;">'+
            '<select name="level'+i+'" style="width:286px" > ' +
            '<option value="Базовые знания">Базовые знания</option> ' +
            '<option value="Среднее">Среднее</option> ' +
            '<option value="Высшее">Высшее</option> ' +
            '</select> ' +
            '</div> ' +
            '</div> ' +
            '</div>';
        $( "#langs" ).append( elem );

        $('#count_lang').val(parseInt(i)+1);
    }
    function add_experience(){
       // alert($('#count_lang').val());
        var i =$('#count_experience').val();
        var elem = '<div class="cf"> ' +
                '<div class="row"  style="margin-left:0">'+
            '<div class="input_holder_1"> ' +
            '<label class="main_label important_input" for="name_mail1">Годы работы:</label> ' +
            '<div class="small_slct smallest_slct"> ' +
            '<label class="small_label" style="float: left; top:0"> С </label>' +
            '<div class="styled-select slate" style="float: left">'+
            '<select name="m_w_b'+i+'" > ' +
                '<option value="Янваль">Янваль</option> ' +
                '<option value="Февраль">Февраль</option> ' +
                '<option value="Март">Март</option> ' +
                '<option value="Апрель">Апрель</option> ' +
                '<option value="Май">Май</option> ' +
                '<option value="Июнь">Июнь</option> ' +
                '<option value="Август">Август</option> ' +
                '<option value="Сентябрь">Сентябрь</option> ' +
                '<option value="Октябрь">Октябрь</option> ' +
                '<option value="Ноябрь">Ноябрь</option> ' +
                '<option value="Декабрь">Декабрь</option> ' +
            '</select>'+
            '</div>' +
            '<div class="styled-select slate" style="float: left">'+
            '<select name="y_w_b'+i+'" > ' +
            '<option value="1985">1985</option> ' +
            '<option value="1986">1986</option> ' +
            '<option value="1987">1987</option> ' +
            '<option value="1988">1988</option> ' +
            '<option value="1989">1989</option> ' +
            '<option value="1990">1990</option> ' +
            '<option value="1991">1991</option> ' +
            '<option value="1992">1992</option> ' +
            '<option value="1993">1993</option> ' +
            '<option value="1994">1994</option> ' +
            '<option value="1995">1995</option> ' +
            '<option value="1996">1996</option> ' +
            '<option value="1997">1997</option> ' +
            '<option value="1998">1998</option> ' +
            '<option value="1999">1999</option> ' +
            '<option value="2000">2000</option> ' +
            '<option value="2001">2001</option> ' +
            '<option value="2002">2002</option> ' +
            '<option value="2003">2003</option>' +
            '<option value="2004">2004</option> ' +
            '<option value="2005">2005</option> ' +
            '<option value="2006">2006</option> ' +
            '<option value="2007">2007</option> ' +
            '<option value="2008">2008</option>' +
            ' <option value="2009">2009</option> ' +
            '</select>'+
            '</div>' +
            '<label class="small_label" style="float: left; top:0"> По </label>' +
            '<div class="styled-select slate" style="float: left">'+
            '<select name="m_w_e'+i+'" > ' +
            '<option value="Янваль">Янваль</option> ' +
            '<option value="Февраль">Февраль</option> ' +
            '<option value="Март">Март</option> ' +
            '<option value="Апрель">Апрель</option> ' +
            '<option value="Май">Май</option> ' +
            '<option value="Июнь">Июнь</option> ' +
            '<option value="Август">Август</option> ' +
            '<option value="Сентябрь">Сентябрь</option> ' +
            '<option value="Октябрь">Октябрь</option> ' +
            '<option value="Ноябрь">Ноябрь</option> ' +
            '<option value="Декабрь">Декабрь</option> ' +
            '</select>'+
            '</div>' +

            '<div class="styled-select slate" style="float: left">'+
            '<select name="y_w_e'+i+'" > ' +
            '<option value="1985">1985</option> ' +
            '<option value="1986">1986</option> ' +
            '<option value="1987">1987</option> ' +
            '<option value="1988">1988</option> ' +
            '<option value="1989">1989</option> ' +
            '<option value="1990">1990</option> ' +
            '<option value="1991">1991</option> ' +
            '<option value="1992">1992</option> ' +
            '<option value="1993">1993</option> ' +
            '<option value="1994">1994</option> ' +
            '<option value="1995">1995</option> ' +
            '<option value="1996">1996</option> ' +
            '<option value="1997">1997</option> ' +
            '<option value="1998">1998</option> ' +
            '<option value="1999">1999</option> ' +
            '<option value="2000">2000</option> ' +
            '<option value="2001">2001</option> ' +
            '<option value="2002">2002</option> ' +
            '<option value="2003">2003</option>' +
            '<option value="2004">2004</option> ' +
            '<option value="2005">2005</option> ' +
            '<option value="2006">2006</option> ' +
            '<option value="2007">2007</option> ' +
            '<option value="2008">2008</option>' +
            ' <option value="2009">2009</option> ' +
            '</select>'+
            '</div>' +
            '</div> ' +
            '</div> ' +
            '</div> ' +
            '<div class="row" style="margin-left:0">'+
            '<div class="input_holder_2 reg_icon"> ' +
            '<label class="main_label important_input" for="name_mail1">Место работы (страна, город, место):</label> ' +
            '<textarea name="sity_work'+i+'"  cols="30" rows="10"></textarea> ' +
            '</div> ' +
            '<div class="input_holder_2 reg_icon"> ' +
            '<label class="main_label important_input" for="name_mail1">Должность:</label> ' +
            '<textarea name="situation'+i+'"  cols="30" rows="10"></textarea> ' +
            '</div> ' +
            '</div> ' +
            '</div>';
        $( "#experience" ).append( elem );

        $('#count_experience').val(parseInt(i)+1);
    }
    function add_vuz(){
         //alert($('#count_vuz').val());
        var i =$('#count_vuz').val();
        var elem = '<div style="margin-left: 10px" class="row"> ' +
            '<div class="input_holder_3"> ' +
                '<label for="name_mail1" class="main_label important_input">ВУЗ:</label> ' +
                '<input type="text" value="" name="vuz_name'+i+'"> ' +
            '</div> ' +
            '<div class="input_holder_3 reg_icon"> ' +
                '<label for="name_mail1" class="main_label important_input">Факультет/институт:</label> ' +
                '<input type="text" value="" name="vuz_faculty'+i+'" id="name_mail1"> ' +
            '</div> ' +
            '<div class="input_holder_3 reg_icon"> ' +
                '<label for="name_mail1" class="main_label important_input">Специальность:</label> ' +
                '<input type="text" value="" name="vuz_specialty'+i+'" id="name_mail1"> ' +
            '</div> ' +
            '<div style="margin-left: 0px" class="row"> ' +
                '<label for="name_mail1" class="main_label important_input">Годы обучения (магистратура):</label> ' +
                '<div class="small_slct smallest_slct"> ' +
                    '<label class="small_label" style="float: left; top:0;">Месяц поступления </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                            '<select name="m_b_m'+i+'" > ' +
                                '<option value="Янваль">Янваль</option> ' +
                                '<option value="Февраль">Февраль</option> ' +
                                '<option value="Март">Март</option> ' +
                                '<option value="Апрель">Апрель</option> ' +
                                '<option value="Май">Май</option> ' +
                                '<option value="Июнь">Июнь</option> ' +
                                '<option value="Август">Август</option> ' +
                                '<option value="Сентябрь">Сентябрь</option> ' +
                                '<option value="Октябрь">Октябрь</option> ' +
                                '<option value="Ноябрь">Ноябрь</option> ' +
                                '<option value="Декабрь">Декабрь</option> ' +
                            '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Месяц окончания </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                        '<select name="m_e_m'+i+'" > ' +
                            '<option value="Янваль">Янваль</option> ' +
                            '<option value="Февраль">Февраль</option> ' +
                            '<option value="Март">Март</option> ' +
                            '<option value="Апрель">Апрель</option> ' +
                            '<option value="Май">Май</option> ' +
                            '<option value="Июнь">Июнь</option> ' +
                            '<option value="Август">Август</option> ' +
                            '<option value="Сентябрь">Сентябрь</option> ' +
                            '<option value="Октябрь">Октябрь</option> ' +
                            '<option value="Ноябрь">Ноябрь</option> ' +
                            '<option value="Декабрь">Декабрь</option> ' +
                        '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Год поступления </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                        '<select name="y_b_m'+i+'" > ' +
                            '<option value="1985">1985</option> ' +
                            '<option value="1986">1986</option> ' +
                            '<option value="1987">1987</option> ' +
                            '<option value="1988">1988</option> ' +
                            '<option value="1989">1989</option> ' +
                            '<option value="1990">1990</option> ' +
                            '<option value="1991">1991</option> ' +
                            '<option value="1992">1992</option> ' +
                            '<option value="1993">1993</option> ' +
                            '<option value="1994">1994</option> ' +
                            '<option value="1995">1995</option> ' +
                            '<option value="1996">1996</option> ' +
                            '<option value="1997">1997</option> ' +
                            '<option value="1998">1998</option> ' +
                            '<option value="1999">1999</option> ' +
                            '<option value="2000">2000</option> ' +
                            '<option value="2001">2001</option> ' +
                            '<option value="2002">2002</option> ' +
                            '<option value="2003">2003</option>' +
                            '<option value="2004">2004</option> ' +
                            '<option value="2005">2005</option> ' +
                            '<option value="2006">2006</option> ' +
                            '<option value="2007">2007</option> ' +
                            '<option value="2008">2008</option>' +
                            ' <option value="2009">2009</option> ' +
                        '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Год окончания </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                        '<select name="y_e_m'+i+'" > ' +
                            '<option value="1985">1985</option> ' +
                            '<option value="1986">1986</option> ' +
                            '<option value="1987">1987</option> ' +
                            '<option value="1988">1988</option> ' +
                            '<option value="1989">1989</option> ' +
                            '<option value="1990">1990</option> ' +
                            '<option value="1991">1991</option> ' +
                            '<option value="1992">1992</option> ' +
                            '<option value="1993">1993</option> ' +
                            '<option value="1994">1994</option> ' +
                            '<option value="1995">1995</option> ' +
                            '<option value="1996">1996</option> ' +
                            '<option value="1997">1997</option> ' +
                            '<option value="1998">1998</option> ' +
                            '<option value="1999">1999</option> ' +
                            '<option value="2000">2000</option> ' +
                            '<option value="2001">2001</option> ' +
                            '<option value="2002">2002</option> ' +
                            '<option value="2003">2003</option>' +
                            '<option value="2004">2004</option> ' +
                            '<option value="2005">2005</option> ' +
                            '<option value="2006">2006</option> ' +
                            '<option value="2007">2007</option> ' +
                            '<option value="2008">2008</option>' +
                            ' <option value="2009">2009</option> ' +
                        '</select>'+
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div style="margin-left: 0px" class="row"> ' +
                '<label for="name_mail1" class="main_label important_input">Годы обучения (бакалавриат):</label> ' +
                '<div class="small_slct smallest_slct"> ' +
                    '<label class="small_label" style="float: left; top:0;">Месяц поступления </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                    '<select name="m_b_b'+i+'" > ' +
                    '<option value="Янваль">Янваль</option> ' +
                    '<option value="Февраль">Февраль</option> ' +
                    '<option value="Март">Март</option> ' +
                    '<option value="Апрель">Апрель</option> ' +
                    '<option value="Май">Май</option> ' +
                    '<option value="Июнь">Июнь</option> ' +
                    '<option value="Август">Август</option> ' +
                    '<option value="Сентябрь">Сентябрь</option> ' +
                    '<option value="Октябрь">Октябрь</option> ' +
                    '<option value="Ноябрь">Ноябрь</option> ' +
                    '<option value="Декабрь">Декабрь</option> ' +
                    '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Месяц окончания </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                    '<select name="m_e_b'+i+'" > ' +
                    '<option value="Янваль">Янваль</option> ' +
                    '<option value="Февраль">Февраль</option> ' +
                    '<option value="Март">Март</option> ' +
                    '<option value="Апрель">Апрель</option> ' +
                    '<option value="Май">Май</option> ' +
                    '<option value="Июнь">Июнь</option> ' +
                    '<option value="Август">Август</option> ' +
                    '<option value="Сентябрь">Сентябрь</option> ' +
                    '<option value="Октябрь">Октябрь</option> ' +
                    '<option value="Ноябрь">Ноябрь</option> ' +
                    '<option value="Декабрь">Декабрь</option> ' +
                    '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Год поступления </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                    '<select name="y_b_b'+i+'" > ' +
                    '<option value="1985">1985</option> ' +
                    '<option value="1986">1986</option> ' +
                    '<option value="1987">1987</option> ' +
                    '<option value="1988">1988</option> ' +
                    '<option value="1989">1989</option> ' +
                    '<option value="1990">1990</option> ' +
                    '<option value="1991">1991</option> ' +
                    '<option value="1992">1992</option> ' +
                    '<option value="1993">1993</option> ' +
                    '<option value="1994">1994</option> ' +
                    '<option value="1995">1995</option> ' +
                    '<option value="1996">1996</option> ' +
                    '<option value="1997">1997</option> ' +
                    '<option value="1998">1998</option> ' +
                    '<option value="1999">1999</option> ' +
                    '<option value="2000">2000</option> ' +
                    '<option value="2001">2001</option> ' +
                    '<option value="2002">2002</option> ' +
                    '<option value="2003">2003</option>' +
                    '<option value="2004">2004</option> ' +
                    '<option value="2005">2005</option> ' +
                    '<option value="2006">2006</option> ' +
                    '<option value="2007">2007</option> ' +
                    '<option value="2008">2008</option>' +
                    ' <option value="2009">2009</option> ' +
                    '</select>'+
                    '</div>' +
                    '<label class="small_label" style="float: left; top:0;">Год окончания </label> ' +
                    '<div class="styled-select slate" style="float: left">'+
                    '<select name="y_e_b'+i+'" > ' +
                    '<option value="1985">1985</option> ' +
                    '<option value="1986">1986</option> ' +
                    '<option value="1987">1987</option> ' +
                    '<option value="1988">1988</option> ' +
                    '<option value="1989">1989</option> ' +
                    '<option value="1990">1990</option> ' +
                    '<option value="1991">1991</option> ' +
                    '<option value="1992">1992</option> ' +
                    '<option value="1993">1993</option> ' +
                    '<option value="1994">1994</option> ' +
                    '<option value="1995">1995</option> ' +
                    '<option value="1996">1996</option> ' +
                    '<option value="1997">1997</option> ' +
                    '<option value="1998">1998</option> ' +
                    '<option value="1999">1999</option> ' +
                    '<option value="2000">2000</option> ' +
                    '<option value="2001">2001</option> ' +
                    '<option value="2002">2002</option> ' +
                    '<option value="2003">2003</option>' +
                    '<option value="2004">2004</option> ' +
                    '<option value="2005">2005</option> ' +
                    '<option value="2006">2006</option> ' +
                    '<option value="2007">2007</option> ' +
                    '<option value="2008">2008</option>' +
                    ' <option value="2009">2009</option> ' +
                    '</select>'+
                    '</div>' +
                    '</div>' +
                '</div>' +
            '<div>';


        $( "#vuzs" ).append( elem );

        $('#count_vuz').val(parseInt(i)+1);
    }

</script>