<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="content cf small_dth">
    <div class="sidebar_left sidebar">
        <div class="title"><span>наши проекты</span></div>
        <div class="projects">
            <ul>
                <li><a href="#3"><i class="mi_icon project_1"></i>Презентации, тренинги...</a></li>
                <li><a href="#3"><i class="mi_icon project_2"></i>Технические кейсы</a></li>
                <li><a href="#3"><i class="mi_icon project_3"></i>Ярмарки вакансий</a></li>
                <li><a href="#3"><i class="mi_icon project_4"></i>Печатные издания</a></li>
                <li><a href="#3"><i class="mi_icon project_5"></i>Рекламное продвижение</a></li>
                <li><a href="#3"><i class="mi_icon project_6"></i>Будь лучше</a></li>
                <li><a href="#3"><i class="mi_icon project_7"></i>Архив проектов</a></li>
            </ul>
        </div>
        <?= \app\widgets\NewsWidget::widget()?>

        <div class="sidebar_slider">
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
        </div>
        <div class="description">Переключение опросов</div>
    </div>

    <div class="middle_sbl mbts">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>    </div>


    <?= \app\widgets\VacanciesHomeWidget::widget() ?>
    <div class="sidebar_right sidebar">
        <?= \app\widgets\HotVacanciesWidget::widget()?>

        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <?= \app\widgets\EventsbarWidget::widget()?>
        <?= \app\widgets\InformationWidget::widget()?>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>
