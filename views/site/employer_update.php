<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 *
 */
?>

<div class="content cf">

    <?= \app\widgets\UserWidget::widget()?>
    <div class="middle_sbl mb15 mbs">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>    </div>
</div>
<div class="middle_full cf">
    <div class="middle_sbr cf">
        <div class="user_content update_page">
            <?php $form = ActiveForm::begin(); ?>
            <div class="updt_section_1 updt_section cf">
                <div class="title_8">Редактировать данные:</div>
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_input1">Ваш логин:</label>
                        <?= $form->field($user, 'login')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_1">Ваш пароль:</label>
                        <?= $form->field($user, 'password')->passwordInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Повторить пароль:</label>
                        <?= $form->field($user, 'password_repeat')->passwordInput(['maxlength' => true,'value'=>$user->password])->label(false) ?>
                    </div>
                </div>

            </div>
            <div class="updt_section_2 updt_section cf">
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="">Ваша фамилия:</label>
                        <?= $form->field($user, 'surname')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="">Ваше имя: </label>
                        <?= $form->field($user, 'username')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="">Ваше отчество:</label>
                        <?= $form->field($user, 'lastname')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="">Должность:</label>
                        <?= $form->field($kompany, 'user_post')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="">Название компании:</label>
                        <?= $form->field($kompany, 'name')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_password1_2">Правовая форма:</label>
                        <?=
                        Html::dropDownList('Kompany[pravovaya_form]',$kompany,[
                             'Товарищество'=>'Товарищество',
                             'Общесто'=>'Общесто',
                             'Акционерное общество'=>'Акционерное общество',
                             'Унитарное предприятиу'=>'Унитарное предприятиу'
                             ,'Другое'=>'Другое'
                         ],[
                             'prompt' => ''
                         ]);
                        ?>

                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_password1_2">Отрасль:</label>
                        <?= $form->field($kompany, 'industry')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_password1_2">Вид деятельности:</label>
                        <?= Html::dropDownList('Kompany[type_activity]', $kompany->type_activity, [
                            'Оптовая торговля'=>'Оптовая торговля',
                            'Розничная торговля'=>'Розничная торговля',
                            'Комиссионная торговля'=>'Комиссионная торговля',
                            'Посреднические услуги'=>'Посреднические услуги',
                            'Пищевое производство'=>'Пищевое производство',
                            'Непищевое производство'=>'Непищевое производство',
                            'Общественное питание'=>'Общественное питание',
                            'Строительство'=>'Строительство',
                            'Туризм'=>'Туризм',
                            'Связь'=>'Связь',
                            'Транспорт'=>'Транспорт',
                            'Недвижимость (продажа, аренда)'=>'Недвижимость (продажа, аренда)',
                            'Страхование'=>'Страхование',
                            'Ценные бумаги'=>'Ценные бумаги',
                            'Образование'=>'Образование',
                            'Медицина'=>'Медицина',
                            'Другое'=>'Другое'
                        ],['prompt'=>'']) ?>

                    </div>
                </div>
            </div>
            <div class="updt_section_3 updt_section cf">
                <div class="input_holder_3 reg_icon ">
                    <label class="main_label important_input" for="name_password1_2">Сайт компании:</label>
                    <?= $form->field($kompany, 'site')->textInput(['maxlength' => true])->label(false) ?>
                </div>

                <div class="input_holder_3 reg_icon">
                    <label class="main_label important_input" for="name_password1_2">Номер стационарного телефона:</label>
                    <?= $form->field($user, 'tel_d')->textInput(['maxlength' => true])->label(false) ?>
                </div>
                <div class="input_holder_3 reg_icon">
                    <label class="main_label important_input" for="name_password1_2">Номер мобильного телефона: </label>
                    <?= $form->field($user, 'tel_m')->textInput(['maxlength' => true])->label(false) ?>
                </div>
                <div class="input_holder_3 reg_icon">
                    <label class="main_label important_input" for="name_password1_2">Электронная почта:</label>
                    <?= $form->field($user, 'email')->textInput(['maxlength' => true])->label(false) ?>
                </div>


            </div>
            <div class="updt_section_4 updt_section cf">

                <div class="title_8">Если Вы планируете участие в наших проектах заполните следующие поля:</div>
                <div class="cf">

                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Полное наименование компании:</label>
                        <?= $form->field($kompany, 'full_name')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">ФИО подписанта договора:</label>
                        <?= $form->field($kompany, 'fio_podpysanta')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Должность подписанта договора:</label>
                        <?= $form->field($kompany, 'industry_podpysanta')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Основание для подписи<br />
                            (Устав или № и дата доверенности):
                        </label>
                        <?= $form->field($kompany, 'osnovanie')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Юридический адрес (с индексом):</label>
                        <?= $form->field($kompany, 'u_adres')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Фактический адрес (с индексом):
                            <?= $form->field($kompany, 'f_adres')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">ИНН:</label>
                        <?= $form->field($kompany, 'inn')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">КПП:</label>
                        <?= $form->field($kompany, 'kpp')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Расчетный счет:</label>
                        <?= $form->field($kompany, 'country')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Наименование банка:</label>
                        <?= $form->field($kompany, 'name_bank')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Корреспондентский счет банка:
                        </label>
                        <?= $form->field($kompany, 'country_bank')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">БИК:
                        </label>
                        <?= $form->field($kompany, 'bik')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">КОД ОКПО, ОКОНХ / ОКВЭД:</label>
                        <?= $form->field($kompany, 'kod')->textInput(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
                <div class="btns_line cf">
                    <button type="submit" class="btn_4">Сохранить все изменения</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

            <div class="user_events">
                <div class="title_6"><span>Мои мероприятия</span></div>
                <div class="cf user_events_list">
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/001.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/002.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/003.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="cf">
                        <a href="#3" class="near_info">Посмотреть ближайшие</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar_right sidebar">
        <div class="sidebar_section mb15 sb_hot_vacancies">
            <div><img src="img/rocket.png" alt=""></div>
            <div class="sb_title_4">горящие вакансии!</div>
            <p>Обратите внимание на горящие
                вакансии! Спешите найти работу,
                которая подходит именно вам!
            </p>
            <a href="#3" class="sb_orenge_btn">Смотреть горящие вакансии</a>
        </div>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <div class="title"><span>Анонс мероприятий</span></div>
        <div class="sidebar_one_post mb70">
            <a class="img_wrapper">
                <img src="img/content/004.jpg" alt="">
            </a>
            <span class="title_3">Презентация компании шлюмберже, которая состоится 7 октября!</span>
            <p>Посетите презентацию компании шлюмберже, которая состоится 7 октября в г. Москва,на бульваре Маяковского 89, зал 3.Будет интересно и незабываемо! Напиткии еда как бесплатное угощение!</p>
            <a href="#3" class="all_news_btn">Подробнее</a>
        </div>
        <div class="title"><span>полезная информация</span></div>
        <div class="sidebar_news">
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья. Правила работы, которые приведут
                        к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский палеонтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 2. Для многих карьера и зарплата являются чуть ли не синонимами.</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 3. Правила работы, которые приведут к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <a href="#3" class="all_news_btn green">Все новости</a>
        </div>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>


