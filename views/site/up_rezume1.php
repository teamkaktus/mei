<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="content cf">
    <div class="sidebar_left sidebar user_sidebar">
        <p class="user_name_desc">Ваш личный кабинет:</p>
        <div class="user_name"><i class="mi_icon login_icon"></i> Николай Всерадужский</div>
        <div class="about_user">
            <div class="avatar_part">
                <div class="img_wrap">
                    <img src="img/content/001.jpg" alt="">
                </div>
                <p>Так вас видят пользо-
                    ватели</p>
                <a href="">Изменить аватар</a>
            </div>
            <div class="status_part">
                <div class="status">Вы сейчас онлайн</div>
                <div class="data_of_reg">Вы зарегистрировались на сайте как соискатель <span>14 декабря, 2015</span></div>
            </div>
        </div>

    </div>

    <div class="middle_sbl mbs">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>    </div>
</div>
<div class="middle_full cf">
    <div class="middle_sbr cf">
        <div class="user_content update_page">
            <form method="post">
            <div class="updt_section_1 updt_section cf">
                <div class="title_8">Редактировать данные:</div>
                <p>Если вы хотите сохранить или распечатать готовое резюме, необходимо заполнить все поля, если Вы хотите откликаться на вакансии и регистрироваться на мероприятии, но готовое резюме Вам не нужно, достаточно заполнить поля со вездочкой</p>
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_input1">Ваш логин:</label>
                        <input type="text" id="name_input1" name="login" required value="<?=$user->login?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_1">Ваш пароль:</label>
                        <input type="password" id="name_password1_1" name="password" required value="<?=$user->password?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2">Повторить пароль:</label>
                        <input type="password" id="name_password1_2" name="repit_password" required value="<?=$user->password?>"/>
                    </div>
                </div>
                <a class="update_info" href="#3">Редактировать данные</a>
            </div>
            <div class="updt_section_2 updt_section cf">
                <div class="cf">
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_input2" >Фамилия:</label>
                        <input type="text"  name="surname" required value="<?=$user->surname?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_1">Имя:</label>
                        <input type="text"  name="username" required value="<?=$user->username?>"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_password1_2"  >Отчество:</label>
                        <input type="text"  name="lastname" required value="<?=$user->lastname?>"/>
                    </div>
                </div>
                <div class="cf">
                    <div class="input_holder_3_2">
                        <label class="main_label important_input" for="">Ваша дата рождения:</label>
                        <div class="small_slct smallest_slct">
                            <label class="small_label">
                                Число
                            </label>
                            <select class="" name="date_d" required>
                                <?php for($i=1;$i<32;$i++ ){?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <?php }?>
                            </select>
                            <label class="small_label">
                                Месяц
                            </label>
                            <select name="date_m" required>
                                <?php for($i=1;$i<13;$i++ ){?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <?php }?>
                            </select>
                            <label class="small_label">
                                Год
                            </label>
                            <select name="date_r" required >
                                <?php for($i=1980;$i<2000;$i++ ){?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_password1_2">Ваш пол:</label>
                        <select name="sex" required>
                            <option value="М">М</option>
                            <option value="Ж">Ж</option>
                        </select>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_phone1">Ваш номер телефона:</label>
                        <input type="text" id="name_phone1" name="tel_m" required value="<?=$user->tel_m?>" />
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Ваша электронная почта:</label>
                        <input type="email" id="name_mail1" name="email" required value="<?=$user->email?>"/>
                    </div>
                </div>
            </div>
            <div class="updt_section_3 updt_section cf">
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_phone1">Ваше место жительства:</label>
                    <input type="text" id="name_phone1" name="sity" required value="<?=$rezume->sity?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Станция метро:</label>
                    <input type="text" id="name_phone1" name="st_metro" required value="<?=$rezume->st_metro?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Ваше гражданство:</label>
                    <input type="text" id="name_phone1" name="citizenship" required value="<?=$rezume->citizenship?>" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Переезд:</label>
                    <select name="crossing" required>
                        <option value="Согласен">Согласен</option>
                        <option value="Не согласен">Не согласен</option>
                        <option value="Возможно обсуждение">Возможно обсуждение</option>
                    </select>
                </div>
                <div class="input_holder_3">
                    <label class="main_label important_input" for="name_mail1">Родной язык:</label>
                    <input type="text" id="name_phone1" name="citizenship" required value="<?=$rezume->native_lang?>"
                </div>

            </div>
            <div class="updt_section_4 updt_section cf">
                <div class="cf">
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_mail1">Отрасль в которой Вы хотели бы работать:</label>
                        <select name="" >
                            <option value="">Русский</option>
                            <option value="">English</option>
                        </select>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_mail1">Ваше образование:</label>
                        <select name="" >
                            <option value="">Русский</option>
                            <option value="">English</option>
                        </select>
                    </div>
                    <div class="input_holder_3">
                        <label class="main_label important_input" for="name_mail1">ВУЗ:</label>
                        <select name="" >
                            <option value="">Русский</option>
                            <option value="">English</option>
                        </select>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Факультет/институт:</label>
                        <input type="email" id="name_mail1"/>
                    </div>
                    <div class="input_holder_3 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Специальность:</label>
                        <input type="email" id="name_mail1"/>
                    </div>
                    <div class="input_holder_2">
                        <label class="main_label important_input" for="name_mail1">Годы обучения (магистратура):</label>
                        <div class="small_slct smallest_slct">
                            <label class="small_label">
                                Год поступления
                            </label>
                            <select name="" >
                                <option value="">2001</option>
                                <option value="">2002</option>
                                <option value="">2003</option>
                            </select>
                            <label class="small_label">
                                Год окончания
                            </label>
                            <select name="" >
                                <option value="">2001</option>
                                <option value="">2002</option>
                                <option value="">2003</option>
                            </select>
                        </div>
                    </div>
                    <div class="input_holder_2">
                        <label class="main_label important_input" for="name_mail1">Годы обучения (бакалавриат):</label>
                        <div class="small_slct smallest_slct">
                            <label class="small_label">
                                Год поступления
                            </label>
                            <select name="" >
                                <option value="">2001</option>
                                <option value="">2002</option>
                                <option value="">2003</option>
                            </select>
                            <label class="small_label">
                                Год окончания
                            </label>
                            <select name="" >
                                <option value="">2001</option>
                                <option value="">2002</option>
                                <option value="">2003</option>
                            </select>
                        </div>
                    </div>
                </div>
                <a href="#3" class="small_red_btn">Добавить еще ВУЗ</a>
            </div>
            <div class="updt_section_5 updt_section cf">
                <div class="input_holder_2 reg_icon">
                    <label class="main_label important_input" for="name_mail1">Дополнительное образование:</label>
                    <textarea name=""  cols="30" rows="10"></textarea>
                </div>
                <div class="input_holder_2 reg_icon">
                    <label class="main_label important_input" for="name_mail1">Курсы:</label>
                    <textarea name=""  cols="30" rows="10"></textarea>
                </div>
            </div>
            <div class="updt_section_6 updt_section cf">
                <div class="cf">
                    <div class="input_holder_5">
                        <label class="main_label important_input" for="name_mail1">Опыт работы:</label>
                        <select name="" >
                            <option value="">Русский</option>
                            <option value="">English</option>
                        </select>
                    </div>
                    <div class="input_holder_2">
                        <label class="main_label important_input" for="name_mail1">Годы работы:</label>
                        <div class="small_slct smallest_slct">
                            <label class="small_label">
                                С
                            </label>
                            <select name="" >
                                <option value="">Русский</option>
                                <option value="">English</option>
                            </select>

                            <label class="small_label">
                                По
                            </label>
                            <select name="" >
                                <option value="">Русский</option>
                                <option value="">English</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="cf">
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Место работы (страна, город, место):</label>
                        <textarea name=""  cols="30" rows="10"></textarea>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Должность:</label>
                        <textarea name=""  cols="30" rows="10"></textarea>
                    </div>
                </div>
                <a href="#3" class="small_red_btn add_univer">Добавить еще ВУЗ</a>

                <div class="cf">
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Рекомендации:</label>
                        <textarea name=""  cols="30" rows="10"></textarea>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Увлечения:</label>
                        <textarea name=""  cols="30" rows="10"></textarea>
                    </div>
                    <div class="input_holder_2 reg_icon">
                        <label class="main_label important_input" for="name_mail1">Ключевые навыки / доп информация:</label>
                        <textarea name=""  cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="btns_line cf">
                    <button type="submit" class="btn_1">Сохранить все изменения</button>
<!--                    <a href="#3" class="btn_1">Сохранить все изменения</a>-->
                    <a href="#3" class="btn_3">Скачать резюме</a>
                    <a href="#3" class="btn_2 summary_opener">Посмотреть резюме</a>
                </div>
            </div>
            </form>
        </div><div class="user_content update_page">













            <div class="user_events">
                <div class="title_6"><span>Мои мероприятия</span></div>
                <div class="cf user_events_list">
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/001.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/002.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="user_event_item">
                        <div class="img_wrap">
                            <img src="img/content/003.jpg" alt="">
                        </div>
                        <div class="title_7">Презентация компании шлюмберже, которая состоится 7 октября! </div>
                        <div class="reg_status">Я зарегистрировался</div>
                        <div class="bottom_part cf">
                            <div class="left_part">
                                <a href="" class="small_blue_btn">Подробнее</a>
                            </div>
                            <div class="right_part"><i class="mi_icon mi_calendar"></i><span>29.04.2015</span></div>
                        </div>
                    </div>
                    <div class="cf">
                        <a href="#3" class="near_info">Посмотреть ближайшие</a>
                    </div>
                </div>
            </div>
            <div class="user_vacancies_list">
                <div class="title_6"><span>Мои вакансии:</span></div>
                <a href="#3" class="archive_btn">Архив вакансий</a>
                <div class="history_of_vac">
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Бухгалтер (девушка),</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Системный администратор,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Менеджер по продажам,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Флорист,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Футболист,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Маркетолог,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Консультант ЗОЖ,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="history_of_vac_item"><div class="vac_title_wrap"><a class="vac_title">Тренер,</a> <span>35000 руб.</span></div>Код: 34929345</div>
                    <div class="read_more"><a href="" class="">Посмотреть еще</a></div>
                </div>
            </div>
        </div>

        <div style="display: none;">
            <div class="box-modal" id="summary_modal">
                <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
                <div class="modal_title_3">
                    просмотр вашего резюме
                </div>
                <div class="modal_wrap">
                    <div class="sum_create_date">Дата создания 13 июня 2012</div>
                    <div class="modal_title_5">Иванова Ирина Михайловна</div>
                    <div class="some">Мастер СМР, Инженер проектировщик, Инженер ПТО</div>
                    <div class="sum_info_table">
                        <div class="sum_info_row cf">
                            <div class="sum_param">Мобильный телефон</div>
                            <div class="sum_value">+7 (916) 999-99-99</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Примерная зарплата</div>
                            <div class="sum_value">35 000 руб.</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Электронная почта</div>
                            <div class="sum_value">irina@mail.ua</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Возраст</div>
                            <div class="sum_value">22 года (27.05.1980)</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Проживание</div>
                            <div class="sum_value">г. Москва (ст. м. Беляево)</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Переезд</div>
                            <div class="sum_value">
                                Возможен
                                <div class="sum_sub_info">Предпочтительное время в пути до будущей работы не является критерием для выбора работы</div>
                            </div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Гражданство</div>
                            <div class="sum_value">Россия</div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param">Языки</div>
                            <div class="sum_value">
                                <p>Русский (родной)</p>
                                <p>Английский (базовые знания)</p>
                            </div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param sub_title">Специализация</div>
                            <div class="sum_value">
                                <b class="addition_param">Строительство, недвижимость</b>
                                <p>Инженер. Строительство. Начальный уровень / Мало опыта</p>
                            </div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param sub_title">Основное</div>
                            <div class="sum_value">
                                <b class="addition_param">Строительство, недвижимость</b>
                                <p>Инженер. Строительство. Начальный уровень / Мало опыта</p>
                                <b class="addition_param">Повышение квалификации, курсы</b>
                                <p>2011 - работа с программой Smeta.ru компьютерный центр обучения “специалист”
                                    при МГТУ им. Баумана</p>
                            </div>
                        </div>
                        <div class="sum_info_row cf">
                            <div class="sum_param sub_title">Опыт работы</div>
                            <div class="sum_value">
                                <p>Июль 2011 - продолжаю работать ТермоИзоляция, ООО (Москва).  Строительство
                                    / архитектура/недвижимость. Мастер СМР
                                </p>
                                <b class="addition_param"> Строительство наружных сетей и сооружений:</b>
                                <ul>
                                    <li>- подготовка плана и графика производства работ,</li>
                                    <li>- подготовка плана и графика производства работ,</li>
                                    <li>- обеспечение рабочих материалами и инструментами,</li>
                                    <li>- расстановка рабочих на объекте,</li>
                                    <li>- контроль качества и сроков проведения работ,</li>
                                    <li>- подписание актов приемки работ.</li>
                                    <li>- формы М-29</li>
                                    <li>- ведение переговоров с заказчиком,</li>
                                    <li>- организация и контроль на строительном участке;</li>
                                </ul>
                                <p>Июль 2010—июль 2011 <strong>ВеК проект</strong>, ООО (Москва, www.vekproject.ru)
                                    Строительство/архитектура/недвижимость</p>
                                <p><span class="addition_param">Проектировщик ВК:</span> <i>разработка раздела ВК (внутриплощадочные сети‚ КНС‚
                                        внутренний водопровод и канализация) на стадиях П и Р</i>
                                </p>

                            </div>
                        </div>





                        <div class="sum_info_row cf">
                            <div class="sum_param sub_title">Ключевые навыки</div>
                            <div class="sum_value">
                                <b class="addition_param">Управленческие</b>
                                <p>
                                    Развит навык планирования, анализа. Умение работать в команде. Хорошие
                                    организаторские навыки. Амбициозность, уверенность, целеустремленность
                                </p>
                                <b class="addition_param">Личные</b>
                                <p>
                                    Высокая организованность. Хорошо развитые коммуникативные навыки. Умение
                                    работать с большим объемом информации. Порядочность. Пунктуальность
                                </p>
                                <b class="addition_param">Мобильность</b>
                                <p>
                                    Готовность к командировкам. Готовность к ненормированному рабочему дню
                                </p>
                                <b class="addition_param">ПК</b>
                                <p>
                                    Опытный пользователь ПК. Знание основ Windows. Свободное владение пакетом
                                    Microsoft Office (Excel, Word, PP, Access - в совершенстве). Уверенный пользова-
                                    тель AutoCad. Свободное владение офисной техникой
                                </p>
                                <b class="addition_param">Иностранные языки</b>
                                <p>
                                    Английский (чтение, перевод, восприятие речи; разговорный - слабый (давно
                                    нет практики)
                                </p>

                            </div>
                        </div>
                        <div class="title_6"><span>Иванова Ирина Михайловна</span></div>
                    </div>
                </div>
            </div>
        </div>





    </div>
    <div class="sidebar_right sidebar">
        <div class="sidebar_section mb15 sb_hot_vacancies">
            <div><img src="img/rocket.png" alt=""></div>
            <div class="sb_title_4">горящие вакансии!</div>
            <p>Обратите внимание на горящие
                вакансии! Спешите найти работу,
                которая подходит именно вам!
            </p>
            <a href="#3" class="sb_orenge_btn">Смотреть горящие вакансии</a>
        </div>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>


        <div class="title"><span>Анонс мероприятий</span></div>
        <div class="sidebar_one_post mb70">
            <a class="img_wrapper">
                <img src="img/content/004.jpg" alt="">
            </a>
            <span class="title_3">Презентация компании шлюмберже, которая состоится 7 октября!</span>
            <p>Посетите презентацию компании шлюмберже, которая состоится 7 октября в г. Москва,на бульваре Маяковского 89, зал 3.Будет интересно и незабываемо! Напиткии еда как бесплатное угощение!</p>
            <a href="#3" class="all_news_btn">Подробнее</a>
        </div>
        <div class="title"><span>полезная информация</span></div>
        <div class="sidebar_news">
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья. Правила работы, которые приведут
                        к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский палеонтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 2. Для многих карьера и зарплата являются чуть ли не синонимами.</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <div class="sidebar_post_item">
                <a href="#3">
                    <strong>Статья 3. Правила работы, которые приведут к карьерному росту</strong>
                    <p>Впервые Россию посещает известный норвежский пале-
                        онтолог, первооткрыватель знамени...</p>
                </a>
                <div class="publication_data"><i class="mi_icon mi_calendar"></i>29.04.2015</div>
            </div>
            <a href="#3" class="all_news_btn green">Все новости</a>
        </div>
        <!--    <div class="sidebar_section">-->
        <!--        <a href="#3" class="simple_sidebar_btn">-->
        <!--            Смотреть еще вакансии-->
        <!--        </a>-->
        <!--    </div>-->
    </div>
</div>