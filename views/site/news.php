<?php
/**
 *
 */?>
<div class="middle_full anns cf">
    <div class="middle_sbr cf">
        <div class="title mb15"><span>новости</span></div>
        <div class="mb27">
            <div class="top_slider_section">
                <div class="top_slider">
                    <div class="item">
                        <div class="sd_bg">
                            <img src="img/content/single_page_slider1.jpg" alt="">
                        </div>
                        <div class="sd_content">
                            <a href="#3">
                                <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                                <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                    бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                    выборе нескольких проектов</p>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sd_bg">
                            <img src="img/content/single_page_slider2.jpg" alt="">
                        </div>
                        <div class="sd_content">
                            <a href="#3">
                                <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                                <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                    бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                    выборе нескольких проектов</p>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sd_bg">
                            <img src="img/content/single_page_slider3.jpg" alt="">
                        </div>
                        <div class="sd_content">
                            <a href="#3">
                                <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                                <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                    бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                    выборе нескольких проектов</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>    </div>
        <div class="middle_content">
            <?php foreach($news as $new){?>
            <div class="anons_item shadow_box cf">
                <div class="img_wrapper">
                    <img src="<?=$new->img?>" alt="">
                </div>
                <div content="info_section cf">
                    <div class="small_img_wrap">
                        <img src="<?=$new->img?>" alt="">
                    </div>
                    <div class="content_section">
                        <div class="title_4"><?=$new->name?></div>
                        <div class="text_section">
                            <?=$new->desc?>

                        </div>
                        <div class="vis_when_close cf">
                            <a href="#3" class="open_anons_btn">
                                Подробнее
                            </a>
                        </div>

                        <div class="vis_when_open cf">
                            <div class="left_side">
                                <div class="anons_data"><i class="mi_icon mi_calendar"></i><span><?=Yii::$app->formatter->asDate($new->updated_at)?></span></div>
                            </div>
                            <div class="right_side">
                                <a class="small_btn open_anons_btn">
                                    Свернуть
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            <?php echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>
    </div>
    <div class="sidebar_right sidebar">
        <?=\app\widgets\HotVacanciesWidget::widget()?>
        <?=\app\widgets\PostsWidget::widget()?>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>
    </div>
</div>

