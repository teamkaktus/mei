<?php
/**
 *
 * @var PsiWhiteSpace $models
 * @var \yii\data\Pagination $pages
 */
?>
<div class="content cf">
    <div class="sidebar_left sidebar">
        <?= \app\widgets\CribsWidget::widget()?>

        <div class="title mb30"><span>Опросы</span></div>

        <div class="sidebar_slider">
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
            <div class="item">
                <div class="title_2">Вы находите работу по интернету?</div>
                <form action="">
                    <div class="radio_btns">
                        <div class="cf"><input type="radio" name="group2" value="Water" id="for_label1"><label for="for_label1">&mdash; Угу</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label2"><label for="for_label2">&mdash; Не</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label3"><label for="for_label3">&mdash; Работу??</label><br></div>
                        <div class="cf"><input type="radio" name="group2" value="Beer" id="for_label4"><label for="for_label4">&mdash; Уже</label><br></div>
                    </div>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
        </div>
        <div class="description">Переключение опросов</div>
    </div>

    <div class="middle_sbl mbs">
        <div class="top_slider_section">
            <div class="top_slider">
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider1.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider2.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
                <div class="item">
                    <div class="sd_bg">
                        <img src="img/content/single_page_slider3.jpg" alt="">
                    </div>
                    <div class="sd_content">
                        <a href="#3">
                            <strong class="sd_title">бАННЕР ДЛЯ АНОНСОВ МЕРОПРИЯТИЙ</strong>
                            <p class="sd_text">В наше экономически сложное время мы верны своим социальным принципам и размещаем вакансии
                                бесплатно, во всем остальном, мы достаточно гибки в своем ценообразовании, предоставляем различные скидки при
                                выборе нескольких проектов</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>    </div>

    <div class="middle_sblr">

        <div class="title"><span>статьи</span></div>
        <div class="articles_list">
            <?php foreach($models as $model){?>
            <div class="article_item shadow_box">
                <a href="<?=\yii\helpers\Url::to(['/site/posts', 'slug' => $model->url]);?>" class="article_title">
                    <?=$model->name?>
                </a>
                <div class="article_text">
                    <p><?=$model->small_desc?></p>
                </div>
                <div class="article_data"><i class="mi_icon mi_calendar"></i><span><?=Yii::$app->formatter->asDate($model->updated_at)?></span></div>
            </div>
            <?php }?>
            <?php echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
            <a href="#3" class="all_news_btn"><span>На главную</span></a>
        </div>
    </div>
    <div class="sidebar_right sidebar">
        <div class="sidebar_section mb15 sb_hot_vacancies">
            <div><img src="img/rocket.png" alt=""></div>
            <div class="sb_title_4">горящие вакансии!</div>
            <p>Обратите внимание на горящие
                вакансии! Спешите найти работу,
                которая подходит именно вам!
            </p>
            <a href="#3" class="sb_orenge_btn">Смотреть горящие вакансии</a>
        </div>
        <a href="" class="sidebar_section mb70 commercial_section">
            <img class="baner_place" src="img/baner.png" alt="">
        </a>
        <div class="title"><span>Анонс мероприятий</span></div>
        <div class="sidebar_one_post mb70">
            <a class="img_wrapper">
                <img src="img/content/004.jpg" alt="">
            </a>
            <span class="title_3">Презентация компании шлюмберже, которая состоится 7 октября!</span>
            <p>Посетите презентацию компании шлюмберже, которая состоится 7 октября в г. Москва,на бульваре Маяковского 89, зал 3.Будет интересно и незабываемо! Напиткии еда как бесплатное угощение!</p>
            <a href="#3" class="all_news_btn">Подробнее</a>
        </div>
    </div>
</div>

