<?php
/**
 *
 */?>

<div class="col-md-12">
    <div class="col-md-4" style="color: #a9a9a9; font-size: 13px;">Дата создания <?php echo Yii::$app->formatter->asDate( $rezume->created_at);?></div>
    <div class="col-md-12" style="font-size: 38px;
    font-weight: bold;
    margin-bottom: 8px;"><?=$user->surname?> <?=$user->username?> <?=$user->lastname?></div>
    <div class="col-md-12" style="font-size: 15px;
    line-height: 26px;
    margin-bottom: 39px;"><?=$rezume->sector_job?></div>

    <div class="col-md-6 sum_param">Мобильный телефон</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$user->tel_m?></div>

    <div class="col-md-6 sum_param">Примерная зарплата</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$rezume->salary?> руб.</div>

    <div class="col-md-6 sum_param">Электронная почта</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$user->email?></div>

    <div class="col-md-6 sum_param">Возраст</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%">22 года (<?=$user->date?>)</div>

    <div class="col-md-6 sum_param">Проживание</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$rezume->sity?> <?php if ($rezume->st_metro) echo'(ст. м. '.$rezume->st_metro.')';?></div>

    <div class="col-md-6 sum_param">Переезд</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$rezume->crossing?><div style="color: #a9a9a9;
    font-size: 12px;
    line-height: 14px;">Предпочтительное время в пути до будущей работы не является критерием для выбора работы</div></div>

    <div class="col-md-6 sum_param">Гражданство</div>
    <div style="display: inline-block; float: left; margin: 0 2% 20px 0; width: 63%"><?=$rezume->citizenship?></div>



</div>