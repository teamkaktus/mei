<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\MyEvents;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class EventsWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $events= MyEvents::find()->joinWith('event')->where(['>', 'date_for_time', time()]);
        $pages = new Pagination(['totalCount' => $events->count(), 'pageSize' => 3]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $events->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
       return $this->render('events', [
            //'paner'=>$baner,
           'pages' => $pages,
           'events' => $models,
        ]);
    }
}
