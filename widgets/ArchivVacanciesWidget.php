<?php
namespace app\widgets;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class ArchivVacanciesWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());
        $kompany= Kompany::find()->where(['user_id'=>Yii::$app->user->id])->one();
        $vacanc= Vacancy::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0']);

        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 5]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('archiv_vacancies', [
            'vacancies'=>$models,
            'pages' => $pages,
            'kompany' => $kompany,
        ]);
    }
}