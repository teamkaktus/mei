<?php
namespace app\widgets;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\Lang;
use app\modules\ls_admin\models\LangVac;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use yii\base\Widget;
use Yii;

class VacancyWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(\Yii::$app->request->post());

        $user= User::findOne(Yii::$app->user->id);
        $kompany = Kompany::find()->where(['user_id'=>Yii::$app->user->id])->one();
        $vacancy = new Vacancy();
        $vacancy->user_id = Yii::$app->user->id;
        if ( $user->load(\Yii::$app->request->post()) && $kompany->load(\Yii::$app->request->post()) && $vacancy->load(\Yii::$app->request->post())){
            $user->save();
            $kompany->save();
            $vacancy->status='1';
            $vacancy->save();

                $i=1;
                for($i; $i < Yii::$app->request->post('count_lang'); $i++){
                    if (Yii::$app->request->post('lang'.$i)){
                        $item_lang= new LangVac();
                        $item_lang->name=Yii::$app->request->post('lang'.$i);
                        $item_lang->level=Yii::$app->request->post('level'.$i);
                        $item_lang->vac_id=$vacancy->id;
                        $item_lang->save();
                    }
                }

            //var_dump(Yii::$app->request->post());
            //var_dump(\Yii::$app->request->post());
            echo ' <script type="text/javascript">alert("Ожидайте звонка!");</script>';
        }

        return $this->render('vacancy', [
            'user'=>$user,
            'kompany'=>$kompany,
            'vacancy'=>$vacancy,
        ]);
    }
}