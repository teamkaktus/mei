<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use yii\base\Widget;
use Yii;

class UserWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());

        $user= User::findOne(Yii::$app->user->id);
       return $this->render('user', [
            'user'=>$user,
        ]);
    }
}
