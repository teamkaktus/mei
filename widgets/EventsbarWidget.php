<?php
namespace app\widgets;
use app\modules\ls_admin\models\Events;
use yii\base\Widget;
use Yii;

class EventsbarWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());
        $model = Events::find()->where(['>', 'date_for_time', time()])->orderBy(['updated_at'=>SORT_DESC])->limit(3)->all();
       return $this->render('eventsbar', [
            'news'=>$model,
        ]);
    }
}
