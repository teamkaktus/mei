<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\Cribs;
use app\modules\ls_admin\models\News;
use app\modules\ls_admin\models\Posts;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use yii\base\Widget;
use Yii;

class PostsWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());

        $models = Posts::find()->where(['<', 'updated_at', time()])->andWhere(['type'=>'Статья'])->orderBy(['updated_at'=>SORT_DESC])->limit(3)->all();
       return $this->render('posts', [
            'models'=>$models,
        ]);
    }
}
