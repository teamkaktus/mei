<?php
namespace app\widgets;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class VacanciesHomeWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());


        $vacanc= Vacancy::find()->where(['status'=>'1', 'status_admin'=>'1'])->orderBy(['updated_at'=>SORT_DESC]);

        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 10]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('vacancies_home', [
            'vacancies'=>$models,
            'pages' => $pages,
        ]);
    }
}