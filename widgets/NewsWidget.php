<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\News;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use yii\base\Widget;
use Yii;

class NewsWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());

        $news = News::find()->where(['<', 'updated_at', time()])->orderBy(['updated_at'=>SORT_DESC])->limit(3)->all();
       return $this->render('news', [
            'news'=>$news,
        ]);
    }
}
