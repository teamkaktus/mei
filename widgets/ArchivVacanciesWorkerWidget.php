<?php
namespace app\widgets;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use app\modules\ls_admin\models\WorkerVacamcies;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class ArchivVacanciesWorkerWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        $vacanc= WorkerVacamcies::find()->where(['user_id'=>Yii::$app->user->id,'to_respond'=>1])->orderBy(['updated_at'=>SORT_DESC]) ;

        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 5]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('archiv_vacancies_worker', [
            'worker_vacs'=>$models,
            'pages' => $pages,
        ]);
    }
}