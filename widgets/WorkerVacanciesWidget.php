<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\WorkerVacamcies;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class WorkerVacanciesWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $worker_vac = WorkerVacamcies::find()->where(['user_id'=>Yii::$app->user->id,'to_respond'=>0])->orderBy(['updated_at'=>SORT_DESC]);
        $pages = new Pagination(['totalCount' => $worker_vac->count(), 'pageSize' => 6,'pageParam'=>'page_v_w']);
        $pages->pageSizeParam = false;
        $models = $worker_vac->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        //print_r(Yii::$app->request->post());
       return $this->render('worker_vacancies', [
            'worker_vac'=>$models,
           'pages' => $pages,
        ]);
    }
}
