<?php
namespace app\widgets;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\LangVac;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use yii\base\Widget;
use Yii;
use yii\data\Pagination;

class PlacementVacanciesWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        //print_r(Yii::$app->request->post());
        $kompany= Kompany::find()->where(['user_id'=>Yii::$app->user->id])->one();

        $vacanc= Vacancy::find()->where(['user_id'=>Yii::$app->user->id, 'status'=>'1', 'status_admin'=>'1'])->orderBy(['updated_at'=>SORT_DESC]);
        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 5,'pageParam'=>'page_placement']);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('placement_vacancies', [
            'vacancies'=>$models,
            'pages' => $pages,
            'kompany' => $kompany,
            //'langs' => $langs,

        ]);
    }
}