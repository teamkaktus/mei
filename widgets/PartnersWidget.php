<?php
namespace app\widgets;
use app\models\CallForm;
use app\modules\ls_admin\models\Partners;
use app\modules\ls_admin\models\Store;
use app\modules\ls_admin\models\User;
use yii\base\Widget;
use Yii;

class PartnersWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $partners=Partners::find()->all();
       return $this->render('partners', [
            'partners'=>$partners,
        ]);
    }
}
