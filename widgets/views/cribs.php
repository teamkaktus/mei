<?php
/**
 *
 * @var PsiElement(assign) $user
 */
?>
<div class="title"><span>шпаргалки</span></div>
<div class="sidebar_news">
<?php foreach($models as $model){?>
    <div class="sidebar_post_item">
        <a class="img_wrapper">
            <img src="<?=$model->img?>" alt="<?=$model->img_alt?>">
        </a>
        <a href="<?=\yii\helpers\Url::to(['/site/cribs', 'slug' => $model->url]);?>">
            <strong><?=$model->name?></strong>
            <p><?=$model->small_desc?>...</p>
        </a>
        <div class="publication_data"><i class="mi_icon mi_calendar"></i><?=Yii::$app->formatter->asDate($model->updated_at)?></div>
    </div>
<?php }?>
    <a href="/cribs" class="all_news_btn">Все шпаргалки</a>
</div>
