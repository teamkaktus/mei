<?php
/**
 *
 */?>
<div class="sidebar_section mb15 sb_hot_vacancies">
    <div><img src="/img/rocket.png" alt=""></div>
    <div class="sb_title_4">горящие вакансии!</div>
    <p>Обратите внимание на горящие
        вакансии! Спешите найти работу,
        которая подходит именно вам!
    </p>
    <a href="<?=\yii\helpers\Url::to([ '/site/hotvacancies'])?>" class="sb_orenge_btn">Смотреть горящие вакансии</a>
</div>
