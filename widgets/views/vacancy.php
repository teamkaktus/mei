<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/**
 *
 * @var PsiElement(assign) $user
 */
?>
<div class="modal_wrap">
    <?php if ($user->role == 'работодатель'){?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="modal_title_2">
        <span>размещение вакансии</span>
    </div>
    <div class="title_6">
        <span>Личные данные</span>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label " for="vac_second_name1">Ваша фамилия:</label>
            <?= $form->field($user, 'surname')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label " for="vac_name_input1">Ваше имя:</label>
            <?= $form->field($user, 'username')->textInput(['maxlength' => true])->label(false) ?>
        </div>

        <div class="input_holder_3">
            <label class="main_label " for="vac_middle_name_input1">Ваше отчество:</label>
            <?= $form->field($user, 'lastname')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label " for="vac_phone_1">Номер мобильного телефона:</label>
            <?= $form->field($user, 'tel_m')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label " for="vac_mail_1">Электронная почта:</label>
            <?= $form->field($user, 'email')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label " for="vac_role">Должность сотрудника:</label>
            <?= $form->field($kompany, 'user_post')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="title_6">
        <span>Информация о компании</span>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label " for="vac_company_name">Название компании:</label>
            <?= $form->field($kompany, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label " for="business_type">Правовая форма</label>
            <?=
            Html::dropDownList('Kompany[pravovaya_form]',$kompany->pravovaya_form,[
                'Товарищество'=>'Товарищество',
                'Общесто'=>'Общесто',
                'Акционерное общество'=>'Акционерное общество',
                'Унитарное предприятиу'=>'Унитарное предприятиу'
                ,'Другое'=>'Другое'
            ],[
                'prompt' => ''
            ]);
            ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label " for="kind_of_activity">Вид деятельности:</label>
            <?= Html::dropDownList('Kompany[type_activity]', $kompany->type_activity, [
                'Оптовая торговля'=>'Оптовая торговля',
                'Розничная торговля'=>'Розничная торговля',
                'Комиссионная торговля'=>'Комиссионная торговля',
                'Посреднические услуги'=>'Посреднические услуги',
                'Пищевое производство'=>'Пищевое производство',
                'Непищевое производство'=>'Непищевое производство',
                'Общественное питание'=>'Общественное питание',
                'Строительство'=>'Строительство',
                'Туризм'=>'Туризм',
                'Связь'=>'Связь',
                'Транспорт'=>'Транспорт',
                'Недвижимость (продажа, аренда)'=>'Недвижимость (продажа, аренда)',
                'Страхование'=>'Страхование',
                'Ценные бумаги'=>'Ценные бумаги',
                'Образование'=>'Образование',
                'Медицина'=>'Медицина',
                'Другое'=>'Другое'
            ],['prompt'=>'']) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3_2">
            <label class="main_label " for="vac_address_1">Адрес</label>
            <?= $form->field($kompany, 'f_adres')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
        </div>
    </div>
    <div class="title_6">
        <span>Информация о вакансии</span>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_position_name">Название вакансии:</label>
            <?= $form->field($vacancy, 'name')->textInput(['maxlength' => true, 'required'=>true])->label(false) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label" for="vac_amount">Количество вакантных мест:</label>
            <?= $form->field($vacancy, 'count_m')->input('number')->label(false) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3_2">
            <label class="main_label" for="vac_duties">Краткое описание:</label>
            <?= $form->field($vacancy, 'small_desc')->textarea(['cols' =>30, 'rows'=>10, 'required'=>true ])->label(false) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3_2">
            <label class="main_label" for="vac_duties">Обязанности:</label>
            <?= $form->field($vacancy, 'duties')->textarea(['cols' =>30, 'rows'=>10, 'id'=>'my_ob'])->label(false) ?>

        </div>
    </div>
    <div class="title_6">
        <span>Требования</span>
    </div>

    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_education">Образование:</label>
            <?= Html::dropDownList('Vacancy[education]', $vacancy->education, ['Среднее'=>'Среднее','Высшее'=>'Высшее'],['prompt'=>'']) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label" for="vac_exp">Опыт работы:</label>
            <?= Html::dropDownList('Vacancy[experience]', $vacancy->experience, ['Есть'=>'Есть','Нет'=>'Нет'],['prompt'=>'']) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label" for="vac_specialisation">Факультет / Институт:</label>
            <?= $form->field($vacancy, 'faculty')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="cf" id="langs">

            <div class="row" style="margin-left: 10px;">
                <div class="input_holder_3">
                    <label class="main_label " for="name_mail1">Язык:</label>
                    <input type="text" id="name_phone1" name="lang1" value="" />
                </div>
                <div class="input_holder_3">
                    <label class="main_label " for="name_mail1">
                        Уровень владения языком:</label>
                    <select name="level1" >
                            <option value="Базовые знания">Базовые знания</option>
                            <option value="Среднее">Среднее</option>
                            <option value="Высшее">Высшее</option>
                    </select>
                </div>
            </div>
        <input type="hidden" value="2" id="count_lang" name="count_lang"/>

    </div>
    <a class="small_red_btn" onclick="add_lang()">Добавить еще</a>
    <div class="cf">
        <div class="input_holder_1">
            <label class="main_label" for="vac_pc_skills">Владение компьютером:</label>
            <div class="cb_line cf" id="vac_pc_skills">
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_internet]', $vacancy->ps_internet, ['label' => '- интернет']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_office]', $vacancy->ps_office, ['label' => '- MS Office']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_photoshop]', $vacancy->ps_photoshop, ['label' => '- Photoshop']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_autocad]', $vacancy->ps_autocad, ['label' => '- Autocad']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_corel]', $vacancy->ps_corel, ['label' => '- Corel Draw']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[ps_1c]', $vacancy->ps_1c, ['label' => '- 1С']);?>
                </div>
            </div>
        </div>
        <div class="input_holder_3_2">
            <label class="main_label" for="vac_lang2">Другое:</label>
            <?= $form->field($vacancy, 'other')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
        </div>
        <div class="input_holder_1">
            <label class="main_label" for="vac_driver_skills">Водительское удостоверение:</label>
            <div class="cb_line cf" id="vac_driver_skills">
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[auto_cat_a]', $vacancy->auto_cat_a, ['label' => '- категория А']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[auto_cat_b]', $vacancy->auto_cat_b, ['label' => '- категория В']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[auto_cat_c]', $vacancy->auto_cat_c, ['label' => '- категория С']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[auto_cat_d]', $vacancy->auto_cat_d, ['label' => '- категория D']);?>
                </div>
                <div class="cb_wrap">
                    <?= Html::checkbox('Vacancy[auto_cat_e]', $vacancy->auto_cat_e, ['label' => '- категория E']);?>
                </div>
            </div>
        </div>
        <div class="input_holder_3_2">
            <label class="main_label" for="vac_addition_skills">Дополнительные требования:</label>
            <?= $form->field($vacancy, 'additional')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
        </div>
    </div>
    <div class="title_6">
        <span>Условия</span>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_payment_min">Зарплата:</label>
            <div class="special_section">
                <label class="small_label">От</label>
                <?= $form->field($vacancy, 'salary_b')->textInput(['maxlength' => true])->label(false) ?>
                <label class="small_label">До</label>
                <?= $form->field($vacancy, 'salary_e')->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <?= Html::checkbox('Vacancy[results_sobesedovanyya]', $vacancy->results_sobesedovanyya, ['label' => '- по результатам собеседования']);?>
        </div>
        <div class="input_holder_3">
            <label class="main_label" for="vac_bonuses">Премии:</label>
            <?= Html::dropDownList('Vacancy[premiums]', $vacancy->premiums, ['Ежемесячно'=>'Ежемесячно','Ежеквартально'=>'Ежеквартально'],['prompt'=>'']) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_work_days">Режим работы:</label>
            <?= Html::dropDownList('Vacancy[daily_work]', $vacancy->daily_work, [
                'Пн - Пт'=>'Пн - Пт',
                'Вт - Сб'=>'Вт - Сб',
                'Свободный'=>'Свободный',
                'Удаленный'=>'Удаленный',
                'Сессионный'=>'Сессионный',
                '1 через 2'=>'1 через 2'
            ],['prompt'=>'']) ?>
        </div>
        <div class="input_holder_3">
            <label class="main_label" for="vac_test_time">Испытательный срок:</label>
            <?= Html::dropDownList('Vacancy[probation]', $vacancy->probation, [
                '1 месяц'=>'1 месяц',
                '2 месяц'=>'2 месяц',
                '3 месяц'=>'3 месяц',
                'Без испытательного'=>'Без испытательного'
            ],['prompt'=>'']) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_test_payment">Оплата на испытательный срок:</label>
            <?= $form->field($vacancy, 'payment_probation')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="cf">
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[training_company]', $vacancy->training_company, ['label' => '- обучение за счет компании']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[compensation_lunches]', $vacancy->compensation_lunches, ['label' => '- компенсация обедов']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[corporate_dining]', $vacancy->corporate_dining, ['label' => '- корпоративная столовая']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[lca]', $vacancy->lca, ['label' => '- ДМС']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[sick_pay]', $vacancy->sick_pay, ['label' => '- оплата больничного листа']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[paid_holiday]', $vacancy->paid_holiday, ['label' => '- оплачиваемый отпуск']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[corporate_transportation]', $vacancy->corporate_transportation, ['label' => '- корпоративный транспорт']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[fitness_club]', $vacancy->fitness_club, ['label' => '- фитнес клуб']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[mobile_payment]', $vacancy->mobile_payment, ['label' => '- компенсация мобильной связи']);?>
        </div>
        <div class="cb_wrap">
            <?= Html::checkbox('Vacancy[corporate_mobile_communication]', $vacancy->corporate_mobile_communication, ['label' => '- корпоративная моб связь']);?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3">
            <label class="main_label" for="vac_business_trips">Командировки:</label>
            <?= Html::dropDownList('Vacancy[business_trips]', $vacancy->business_trips, [
                '1-2 раза в неделю'=>'1-2 раза в неделю',
                '1-2 раза в месяц'=>'1-2 раза в месяц',
                'сезонно'=>'сезонно',
                'нет'=>'нет'
            ],['prompt'=>'']) ?>
        </div>
    </div>
    <div class="cf">
        <div class="input_holder_3_2">
            <label class="main_label" for="vac_addition_conditions">Дополнительные условия:</label>
            <?= $form->field($vacancy, 'additional_terms')->textarea(['cols' =>30, 'rows'=>10 ])->label(false) ?>
        </div>
    </div>
    <div class="btns_line cf">
        <a href="#3" class="btn_1">Предпросмотр вакансии</a>
        <button type="submit" class="btn_3">Отправить на модерацию</button>
        <button type="submit" name="add_vac" value="1" class="btn_2">Добавить еще вакансию</button>
    </div>
    <?php ActiveForm::end(); ?>
    <?php }?>
</div>

<script>
    function add_lang(){
        // alert($('#count_lang').val());
        var i =$('#count_lang').val();
        var elem = '<div class="row" style="margin-left: 10px;"> ' +
            '<div class="input_holder_3"> ' +
            '<label class="main_label important_input" for="name_mail1">Дополнительный язык:</label> ' +
            '<input type="text" id="name_phone1" name="lang'+i+'" value="" /> ' +
            '</div> ' +
            '<div class="input_holder_3"> ' +
            '<label class="main_label important_input" for="name_mail1">Уровень владения языком:</label> ' +
            '<div class="styled-select slate" style="float: left; width:268px;">'+
            '<select name="level'+i+'" style="width:286px" > ' +
            '<option value="Базовые знания">Базовые знания</option> ' +
            '<option value="Среднее">Среднее</option> ' +
            '<option value="Высшее">Высшее</option> ' +
            '</select> ' +
            '</div> ' +
            '</div> ' +
            '</div>';
        $( "#langs" ).append( elem );

        $('#count_lang').val(parseInt(i)+1);
    }
</script>