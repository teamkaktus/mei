<?php
/**
 *
 * @var PsiElement(assign) $user
 */
if ($models){
?>
<div class="title"><span>полезная информация</span></div>
<div class="sidebar_news">
    <?php foreach($models as $model){?>
    <div class="sidebar_post_item">
        <a href="<?=\yii\helpers\Url::to(['/site/posts', 'slug' => $model->url]);?>">
            <strong><?=$model->name?></strong>
            <p><?=$model->small_desc?>...</p>
        </a>
        <div class="publication_data"><i class="mi_icon mi_calendar"></i><?=Yii::$app->formatter->asDate($model->updated_at)?></div>
    </div>
    <?php }?>
    <a href="/information" class="all_news_btn green">Все статьи</a>
</div>

<?php }?>