<?php
/**
 *
 */?>

<div class="user_events">
    <div class="title_6"><span>Мои мероприятия</span></div>
    <div class="cf user_events_list">
        <div class="row">
            <?php foreach($events as $event){?>

                <div class="user_event_item">
                    <div class="img_wrap">
                        <img alt="" src="<?=$event->event->img?>">
                    </div>
                    <div class="title_7"><?=$event->event->name?> </div>
                    <div class="reg_status">Я зарегистрировался</div>
                    <div class="bottom_part cf">
                        <div class="left_part">
                            <a class="small_blue_btn" href="<?=\yii\helpers\Url::to(['/site/events','slug'=>$event->event->url])?>">Подробнее</a>
                        </div>
                        <div class="right_part"><i class="mi_icon mi_calendar"></i><span><?=$event->event->date_for?></span></div>
                    </div>
                </div>
            <?php }?>
        </div>

        <div class="row">
                <?php echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                ]); ?>
        </div>
            <div class="row">
                <div class="cf">
                    <a class="near_info" href="/events" style="margin: 0">Посмотреть ближайшие</a>
                </div>
            </div>
    </div>

</div>
