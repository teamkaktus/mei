<?php
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\bootstrap\Modal;
/**
 *
 */
?>
<div class="sidebar_left sidebar user_sidebar">
    <p class="user_name_desc">Ваш личный кабинет:</p>
    <div class="user_name"><i class="mi_icon login_icon"></i><?=$user->surname?> <?=$user->username?> <?=$user->lastname?></div>
    <div class="about_user">
        <div class="avatar_part">
            <div class="img_wrap">
                <?php if ($user->avar){?>
                    <img alt="" src="<?=$user->avar?>">
                <?php } else {?>
                <img alt="" src="img/content/001.jpg">
                <?php }?>
            </div>
            <p>Так вас видят пользо-
                ватели</p>
            <a onclick="$('#avatar_modal').arcticmodal();">Изменить аватар</a>
        </div>
        <div class="status_part">
            <div class="status">Вы сейчас онлайн</div>
            <div class="data_of_reg">Вы зарегистрировались на сайте как <?=$user->role?> <span><?php echo Yii::$app->formatter->asDate( $user->created_at);?></span></div>
        </div>
    </div>

</div>
<div style="display: none;">
    <div class="box-modal" id="avatar_modal">
        <div class="box-modal_close arcticmodal-close"><i class="mi_icon mi_close"></i></div>
        <div>
            <div class="modal_title_1">
                Аватар
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'login1-form',
                'action' => '/api/avatar',
                'options' => ['enctype'=>'multipart/form-data']
            ]) ?>
            <input type="file" name="image" required="true">
            <?php
            echo Html::submitButton('Сохранить', ['class'=>'btn btn-primary','style'=>'margin-top:-25px']);
            ?>
            <?php ActiveForm::end() ?>

        </div>
    </div>
</div>

