<?php
/**
 *
 * @var PsiElement(assign) $user
 */
if ($news) {
?>
<div class="title"><span>Анонс мероприятий</span></div>
<div class="sidebar_news">
<?php foreach($news as $new){?>
    <div class="sidebar_post_item">
        <a class="img_wrapper">
            <img src="<?=$new->img?>" alt="<?=$new->img_alt?>">
        </a>
        <a href="<?=\yii\helpers\Url::to(['/site/events', 'slug' => $new->url]);?>">
            <strong><?=$new->name?></strong>
            <p><?=$new->small_desc?>...</p>
        </a>
    </div>
<?php }?>
    <a href="/events" class="all_news_btn">Все мероприятия</a>
</div>
<?php }?>