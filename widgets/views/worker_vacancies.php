<?php
/**
 *
 */?>
<?php if($worker_vac){?>
    <div class="user_vacancies_list" style="margin-bottom: 60px">
        <div class="title_6"><span>Мои вакансии:</span></div>
        <div class="vacancies_section">
            <?php $k=0; foreach ($worker_vac as $item) {
                $vacancy= \app\modules\ls_admin\models\Vacancy::findOne($item->vac_id);
                $kompany = \app\modules\ls_admin\models\Kompany::find()->where(['user_id'=>$vacancy->user_id])->one();
                $k++;
                ?>
                <div class="vacancies_item">
                    <div class="top_hidden_section cf">
                        <div class="left_part">Код <?= $vacancy->kod?></div>
                        <div class="right_part">
                            <div class="vac_data"><i class="mi_icon mi_calendar"></i>вакансия от <span><?=Yii::$app->formatter->asDate($vacancy->updated_at)?></span></div>
                            <?php if($vacancy->type){?><b class="vac_tags">Горящая</b><?php }?>
                        </div>
                    </div>
                    <div class="vac_title" style="text-decoration: none;">
                        </b> <input type="checkbox" id="vac_<?=$k?>" value="<?=$item->id?>"/> <a class="vac_name"><?=$vacancy->name?>,</a> <span class="vac_payment"><?php if($vacancy->results_sobesedovanyya) echo 'по результатам собеседования'; else echo $vacancy->salary_e.'руб.'; ?></span><?php if($vacancy->type){?><b class="vac_tags">Горящая</b><?php }?>
                    </div>
                    <div class="small_desc">
                        <div class="vac_data"><i class="mi_icon mi_calendar"></i>вакансия от <span><?=Yii::$app->formatter->asDate($vacancy->updated_at)?></span></div>
                    </div>
                    <div class="short_desc">
                        <div class="title_5">Компания “<?=$kompany->name?>”</div>
                        <div class="main_conditions">
                            <p>Режим работы: <?=$vacancy->daily_work?></p>
                        </div>
                        <div class="conditions_section">
                            <div class="title_5">Обязанности:</div>
                            <p><?=$vacancy->duties?></p>
                            <br>
                        </div>
                        <div class="conditions_section">
                            <div class="title_5">Требования:</div>
                            <?php if($vacancy->education) echo '<p>-'.$vacancy->education.'</p>'; ?>
                            <?php if($vacancy->experience) echo '<p>- Опыт работы: '.$vacancy->experience.'</p>'; ?>
                            <?php if($vacancy->faculty) echo '<p>- Факультет / Институт: '.$vacancy->faculty.'</p>'; ?>
                            <?php
                            $langs = \app\modules\ls_admin\models\LangVac::find()->where(['vac_id'=>$vacancy->id])->all();
                            if($langs){
                                ?> <p>- Владение языками: <ul style="margin: 0; margin-left: 10px"><?php
                                foreach ($langs as $lang){
                                    echo '<li>'.$lang->name.' ('.$lang->level.')</li>';
                                }
                            ?></ul></p><?php
                            }
                            ?>
                            <?php if($vacancy->ps_internet || $vacancy->ps_office || $vacancy->ps_photoshop|| $vacancy->ps_autocad || $vacancy->ps_corel || $vacancy->ps_1c || $vacancy->other ){?>
                        <p>- Владение компьютером:
                            <ul style="margin: 0; margin-left: 10px">
                                <?php if($vacancy->ps_internet) echo '<li>- интернет </li>'; ?>
                                <?php if($vacancy->ps_office) echo '<li>- MS Office </li>'; ?>
                                <?php if($vacancy->ps_photoshop) echo '<li>- Photoshop </li>'; ?>
                                <?php if($vacancy->ps_autocad) echo '<li>- Autocad </li>'; ?>
                                <?php if($vacancy->ps_corel) echo '<li>- Corel Draw </li>'; ?>
                                <?php if($vacancy->ps_1c) echo '<li>- 1С </li>'; ?>
                                <?php if($vacancy->other) echo '<li>- '.$vacancy->other.' </li>'; ?>
                            </ul>
                        </p>
                    <?php }?>
                            <?php if($vacancy->ps_internet || $vacancy->ps_office || $vacancy->ps_photoshop|| $vacancy->ps_autocad || $vacancy->ps_corel || $vacancy->ps_1c || $vacancy->other ){?>
                        <p>- Водительское удостоверение:
                            <ul style="margin: 0; margin-left: 10px">
                                <?php if($vacancy->auto_cat_a) echo '<li>- категория А </li>'; ?>
                                <?php if($vacancy->auto_cat_b) echo '<li>- категория B </li>'; ?>
                                <?php if($vacancy->auto_cat_c) echo '<li>- категория C </li>'; ?>
                                <?php if($vacancy->auto_cat_d) echo '<li>- категория D </li>'; ?>
                                <?php if($vacancy->auto_cat_e) echo '<li>- категория E </li>'; ?>

                            </ul>
                        </p>
                    <?php }?>

                            <?php if($vacancy->additional) echo '<p>- Дополнительные требования: <p style="margin-left: 10px;">'.$vacancy->additional.'</p></p>'; ?>
                            </br>
                        </div>
                        <div class="conditions_section">
                            <div class="title_5"> Условия:</div>
                            <p>- Зарплата:
                                <?php if($vacancy->results_sobesedovanyya)
                                    echo 'по результатам собеседования';
                                else {
                                    echo 'От: '.$vacancy->salary_b.' руб., До '.$vacancy->salary_e.'руб.';
                                }
                                ?>
                            </p>
                            <?php if($vacancy->premiums) echo '<p>- Премии: '.$vacancy->premiums.'</p>'; ?>
                            <?php if($vacancy->probation) echo '<p>- Испытательный срок: '.$vacancy->probation.'</p>'; ?>
                            <?php if($vacancy->payment_probation) echo '<p>- Оплата на испытательный срок: '.$vacancy->payment_probation.'</p>'; ?>
                            <?php if($vacancy->training_company) echo '<p>- обучение за счет компании</p>'; ?>
                            <?php if($vacancy->compensation_lunches) echo '<p>- компенсация обедов</p>'; ?>
                            <?php if($vacancy->corporate_dining) echo '<p>- корпоративная столовая</p>'; ?>
                            <?php if($vacancy->lca) echo '<p>- ДМС</p>'; ?>
                            <?php if($vacancy->sick_pay) echo '<p>- оплата больничного листа</p>'; ?>
                            <?php if($vacancy->paid_holiday) echo '<p>- оплачиваемый отпуск</p>'; ?>
                            <?php if($vacancy->corporate_transportation) echo '<p>- корпоративный транспорт</p>'; ?>
                            <?php if($vacancy->fitness_club) echo '<p>- фитнес клуб</p>'; ?>
                            <?php if($vacancy->mobile_payment) echo '<p>- компенсация мобильной связи</p>'; ?>
                            <?php if($vacancy->corporate_mobile_communication) echo '<p>- корпоративная моб связь</p>'; ?>
                            <?php if($vacancy->business_trips) echo '<p>- Командировки: '.$vacancy->business_trips.'</p>'; ?>
                            <?php if($vacancy->additional_terms) echo '<p>- '.$vacancy->additional_terms.'</p>'; ?>
                        </div>
                        <div class="bottom_part cf">
                            <div class="right_part">
                                <a class="close_btn">Свернуть</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
            <button class="btn btn-primary" onclick="up_vac(<?=$k?>)">Отправить запрос</button>
            <?php echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]); ?>

        </div>
    </div>
    <script>
        function up_vac(count){

            var data=[];
            var d =0;
            for (var i=1;i<=count;i++){
                if( $("#vac_"+i).is(':checked')){
                    data[d]= $("#vac_"+i).val();
                    d++;
                }
            }

            if (data.length>0){
                $.post("/api/respond",{data:data}, function(data) {
                    if(data){
                        location.reload();
                    }
                });
            }
        }

    </script>
<?php }?>
