<?php
/**
 *
 * @var PsiElement(assign) $user
 */
if ($news) {
?>
<div class="title"><span>новости</span></div>
<div class="sidebar_news">
<?php foreach($news as $new){?>
    <div class="sidebar_post_item">
        <a class="img_wrapper">
            <img src="<?=$new->img?>" alt="<?=$new->img_alt?>">
        </a>
        <a href="<?=\yii\helpers\Url::to(['/site/news', 'slug' => $new->url]);?>">
            <strong><?=$new->name?></strong>
            <p><?=$new->small_desc?>...</p>
        </a>
        <div class="publication_data"><i class="mi_icon mi_calendar"></i><?=Yii::$app->formatter->asDate($new->updated_at)?></div>
    </div>
<?php }?>
    <a href="/news" class="all_news_btn">Все новости</a>
</div>
<?php }?>