jQuery(document).ready(function () {

    //Слайдер
    $('.owl-carousel').owlCarousel({
        margin:10,
        loop:true,
        autoWidth:true,
        items:4,
        nav:true
    });
    $('.top_slider').owlCarousel({
        loop:true,
        nav:true,
        items:1
    });
    $('.sidebar_slider').owlCarousel({
        loop:true,
        nav:true,
        items:1
    });


    //  Инициация табов
    var first_tab =  $(".tabs-menu a:first-child").attr("href");
    $(first_tab).fadeIn();
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

    //  Раскрыти расширенного поиска
    $('.open_search').on('click', function(){
        if ($('.search').hasClass('search_open')){
            $('.open_search b').text("Развернуть");
            console.log(1);
        }else {
            console.log(2);
            $('.open_search b').text('Свернуть');
        }
        $('.search').toggleClass('search_open');
    });

    //развертование блоков
    $('.open_anons_btn').on('click', function(){
        $(this).parents('.anons_item').toggleClass('open_anons');
    });
    $('.open_news_btn').on('click', function(){
        $(this).parents('.news_item').toggleClass('open_anons');
    });
    $('.open_cribs_btn').on('click', function(){
        $(this).parents('.cribs_item').toggleClass('open_anons');
    });
    $('.open_publication_btn').on('click', function(){
        $(this).parents('.publication_item').toggleClass('open_anons');
    });
    $('.vacancies_section .vac_name').on('click', function(){
        $(this).parents('.vacancies_item').toggleClass('vacancy_open');

    });
    $('.vacancies_section .close_btn').on('click', function(){
        $(this).parents('.vacancies_item').removeClass('vacancy_open');
    });
    $('.prj_header').on('click', function(){
        $(this).parents('.project').toggleClass('open_project');
        if ($('.project').hasClass('open_project')){
            $('.green_btn').text('Свернуть');
        }else {
            $('.green_btn').text("Подробнее о проекте");
        }
    });
    $('.project .green_btn').on('click', function(){
        $(this).parents('.project').toggleClass('open_project');
        if ($('.project').hasClass('open_project')){
            $('.green_btn').text('Свернуть');
        }else {
            $('.green_btn').text("Подробнее о проекте");
        }
    });


    //Активация стилизации инпутов
    $(function() {
        $('input, select').styler();
    });
    $('.login_opener').on('click', function (){
        $("#logon_modal").arcticmodal();
    });
    $('.review_opener').on('click', function (){
        $("#review_modal").arcticmodal();
    });
    $('.rules_opener').on('click', function (){
        $("#rules").arcticmodal();
    });
    $('.registration_opener').on('click', function (){
        $("#registration_modal").arcticmodal();
    });
    $('.vacancy_opener').on('click', function (){
        $("#vacancy_modal").arcticmodal();
    });
    $('.summary_opener').on('click', function (){
        $("#summary_modal").arcticmodal();
    });


    //listat


    //$("#my_ob").jqte();

    $("#ls_reg_form").submit(function(event) {
        event.preventDefault();

        if (($('#reg_name').val()=='') || ($('#reg_mail').val()=='') || ($('#reg_login').val()=='') || ($('#reg_pass').val()=='') ){
            console.log($('#reg_name').val());
            console.log($('#reg_mail').val());
            console.log($('#reg_login').val());
            console.log($('#reg_pass').val());
            sweetAlert(
                'Упс...',
               'Вы не заполнили одно из полив!' ,
                'error'
            )
        } else {
            if($('#reg_login').val().length < 4){
                sweetAlert(
                    'Упс...',
                    'Логин должен быть больше 4 символов!' ,
                    'error'
                )
            }else if ($('#reg_pass').val().length <6){
                sweetAlert(
                    'Упс...',
                    'Пароль должен быть больше 6 символов!' ,
                    'error'
                )
            }else {
               //alert(unice_fild($('#reg_mail').val()));
                $.post("/api/unicemail",{email:$('#reg_mail').val()}, function(data) {
                    if(data){
                        sweetAlert(
                            'Упс...',
                            'Такой email уже занят!' ,
                            'error'
                        )
                    } else {
                        $.post("/api/unicelogin",{login:$('#reg_login').val()}, function(login) {
                            if(login){
                                sweetAlert(
                                    'Упс...',
                                    'Такой логин уже занят!' ,
                                    'error'
                                )
                            }else {
                                $("#rules").arcticmodal();
                            }
                        });
                    }
                });
                //$("#rules").arcticmodal();
            }
        }


    });

    $('#ls_red_end').on('click', function (){
       console.log($("#license").is(':checked') );
        console.log($("#worker").is(':checked') );
        var worker ='';
        if( $("#worker").is(':checked'))
            worker=1;
        else
            worker=2;
       if( $("#license").is(':checked')){
           var data={
               email:$('#reg_mail').val(),
               username:$('#reg_name').val(),
               password:$('#reg_pass').val(),
               login:$('#reg_login').val(),
               worker: worker
           };
           $.post("/api/register", data, function(result){
               console.log(result['email']);
                if (result.email)
                    sweetAlert(
                        'Упс...',
                        'Такой email уже занят!' ,
                        'error'
                    )
               if (result.login)
                   sweetAlert(
                       'Упс...',
                       'Такой логин уже занят!' ,
                       'error'
                   )
           });
       }
    });
});

function unice_fild(email) {
    $.post("/api/unicemail",{email:email}, function(data) {
        if(data){

        } else {

        }
    });
}

function open_rezume(){
    $("#summary_modal").arcticmodal();
}

 function vac_add_worker  (vac_id){
    //alert('asd');
    $.post("/api/vacaddworker",{vac_id:vac_id}, function(data) {
        if(data){
            sweetAlert(
                'Вакансия добавлена!',
                'Для отправки запроса и получения контактов работодателя зайдите в Лич каб',
                'success'
            )
        } else {
            sweetAlert(
                'Вакансия уже добавлена!',
                '',
                'error'
            )
        }
    });
}

function events_reg(events_id){
    //alert('asd');
    $.post("/api/eventsreg",{events_id:events_id}, function(data) {
        if(data){
            sweetAlert(
                'Вы удачно зарегистрированы на мероприятие!',
                '',
                'success'
            )
        } else {
            sweetAlert(
                'Вы уже зарегистрированы на ето мероприятие!',
                '',
                'error'
            )
        }
    });
}