-- phpMyAdmin SQL Dump
-- version 2.6.1-rc2
-- http://www.phpmyadmin.net
-- 
-- Хост: localhost
-- Час створення: Сер 27 2013 р., 14:28
-- Версія сервера: 5.1.55
-- Версія PHP: 5.3.6
-- 
-- БД: `bd_kitchen`
-- 

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_cokolj`
-- 

CREATE TABLE `dc_order_cokolj` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `cokolj_id` int(11) NOT NULL COMMENT 'Цоколь',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `k1` int(11) NOT NULL COMMENT 'Кути внутрішні',
  `k2` int(11) NOT NULL COMMENT 'Кути зовнішні',
  `k3` int(11) NOT NULL COMMENT 'Кути універсальні',
  `z` int(11) NOT NULL COMMENT 'Заглушка',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- Дамп даних таблиці `dc_order_cokolj`
-- 

INSERT INTO `dc_order_cokolj` VALUES (1, 1, 2, 32, '2', 0, 0, 0, 0, 960);
INSERT INTO `dc_order_cokolj` VALUES (2, 2, 3, 0, '', 0, 0, 0, 0, 0);
INSERT INTO `dc_order_cokolj` VALUES (3, 3, 2, 1, '3', 1, 1, 0, 2, 130);
INSERT INTO `dc_order_cokolj` VALUES (4, 4, 2, 1, '3', 1, 1, 1, 1, 130);
INSERT INTO `dc_order_cokolj` VALUES (5, 8, 2, 1, '3', 1, 0, 0, 2, 80);
INSERT INTO `dc_order_cokolj` VALUES (6, 10, 2, 1, '3', 1, 0, 0, 2, 80);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_dekorat`
-- 

CREATE TABLE `dc_order_dekorat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `dekorat_id` int(11) NOT NULL COMMENT 'Декоративні елементи',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Дамп даних таблиці `dc_order_dekorat`
-- 

INSERT INTO `dc_order_dekorat` VALUES (1, 1, 1, 8, '', 160);
INSERT INTO `dc_order_dekorat` VALUES (2, 3, 1, 2, '3', 40);
INSERT INTO `dc_order_dekorat` VALUES (3, 3, 4, 2, '2', 100);
INSERT INTO `dc_order_dekorat` VALUES (4, 3, 6, 6, '2', 600);
INSERT INTO `dc_order_dekorat` VALUES (5, 3, 10, 2, '3', 1200);
INSERT INTO `dc_order_dekorat` VALUES (6, 4, 1, 2, '3', 40);
INSERT INTO `dc_order_dekorat` VALUES (7, 7, 4, 1, '2', 50);
INSERT INTO `dc_order_dekorat` VALUES (8, 8, 4, 1, '2', 50);
INSERT INTO `dc_order_dekorat` VALUES (9, 8, 5, 1, '3', 70);
INSERT INTO `dc_order_dekorat` VALUES (10, 9, 5, 2, '3', 140);
INSERT INTO `dc_order_dekorat` VALUES (11, 9, 10, 2, '3', 1200);
INSERT INTO `dc_order_dekorat` VALUES (12, 10, 4, 1, '2', 50);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_komplektacija`
-- 

CREATE TABLE `dc_order_komplektacija` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `firma_komplektacija_id` text NOT NULL,
  `komplektacija_id` int(11) NOT NULL COMMENT 'Кількість',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

-- 
-- Дамп даних таблиці `dc_order_komplektacija`
-- 

INSERT INTO `dc_order_komplektacija` VALUES (1, 1, '13', 937, 3, '', 18);
INSERT INTO `dc_order_komplektacija` VALUES (2, 2, '1', 1327, 2, '3', 22);
INSERT INTO `dc_order_komplektacija` VALUES (3, 3, '1', 1327, 20, '3', 220);
INSERT INTO `dc_order_komplektacija` VALUES (5, 3, '6', 0, 1, '3', 0);
INSERT INTO `dc_order_komplektacija` VALUES (6, 3, '8', 920, 1, '3', 410);
INSERT INTO `dc_order_komplektacija` VALUES (7, 3, '5', 1616, 1, '3', 400);
INSERT INTO `dc_order_komplektacija` VALUES (8, 3, '4', 1295, 1, '3', 700);
INSERT INTO `dc_order_komplektacija` VALUES (9, 3, '4', 1292, 2, '3', 80);
INSERT INTO `dc_order_komplektacija` VALUES (10, 3, '4', 1310, 3, '3', 810);
INSERT INTO `dc_order_komplektacija` VALUES (11, 3, '9', 1669, 20, '3', 0);
INSERT INTO `dc_order_komplektacija` VALUES (12, 4, '1', 1328, 2, '3', 30);
INSERT INTO `dc_order_komplektacija` VALUES (13, 4, '1', 1333, 2, '3', 0);
INSERT INTO `dc_order_komplektacija` VALUES (14, 4, '1', 1340, 1, '3', 282);
INSERT INTO `dc_order_komplektacija` VALUES (15, 6, '1', 1327, 0, '', 0);
INSERT INTO `dc_order_komplektacija` VALUES (16, 7, '1', 1, 20, '3', 60);
INSERT INTO `dc_order_komplektacija` VALUES (17, 7, '1', 2, 2, '3', 10);
INSERT INTO `dc_order_komplektacija` VALUES (18, 7, '1', 38, 1, '3', 40);
INSERT INTO `dc_order_komplektacija` VALUES (19, 7, '1', 40, 3, '3', 180);
INSERT INTO `dc_order_komplektacija` VALUES (20, 7, '1', 32, 1, '3', 550);
INSERT INTO `dc_order_komplektacija` VALUES (21, 7, '5', 469, 1, '3', 329);
INSERT INTO `dc_order_komplektacija` VALUES (22, 7, '10', 824, 20, '3', 820);
INSERT INTO `dc_order_komplektacija` VALUES (23, 7, '6', 731, 1, '3', 480);
INSERT INTO `dc_order_komplektacija` VALUES (24, 7, '8', 803, 1, '3', 325);
INSERT INTO `dc_order_komplektacija` VALUES (25, 8, '1', 1, 20, '3', 60);
INSERT INTO `dc_order_komplektacija` VALUES (26, 8, '1', 2, 2, '3', 10);
INSERT INTO `dc_order_komplektacija` VALUES (27, 8, '1', 43, 3, '3', 705);
INSERT INTO `dc_order_komplektacija` VALUES (28, 8, '5', 468, 1, '3', 310);
INSERT INTO `dc_order_komplektacija` VALUES (29, 9, '9', 2052, 40, '3', 720);
INSERT INTO `dc_order_komplektacija` VALUES (30, 9, '4', 239, 27, '3', 594);
INSERT INTO `dc_order_komplektacija` VALUES (31, 9, '4', 254, 2, '3', 1380);
INSERT INTO `dc_order_komplektacija` VALUES (32, 10, '4', 231, 20, '3', 180);
INSERT INTO `dc_order_komplektacija` VALUES (33, 10, '4', 235, 4, '3', 92);
INSERT INTO `dc_order_komplektacija` VALUES (34, 10, '4', 278, 3, '3', 660);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_operation`
-- 

CREATE TABLE `dc_order_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `tovar_id` text NOT NULL COMMENT 'Товар',
  `x` int(11) NOT NULL COMMENT 'Довжина',
  `y` int(11) NOT NULL COMMENT 'Висота',
  `operation_id` int(11) NOT NULL COMMENT 'Операція',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Дамп даних таблиці `dc_order_operation`
-- 

INSERT INTO `dc_order_operation` VALUES (1, 1, '1', 32, 12, 2, 1, 3840);
INSERT INTO `dc_order_operation` VALUES (2, 3, '1', 2500, 100, 1, 1, 5e+06);
INSERT INTO `dc_order_operation` VALUES (3, 4, '1', 2000, 100, 1, 1, 4e+06);
INSERT INTO `dc_order_operation` VALUES (4, 8, '1', 2000, 100, 1, 1, 4);
INSERT INTO `dc_order_operation` VALUES (5, 10, '1', 100, 2000, 1, 1, 4);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_plintus`
-- 

CREATE TABLE `dc_order_plintus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `firma_plintus_id` int(11) NOT NULL COMMENT 'Фірма',
  `modelj_plintus_id` int(11) NOT NULL,
  `plintus_id` int(11) NOT NULL COMMENT 'Плінтус',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `k1` int(11) NOT NULL COMMENT 'Кути внутрішні',
  `k2` int(11) NOT NULL COMMENT 'Кути зовнішні',
  `z1` int(11) NOT NULL COMMENT 'Закінчення ліве',
  `z2` int(11) NOT NULL COMMENT 'Закінчення праве',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Дамп даних таблиці `dc_order_plintus`
-- 

INSERT INTO `dc_order_plintus` VALUES (1, 1, 1, 2, 4, 3, '', 0, 0, 0, 0, 45);
INSERT INTO `dc_order_plintus` VALUES (2, 2, 1, 1, 2, 1, '3', 2, 0, 1, 1, 22);
INSERT INTO `dc_order_plintus` VALUES (3, 3, 2, 0, 0, 2, '3', 1, 1, 1, 0, 0);
INSERT INTO `dc_order_plintus` VALUES (4, 4, 1, 1, 3, 1, '3', 2, 1, 1, 1, 19);
INSERT INTO `dc_order_plintus` VALUES (5, 7, 1, 1, 1, 1, '3', 1, 0, 2, 2, 31);
INSERT INTO `dc_order_plintus` VALUES (6, 8, 1, 1, 1, 1, '3', 1, 1, 1, 1, 28);
INSERT INTO `dc_order_plintus` VALUES (7, 10, 1, 1, 2, 1, '3', 1, 0, 1, 1, 21);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_sklo`
-- 

CREATE TABLE `dc_order_sklo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `modelj_sklo_id` int(11) NOT NULL COMMENT 'Модель',
  `sklo_id` int(11) NOT NULL COMMENT 'Скло',
  `x` int(11) NOT NULL COMMENT 'Довжина',
  `y` int(11) NOT NULL COMMENT 'Висота',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Дамп даних таблиці `dc_order_sklo`
-- 

INSERT INTO `dc_order_sklo` VALUES (1, 1, 1, 6, 21, 23, 2, '', 42504);
INSERT INTO `dc_order_sklo` VALUES (2, 3, 2, 8, 200, 200, 2, '3', 2.4e+06);
INSERT INTO `dc_order_sklo` VALUES (3, 4, 2, 8, 200, 400, 1, '3', 2.4e+06);
INSERT INTO `dc_order_sklo` VALUES (4, 4, 2, 8, 200, 600, 1, '3', 3.6e+06);
INSERT INTO `dc_order_sklo` VALUES (5, 6, 1, 3, 3450, 400, 3, '1', 124.2);
INSERT INTO `dc_order_sklo` VALUES (6, 7, 2, 8, 0, 0, 2, '1', 0);
INSERT INTO `dc_order_sklo` VALUES (7, 8, 2, 8, 200, 700, 2, '1', 8.4);
INSERT INTO `dc_order_sklo` VALUES (8, 10, 2, 8, 250, 760, 2, '1', 11.4);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_stiljnycja`
-- 

CREATE TABLE `dc_order_stiljnycja` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `modelj_stiljnycja_id` text NOT NULL COMMENT 'Модель',
  `stiljnycja_id` int(11) NOT NULL COMMENT 'Стільниця (код)',
  `glybyna_id` text NOT NULL COMMENT 'Глибина',
  `dovzyna` int(11) NOT NULL COMMENT 'Довжина',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `obrobka` tinyint(1) NOT NULL COMMENT 'Обробляти?',
  `obrobka_id` int(11) NOT NULL COMMENT 'Обробка',
  `price_st` float NOT NULL,
  `price_ob` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- Дамп даних таблиці `dc_order_stiljnycja`
-- 

INSERT INTO `dc_order_stiljnycja` VALUES (1, 1, '1', 1, '1', 32, 3, '', 1, 1, 45, 90, 135);
INSERT INTO `dc_order_stiljnycja` VALUES (2, 3, '2', 3, '1', 3050, 1, '3', 1, 1, 20, 30, 50);
INSERT INTO `dc_order_stiljnycja` VALUES (3, 4, '2', 3, '1', 4050, 1, '3', 1, 1, 20, 30, 50);
INSERT INTO `dc_order_stiljnycja` VALUES (4, 6, '2', 3, '1', 3050, 3, '2', 1, 1, 60, 90, 150);
INSERT INTO `dc_order_stiljnycja` VALUES (5, 7, '1', 1, '1', 3050, 1, '3', 1, 2, 15, 35, 50);
INSERT INTO `dc_order_stiljnycja` VALUES (6, 8, '2', 3, '1', 3050, 1, '3', 1, 2, 20, 35, 55);
INSERT INTO `dc_order_stiljnycja` VALUES (7, 9, '3', 2, '1', 4100, 2, '3', 0, 2, 60, 0, 60);
INSERT INTO `dc_order_stiljnycja` VALUES (8, 10, '2', 3, '1', 3050, 1, '3', 1, 2, 20, 35, 55);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_tumba`
-- 

CREATE TABLE `dc_order_tumba` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `firma_id` text NOT NULL COMMENT 'Фірма',
  `tumba_id` int(11) NOT NULL COMMENT 'Тумба',
  `fasad_id` text NOT NULL COMMENT 'Фасад',
  `kl` int(11) NOT NULL COMMENT 'Кількість',
  `odyn_vymir_id` text NOT NULL COMMENT 'Вимір',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

-- 
-- Дамп даних таблиці `dc_order_tumba`
-- 

INSERT INTO `dc_order_tumba` VALUES (1, 1, 'Edi', 1, 'mdf_1', 2, '1', 154.452);
INSERT INTO `dc_order_tumba` VALUES (2, 2, 'Edi', 1, 'gljan', 1, '3', 84.226);
INSERT INTO `dc_order_tumba` VALUES (3, 2, 'Edi', 3, 'gljan', 1, '3', 89.7821);
INSERT INTO `dc_order_tumba` VALUES (4, 2, 'Edi', 12, 'gljan', 1, '3', 100.49);
INSERT INTO `dc_order_tumba` VALUES (5, 3, 'BRW', 1, '', 1, '3', 43.7276);
INSERT INTO `dc_order_tumba` VALUES (7, 4, 'Edi', 6, 'gljan', 2, '3', 206.98);
INSERT INTO `dc_order_tumba` VALUES (8, 4, 'Edi', 14, 'gljan', 1, '3', 300.388);
INSERT INTO `dc_order_tumba` VALUES (9, 4, 'Edi', 19, 'gljan', 1, '3', 448.639);
INSERT INTO `dc_order_tumba` VALUES (10, 6, 'Edi', 1, 'gljan', 2, '3', 168.452);
INSERT INTO `dc_order_tumba` VALUES (11, 6, 'Edi', 4, 'gljan', 4, '3', 369.145);
INSERT INTO `dc_order_tumba` VALUES (12, 7, 'Edi', 2, 'gljan', 1, '3', 99.2863);
INSERT INTO `dc_order_tumba` VALUES (13, 7, 'Edi', 9, 'mat', 1, '3', 72.6049);
INSERT INTO `dc_order_tumba` VALUES (14, 7, 'MDF', 12, '', 1, '3', 44.5906);
INSERT INTO `dc_order_tumba` VALUES (15, 7, 'Edi', 14, 'gljan', 1, '3', 300.388);
INSERT INTO `dc_order_tumba` VALUES (16, 8, 'Edi', 3, 'gljan', 1, '3', 89.7821);
INSERT INTO `dc_order_tumba` VALUES (17, 8, 'Edi', 6, 'gljan', 1, '3', 103.49);
INSERT INTO `dc_order_tumba` VALUES (18, 9, 'BRW', 2, '', 1, '3', 44.5279);
INSERT INTO `dc_order_tumba` VALUES (19, 9, '', 0, '', 0, '', 0);
INSERT INTO `dc_order_tumba` VALUES (20, 10, 'Edi', 4, 'mat', 1, '3', 92.2863);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_order_vartistj`
-- 

CREATE TABLE `dc_order_vartistj` (
  `order_id` int(11) NOT NULL COMMENT 'Замовлення',
  `var_dekorat` int(11) NOT NULL COMMENT 'Вартість декоративних елементів',
  `var_sklo` int(11) NOT NULL COMMENT 'Вартість скла',
  `var_stiljnycja` int(11) NOT NULL COMMENT 'Вартість стільниця',
  `var_plintus` int(11) NOT NULL COMMENT 'Вартість стільниця',
  `var_cokolj` int(11) NOT NULL COMMENT 'Вартість цоколь',
  `var_operation` int(11) NOT NULL COMMENT 'Вартість операцій',
  `var_tumba` int(11) NOT NULL COMMENT 'Вартість тумб',
  `total` int(11) NOT NULL COMMENT 'Загальна вартість'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `dc_order_vartistj`
-- 


-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_tumba`
-- 

CREATE TABLE `dc_tumba` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `tumba_id` int(11) NOT NULL COMMENT 'Назва',
  `shyryna_id` int(11) NOT NULL COMMENT 'Ширина',
  `vysota_id` int(11) NOT NULL COMMENT 'Висота',
  `dovzyna` int(11) NOT NULL COMMENT 'Довжина',
  `zaveshky` int(11) NOT NULL COMMENT 'Завешки',
  `polkotrymach` int(11) NOT NULL COMMENT 'Полкотримачі',
  `price_polk_zavesh` float NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- 
-- Дамп даних таблиці `dc_tumba`
-- 

INSERT INTO `dc_tumba` VALUES (1, 1, 150, 718, 601, 2, 3, 7, 43.7276);
INSERT INTO `dc_tumba` VALUES (2, 2, 200, 718, 601, 2, 3, 7, 44.5279);
INSERT INTO `dc_tumba` VALUES (3, 3, 250, 601, 601, 0, 0, 0, 32.0893);
INSERT INTO `dc_tumba` VALUES (4, 4, 200, 718, 601, 0, 0, 0, 37.5279);
INSERT INTO `dc_tumba` VALUES (5, 7, 150, 601, 718, 0, 0, 0, 30.7985);
INSERT INTO `dc_tumba` VALUES (6, 6, 200, 718, 601, 4, 2, 10, 47.5906);
INSERT INTO `dc_tumba` VALUES (7, 7, 150, 601, 601, 0, 0, 0, 30.7985);
INSERT INTO `dc_tumba` VALUES (8, 8, 150, 718, 601, 2, 0, 4, 40.7903);
INSERT INTO `dc_tumba` VALUES (9, 15, 150, 601, 601, 2, 3, 7, 37.7985);
INSERT INTO `dc_tumba` VALUES (10, 16, 200, 601, 718, 2, 3, 7, 38.4701);
INSERT INTO `dc_tumba` VALUES (11, 17, 150, 718, 601, 2, 3, 7, 43.7903);
INSERT INTO `dc_tumba` VALUES (12, 18, 200, 718, 718, 2, 3, 7, 44.5906);
INSERT INTO `dc_tumba` VALUES (13, 24, 250, 601, 601, 0, 0, 0, 168.367);
INSERT INTO `dc_tumba` VALUES (14, 24, 200, 718, 601, 2, 3, 7, 187.449);
INSERT INTO `dc_tumba` VALUES (15, 24, 200, 718, 601, 2, 3, 7, 187.449);
INSERT INTO `dc_tumba` VALUES (16, 24, 150, 601, 601, 2, 3, 7, 175.367);
INSERT INTO `dc_tumba` VALUES (17, 25, 200, 601, 601, 2, 1, 5, 176.566);
INSERT INTO `dc_tumba` VALUES (18, 24, 150, 718, 718, 2, 2, 6, 186.449);
INSERT INTO `dc_tumba` VALUES (19, 25, 200, 915, 915, 3, 4, 10, 215.679);
INSERT INTO `dc_tumba` VALUES (20, 90, 200, 601, 0, 3, 4, 10, 38.9002);
INSERT INTO `dc_tumba` VALUES (21, 196, 150, 601, 601, 2, 3, 7, 207.83);
INSERT INTO `dc_tumba` VALUES (22, 196, 200, 718, 718, 0, 0, 0, 289.842);
INSERT INTO `dc_tumba` VALUES (23, 196, 200, 601, 718, 2, 3, 7, 249.799);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_tumba1`
-- 

CREATE TABLE `dc_tumba1` (
  `id` int(11) NOT NULL COMMENT 'Код',
  `pcv` float NOT NULL COMMENT 'Пцв',
  `dsp` float NOT NULL COMMENT 'ДСП',
  `dvp` float NOT NULL COMMENT 'ДВП',
  `fasad` float NOT NULL COMMENT 'Фасад',
  KEY `fasad` (`fasad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `dc_tumba1`
-- 

INSERT INTO `dc_tumba1` VALUES (1, 0, 0.431252, 0.101246, 0.101246);
INSERT INTO `dc_tumba1` VALUES (2, 0, 0.431452, 0.136896, 0.136896);
INSERT INTO `dc_tumba1` VALUES (3, 0, 0.361452, 0.144232, 0.144232);
INSERT INTO `dc_tumba1` VALUES (4, 0, 0.431452, 0.136896, 0.136896);
INSERT INTO `dc_tumba1` VALUES (5, 0, 0.361052, 0.087016, 0.087016);
INSERT INTO `dc_tumba1` VALUES (6, 0, 0.431452, 0.139748, 0.139748);
INSERT INTO `dc_tumba1` VALUES (7, 0, 0.361052, 0.087016, 0.087016);
INSERT INTO `dc_tumba1` VALUES (8, 0, 0.431252, 0.104098, 0.104098);
INSERT INTO `dc_tumba1` VALUES (9, 0, 0.361052, 0.087016, 0.087016);
INSERT INTO `dc_tumba1` VALUES (10, 0, 0.361252, 0.116816, 0.116816);
INSERT INTO `dc_tumba1` VALUES (11, 0, 0.431252, 0.104098, 0.104098);
INSERT INTO `dc_tumba1` VALUES (12, 0, 0.431452, 0.139748, 0.139748);
INSERT INTO `dc_tumba1` VALUES (13, 0, 2.03968, 0.236016, 0.236016);
INSERT INTO `dc_tumba1` VALUES (14, 0, 2.17797, 0.282348, 0.282348);
INSERT INTO `dc_tumba1` VALUES (15, 0, 2.17797, 0.282348, 0.282348);
INSERT INTO `dc_tumba1` VALUES (16, 0, 2.03968, 0.236016, 0.236016);
INSERT INTO `dc_tumba1` VALUES (17, 0, 2.03968, 0.38144, 0.38144);
INSERT INTO `dc_tumba1` VALUES (18, 0, 2.17797, 0.282348, 0.282348);
INSERT INTO `dc_tumba1` VALUES (19, 0, 2.41083, 0.5824, 0.5824);
INSERT INTO `dc_tumba1` VALUES (20, 0, 0.361252, 0, 1);
INSERT INTO `dc_tumba1` VALUES (21, 0, 2.51038, 0, 1);
INSERT INTO `dc_tumba1` VALUES (22, 0, 3.62303, 0, 1.125);
INSERT INTO `dc_tumba1` VALUES (23, 0, 3.03499, 0, 1);

-- --------------------------------------------------------

-- 
-- Структура таблиці `dc_tumba2`
-- 

CREATE TABLE `dc_tumba2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `tumba_id` int(11) NOT NULL COMMENT 'Назва',
  `shyryna_id` int(11) NOT NULL COMMENT 'Ширина',
  `vysota_id` int(11) NOT NULL COMMENT 'Висота',
  `pcv` int(11) NOT NULL COMMENT 'Пцв',
  `dsp` int(11) NOT NULL COMMENT 'ДСП',
  `dvp` int(11) NOT NULL COMMENT 'ДВП',
  `fasad` int(11) NOT NULL COMMENT 'Фасад',
  `zaveshky` int(11) NOT NULL COMMENT 'Завешки',
  `polkotrymach` int(11) NOT NULL COMMENT 'Полкотримачі',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Дамп даних таблиці `dc_tumba2`
-- 


-- --------------------------------------------------------

-- 
-- Структура таблиці `ob_order`
-- 

CREATE TABLE `ob_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `name` text NOT NULL COMMENT 'Назва замовлення',
  `evdate` date NOT NULL COMMENT 'Дата замовлення',
  `korpus_id` int(11) NOT NULL,
  `notice` text NOT NULL COMMENT 'Інформація про замовлення',
  `var_dekorat` int(11) NOT NULL COMMENT 'Вартість декоративних елементів',
  `var_sklo` int(11) NOT NULL COMMENT 'Вартість скла',
  `var_stiljnycja` int(11) NOT NULL COMMENT 'Вартість стільниця',
  `var_plintus` int(11) NOT NULL COMMENT 'Вартість плінтус',
  `var_cokolj` int(11) NOT NULL COMMENT 'Вартість цоколь',
  `var_operation` int(11) NOT NULL COMMENT 'Вартість операцій',
  `var_tumba` int(11) NOT NULL COMMENT 'Вартість тумб',
  `var_komplektacija` int(11) NOT NULL COMMENT 'Вартість скла',
  `total` int(11) NOT NULL COMMENT 'Загальна вартість',
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- 
-- Дамп даних таблиці `ob_order`
-- 

INSERT INTO `ob_order` VALUES (1, 'Olyvko', '2013-06-13', 5, 'Компютерний стіл', 160, 42504, 135, 45, 960, 3840, 154, 18, 95632, 1);
INSERT INTO `ob_order` VALUES (2, 'симферополь 15', '2013-06-16', 2, '', 0, 0, 0, 22, 0, 0, 274, 22, 636, 13);
INSERT INTO `ob_order` VALUES (3, 'ввв', '2013-01-01', 1, 'Неправильно введено ширина, довжина... потрібно вводити у метрах, отже наприклад у скло ширина введена  200, а повинно бути 2 тоді і ціна буде рахуватися правильно', 1940, 2400000, 0, 0, 0, 5000000, 44, 2620, 14809208, 1);
INSERT INTO `ob_order` VALUES (4, 'портал', '2013-08-05', 1, 'вишня патина', 40, 6000000, 0, 19, 0, 4000000, 956, 312, 20002654, 15);
INSERT INTO `ob_order` VALUES (5, '', '2013-01-01', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO `ob_order` VALUES (6, 'gdfh', '2013-01-01', 1, '', 0, 124, 0, 0, 0, 0, 538, 0, 1324, 15);
INSERT INTO `ob_order` VALUES (7, 'пані кухня 1', '2013-08-22', 1, '', 50, 0, 0, 31, 0, 0, 517, 2794, 6784, 13);
INSERT INTO `ob_order` VALUES (8, 'пані кухня 1', '2013-08-23', 1, '', 120, 8, 0, 28, 0, 4, 193, 1085, 2876, 18);
INSERT INTO `ob_order` VALUES (9, '', '2013-01-01', 42, '', 1340, 0, 0, 0, 0, 0, 45, 2694, 8158, 20);
INSERT INTO `ob_order` VALUES (10, 'пані кухня 2', '2013-08-23', 2, '', 50, 11, 0, 21, 0, 4, 92, 932, 2220, 13);
INSERT INTO `ob_order` VALUES (11, 'fdfdfdfddf', '2013-01-01', 8, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO `ob_order` VALUES (12, '3', '2013-08-27', 4, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 13);

-- --------------------------------------------------------

-- 
-- Структура таблиці `ob_person`
-- 

CREATE TABLE `ob_person` (
  `id` int(5) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `patronymic` varchar(15) NOT NULL,
  `sex` enum('чол','жін') NOT NULL,
  `dn` date NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `post_id` char(3) NOT NULL,
  `notice` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `ob_person`
-- 

INSERT INTO `ob_person` VALUES (1, 'Оливко', 'Роман', 'Миронович', 'чол', '1992-10-31', 'вул', '911', 'rom@kitchen.ua', '?', 'адміністратор)');
INSERT INTO `ob_person` VALUES (2, 'aaa', 'aaa', 'aaa', 'чол', '2013-04-08', 'aaa', '123', 'aaa111', '123', 'test');
INSERT INTO `ob_person` VALUES (3, '', 'bbb', '', '', '0000-00-00', '', '', 'bbb', '', '');
INSERT INTO `ob_person` VALUES (4, 'sad', 'Roman', '', '', '0000-00-00', 'Lviv', '09999', 'ol@mail.ru', '', '');
INSERT INTO `ob_person` VALUES (5, 'Ковалишин', 'Тарас', '', '', '0000-00-00', 'Львів', '0676707485', 'Kovaluchun@ukr.net', '', '');
INSERT INTO `ob_person` VALUES (6, 'Зимба', 'Дмитрий', '', '', '0000-00-00', 'Выговського,41', '+380975819402', 'zimba.dmitrij@gmail.com', '', '');
INSERT INTO `ob_person` VALUES (7, 'Ковалишин', 'Тарас', '', '', '0000-00-00', 'Львів', '067670748', 'Kovaluchun_n@ukr.net', '', '');
INSERT INTO `ob_person` VALUES (8, '234', '234', '', '', '0000-00-00', '234', '234', '234', '', '');
INSERT INTO `ob_person` VALUES (9, 'po', 'po', '', '', '0000-00-00', 'po', 'po', 'po', '', '');
INSERT INTO `ob_person` VALUES (10, '111', '111', '', '', '0000-00-00', 'Lviv', '122314', '1234', '', '');
INSERT INTO `ob_person` VALUES (11, '666', '666', '', '', '0000-00-00', 'рпаопро', '353465', 'пмрапорап', '', '');
INSERT INTO `ob_person` VALUES (12, 'fhfdh', 'gdf', '', '', '0000-00-00', 'rydfhh', '354', 'gxcvnvc', '', '');
INSERT INTO `ob_person` VALUES (13, 'wetwet', 'etwet', '', '', '0000-00-00', 'gfhgf', '43657', 'cbcxbcvxn', '', '');
INSERT INTO `ob_person` VALUES (14, 'Зімба', 'Максим', '', '', '0000-00-00', 'Чукарина, 6, кв.50', '1111222222', ',m.m,.,', '', '');
INSERT INTO `ob_person` VALUES (15, 'Литвин', 'Василь', '', '', '0000-00-00', 'ывпы', 'впчсми', 'счимст', '', '');
INSERT INTO `ob_person` VALUES (16, '55', '35235', '', '', '0000-00-00', '235325', '235532', '35352532', '', '');
INSERT INTO `ob_person` VALUES (18, '1111', '1111', '', '', '0000-00-00', '1111', '1111', 'dmitrij68@ukr.net', '', '');
INSERT INTO `ob_person` VALUES (20, 'Брик', 'Зоряна', '', '', '0000-00-00', 'пані кухня', '0673391689', 'pani_kuhnia@ukr.net', '', '');

-- --------------------------------------------------------

-- 
-- Структура таблиці `ob_user`
-- 

CREATE TABLE `ob_user` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `person_id` smallint(3) NOT NULL,
  `nick` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `evdate` date NOT NULL,
  `group_id` char(3) NOT NULL,
  `act` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `person_id` (`person_id`),
  UNIQUE KEY `nick` (`nick`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- 
-- Дамп даних таблиці `ob_user`
-- 

INSERT INTO `ob_user` VALUES (1, 1, 'adm', 'adm', '2013-04-06', '1', 1);
INSERT INTO `ob_user` VALUES (2, 2, 'aaa', 'aaa', '2013-04-08', '2', 1);
INSERT INTO `ob_user` VALUES (3, 3, 'bbb', 'bbb', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (4, 4, 'roman', 'roman', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (5, 5, 'Тарас', '123', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (6, 6, 'zimba.dmitrij@g', 'lena1978', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (7, 7, 'klj', 'klj', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (8, 8, '234', '234', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (9, 9, 'po', 'po', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (10, 10, '111', '111', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (11, 11, '666', '666', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (12, 12, '777', '777', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (13, 13, 'dmitrij', 'lena1978', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (14, 14, 'maxim', 'olena1978', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (15, 15, 'vasyl', 'vasyl', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (16, 16, '23432', '3244', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (17, 17, '1111', '1111', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (18, 18, '11112222', '11112222', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (19, 19, 'zoryana', 'zoryana', '0000-00-00', '2', 1);
INSERT INTO `ob_user` VALUES (20, 20, 'zoryana2', 'zoryana', '0000-00-00', '2', 1);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_cokolj`
-- 

CREATE TABLE `rf_cokolj` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_cokolj`
-- 

INSERT INTO `rf_cokolj` VALUES (1, 'пластиковий-150', 20);
INSERT INTO `rf_cokolj` VALUES (2, 'пластиковий-100', 30);
INSERT INTO `rf_cokolj` VALUES (3, 'ДСП+ущільнювач прозорий-100', 20);
INSERT INTO `rf_cokolj` VALUES (4, 'ДСП+ущільнювач прозорий-150', 10);
INSERT INTO `rf_cokolj` VALUES (5, 'пластиковий-80', 20);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_dekorat`
-- 

CREATE TABLE `rf_dekorat` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_dekorat`
-- 

INSERT INTO `rf_dekorat` VALUES (4, 'балюстрада пряма', 50);
INSERT INTO `rf_dekorat` VALUES (5, 'балюстрада гнута', 70);
INSERT INTO `rf_dekorat` VALUES (6, 'карниз прямий верхній', 100);
INSERT INTO `rf_dekorat` VALUES (7, 'карниз прямий нижній', 55);
INSERT INTO `rf_dekorat` VALUES (8, 'карниз гнутий вверх', 65);
INSERT INTO `rf_dekorat` VALUES (9, 'карниз гнутий вниз', 70);
INSERT INTO `rf_dekorat` VALUES (10, 'пілястра', 600);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_dovzyna`
-- 

CREATE TABLE `rf_dovzyna` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_dovzyna`
-- 

INSERT INTO `rf_dovzyna` VALUES (601);
INSERT INTO `rf_dovzyna` VALUES (718);
INSERT INTO `rf_dovzyna` VALUES (915);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_fasad`
-- 

CREATE TABLE `rf_fasad` (
  `id` varchar(10) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_fasad`
-- 

INSERT INTO `rf_fasad` VALUES ('2', 'Шпоновані');
INSERT INTO `rf_fasad` VALUES ('3', 'Мдф фарбовані');
INSERT INTO `rf_fasad` VALUES ('4', 'Дерево');
INSERT INTO `rf_fasad` VALUES ('5', 'Мдф рамкові');
INSERT INTO `rf_fasad` VALUES ('6', 'Скло+алюміній');
INSERT INTO `rf_fasad` VALUES ('7', 'Акрил');
INSERT INTO `rf_fasad` VALUES ('8', 'Постформінг');
INSERT INTO `rf_fasad` VALUES ('mdf_pliv', 'Мдф плівкові');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_firma`
-- 

CREATE TABLE `rf_firma` (
  `id` varchar(5) NOT NULL,
  `name` text NOT NULL,
  `ukr_id` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_firma`
-- 

INSERT INTO `rf_firma` VALUES ('BRW', 'BRW', 'Pol');
INSERT INTO `rf_firma` VALUES ('DRE', 'DREWPOL', 'Pol');
INSERT INTO `rf_firma` VALUES ('Edi', 'Едісан', 'Ukr');
INSERT INTO `rf_firma` VALUES ('FRO', 'FRONTRES', 'Pol');
INSERT INTO `rf_firma` VALUES ('Ilc', 'Ilcam', 'Ita');
INSERT INTO `rf_firma` VALUES ('MDF', 'МДФ Дизайн', 'Ukr');
INSERT INTO `rf_firma` VALUES ('Mob', 'Mobiclan', 'Ita');
INSERT INTO `rf_firma` VALUES ('OLD', 'Oldiz', 'Ukr');
INSERT INTO `rf_firma` VALUES ('PRO', 'PROFORM', 'Pol');
INSERT INTO `rf_firma` VALUES ('Sti', 'Stival', 'Ita');
INSERT INTO `rf_firma` VALUES ('STO', 'STOLZEN', 'Pol');
INSERT INTO `rf_firma` VALUES ('ZOV', 'ЗОВ', 'Bil');
INSERT INTO `rf_firma` VALUES ('СММ', 'СММ', 'Pol');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_firma_komplektacija`
-- 

CREATE TABLE `rf_firma_komplektacija` (
  `id` varchar(7) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_firma_komplektacija`
-- 

INSERT INTO `rf_firma_komplektacija` VALUES ('1', 'Hafele');
INSERT INTO `rf_firma_komplektacija` VALUES ('10', 'Ручки GUISTI');
INSERT INTO `rf_firma_komplektacija` VALUES ('11', 'Ручки FALSO класика');
INSERT INTO `rf_firma_komplektacija` VALUES ('12', 'Ручки FALSO модерн');
INSERT INTO `rf_firma_komplektacija` VALUES ('13', 'Інше');
INSERT INTO `rf_firma_komplektacija` VALUES ('2', 'KESSKESSEBOHMER');
INSERT INTO `rf_firma_komplektacija` VALUES ('3', 'HAFELE світло');
INSERT INTO `rf_firma_komplektacija` VALUES ('4', 'BLUM');
INSERT INTO `rf_firma_komplektacija` VALUES ('5', 'STARAX');
INSERT INTO `rf_firma_komplektacija` VALUES ('6', 'VIBO');
INSERT INTO `rf_firma_komplektacija` VALUES ('7', 'Плінтус,цоколь,торцовка стільн');
INSERT INTO `rf_firma_komplektacija` VALUES ('8', 'Сушки для посуду');
INSERT INTO `rf_firma_komplektacija` VALUES ('9', 'Ручки меблеві');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_firma_plintus`
-- 

CREATE TABLE `rf_firma_plintus` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_firma_plintus`
-- 

INSERT INTO `rf_firma_plintus` VALUES (1, 'Rauwalon');
INSERT INTO `rf_firma_plintus` VALUES (2, 'Scilm');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_frez`
-- 

CREATE TABLE `rf_frez` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_frez`
-- 

INSERT INTO `rf_frez` VALUES (1, 'Меделко');
INSERT INTO `rf_frez` VALUES (2, 'Декоративне');
INSERT INTO `rf_frez` VALUES (3, 'Стандартне');
INSERT INTO `rf_frez` VALUES (4, 'Повноколірний меделко');
INSERT INTO `rf_frez` VALUES (5, 'Повноколірний фрез');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_komplektacija`
-- 

CREATE TABLE `rf_komplektacija` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `firma_name` varchar(5) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_komplektacija`
-- 

INSERT INTO `rf_komplektacija` VALUES (1, 'METALLA A петля 110 ° накладна сталева нікельована \r\n55.5х13.1мм шаблон свердління: 48/6 глибина: 12мм + Монтажна планка METALLA  А хрестова, \r\nпід шуруп, 0 мм, 37/32, сталева, нікельована', '1', 3);
INSERT INTO `rf_komplektacija` VALUES (2, 'METALLA A петля 45° сталь нікельована шаблон: 48/6 під шуруп + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', 5);
INSERT INTO `rf_komplektacija` VALUES (3, 'METALLA A петля 30° сталь нікельована шаблон: 48/6 під шуруп + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', 3);
INSERT INTO `rf_komplektacija` VALUES (4, 'METALLA A петля 180° рівнолегла + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', 5);
INSERT INTO `rf_komplektacija` VALUES (5, 'METALLA A Петля 165 ° (крокодил) + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', 12);
INSERT INTO `rf_komplektacija` VALUES (6, 'METALLAMAT A петля 60° (допоміжна до крокодилу) + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', 23);
INSERT INTO `rf_komplektacija` VALUES (7, 'METALLA D SM петля  110° з дотягом + Монтажна планка METALLA SM хрестова 0мм сталева оцинкована під шуруп з регулюванням', '1', 14);
INSERT INTO `rf_komplektacija` VALUES (8, 'SALICE SILENTIA петля 180° (рівнолегла) з дотягом + Планка монтажна DUOMATIC SM хрестовидна 3 мм \r\nцамак нікельована під шуруп', '1', 38);
INSERT INTO `rf_komplektacija` VALUES (9, 'SILENTIA Петля 45° з дотягом + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', 32);
INSERT INTO `rf_komplektacija` VALUES (10, 'Петля SALICE SILENTIA 700 30° сталь нікельована шаблон: 45/9.5 під шуруп + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', 32);
INSERT INTO `rf_komplektacija` VALUES (11, 'SILENTIA Петля 155° (крокодил) з дотягом + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', 57);
INSERT INTO `rf_komplektacija` VALUES (12, 'PUSH DUOMATIC петля 110° накладна сталь нікельована шаблон: 45/9.5мм під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', 32);
INSERT INTO `rf_komplektacija` VALUES (13, 'PUSH DUOMATIC петля 110° внутрішня сталева нікельована шаблон свердління 45/9.5 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', 52);
INSERT INTO `rf_komplektacija` VALUES (14, 'PUSH DUOMATIC петля  94° пряма сталева нікельована шаблон свердління 48/6 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 3 мм цамак нікельована під шуруп', '1', 67);
INSERT INTO `rf_komplektacija` VALUES (15, 'PUSH DUOMATIC петля  165° накладна шаблон свердління 48/6 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', 67);
INSERT INTO `rf_komplektacija` VALUES (16, 'PUSH Магнітний тримач для петель Push колір \r\nсірий довжина 40мм зі штирем', '1', 25);
INSERT INTO `rf_komplektacija` VALUES (17, 'PUSH Адаптер для магнітної защіпки з позиціонуванням пластиковий бежевий', '1', 5);
INSERT INTO `rf_komplektacija` VALUES (18, ' FREE 3677 Повний комплект кут розкриття 100° модель В, С, D, E', '1', 320);
INSERT INTO `rf_komplektacija` VALUES (19, ' FREE 3677 Повний комплект кут розкриття 100° модель F', '1', 422);
INSERT INTO `rf_komplektacija` VALUES (20, ' FREE 3677 Кріплення до фасаду з алюмінієвими рамами 20мм товщина бокової стінки 19мм цамак хромоване матове', '1', 105);
INSERT INTO `rf_komplektacija` VALUES (21, 'STRATO SM 3685 Повний комплект висота фасаду: від 342-420мм вага фасаду: 4-7.2кг до 500-550мм вага фасаду: 3.8-10кг', '1', 850);
INSERT INTO `rf_komplektacija` VALUES (22, 'STRATO SM 3685 Повний комплект висота фасаду: від 500-550мм вага фасаду: 6-15кг до 550-650 мм вага фасаду: 6-14,5 кг', '1', 920);
INSERT INTO `rf_komplektacija` VALUES (23, 'STRATO Кріплення до фасадів з алюмінієвими рамами 20мм товщина бокової стінки 19мм сталь хромована матова', '1', 130);
INSERT INTO `rf_komplektacija` VALUES (24, 'Ewiva Ком.підй.фур. для лиц.пан. дер.з ал.проф.зручк.шир.до 600мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+аморт+з''єдн.для лиц.пан+синхр.штанга (0015600050)', '1', 733);
INSERT INTO `rf_komplektacija` VALUES (25, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев..шир.до 900мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (2 шт.) (0015570050)', '1', 579);
INSERT INTO `rf_komplektacija` VALUES (26, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев.. шир.до 1200мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (0015580050 (2 шт.)', '1', 591);
INSERT INTO `rf_komplektacija` VALUES (27, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев.. шир.до 1500мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (0015650050 (2 шт.)', '1', 604);
INSERT INTO `rf_komplektacija` VALUES (28, 'Ewiva Набір амортизаторів (2 пар лів.х прав.) до підйомної фурнітури EWIVA колір сірий вага панелі 2.5-4.8 кг з ручкою (0015627931)', '1', 141);
INSERT INTO `rf_komplektacija` VALUES (29, 'Ewiva Набір амортизаторів (пар лів.х прав.)  до підйомної фурнітури колір сірий вага панелі 3.3-6.6 кг з ручкою (0015637931)', '1', 149);
INSERT INTO `rf_komplektacija` VALUES (30, 'Набір амортизаторів (2 пар лів.х прав.) до підйомної фурнітури EWIVA колір сірий вага панелі 5.3-8.0 кг з ручкою (0015647931)', '1', 145);
INSERT INTO `rf_komplektacija` VALUES (31, 'Ewiva Опора для алюмінієвого профілю лиц.панелі підйомного механізму (2 пари) (0015450050)', '1', 131);
INSERT INTO `rf_komplektacija` VALUES (32, 'SENSO 3674 повний комплект висота фасаду: 540-580мм вага фасаду: 3-6.5 кг', '1', 550);
INSERT INTO `rf_komplektacija` VALUES (33, 'VERSO SM 3684 Повний комплект модель А,B,C,D', '1', 690);
INSERT INTO `rf_komplektacija` VALUES (34, 'WOODPRO Направляючі прихованого монтажу , 450мм, част. витяг. сталь оцинк., м’яке закриття, пласт. кріплення,  товщина боковини 16-19мм', '1', 75);
INSERT INTO `rf_komplektacija` VALUES (35, 'WOODPRO Направляючі прихованого монтажу , 450мм, повний витяг. сталь оцинк., м’яке закриття, пласт. кріплення,  товщина боковини 16-19мм', '1', 125);
INSERT INTO `rf_komplektacija` VALUES (36, ' PUSH, Направляючі ролик. прихов. монтажу 460мм  повний. витяг, навантаж. до 25 кг  частков. висув.сталь оцинков.', '1', 152);
INSERT INTO `rf_komplektacija` VALUES (37, 'PUSH, Направляючі кульк. прихов. монтажу 452мм  част. витяг навантаж. до 25 кг  частков. висув.сталь оцинков.', '1', 80);
INSERT INTO `rf_komplektacija` VALUES (38, 'Метал. бокс 450 мм, Н=54 мм, сталь, білий', '1', 40);
INSERT INTO `rf_komplektacija` VALUES (39, 'Метал. бокс 450 мм, Н=86 мм, сталь, білий', '1', 45);
INSERT INTO `rf_komplektacija` VALUES (40, 'Метал.бокс 450 мм, Н=150 мм, сталь, білий', '1', 60);
INSERT INTO `rf_komplektacija` VALUES (41, 'Метал.бокс система поздовжньої огорожі 450 мм, ст., білий', '1', 21);
INSERT INTO `rf_komplektacija` VALUES (42, 'Метал бокс Демпфер пластиковий сірий для метал.бокс', '1', 15);
INSERT INTO `rf_komplektacija` VALUES (43, 'MOOVIT Система для висувних ящиків з доводчиком сіра 92/450мм 50кг', '1', 235);
INSERT INTO `rf_komplektacija` VALUES (44, 'MOOVIT Система для висувних ящиків  з доводчиком біла 92/450мм 50кг', '1', 235);
INSERT INTO `rf_komplektacija` VALUES (45, 'MOOVIT Релінги для MOOVIT сталеві сірі/білі 450мм', '1', 40);
INSERT INTO `rf_komplektacija` VALUES (46, 'MOOVIT Фасад для внутрішнього висувного ящика алюмінієвий сірий/білий 600мм, 900мм, 1200мм + тримач фасаду + накладка для стабілізації', '1', 225);
INSERT INTO `rf_komplektacija` VALUES (47, 'Classic Карго 150мм 2-рівневе 90° з доводжувачем, сталь. хром.', '2', 860);
INSERT INTO `rf_komplektacija` VALUES (48, ' STYLE Карго 150мм 2-рівневе 90° з доводжувачем, сталь. хром.', '2', 950);
INSERT INTO `rf_komplektacija` VALUES (49, ' CLASSIC Карго для рушників 150мм  90° з доводжувачем, сталь. хром.', '2', 950);
INSERT INTO `rf_komplektacija` VALUES (50, 'STYLE Карго для рушників 150мм 90° з доводжувачем, сталь. хром.', '2', 990);
INSERT INTO `rf_komplektacija` VALUES (51, ' CLASSIC Карго для піддону для випічки, 150мм 90° з доводжувачем, сталь. хром.', '2', 930);
INSERT INTO `rf_komplektacija` VALUES (52, 'STYLE Карго для піддону для випічки, 150мм 90° з доводжувачем, сталь. хром.', '2', 980);
INSERT INTO `rf_komplektacija` VALUES (53, 'CLASSIC Розділювач до висувної корзини 150мм сталь хром.', '2', 30);
INSERT INTO `rf_komplektacija` VALUES (54, 'Розділювач STYLE до висувної корзини 150мм сталь хром.', '2', 35);
INSERT INTO `rf_komplektacija` VALUES (55, 'Комплект дротяних висувних полиць 400мм\r\n', '2', 750);
INSERT INTO `rf_komplektacija` VALUES (56, 'Комплект дротяних висувних полиць 500мм', '2', 780);
INSERT INTO `rf_komplektacija` VALUES (57, 'Комплект дротяних висувних полиць 600мм', '2', 800);
INSERT INTO `rf_komplektacija` VALUES (58, 'Comfort 30 Повний комплект карго, праве/ліве, з трьома дротяними корзинами на 110 мм ', '2', 1150);
INSERT INTO `rf_komplektacija` VALUES (59, 'Comfort 30 Повний комплект карго, ліве/праве, з двома дротяними корзинами на 210 мм ', '2', 1050);
INSERT INTO `rf_komplektacija` VALUES (60, 'Comfort 30 Повний комплект карго Comfort 30, ліве/праве, з трьома дротяними корзинами на 210 мм ', '2', 1220);
INSERT INTO `rf_komplektacija` VALUES (61, 'Comfort 30 Повний комплект карго, ліве/праве, з двома  корзинами Arena Classic на 228 мм ', '2', 1617);
INSERT INTO `rf_komplektacija` VALUES (62, ' PORTERO Висувний механізм з доводжувачем хром.без покр/срібл.Н=400мм з 1 нав. корз.161x495x395мм\r\n', '2', 990);
INSERT INTO `rf_komplektacija` VALUES (63, 'PORTERO Висувний механізм з доводжувачем, хром.без покр/срібл.Н=400мм з 2 нав.корз.без руч274x495x395мм\r\n', '2', 1360);
INSERT INTO `rf_komplektacija` VALUES (64, 'PORTERO Висувний механізм з доводжувачем, хром.без покр/срібл.Н=511мм з 3 нав.корз\r\n', '2', 1954);
INSERT INTO `rf_komplektacija` VALUES (65, 'TOPFLEX Фурнітура для висувного столу  довж. висування 810 мм глиб.внутр.ящика 500мм навантаження до 30кг', '2', 1235);
INSERT INTO `rf_komplektacija` VALUES (66, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 450 мм, правий на 2 полиці', '2', 2300);
INSERT INTO `rf_komplektacija` VALUES (67, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 600 мм, правий на 2 полиці', '2', 3315);
INSERT INTO `rf_komplektacija` VALUES (68, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 450 мм, правий на 4 полиці', '2', 6025);
INSERT INTO `rf_komplektacija` VALUES (69, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 600 мм, правий на 4 полиці', '2', 6690);
INSERT INTO `rf_komplektacija` VALUES (70, 'Magic Corner Повний комплект висувний поворотний для кутових шаф\r\n(поворотно висувний блок та набір корзин 2+2), серія Arena\r\nCLASSIC, правий/лівий', '2', 4855);
INSERT INTO `rf_komplektacija` VALUES (71, 'Magic Corner Повний комплект висувний поворотний для кутових шаф\r\n(поворотно висувний блок та набір корзин 2+2), серія Arena\r\nSTYLE, правий/лівий', '2', 4980);
INSERT INTO `rf_komplektacija` VALUES (72, 'Комплект поворот. фурнітури 900х900 мм\r\nна 3/4 кола D820 мм, з двома полицями, дротяні полиці', '2', 1890);
INSERT INTO `rf_komplektacija` VALUES (73, 'Комплект поворот. фурнітури 900х900 мм\r\nна 3/4 кола D820 мм, з двома полицями, серія Arena CLASSIC', '2', 2145);
INSERT INTO `rf_komplektacija` VALUES (74, 'Dispensa Повний комплект шафи 300 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма корзинами Arena Classic 250x462x106 мм', '2', 3890);
INSERT INTO `rf_komplektacija` VALUES (75, ' Dispensa Dispensa Повний комплект шафи 300 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма дротяними корзинами 250x467x110 мм', '2', 3370);
INSERT INTO `rf_komplektacija` VALUES (76, ' Dispensa Повний комплект шафи 400 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма корзинами Arena Classic 250x462x106 мм', '2', 4275);
INSERT INTO `rf_komplektacija` VALUES (77, ' Dispensa Повний комплект шафи 400 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма дротяними корзинами 250x467x110 мм', '2', 3655);
INSERT INTO `rf_komplektacija` VALUES (78, ' Dispensa Набір ємкостей  для кутової шафи по 500мл 4 шт., прозорий, сірі кришки ', '2', 375);
INSERT INTO `rf_komplektacija` VALUES (79, ' Dispensa Набір лотків  2 ємкості для кутової шафи, пластм..,тримач алюм. ', '2', 250);
INSERT INTO `rf_komplektacija` VALUES (80, 'Tandem із SoftStopp Повний комплект для ширини шафи 600 мм висота 1700 мм колір рами алюміній (рама, 6 поличок), Arena CLASSIC', '2', 6475);
INSERT INTO `rf_komplektacija` VALUES (81, 'Tandem із SoftStopp Повний комплект TANDEM Arena Classic із SoftStopp для ширини шафи 600 мм  висота 1100 мм колір рами алюміній ', '2', 5160);
INSERT INTO `rf_komplektacija` VALUES (82, 'Tandem для ванни 570 мм три яруси', '2', 1405);
INSERT INTO `rf_komplektacija` VALUES (83, 'Tandem для ванни 360 мм два яруси', '2', 1125);
INSERT INTO `rf_komplektacija` VALUES (84, 'Світильник LED 2018 срібно-сірий 12V/2W тепле біле світло бокове кріплення', '3', 285);
INSERT INTO `rf_komplektacija` VALUES (85, 'Світильник LED 2018 срібно-сірий 12V/2W тепле біле світло нижнє кріплення', '3', 285);
INSERT INTO `rf_komplektacija` VALUES (86, 'Світильник LED 2001 колір: срібний 12V/1.7W холодне біле світло', '3', 122);
INSERT INTO `rf_komplektacija` VALUES (87, 'Світильник LED 2001 колір: білий 12V/1.7W холодне біле світло', '3', 122);
INSERT INTO `rf_komplektacija` VALUES (88, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W холодне біле світло', '3', 105);
INSERT INTO `rf_komplektacija` VALUES (89, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W тепле біле світло', '3', 105);
INSERT INTO `rf_komplektacija` VALUES (90, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W голубе світло', '3', 105);
INSERT INTO `rf_komplektacija` VALUES (91, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W холодне біле світло 575мм', '3', 190);
INSERT INTO `rf_komplektacija` VALUES (92, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W холодне біле світло 820мм', '3', 295);
INSERT INTO `rf_komplektacija` VALUES (93, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W тепле біле світло 575мм', '3', 190);
INSERT INTO `rf_komplektacija` VALUES (94, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W тепле біле світло 820мм', '3', 295);
INSERT INTO `rf_komplektacija` VALUES (95, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W тепле біле світло 575мм', '3', 190);
INSERT INTO `rf_komplektacija` VALUES (96, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W тепле біле світло 820мм', '3', 295);
INSERT INTO `rf_komplektacija` VALUES (97, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W холодне біле світло 575мм', '3', 190);
INSERT INTO `rf_komplektacija` VALUES (98, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W холодне біле світло 820мм', '3', 310);
INSERT INTO `rf_komplektacija` VALUES (99, 'Полиця скляна з LED 2006 12V/2-4W RGB 70х175х600мм', '3', 620);
INSERT INTO `rf_komplektacija` VALUES (100, 'Полиця скляна з LED 2006 12V/2-6W RGB 70x175x900мм', '3', 800);
INSERT INTO `rf_komplektacija` VALUES (101, 'Полиця скляна з LED 2006 12V/4W голубе світло 70х175х600мм', '3', 480);
INSERT INTO `rf_komplektacija` VALUES (102, 'Полиця скляна з LED 2006 12V/5W голубе світло 70х175х900мм', '3', 640);
INSERT INTO `rf_komplektacija` VALUES (103, 'Полиця скляна з LED 2006 12V/4W холодне біле світло 70х175х600мм', '3', 455);
INSERT INTO `rf_komplektacija` VALUES (104, 'Полиця скляна з LED 2006 12V/5W холодне біле світло 70х175х900мм', '3', 610);
INSERT INTO `rf_komplektacija` VALUES (105, 'Профіль LED 2007 12V/2.5W алюміній колір: срібний тепле біле світло 12х15х563мм', '3', 180);
INSERT INTO `rf_komplektacija` VALUES (106, 'Профіль LED 2007 12V/4W алюміній колір: срібний тепле біле світло 12х15х863мм', '3', 257);
INSERT INTO `rf_komplektacija` VALUES (107, 'Профіль LED 2007 12V/2.5W алюміній колір: срібний голубе світло 12х15х563мм', '3', 180);
INSERT INTO `rf_komplektacija` VALUES (108, 'Профіль LED 2007 12V/4W алюміній колір: срібний голубе світло 12х15х863мм', '3', 270);
INSERT INTO `rf_komplektacija` VALUES (109, 'Світильник LED 2003 з сенсорним вимикачем 12V/3.2W алюміній колір: срібний холодне біле світло 20х30х600мм', '3', 356);
INSERT INTO `rf_komplektacija` VALUES (110, 'Світильник LED 2003 з сенсорним вимикачем 12V/3.2W алюміній колір: срібний холодне біле світло 20х30х900мм', '3', 490);
INSERT INTO `rf_komplektacija` VALUES (111, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/1.6W холодне біле світло 405мм', '3', 300);
INSERT INTO `rf_komplektacija` VALUES (112, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/2.4W холодне біле світло 555мм', '3', 370);
INSERT INTO `rf_komplektacija` VALUES (113, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/3.2W холодне біле світло 755мм', '3', 425);
INSERT INTO `rf_komplektacija` VALUES (114, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/3.2W холодне біле світло 855мм', '3', 440);
INSERT INTO `rf_komplektacija` VALUES (115, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/4W холодне біле світло 955мм', '3', 490);
INSERT INTO `rf_komplektacija` VALUES (116, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/4.8W холодне біле світло 1155мм', '3', 565);
INSERT INTO `rf_komplektacija` VALUES (117, 'Світильник LED 2009 нижній 12V/1.2W RGB нержавіюча сталь матова D58мм', '3', 135);
INSERT INTO `rf_komplektacija` VALUES (118, 'Світильник LED 2010 нижній 12V/1.7W RGB нержавіюча сталь матова 52х52мм', '3', 170);
INSERT INTO `rf_komplektacija` VALUES (119, 'Стрічка LED 2011 пластикова біла 12V/0.8W холодне біле світло 300мм', '3', 90);
INSERT INTO `rf_komplektacija` VALUES (120, 'Стрічка LED 2011 пластикова біла 12V/5W холодне біле світло 2000мм', '3', 500);
INSERT INTO `rf_komplektacija` VALUES (121, 'Стрічка LED 2011 пластикова біла 12V/0.8W тепле біле світло 300мм', '3', 90);
INSERT INTO `rf_komplektacija` VALUES (122, 'Стрічка LED 2011 пластикова біла 12V/5W тепле біле світло 2000мм', '3', 500);
INSERT INTO `rf_komplektacija` VALUES (123, 'З''єднувач Т-подібний LED 2011 12V/0.2W холодне біле світло 75мм', '3', 23);
INSERT INTO `rf_komplektacija` VALUES (124, 'З''єднувач Т-подібний LED 2011 12V/0.2W тепле біле світло 75мм', '3', 23);
INSERT INTO `rf_komplektacija` VALUES (125, 'Кабель LED стрічка/блок живлення білий 2500мм', '3', 23);
INSERT INTO `rf_komplektacija` VALUES (126, 'Кабель LED для кутових з''єднань білий 50мм', '3', 6);
INSERT INTO `rf_komplektacija` VALUES (127, 'Стрічка LED 2012 пластикова біла 12V/2W RGB 300мм', '3', 171);
INSERT INTO `rf_komplektacija` VALUES (128, 'Стрічка LED 2012 пластикова біла 12V/12W RGB 2000мм', '3', 1141);
INSERT INTO `rf_komplektacija` VALUES (129, 'Мікшер RGB з пультом дистанційного управління 12V/30W пластиковий білий', '3', 462);
INSERT INTO `rf_komplektacija` VALUES (130, 'Кабель LED стрічка/мікшер RGB білий 2500мм', '3', 34);
INSERT INTO `rf_komplektacija` VALUES (131, 'Кабель LED для кутових з''єднань RGB білий 50мм', '3', 19);
INSERT INTO `rf_komplektacija` VALUES (132, 'Стрічка LED 2013 пластикова біла 12V/24W тепле біле світло 5000мм (300)', '3', 703);
INSERT INTO `rf_komplektacija` VALUES (133, 'Стрічка LED 2013 пластикова біла 12V/24W холодне біле світло 5000мм (300)', '3', 703);
INSERT INTO `rf_komplektacija` VALUES (134, 'Стрічка LED 2013 пластикова біла 12V/24W голубе світло 5000мм (300)', '3', 703);
INSERT INTO `rf_komplektacija` VALUES (135, 'Стрічка LED 2013 пластикова біла 12V/24W червоне світло 5000мм (300)', '3', 660);
INSERT INTO `rf_komplektacija` VALUES (136, 'Розподілювач RGB на 9 виходів пластиковий білий', '3', 28);
INSERT INTO `rf_komplektacija` VALUES (137, 'Кабель LED стрічка/блок живлення 2000мм', '3', 15);
INSERT INTO `rf_komplektacija` VALUES (138, 'Кабель LED стрічка/стрічка 500мм', '3', 15);
INSERT INTO `rf_komplektacija` VALUES (139, 'Роз''єм для стрічок LED', '3', 14);
INSERT INTO `rf_komplektacija` VALUES (140, 'Стрічка LED 2014 пластикова біла 12V/24W RGB 5000мм (300)', '3', 703);
INSERT INTO `rf_komplektacija` VALUES (141, 'Стрічка LED 2015 пластикова біла 12V/36W тепле біле світло 5000мм (150)', '3', 970);
INSERT INTO `rf_komplektacija` VALUES (142, 'Стрічка LED 2015 пластикова біла 12V/36W холодне біле світло 5000мм (150)', '3', 970);
INSERT INTO `rf_komplektacija` VALUES (143, 'Стрічка LED 2015 пластикова біла 12V/36W голубе світло 5000мм (150)', '3', 1014);
INSERT INTO `rf_komplektacija` VALUES (144, 'Стрічка LED 2015 пластикова біла 12V/36W червоне світло 5000мм (150)', '3', 1014);
INSERT INTO `rf_komplektacija` VALUES (145, 'Стрічка LED 2016 пластикова біла 12V/36W RGB 5000мм (150)', '3', 1012);
INSERT INTO `rf_komplektacija` VALUES (146, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 8х17х2500мм нижнє кріплення', '3', 138);
INSERT INTO `rf_komplektacija` VALUES (147, 'Заглушка пластикова колір: срібний (до 833.74.729)', '3', 8);
INSERT INTO `rf_komplektacija` VALUES (148, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 8х17х2500мм врізний', '3', 150);
INSERT INTO `rf_komplektacija` VALUES (149, 'Заглушка пластикова колір: срібний (до 833.72.705)', '3', 6);
INSERT INTO `rf_komplektacija` VALUES (150, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 13х18х2500мм кутовий', '3', 173);
INSERT INTO `rf_komplektacija` VALUES (151, 'Заглушка пластикова колір: срібний (до 833.72.715)', '3', 10);
INSERT INTO `rf_komplektacija` VALUES (152, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 26х17х2500мм врізний', '3', 202);
INSERT INTO `rf_komplektacija` VALUES (153, 'Заглушка пластикова колір: срібний (до 833.72.725)', '3', 11);
INSERT INTO `rf_komplektacija` VALUES (154, 'Штанга гардеробна для стрічок LED 2013/2015 алюміній колір: срібний 2500мм', '3', 210);
INSERT INTO `rf_komplektacija` VALUES (155, 'Штанготримач для кріплення до бокової стінки, цинк', '3', 1);
INSERT INTO `rf_komplektacija` VALUES (156, 'Середній кронштейн д/шафної штанги 85-105 мм, хром', '3', 30);
INSERT INTO `rf_komplektacija` VALUES (157, 'Комплект стрічки LED2017 12V/2.16W холодне біле світло 300мм (3 частини)', '3', 15);
INSERT INTO `rf_komplektacija` VALUES (158, 'Комплект стрічки LED2017 12V/2.16W тепле біле світло 300мм (3 частини)', '3', 160);
INSERT INTO `rf_komplektacija` VALUES (159, 'Розгалужувач на 3 світильники для LED 12V 34х45мм', '3', 34);
INSERT INTO `rf_komplektacija` VALUES (160, 'Блок живлення для LED 12V/15W пластиковий чорний на 6 виходів 20х45х155мм', '3', 146);
INSERT INTO `rf_komplektacija` VALUES (161, 'Блок живлення для LED 12V/30W пластиковий чорний на 6 виходів 28х50х165мм', '3', 189);
INSERT INTO `rf_komplektacija` VALUES (162, 'Шнур з вилкою EU 250V 2000мм', '3', 21);
INSERT INTO `rf_komplektacija` VALUES (163, 'DP 192', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (164, 'DP 193', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (165, 'DP 195', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (166, 'DP 197', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (167, 'DG 191', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (168, 'DG 192', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (169, 'DG 193', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (170, 'DG 195', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (171, 'DG 197', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (172, 'Г-7125', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (173, 'UG1705', '9', 16);
INSERT INTO `rf_komplektacija` VALUES (174, 'UR0705 бронза', '9', 13);
INSERT INTO `rf_komplektacija` VALUES (175, 'UR2005', '9', 8);
INSERT INTO `rf_komplektacija` VALUES (176, 'UR1005', '9', 5);
INSERT INTO `rf_komplektacija` VALUES (177, 'UG3205', '9', 6);
INSERT INTO `rf_komplektacija` VALUES (178, 'UN8803 сатин', '9', 7);
INSERT INTO `rf_komplektacija` VALUES (180, 'UN8303 сатин, золото', '9', 8);
INSERT INTO `rf_komplektacija` VALUES (182, 'Hafele 121.XX.10 ', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (183, 'Hafele 125.XX.1  ', '9', 6);
INSERT INTO `rf_komplektacija` VALUES (184, 'UA-326 врізна (алюм+хром)', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (185, 'UA-326 врізна (хром)', '9', 25);
INSERT INTO `rf_komplektacija` VALUES (186, 'Світильник LED 3001 врізний алюмінієвий колір: срібний 24V/1.7W холодне біле світло D65мм', '3', 76);
INSERT INTO `rf_komplektacija` VALUES (187, 'Світильник LED 3001 врізний алюмінієвий колір: срібний 24V/1.7W тепле біле світло D65мм', '3', 76);
INSERT INTO `rf_komplektacija` VALUES (188, 'Монтажне кільце сталеве колір: срібний D65мм (до 833.75.000/010)', '3', 18);
INSERT INTO `rf_komplektacija` VALUES (189, 'Світильник LED 3002 врізний алюмінієвий колір: срібний 24V/4.4W холодне біле світло D127мм', '3', 393);
INSERT INTO `rf_komplektacija` VALUES (190, 'Світильник LED 3002 врізний алюмінієвий колір: срібний 24V/4.4W тепле біле світло D127мм', '3', 411);
INSERT INTO `rf_komplektacija` VALUES (191, 'Світильник LED 3004 пластиковий нікельований матовий 24V/0.8W тепле біле світло D70мм', '3', 92);
INSERT INTO `rf_komplektacija` VALUES (192, 'Світильник LED 3005 пластиковий нікельований матовий 24V/0.8W тепле біле світло D58мм', '3', 90);
INSERT INTO `rf_komplektacija` VALUES (193, 'Комплект світильників LED 3003 24V/1.65W пластиковий колір: срібний тепле біле світло (3 штуки)', '3', 690);
INSERT INTO `rf_komplektacija` VALUES (194, 'Комплект світильників LED3006 24V/5.4W рухомих алюміній колір: срібний тепле біле світло 900мм (3 штуки)', '3', 592);
INSERT INTO `rf_komplektacija` VALUES (195, 'Світильник LED 3006 24V/1.8W тепле біле світло 55х55х15мм (до 833.76.040)', '3', 95);
INSERT INTO `rf_komplektacija` VALUES (196, 'Розгалужувач на 3 світильники для LED 24V 34х45мм', '3', 34);
INSERT INTO `rf_komplektacija` VALUES (197, 'Блок живлення для LED 24V/15W на 6 виходів пластиковий чорний 20х45х155мм', '3', 147);
INSERT INTO `rf_komplektacija` VALUES (198, 'Блок живлення для LED 24V/30W на 6 виходів пластиковий чорний 28х50х165мм', '3', 190);
INSERT INTO `rf_komplektacija` VALUES (199, 'Шнур з вилкою EU 250V 2000мм', '3', 21);
INSERT INTO `rf_komplektacija` VALUES (200, 'Світильник LED 4009 350mA/3W врізний алюміній нікельований матовий тепле біле світло D65мм', '3', 3);
INSERT INTO `rf_komplektacija` VALUES (201, 'Світильник LED 4009 350mA/3W врізний алюміній нікельований матовий холодне біле світло D65мм', '3', 3);
INSERT INTO `rf_komplektacija` VALUES (202, 'Монтажне кільце сталеве нікельоване D65мм (до 833.78.140/150)', '3', 12);
INSERT INTO `rf_komplektacija` VALUES (203, 'Монтажний трикутник сталевий оцинкований 135х118х40мм (до 833.78.140/150)', '3', 25);
INSERT INTO `rf_komplektacija` VALUES (204, 'Світильник LED 4005 350mA/1W врізний пластиковий колір: срібний тепле біле світло D30мм', '3', 64);
INSERT INTO `rf_komplektacija` VALUES (205, 'Світильник LED 4004 350mA/1W врізний пластиковий колір: срібний тепле біле світло D30мм', '3', 62);
INSERT INTO `rf_komplektacija` VALUES (206, 'Світильник LED 4003 350mA/1W пластиковий колір: срібний тепле біле світло 34х34мм (3 штуки)', '3', 62);
INSERT INTO `rf_komplektacija` VALUES (207, 'Світильник LED 4007 350mA/1W пластиковий колір: срібний тепле біле світло 34х34мм (3 штуки)', '3', 62);
INSERT INTO `rf_komplektacija` VALUES (208, 'Світильник LED 4006 350mA/1W 6-кутний пластиковий колір: срібний тепле біле світло 38мм (3 штуки)', '3', 62);
INSERT INTO `rf_komplektacija` VALUES (209, 'Блок живлення для LED 350mA/1-4W на 4 виходи пластиковий чорний 20х45х155мм', '3', 142);
INSERT INTO `rf_komplektacija` VALUES (210, 'Блок живлення для LED 350mA/5-10W на 10 виходів пластиковий чорний 28х50х165мм', '3', 166);
INSERT INTO `rf_komplektacija` VALUES (211, 'Шнур з вилкою EU 250V 2000мм', '3', 21);
INSERT INTO `rf_komplektacija` VALUES (212, 'Сенсорний вимикач пластиковий колір: срібний 60мм D13мм', '3', 170);
INSERT INTO `rf_komplektacija` VALUES (213, 'Сенсорний вимикач для дверей пластиковий колір: срібний 60мм D13мм', '3', 162);
INSERT INTO `rf_komplektacija` VALUES (214, 'Детектор руху пластиковий колір: срібний 60мм D13мм', '3', 112);
INSERT INTO `rf_komplektacija` VALUES (215, 'Регулятор яскравості пластиковий колір: срібний 60мм D13мм', '3', 107);
INSERT INTO `rf_komplektacija` VALUES (216, 'Вимикач кнопковий пластиковий чорний 30мм D13мм', '3', 28);
INSERT INTO `rf_komplektacija` VALUES (217, 'Кріплення для вимикачів пластикове колір: срібний 20х50х57мм', '3', 8);
INSERT INTO `rf_komplektacija` VALUES (218, 'Здовжувач для LED 12V чорний 500мм', '3', 8);
INSERT INTO `rf_komplektacija` VALUES (219, 'Здовжувач для LED 12V чорний 1000мм', '3', 9);
INSERT INTO `rf_komplektacija` VALUES (220, 'Здовжувач для LED 12V чорний 2000мм', '3', 11);
INSERT INTO `rf_komplektacija` VALUES (221, 'Здовжувач для LED 24V чорний 500мм', '3', 7);
INSERT INTO `rf_komplektacija` VALUES (222, 'Здовжувач для LED 24V чорний 1000мм', '3', 9);
INSERT INTO `rf_komplektacija` VALUES (223, 'Здовжувач для LED 24V чорний 2000мм', '3', 12);
INSERT INTO `rf_komplektacija` VALUES (224, 'Здовжувач для LED 350mA чорний 500мм', '3', 8);
INSERT INTO `rf_komplektacija` VALUES (225, 'Здовжувач для LED 350mA чорний 1000мм', '3', 10);
INSERT INTO `rf_komplektacija` VALUES (226, 'Здовжувач для LED 350mA чорний 2000мм', '3', 12);
INSERT INTO `rf_komplektacija` VALUES (227, 'Захисний профіль самоклеючий для кабелів LED пластиковий білий RAL 9010 2500мм', '3', 60);
INSERT INTO `rf_komplektacija` VALUES (228, 'Захисний профіль самоклеючий для кабелів LED пластиковий коричневий RAL 8007 2500мм', '3', 60);
INSERT INTO `rf_komplektacija` VALUES (229, 'Захисний профіль самоклеючий для кабелів LED пластиковий чорний RAL 9005 2500мм', '3', 60);
INSERT INTO `rf_komplektacija` VALUES (230, 'Світильник LED 9001 на 4 батарейки AAA/1.2W для шухляд пластиковий нікельований матовий холодне біле світло 303мм', '3', 179);
INSERT INTO `rf_komplektacija` VALUES (231, 'CLIP top Завіса для накладних дверцят, відчинення 100?+Опорна планка хрестоподібна під саморізи+ Декор.заглушка для завіс', '4', 9);
INSERT INTO `rf_komplektacija` VALUES (232, 'CLIP top Амортизатор накладний на завісу BLUMOTION', '4', 12);
INSERT INTO `rf_komplektacija` VALUES (233, 'CLIP top Амортизатор врізний (монтаж у ручки), св.-сірий', '4', 12);
INSERT INTO `rf_komplektacija` VALUES (234, 'CLIP top Завіса пряма під фальшпанель, накладна, відч.95? (рівнолегла 180?)+Опорна планка хрестоподібна під саморізи для завіси під фальшпанель, підйом 3мм', '4', 32);
INSERT INTO `rf_komplektacija` VALUES (235, 'CLIP top Завіса кутова +45? І напівнакладна (відчинення 95?)+Опорна планка хрестоподібна під саморізи для завіси +45?, регулюв.', '4', 23);
INSERT INTO `rf_komplektacija` VALUES (236, 'CLIP top Завіса для накладних дверцят, відчинення 170? (крокодил)', '4', 35);
INSERT INTO `rf_komplektacija` VALUES (237, 'CLIP top Завіса для складаних дверцят, відчинення 60?  (допоміжна)', '4', 40);
INSERT INTO `rf_komplektacija` VALUES (238, 'CLIP top Завіса для алюм.рамок Z-1, накладна, відч.95?', '4', 25);
INSERT INTO `rf_komplektacija` VALUES (239, 'CLIP top BLUMOTION Завіса накладна, відч. 110?', '4', 22);
INSERT INTO `rf_komplektacija` VALUES (240, 'CLIP top BLUMOTION Завіса пряма під фальшпанель, відч. 95? (рівнолегла 180?)', '4', 45);
INSERT INTO `rf_komplektacija` VALUES (241, 'CLIP top BLUMOTION Кутова завіса напівнакладна +45?, відч.95?', '4', 40);
INSERT INTO `rf_komplektacija` VALUES (242, 'CLIP top BLUMOTION Завіса для алюм рамок Z-1,накладна, відч. 95?', '4', 40);
INSERT INTO `rf_komplektacija` VALUES (243, 'TIP-ON Завіса без пружини - накладна, відч. 100?', '4', 8);
INSERT INTO `rf_komplektacija` VALUES (244, 'TIP-ON Завіса без пружини - накладна, відч. 120?', '4', 12);
INSERT INTO `rf_komplektacija` VALUES (245, 'TIP-ON Завіса без пружини - під фальшпанель, відч. 95? (рівнолегла 180?)', '4', 28);
INSERT INTO `rf_komplektacija` VALUES (246, 'TIP-ON Завіса без пружини - кутова завіса +45?, відч. 95?', '4', 23);
INSERT INTO `rf_komplektacija` VALUES (247, 'TIP-ON Завіса без пружини - кутова завіса +30?, відч. 95?', '4', 23);
INSERT INTO `rf_komplektacija` VALUES (248, 'TIP-ON стандартний для накладних дверцят', '4', 30);
INSERT INTO `rf_komplektacija` VALUES (249, 'TIP-ON довгий для вкладних і високих дверцят', '4', 35);
INSERT INTO `rf_komplektacija` VALUES (250, 'TIP-ON Пластина під саморіз', '4', 1);
INSERT INTO `rf_komplektacija` VALUES (251, 'TIP-ON Прямий тримач ', '4', 13);
INSERT INTO `rf_komplektacija` VALUES (252, 'HF Підйомник (2 дверки) коеф.потужності F-22 (легкий)', '4', 590);
INSERT INTO `rf_komplektacija` VALUES (253, 'HF Підйомник (2 дверки) коеф.потужності F-25 (середній)', '4', 620);
INSERT INTO `rf_komplektacija` VALUES (254, 'HF Підйомник HF (2 дверки) коеф.потужності F-28 (сильний)', '4', 690);
INSERT INTO `rf_komplektacija` VALUES (255, 'HF Комплект кріплень до алюм.фасаду Z-1', '4', 150);
INSERT INTO `rf_komplektacija` VALUES (256, 'HS Підйомник (1 дверка) коеф.потужності (легкий), Нфас=350-525мм', '4', 630);
INSERT INTO `rf_komplektacija` VALUES (257, 'HS Підйомник (1 дверка)  коеф.потужності (середній), Нфас=526-675мм', '4', 670);
INSERT INTO `rf_komplektacija` VALUES (258, 'HS Підйомник (1 дверка)  коеф.потужності (сильний), Нфас=676-800мм', '4', 910);
INSERT INTO `rf_komplektacija` VALUES (259, 'HS Комплект кріплень до алюм.фасаду Z-1', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (260, 'HL Підйомник (1 дверка) коеф.потужності (легкий), Нфас=300-400мм', '4', 640);
INSERT INTO `rf_komplektacija` VALUES (261, 'HL Підйомник (1 дверка) коеф.потужності (середній), Нфас=400-550мм', '4', 690);
INSERT INTO `rf_komplektacija` VALUES (262, 'HL Підйомник  1 дверка) коеф.потужності (сильний), Нфас=450-580мм', '4', 960);
INSERT INTO `rf_komplektacija` VALUES (263, 'HL Підйомник комплект кріплень  до алюм.фасаду Z-1', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (264, 'HK Підйомник (1 дверка) коеф.потужності (легкий, середній)', '4', 300);
INSERT INTO `rf_komplektacija` VALUES (265, 'HK Підйомник (1 дверка) коеф.потужності (сильний)', '4', 470);
INSERT INTO `rf_komplektacija` VALUES (266, 'HK Підйомник доплата за кріплення до алюм.рамки Z-1', '4', 70);
INSERT INTO `rf_komplektacija` VALUES (267, 'HK Підйомник доплата за ТІР-ОN', '4', 100);
INSERT INTO `rf_komplektacija` VALUES (268, 'HK Підйомник комплект кріплень до алюм.фасаду Z-1', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (269, 'HK S Підйомник (1 дверка) коеф.потужності (легкий)', '4', 150);
INSERT INTO `rf_komplektacija` VALUES (270, 'HK S Підйомник (1 дверка) коеф.потужності (середній, сильний)', '4', 170);
INSERT INTO `rf_komplektacija` VALUES (271, 'HK S Підйомник доплата за  TIP-ON', '4', 35);
INSERT INTO `rf_komplektacija` VALUES (272, 'HK S Підйомник доплата за кріплення до алюм.рамки Z-1', '4', 30);
INSERT INTO `rf_komplektacija` VALUES (273, 'TANDEMBOX M (висота M) 83*450мм(колір сірий, білий)', '4', 220);
INSERT INTO `rf_komplektacija` VALUES (274, 'TANDEMBOX M (висота D) 204*450мм(колір сірий, білий) з подвійним релінгом', '4', 300);
INSERT INTO `rf_komplektacija` VALUES (275, 'TANDEMBOX N (висота N) 68*450мм (колір сірий, білий) під духовку', '4', 260);
INSERT INTO `rf_komplektacija` VALUES (276, 'TANDEMBOX M (висота M) внутрішній 83*450мм(колір сірий, білий) + передня металева панель', '4', 335);
INSERT INTO `rf_komplektacija` VALUES (277, 'TANDEMBOX M (висота M) під мийку 96,5*450мм', '4', 325);
INSERT INTO `rf_komplektacija` VALUES (278, 'TANDEMBOX ANTARO M (висота M) 83*450мм(колір білий шовк)', '4', 220);
INSERT INTO `rf_komplektacija` VALUES (279, 'TANDEMBOX ANTARO M (висота D) 204*450мм(колір білий шовк) з одинарним релінгом прямокутної форми', '4', 270);
INSERT INTO `rf_komplektacija` VALUES (280, 'TANDEMBOX ANTARO (висота N) 68*450мм (колір  білий шовк) під духовку', '4', 260);
INSERT INTO `rf_komplektacija` VALUES (281, 'TANDEMBOX INTIVO M (висота M) внутрішній 83*450мм(колір  білий шовк) + передня металева панель', '4', 350);
INSERT INTO `rf_komplektacija` VALUES (282, 'TANDEM повний висув 450мм, навантаження 30кг', '4', 175);
INSERT INTO `rf_komplektacija` VALUES (283, 'TANDEM повний висув 450мм, навантаження 50кг', '4', 210);
INSERT INTO `rf_komplektacija` VALUES (284, 'TANDEM частковий висув 450мм, навантаження 30кг', '4', 85);
INSERT INTO `rf_komplektacija` VALUES (285, 'TANDEM повний висув 450мм, навантаження 30кг з оранжевими кріпленнями замками', '4', 120);
INSERT INTO `rf_komplektacija` VALUES (286, 'TANDEM частковий висув 450мм, навантаження 30кг з оранжевими кріпленнями замками', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (287, 'TANDEM TIP-ON для TANDEM для повного висування', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (288, 'TANDEM TIP-ON для TANDEM для часткового висування', '4', 60);
INSERT INTO `rf_komplektacija` VALUES (289, 'TANDEM частковий висув 435мм, навантаження 30кг кріплення під фіксатор', '4', 70);
INSERT INTO `rf_komplektacija` VALUES (290, 'TANDEM частковий висув 460мм, навантаження 30кг кріплення під фіксатор', '4', 70);
INSERT INTO `rf_komplektacija` VALUES (291, 'TANDEM TIP ON  частковий висув 435мм, навантаження 30кг', '4', 110);
INSERT INTO `rf_komplektacija` VALUES (292, 'TANDEM TIP ON частковий висув 460мм, навантаження 30кг', '4', 110);
INSERT INTO `rf_komplektacija` VALUES (293, 'MOVENTO повний висув 450мм, навантаження 40кг', '4', 220);
INSERT INTO `rf_komplektacija` VALUES (294, 'MOVENTO повний висув 450мм, навантаження 60кг', '4', 260);
INSERT INTO `rf_komplektacija` VALUES (295, 'MOVENTO TIP ON повний висув 450мм, навантаження 40кг (TIP-ON вмонтований)', '4', 290);
INSERT INTO `rf_komplektacija` VALUES (296, 'MOVENTO TIP ON повний висув 450мм, навантаження 60кг   (TIP-ON вмонтований)', '4', 330);
INSERT INTO `rf_komplektacija` VALUES (297, 'METABOX (висота M) 86*450мм, навантаження 25кг', '4', 65);
INSERT INTO `rf_komplektacija` VALUES (298, 'METABOX (висота Н) 150*450мм, навантаження 25кг', '4', 90);
INSERT INTO `rf_komplektacija` VALUES (299, 'METABOX (висота N) під духовку 54*450мм, навантаження 30кг', '4', 90);
INSERT INTO `rf_komplektacija` VALUES (300, 'METABOX (висота M) внутрішня шуфляда 86*450мм, навантаження 30кг', '4', 70);
INSERT INTO `rf_komplektacija` VALUES (301, 'METABOX комплект релінгів 450мм', '4', 40);
INSERT INTO `rf_komplektacija` VALUES (302, 'METABOX Blumotion для шуфляд метабокс зменшує навантажння на шуфляду на 5кг', '4', 25);
INSERT INTO `rf_komplektacija` VALUES (303, 'вкладиш для столових приладів 240х490х45, секція 300мм (шт)', '5', 46);
INSERT INTO `rf_komplektacija` VALUES (304, 'вкладиш для столових приладів 290х490х45, секція 350мм (шт)', '5', 48);
INSERT INTO `rf_komplektacija` VALUES (305, 'вкладиш для столових приладів 340х490х45, секція 400мм (шт)', '5', 50);
INSERT INTO `rf_komplektacija` VALUES (306, 'вкладиш для столових приладів 390х490х45, секція 450мм (шт)', '5', 52);
INSERT INTO `rf_komplektacija` VALUES (307, 'вкладиш для столових приладів 440х490х45, секція 500мм (шт)', '5', 61);
INSERT INTO `rf_komplektacija` VALUES (308, 'вкладиш для столових приладів 490х490х45, секція 550мм (шт)', '5', 65);
INSERT INTO `rf_komplektacija` VALUES (309, 'вкладиш для столових приладів 540х490х45, секція 600мм (шт)', '5', 69);
INSERT INTO `rf_komplektacija` VALUES (310, 'вкладиш для столових приладів 640х490х45, секція 700мм (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (311, 'вкладиш для столових приладів 740х490х45, секція 800мм (шт)', '5', 129);
INSERT INTO `rf_komplektacija` VALUES (312, 'вкладиш для столових приладів 840х490х45, секція 900мм (шт)', '5', 133);
INSERT INTO `rf_komplektacija` VALUES (313, 'Килимок антиковзаючий(бухта-10м) (м)', '5', 81);
INSERT INTO `rf_komplektacija` VALUES (314, 'консоль барна вертикальна, d42х200, хром, (дерево-дерево) (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (315, 'консоль барна вертикальна, d42х200, хром, (дерево-скло) (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (316, 'консоль барна похила, d42х200, хром, (дерево-дерево) (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (317, 'консоль барна похила, d42х200, хром, (дерево-скло) (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (318, 'Набір для ванної кімнати з круглими кошиками (шт)', '5', 852);
INSERT INTO `rf_komplektacija` VALUES (319, 'Набір для ванної кімнати установка на фасад (шт)', '5', 969);
INSERT INTO `rf_komplektacija` VALUES (320, 'Піддон алюмінієвий 1164мм секція 1200мм (шт)', '5', 76);
INSERT INTO `rf_komplektacija` VALUES (321, 'Піддон алюмінієвий 464мм секція 500мм (шт)', '5', 47);
INSERT INTO `rf_komplektacija` VALUES (322, 'Піддон алюмінієвий 564мм секція 600мм (шт)', '5', 50);
INSERT INTO `rf_komplektacija` VALUES (323, 'Піддон алюмінієвий 564мм секція 600мм низький (шт)', '5', 50);
INSERT INTO `rf_komplektacija` VALUES (324, 'Піддон алюмінієвий 664мм секція 700мм (шт)', '5', 53);
INSERT INTO `rf_komplektacija` VALUES (325, 'Піддон алюмінієвий 664мм секція 700мм низький (шт)', '5', 52);
INSERT INTO `rf_komplektacija` VALUES (326, 'Піддон алюмінієвий 764 мм секція 800мм (шт)', '5', 56);
INSERT INTO `rf_komplektacija` VALUES (327, 'Піддон алюмінієвий 764 мм секція 800мм низький (шт)', '5', 56);
INSERT INTO `rf_komplektacija` VALUES (328, 'Піддон алюмінієвий 864мм секція 900мм (шт)', '5', 61);
INSERT INTO `rf_komplektacija` VALUES (329, 'Піддон алюмінієвий 964мм секція 1000мм (шт)', '5', 66);
INSERT INTO `rf_komplektacija` VALUES (330, 'Піддон для газового балона на напрямних (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (331, 'Сітка повного висунення під мийку  80см (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (332, 'Сітка повного висунення під мийку  80см з крепл.фасада (шт)', '5', 533);
INSERT INTO `rf_komplektacija` VALUES (333, 'Сітка повного висунення під мийку  80см з крепл.фасада алюміній (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (334, 'Сітка повного висунення під мийку  90см (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (335, 'Сітка повного висунення під мийку  90см з крепл.фасада (шт)', '5', 516);
INSERT INTO `rf_komplektacija` VALUES (336, 'Сітка повного висунення під мийку  90см з крепл.фасада алюміній (шт)', '5', 833);
INSERT INTO `rf_komplektacija` VALUES (337, 'сміттєве відро на напрямних 16л (шт) (шт)', '5', 465);
INSERT INTO `rf_komplektacija` VALUES (338, 'сміттєве відро на напрямних 24л (шт) (шт)', '5', 658);
INSERT INTO `rf_komplektacija` VALUES (339, 'сміттєве відро на напрямних 32л (шт) (шт)', '5', 736);
INSERT INTO `rf_komplektacija` VALUES (340, 'сміттєве відро на напрямних 32л крепл.на фасад (шт) (шт)', '5', 1056);
INSERT INTO `rf_komplektacija` VALUES (341, 'сміттєве відро нержавіюча сталь (шт.)', '5', 132);
INSERT INTO `rf_komplektacija` VALUES (342, 'функціональний ящик  під фасад в корпус 600мм, алюміній (шт)', '5', 717);
INSERT INTO `rf_komplektacija` VALUES (343, 'функціональний ящик  під фасад в корпус 700мм, алюміній (шт)', '5', 736);
INSERT INTO `rf_komplektacija` VALUES (344, 'функціональний ящик  під фасад в корпус 800мм, алюміній (шт)', '5', 755);
INSERT INTO `rf_komplektacija` VALUES (345, 'функціональний ящик  під фасад в корпус 900мм, алюміній (шт)', '5', 775);
INSERT INTO `rf_komplektacija` VALUES (346, 'функціональний ящик в корпус 600мм, алюміній (шт)', '5', 657);
INSERT INTO `rf_komplektacija` VALUES (347, 'функціональний ящик в корпус 700мм, алюміній (шт)', '5', 682);
INSERT INTO `rf_komplektacija` VALUES (348, 'функціональний ящик в корпус 800мм, алюміній (шт)', '5', 701);
INSERT INTO `rf_komplektacija` VALUES (349, 'функціональний ящик в корпус 900мм, алюміній (шт)', '5', 719);
INSERT INTO `rf_komplektacija` VALUES (350, 'функціональний ящик-сушка  в корпус 600мм, алюміній (шт)', '5', 701);
INSERT INTO `rf_komplektacija` VALUES (351, 'функціональний ящик-сушка  в корпус 700мм, алюміній (шт)', '5', 719);
INSERT INTO `rf_komplektacija` VALUES (352, 'функціональний ящик-сушка  в корпус 800мм, алюміній (шт)', '5', 738);
INSERT INTO `rf_komplektacija` VALUES (353, 'функціональний ящик-сушка  в корпус 900мм, алюміній (шт)', '5', 756);
INSERT INTO `rf_komplektacija` VALUES (354, 'функціональний ящик-сушка під фасад в корпус 600мм, алюміній (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (355, 'функціональний ящик-сушка під фасад в корпус 700мм, алюміній (шт)', '5', 833);
INSERT INTO `rf_komplektacija` VALUES (356, 'функціональний ящик-сушка під фасад в корпус 800мм, алюміній (шт)', '5', 852);
INSERT INTO `rf_komplektacija` VALUES (357, 'функціональний ящик-сушка під фасад в корпус 900мм, алюміній (шт)', '5', 872);
INSERT INTO `rf_komplektacija` VALUES (358, 'Вішалка висувна 30см (шт) (шт)', '5', 50);
INSERT INTO `rf_komplektacija` VALUES (359, 'Вішалка висувна 35см (шт) (шт)', '5', 54);
INSERT INTO `rf_komplektacija` VALUES (360, 'Вішалка висувна 40см (шт) (шт)', '5', 58);
INSERT INTO `rf_komplektacija` VALUES (361, 'Вішалка висувна 45см (шт) (шт)', '5', 61);
INSERT INTO `rf_komplektacija` VALUES (362, 'Вішалка висувна широка 30см (шт) (шт)', '5', 83);
INSERT INTO `rf_komplektacija` VALUES (363, 'Вішалка висувна широка 35см (шт) (шт)', '5', 85);
INSERT INTO `rf_komplektacija` VALUES (364, 'Вішалка висувна широка 40см (шт) (шт)', '5', 87);
INSERT INTO `rf_komplektacija` VALUES (365, 'Вішалка висувна широка 45см (шт) (шт)', '5', 89);
INSERT INTO `rf_komplektacija` VALUES (366, 'висувний кутовий механізм Secret - лівий 400мм (шт)', '5', 2228);
INSERT INTO `rf_komplektacija` VALUES (367, 'висувний кутовий механізм Secret - лівий (шт)', '5', 2286);
INSERT INTO `rf_komplektacija` VALUES (368, 'висувний кутовий механізм Secret - правий 400мм (шт)', '5', 2228);
INSERT INTO `rf_komplektacija` VALUES (369, 'висувний кутовий механізм Secret - правий (шт)', '5', 2286);
INSERT INTO `rf_komplektacija` VALUES (370, 'висувний кутовий механізм кріплення до фасаду Secret - лівий (шт)', '5', 2131);
INSERT INTO `rf_komplektacija` VALUES (371, 'висувний кутовий механізм кріплення до фасаду Secret - правий (шт)', '5', 2131);
INSERT INTO `rf_komplektacija` VALUES (372, 'Галерея висувна  200-215 45см (шт)', '5', 1259);
INSERT INTO `rf_komplektacija` VALUES (373, 'Галерея висувна  200-215 50см (шт)', '5', 1259);
INSERT INTO `rf_komplektacija` VALUES (374, 'Галерея висувна  85-110  55см (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (375, 'Галерея висувна  85-110 45см (шт)', '5', 775);
INSERT INTO `rf_komplektacija` VALUES (376, 'Галерея висувна  85-110 50см (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (377, 'Галерея висувна 110-125 30см (шт)', '5', 891);
INSERT INTO `rf_komplektacija` VALUES (378, 'Галерея висувна 110-125 40см (шт)', '5', 891);
INSERT INTO `rf_komplektacija` VALUES (379, 'Галерея висувна 125-140 30см (шт)', '5', 969);
INSERT INTO `rf_komplektacija` VALUES (380, 'Галерея висувна 125-140 40см (шт)', '5', 969);
INSERT INTO `rf_komplektacija` VALUES (381, 'Галерея висувна 125-140 45см (шт)', '5', 922);
INSERT INTO `rf_komplektacija` VALUES (382, 'Галерея висувна 155-170 30см (шт)', '5', 1085);
INSERT INTO `rf_komplektacija` VALUES (383, 'Галерея висувна 155-170 35см (шт)', '5', 1085);
INSERT INTO `rf_komplektacija` VALUES (384, 'Галерея висувна 155-170 40см (шт)', '5', 1085);
INSERT INTO `rf_komplektacija` VALUES (385, 'Галерея висувна 155-170 50см (шт)', '5', 1085);
INSERT INTO `rf_komplektacija` VALUES (386, 'Галерея висувна 170-185 30см (шт)', '5', 1162);
INSERT INTO `rf_komplektacija` VALUES (387, 'Галерея висувна 170-185 45см (шт)', '5', 1162);
INSERT INTO `rf_komplektacija` VALUES (388, 'Галерея висувна 185-200 30см (шт)', '5', 1201);
INSERT INTO `rf_komplektacija` VALUES (389, 'Галерея висувна 185-200 35см (шт)', '5', 1201);
INSERT INTO `rf_komplektacija` VALUES (390, 'Галерея висувна 185-200 40см (шт)', '5', 1144);
INSERT INTO `rf_komplektacija` VALUES (391, 'Галерея висувна 200-215 30см (шт)', '5', 1259);
INSERT INTO `rf_komplektacija` VALUES (392, 'Галерея висувна 200-215 35см (шт)', '5', 1259);
INSERT INTO `rf_komplektacija` VALUES (393, 'Галерея висувна 200-215 40см (шт)', '5', 1259);
INSERT INTO `rf_komplektacija` VALUES (394, 'Галерея висувна 230х500х1250-1400, 30см с доводч (шт)', '5', 3197);
INSERT INTO `rf_komplektacija` VALUES (395, 'Галерея висувна 230х500х1700-1850, 30см с доводч (шт)', '5', 3972);
INSERT INTO `rf_komplektacija` VALUES (396, 'Галерея висувна 230х500х1850-2000, 30см с доводч (шт)', '5', 4205);
INSERT INTO `rf_komplektacija` VALUES (397, 'Галерея висувна 230х500х1850-2000, 30см с доводч (шт)', '5', 5620);
INSERT INTO `rf_komplektacija` VALUES (398, 'Галерея висувна 330х500х1250-1400, 40см с доводч (шт)', '5', 3197);
INSERT INTO `rf_komplektacija` VALUES (399, 'Галерея висувна 330х500х1400-1550, 40см с доводч (шт)', '5', 3488);
INSERT INTO `rf_komplektacija` VALUES (400, 'Галерея висувна 330х500х1700-1850, 40см с доводч (шт)', '5', 3972);
INSERT INTO `rf_komplektacija` VALUES (401, 'Галерея висувна 330х500х1850-2000, 40см с доводч (шт)', '5', 4205);
INSERT INTO `rf_komplektacija` VALUES (402, 'Галерея висувна 330х500х1850-2000, 40см с доводч (шт)', '5', 5620);
INSERT INTO `rf_komplektacija` VALUES (403, 'Галерея висувна 380х500х1400-1550, 45см с доводч (шт)', '5', 4903);
INSERT INTO `rf_komplektacija` VALUES (404, 'Галерея висувна 380х500х1700-1850, 45см c доводч (шт)', '5', 3972);
INSERT INTO `rf_komplektacija` VALUES (405, 'Галерея висувна 380х500х1850-2000, 45см с доводч (шт)', '5', 4205);
INSERT INTO `rf_komplektacija` VALUES (406, 'Галерея висувна 430х500х1250-1400, 50см с доводч (шт)', '5', 3197);
INSERT INTO `rf_komplektacija` VALUES (407, 'Галерея висувна 430х500х1700-1850, 50см c доводч (шт)', '5', 3972);
INSERT INTO `rf_komplektacija` VALUES (408, 'Галерея висувна 430х500х1850-2000, 50см с доводч (шт)', '5', 4205);
INSERT INTO `rf_komplektacija` VALUES (409, 'Галерея висувна 430х500х1850-2000, 50см с доводч (шт)', '5', 5620);
INSERT INTO `rf_komplektacija` VALUES (410, 'Галерея висувна 480х500х1550-1700  55см (шт)', '5', 1085);
INSERT INTO `rf_komplektacija` VALUES (411, 'Галерея висувна 60-80 30см (шт)', '5', 736);
INSERT INTO `rf_komplektacija` VALUES (412, 'Галерея висувна 60-80 40см (шт)', '5', 736);
INSERT INTO `rf_komplektacija` VALUES (413, 'Галерея висувна 60-80 45см (шт)', '5', 701);
INSERT INTO `rf_komplektacija` VALUES (414, 'Галерея висувна 85-110  40см (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (415, 'Галерея подвійна Classic 45см (шт)', '5', 1550);
INSERT INTO `rf_komplektacija` VALUES (416, 'Галерея подвійна Luxury 45см (шт)', '5', 2170);
INSERT INTO `rf_komplektacija` VALUES (417, 'Карго 2-х рівневе з посудодержателем в секцію 450мм, хром 360х500х520 с доводч (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (418, 'Карго 2-х рівневе з посудодержателем в секцію 500мм, хром 410х500х520 с доводч (шт)', '5', 571);
INSERT INTO `rf_komplektacija` VALUES (419, 'Карго 2-х рівневе з посудодержателем в секцію 600мм, хром 510х500х520 с доводч (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (420, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм сіре 210х500х520 с доводч (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (421, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм хром (шт)', '5', 355);
INSERT INTO `rf_komplektacija` VALUES (422, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм хром 210х500х520 с довод (шт)', '5', 484);
INSERT INTO `rf_komplektacija` VALUES (423, 'Карго 2-х рівневе з сіткою для пляшок в секцію 350мм хром 260х500х520 с доводч (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (424, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм сіре (шт)', '5', 317);
INSERT INTO `rf_komplektacija` VALUES (425, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм сіре 310х500х520 с доводч (шт)', '5', 484);
INSERT INTO `rf_komplektacija` VALUES (426, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм хром (шт)', '5', 377);
INSERT INTO `rf_komplektacija` VALUES (427, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм хром 310х500х520 с довод (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (428, 'Карго 2-х рівневе з сіткою для пляшок в секцію 450мм хром 360х500х520 с доводч (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (429, 'Карго 2-х рівневе з сіткою для пляшок в секцію 450мм, сіре 360х500х520 с доводч (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (430, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм сіре 410х500х520 с доводч (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (431, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм хром 410х500х520 с доводч (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (432, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм, сіре (шт)', '5', 346);
INSERT INTO `rf_komplektacija` VALUES (433, 'Карго 2-х рівневе з сіткою для пляшок в секцію 600мм хром 510х500х520 с доводч (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (434, 'Карго 2-х рівневе з тарілкодерж. в секцію 600мм, хром 510х500х520 с доводч (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (435, 'Карго 2-х рівневе с хлібницею в секцію 300мм 210х500х520 (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (436, 'Карго 2-х рівневе с хлібницею в секцію 350мм 260х500х520 (шт)', '5', 639);
INSERT INTO `rf_komplektacija` VALUES (437, 'Карго 2-х рівневе с хлібницею в секцію 400мм 310х500х520 (шт)', '5', 658);
INSERT INTO `rf_komplektacija` VALUES (438, 'Карго 2-х рівневе с хлібницею в секцію 450мм 360х500х520 (шт)', '5', 678);
INSERT INTO `rf_komplektacija` VALUES (439, 'Карго 3-х рівневе з відділеннями для ножів та пляшок в секцію 300мм алюмін.240х500х520 (шт)', '5', 969);
INSERT INTO `rf_komplektacija` VALUES (440, 'Карго 3-х рівневе з сіткою для пляшок в секцію 300мм хром 210х500х520 с дов. (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (441, 'Карго 3-х рівневе з сіткою для пляшок в секцію 350мм хром 260х500х520 с дов. (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (442, 'Карго 3-х рівневе з сіткою для пляшок в секцію 400мм хром 310х500х520 с дов. (шт)', '5', 581);
INSERT INTO `rf_komplektacija` VALUES (443, 'Карго 3-х рівневе з сіткою для пляшок в секцію 450мм хром 360х500х520 с дов. (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (444, 'Карго 3-х рівневе з сіткою для пляшок в секцію 500мм хром 410х500х520 с дов. (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (445, 'карусель 1/2 d=740х420x 650-700 хром 450mm (шт)', '5', 639);
INSERT INTO `rf_komplektacija` VALUES (446, 'карусель 3/4 d=600 х 650-700 хром (шт)', '5', 736);
INSERT INTO `rf_komplektacija` VALUES (447, 'карусель 3/4 d=740 х 650-700 хром (шт)', '5', 775);
INSERT INTO `rf_komplektacija` VALUES (448, 'карусель 3/4 d=830 х 650-700 хром (шт)', '5', 813);
INSERT INTO `rf_komplektacija` VALUES (449, 'карусель 3/4 висувна (шт)', '5', 3488);
INSERT INTO `rf_komplektacija` VALUES (450, 'карусель 4/4 D600*700 (шт)', '5', 1007);
INSERT INTO `rf_komplektacija` VALUES (451, 'карусель 4/4 D750*700 (шт)', '5', 1201);
INSERT INTO `rf_komplektacija` VALUES (452, 'кошик алюмінієвий нижнього кріплення в секцію 150мм с доводч (шт)', '5', 578);
INSERT INTO `rf_komplektacija` VALUES (453, 'кошик алюмінієвий нижнього кріплення в секцію 200мм 150х500х520 с доводч (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (454, 'Кошик бічного кріплення , 3-рівневий, лівий,в секцію 250 мм, хром 185*510*615 (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (455, 'Кошик бічного кріплення , 3-рівневий, лівий,в секцію 300 мм, хром 240*510*615 (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (456, 'Кошик бічного кріплення , 3-рівневий, правий,в секцію 250 мм, хром 185*510*615 (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (457, 'Кошик бічного кріплення , 3-рівневий, правий,в секцію 300 мм, хром 240*510*615 (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (458, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію 150 мм,ліва, сірий (шт)', '5', 429);
INSERT INTO `rf_komplektacija` VALUES (459, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію 150 мм,ліва, хром (шт)', '5', 547);
INSERT INTO `rf_komplektacija` VALUES (460, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію150 мм,права, сірий (шт)', '5', 429);
INSERT INTO `rf_komplektacija` VALUES (461, 'кошик бічного кріплення 11см + Тандем напрямні, в секцшю 150 мм,права, хром (шт)', '5', 547);
INSERT INTO `rf_komplektacija` VALUES (462, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, сірий, ліве (шт)', '5', 432);
INSERT INTO `rf_komplektacija` VALUES (463, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, сірий, праве (шт)', '5', 432);
INSERT INTO `rf_komplektacija` VALUES (464, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, хром, ліве (шт)', '5', 557);
INSERT INTO `rf_komplektacija` VALUES (465, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, хром, праве (шт)', '5', 557);
INSERT INTO `rf_komplektacija` VALUES (466, 'Кошик бічного кріплення 45°, ліва,в секцію 250 мм, хром (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (467, 'Кошик бічного кріплення 45°, права ,в секцію 250 мм, хром (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (468, 'кошик бічного кріплення, в секцію 150мм,сірий 105х500х470 с доводч (шт)', '5', 310);
INSERT INTO `rf_komplektacija` VALUES (469, 'кошик бічного кріплення, в секцію 150мм,хром 105х500х470 с доводч (шт)', '5', 329);
INSERT INTO `rf_komplektacija` VALUES (470, 'кошик бічного кріплення, в секцію 200 мм, сірий 155х500х470 с доводч (шт)', '5', 329);
INSERT INTO `rf_komplektacija` VALUES (471, 'кошик бічного кріплення, в секцію 200 мм, хром 155х500х470 с доводч (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (472, 'кошик бічного кріплення, в секцію 260 мм, хром 225/155х500х470 с доводч (шт)', '5', 358);
INSERT INTO `rf_komplektacija` VALUES (473, 'кошик бічного кріплення, в секцію 260 мм, хром 225х500х470 с доводч (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (474, 'кошик бічного кріплення, в секцію 260 мм,сірий 225/155х500х470 с доводч (шт)', '5', 339);
INSERT INTO `rf_komplektacija` VALUES (475, 'кріплення фасаду для висувною сітки (шт)', '5', 48);
INSERT INTO `rf_komplektacija` VALUES (476, 'поворотний кутовий механізм Secret - лівий (шт)', '5', 2131);
INSERT INTO `rf_komplektacija` VALUES (477, 'поворотний кутовий механізм Secret - правий (шт)', '5', 2131);
INSERT INTO `rf_komplektacija` VALUES (478, 'Сітка для галереї 230х440х75 хром (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (479, 'піддон пласт. для сушки  90см (шт)', '5', 46);
INSERT INTO `rf_komplektacija` VALUES (480, 'піддон прозорий для сушки 60см (шт)', '5', 38);
INSERT INTO `rf_komplektacija` VALUES (481, 'піддон прозорий для сушки 70см (шт)', '5', 42);
INSERT INTO `rf_komplektacija` VALUES (482, 'піддон прозорий для сушки 80см (шт)', '5', 54);
INSERT INTO `rf_komplektacija` VALUES (483, 'піддон прозорий для сушки 90см (шт)', '5', 58);
INSERT INTO `rf_komplektacija` VALUES (484, 'полиця барная підвісна 1000х360х480, хром (шт)', '5', 833);
INSERT INTO `rf_komplektacija` VALUES (485, 'сушка для посуду висувна  60 см хром (шт)', '5', 387);
INSERT INTO `rf_komplektacija` VALUES (486, 'сушка для посуду висувна  60 см хром  з крепл.фасада (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (487, 'сушка для посуду з пласт. піддоном 45см хром (шт)', '5', 199);
INSERT INTO `rf_komplektacija` VALUES (488, 'сушка для посуду з прозорим піддоном 60см хром і алюм.рамкой (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (489, 'сушка для посуду з прозорим піддоном 60см, алюм.рамкой сіра (шт)', '5', 244);
INSERT INTO `rf_komplektacija` VALUES (490, 'сушка для посуду з прозорим піддоном 60см, алюм.рамкой хром (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (491, 'сушка для посуду з прозорим піддоном 70см, алюм.рамкой сіра (шт)', '5', 253);
INSERT INTO `rf_komplektacija` VALUES (492, 'сушка для посуду з прозорим піддоном 70см, алюм.рамкой хром (шт)', '5', 261);
INSERT INTO `rf_komplektacija` VALUES (493, 'сушка для посуду з прозорим піддоном 80см, алюм.рамкой сіра (шт)', '5', 263);
INSERT INTO `rf_komplektacija` VALUES (494, 'сушка для посуду з прозорим піддоном 80см, алюм.рамкой хром (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (495, 'сушка для посуду з прозорим піддоном 90см, алюм.рамкой сіра (шт)', '5', 273);
INSERT INTO `rf_komplektacija` VALUES (496, 'сушка для посуду з прозорим піддоном 90см, алюм.рамкой хром (шт)', '5', 281);
INSERT INTO `rf_komplektacija` VALUES (497, 'сушка-нержавіюча сталь з прозорим піддоном 60см, алюм.рамкой (шт)', '5', 465);
INSERT INTO `rf_komplektacija` VALUES (498, 'сушка-нержавіюча сталь з прозорим піддоном 70см, алюм.рамкой (шт)', '5', 484);
INSERT INTO `rf_komplektacija` VALUES (499, 'сушка-нержавіюча сталь з прозорим піддоном 80см, алюм.рамкой (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (500, 'сушка-нержавіюча сталь з прозорим піддоном 90см, алюм.рамкой (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (501, 'сушка-полиця однорядна  80см сіра (шт)', '5', 102);
INSERT INTO `rf_komplektacija` VALUES (502, 'сушка-полиця однорядна  80см хром (шт)', '5', 106);
INSERT INTO `rf_komplektacija` VALUES (503, 'сушка-полиця однорядна  80см хром з прозорим піддоном (шт)', '5', 141);
INSERT INTO `rf_komplektacija` VALUES (504, 'сушка-полиця однорядна  90см  хром (шт)', '5', 108);
INSERT INTO `rf_komplektacija` VALUES (505, 'сушка-полиця однорядна  90см сіра (шт)', '5', 104);
INSERT INTO `rf_komplektacija` VALUES (506, 'тримач келихів 1- рядний 120*355*70 (шт)', '5', 54);
INSERT INTO `rf_komplektacija` VALUES (507, 'тримач келихів 2- рядний 235х355х70 (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (508, 'тримач келихів 2- рядний 235х500х70 (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (509, 'тримач келихів 4- рядний 460х355х70 (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (510, 'тримач келихів 4- рядний 460х500х70 (шт)', '5', 213);
INSERT INTO `rf_komplektacija` VALUES (511, 'тримач келихів 5- рядний 460*250*160 (шт)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (512, 'дошка обробна з тримачем на релінг, дерево-хром (шт)', '5', 155);
INSERT INTO `rf_komplektacija` VALUES (513, 'полиця -сушка навісна (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (514, 'полиця велика алюміній (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (515, 'полиця для cпецій вузька подвійна (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (516, 'полиця для зберігання спецій (8 предметів) (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (517, 'полиця для келихів 5-рядна (шт)', '5', 145);
INSERT INTO `rf_komplektacija` VALUES (518, 'полиця для пляшок (шт)', '5', 145);
INSERT INTO `rf_komplektacija` VALUES (519, 'полиця для рушників (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (520, 'полиця для спецій з тримачем ножів, одинарна, алюміній (шт)', '5', 261);
INSERT INTO `rf_komplektacija` VALUES (521, 'полиця для спецій одинарна вузька (шт)', '5', 116);
INSERT INTO `rf_komplektacija` VALUES (522, 'полиця для спецій одинарна широка (шт)', '5', 120);
INSERT INTO `rf_komplektacija` VALUES (523, 'полиця для спецій та рушників подвійна (шт)', '5', 162);
INSERT INTO `rf_komplektacija` VALUES (524, 'полиця для спецій, вузька, подвійна, дерево (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (525, 'полиця для спецій, двойная.алюміній (шт)', '5', 281);
INSERT INTO `rf_komplektacija` VALUES (526, 'полиця для спецій, метал. подвійна вузька / широка (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (527, 'полиця для спецій, одинарна вузька, дерево-хром (шт)', '5', 175);
INSERT INTO `rf_komplektacija` VALUES (528, 'полиця для спецій, одинарна, алюміній (шт)', '5', 213);
INSERT INTO `rf_komplektacija` VALUES (529, 'полиця для спецій, подвійна вузька (шт)', '5', 174);
INSERT INTO `rf_komplektacija` VALUES (530, 'полиця для спецій, подвійна вузька / широка, дерево-хром (шт)', '5', 308);
INSERT INTO `rf_komplektacija` VALUES (531, 'полиця для спецій, широка, подвійна, дерево (шт)', '5', 308);
INSERT INTO `rf_komplektacija` VALUES (532, 'полиця для спецій,, велика, одинарна, дерево (шт)', '5', 199);
INSERT INTO `rf_komplektacija` VALUES (533, 'полиця кутова класична (шт)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (534, 'полиця-сушка настольная (шт)', '5', 213);
INSERT INTO `rf_komplektacija` VALUES (535, 'полиця-тримач кулінарної книги (шт)', '5', 174);
INSERT INTO `rf_komplektacija` VALUES (536, 'профіль алюмінієвий для вішалок 120 мм (шт)', '5', 43);
INSERT INTO `rf_komplektacija` VALUES (537, 'сушка для посуду однорядна на релінг (шт)', '5', 426);
INSERT INTO `rf_komplektacija` VALUES (538, 'тримач для ножів алюміній (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (539, 'тримач для ножів, дерево-хром (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (540, 'тримач рушників (шт)', '5', 106);
INSERT INTO `rf_komplektacija` VALUES (541, 'тримач рушників настольній (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (542, 'тримач фольги (шт)', '5', 174);
INSERT INTO `rf_komplektacija` VALUES (543, 'чаша для ложок/вилок, дерево-хром (шт)', '5', 213);
INSERT INTO `rf_komplektacija` VALUES (544, 'чаша для ложок/вилок, метал. (шт)', '5', 232);
INSERT INTO `rf_komplektacija` VALUES (545, 'вішалка платтяна потрійна d 25см (шт)', '5', 232);
INSERT INTO `rf_komplektacija` VALUES (546, 'вішалка платтяна потрійна d 50см (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (547, 'комплект на трубу d =50см (шт)', '5', 1938);
INSERT INTO `rf_komplektacija` VALUES (548, 'кошик бічний з кріпленням 360х100 (шт)', '5', 426);
INSERT INTO `rf_komplektacija` VALUES (549, 'кронштейн скляной полици  для труби d-50 (шт)', '5', 174);
INSERT INTO `rf_komplektacija` VALUES (550, 'набір сполучних елементів для труби d-50 (шт)', '5', 155);
INSERT INTO `rf_komplektacija` VALUES (551, 'підставка для ніг  360х100 d=50mm (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (552, 'полиця для келихів (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (553, 'полиця центральна  скляна 360 мм (шт)', '5', 304);
INSERT INTO `rf_komplektacija` VALUES (554, 'полиця центральна 360 мм (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (555, 'стійка кухонна ( комплект) (шт)', '5', 3120);
INSERT INTO `rf_komplektacija` VALUES (556, 'тримач келихив  D=50mm (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (557, 'тримач парасольок D=50mm (шт)', '5', 319);
INSERT INTO `rf_komplektacija` VALUES (558, 'тримач труби D=50мм (шт)', '5', 387);
INSERT INTO `rf_komplektacija` VALUES (559, 'борт для полиці 300х15х65 хром (шт)', '5', 58);
INSERT INTO `rf_komplektacija` VALUES (560, 'борт для полиці 400х15х65 хром (шт)', '5', 61);
INSERT INTO `rf_komplektacija` VALUES (561, 'борт для полиці 500х15х65 хром (шт)', '5', 65);
INSERT INTO `rf_komplektacija` VALUES (562, 'брючніца висувна 230х450х80, хром (шт.)', '5', 222);
INSERT INTO `rf_komplektacija` VALUES (563, 'брючніца висувна 360х500х85, в секцію 400мм, хром (шт)', '5', 261);
INSERT INTO `rf_komplektacija` VALUES (564, 'брючніца висувна 418х500х85, в секцію 450мм, хром (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (565, 'брючніца висувна 468х500х85, в секцію 500мм, хром (шт)', '5', 281);
INSERT INTO `rf_komplektacija` VALUES (566, 'брючніца висувна 518х500х85, в секцію 550мм, хром (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (567, 'брючніца висувна 568х500х85, в секцію 600мм, хром (шт)', '5', 300);
INSERT INTO `rf_komplektacija` VALUES (568, 'брючніца висувна бок. кріплення 215х500х90 алюм. (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (569, 'брючніца висувна бок.кріплення 380х520х95, в секц. 40см, алюм. (шт)', '5', 310);
INSERT INTO `rf_komplektacija` VALUES (570, 'брючніца висувна бок.кріплення 430х470х95, в секц. 45см, алюм. (шт)', '5', 329);
INSERT INTO `rf_komplektacija` VALUES (571, 'брючніца висувна бок.кріплення 480х520х95, в секц. 50см, алюм. (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (572, 'брючніца висувна бок.кріплення 530х520х95, в секц. 55см, алюм. (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (573, 'брючніца висувна бок.кріплення 580х470х95, в секц. 60см, алюм. (шт)', '5', 387);
INSERT INTO `rf_komplektacija` VALUES (574, 'брючніца висувна бок.кріплення 680х520х95, в секц. 70см, алюм. (шт)', '5', 406);
INSERT INTO `rf_komplektacija` VALUES (575, 'брючніца висувна бок.кріплення 780х470х95, в секц. 80см, алюм. (шт)', '5', 426);
INSERT INTO `rf_komplektacija` VALUES (576, 'брючніца висувна бок.кріплення 880х520х95, в секц. 90см, алюм. (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (577, 'брючніца висувна верхнього кріплення 325-355х520х80мм в секцию 400 мм алюм. (шт)', '5', 310);
INSERT INTO `rf_komplektacija` VALUES (578, 'брючніца висувна верхнього кріплення 375-405х520х80мм в секцию 450мм  алюм. (шт)', '5', 329);
INSERT INTO `rf_komplektacija` VALUES (579, 'брючніца висувна верхнього кріплення  425-455х520х80мм в секцию 500 мм  алюм. (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (580, 'брючніца висувна верхнього кріплення 475-505х520х80мм в секцию 550 мм  алюм. (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (581, 'брючніца висувна верхнього кріплення340х450х90 алюм. (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (582, 'брючніца висувна з галст. верхнього кріплення 725-755х470х80мм в секцію 800 мм алюм. (шт)', '5', 426);
INSERT INTO `rf_komplektacija` VALUES (583, 'брючніца висувна з краватками, верхнього кріплення 525-555х520х80мм в секцію 600мм алюм. (шт)', '5', 387);
INSERT INTO `rf_komplektacija` VALUES (584, 'брючніца висувна з краватками, верхнього кріплення 625-655х520х80мм в секцію 700мм алюм. (шт)', '5', 406);
INSERT INTO `rf_komplektacija` VALUES (585, 'брючніца висувна з краватками, верхнього кріплення 825-855х470х80мм в секцію 900мм алюм. (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (586, 'брючніца висувна, бічне кріплення 364х500х45, в секцію 400мм, хром (шт)', '5', 213);
INSERT INTO `rf_komplektacija` VALUES (587, 'брючніца висувна, бічне кріплення 414х500х45, в секцію 450мм, хром (шт)', '5', 222);
INSERT INTO `rf_komplektacija` VALUES (588, 'брючніца висувна, бічне кріплення 464х500х45, в секцію 500мм, хром (шт)', '5', 232);
INSERT INTO `rf_komplektacija` VALUES (589, 'брючніца висувна, бічне кріплення 514х500х45, в секцію 550мм, хром (шт)', '5', 242);
INSERT INTO `rf_komplektacija` VALUES (590, 'брючніца висувна, бічне кріплення 564х500х45, в секцію 600мм, хром (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (591, 'брючніца і 2 сітки висувна325х500х1120, хром (шт)', '5', 716);
INSERT INTO `rf_komplektacija` VALUES (592, 'брючніца на напрямной  485*425*100 хром (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (593, 'брючніца поворотна 460х460х80, хром (шт.)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (594, 'брючніца-полиця з боковим кріпленням 650-680х520х160 в секцию700мм алюмінієва (шт)', '5', 581);
INSERT INTO `rf_komplektacija` VALUES (595, 'брючніца-полиця з боковим кріпленням 750-780х520х160 в секцию800мм алюмінієва (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (596, 'брючніца-полиця з боковим кріпленням 850-880х520х160 в секцию900мм алюмінієва (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (597, 'брючніца-полиця з верхним кріпленням 625-655х520х160 в секцию700мм алюмінієва (шт)', '5', 610);
INSERT INTO `rf_komplektacija` VALUES (598, 'брючніца-полиця з верхним кріпленням 725-755х520х160 в секцию800мм алюмінієва (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (599, 'брючніця-полиця з верхним кріпленням 825-855х520х160 в секцию900мм алюмінієва (шт)', '5', 629);
INSERT INTO `rf_komplektacija` VALUES (600, 'галерея підвісна (4 сітки хром), 235х450х1300, хром (шт)', '5', 511);
INSERT INTO `rf_komplektacija` VALUES (601, 'дошка прасувальна для гардероба розкладна сіра 150-340х1360 (шт)', '5', 2325);
INSERT INTO `rf_komplektacija` VALUES (602, 'комплект для панелі, що обертається, 5 полиць хром (шт)', '5', 775);
INSERT INTO `rf_komplektacija` VALUES (603, 'корзина для білизни з тримачем 280х290х500, в корпус 350мм, хром (шт)', '5', 242);
INSERT INTO `rf_komplektacija` VALUES (604, 'корзина для білизни з тримачем 330х290х500, в корпус 400мм, хром (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (605, 'корзина для білизни з тримачем 380х290х500, в корпус 450мм, хром (шт)', '5', 261);
INSERT INTO `rf_komplektacija` VALUES (606, 'корзина для білизни з тримачем 430х290х500, в корпус 500мм, хром (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (607, 'корзина для білизни з тримачем 480х290х500, в корпус 550мм, хром (шт)', '5', 281);
INSERT INTO `rf_komplektacija` VALUES (608, 'корзина для білизни з тримачем 530х290х500, в корпус 600мм, хром (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (609, 'корзина для білизни кріплення на фасад 230х250х500, корпус 300мм, хром (шт)', '5', 106);
INSERT INTO `rf_komplektacija` VALUES (610, 'корзина для білизни кріплення на фасад 230х290х500, корпус 300мм, хром (шт)', '5', 232);
INSERT INTO `rf_komplektacija` VALUES (611, 'корзина для білизни кріплення на фасад 280х250х500, корпус 350мм, хром (шт)', '5', 116);
INSERT INTO `rf_komplektacija` VALUES (612, 'корзина для білизни кріплення на фасад 330х250х500, корпус 400мм, хром (шт)', '5', 125);
INSERT INTO `rf_komplektacija` VALUES (613, 'корзина для білизни кріплення на фасад 380х250х500, корпус 450мм, хром (шт)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (614, 'корзина для білизни кріплення на фасад 430х250х500, корпус 500мм, хром (шт)', '5', 145);
INSERT INTO `rf_komplektacija` VALUES (615, 'кошик 3-х уровн. алюмін.., бічного кріплення, лівий 280х485х920 (шт)', '5', 872);
INSERT INTO `rf_komplektacija` VALUES (616, 'кошик 3-х уровн. алюмін.., бічного кріплення, правий 280х485х920 (шт)', '5', 872);
INSERT INTO `rf_komplektacija` VALUES (617, 'кошик висувний 210х500х90мм, хром (шт)', '5', 116);
INSERT INTO `rf_komplektacija` VALUES (618, 'ліфт пантограф 600-900х140х850, сірий/хром (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (619, 'ліфт пантограф 800-1150х140х850, сірий/хром (шт.)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (620, 'ліфт пантограф 800-1150х140х850, сірий/хром на 15 кг (шт)', '5', 348);
INSERT INTO `rf_komplektacija` VALUES (621, 'полиця висувна 600 мм, хром (шт)', '5', 308);
INSERT INTO `rf_komplektacija` VALUES (622, 'полиця висувна 600 мм, хром (шт)', '5', 310);
INSERT INTO `rf_komplektacija` VALUES (623, 'полиця висувна 900 мм, хром (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (624, 'полиця для взуття 420-700х200х95 (шт.)', '5', 87);
INSERT INTO `rf_komplektacija` VALUES (625, 'полиця для взуття 900-1100х200х95 (шт)', '5', 106);
INSERT INTO `rf_komplektacija` VALUES (626, 'полиця для взуття висувна 550-580х470х170 в секцію 600 мм хром/алюміній (шт)', '5', 465);
INSERT INTO `rf_komplektacija` VALUES (627, 'полиця для взуття висувна 650-680х470х170 в секцію 700 мм хром/алюміній (шт)', '5', 484);
INSERT INTO `rf_komplektacija` VALUES (628, 'полиця для взуття висувна 750-780х470х170 в секцію 800 мм хром/алюміній (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (629, 'полиця для взуття висувна 850-880х470х170 в секцію 900 мм, хром/алюміній (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (630, 'полиця для взуття висувна нижнє кріплення 525х500х150, хром (шт)', '5', 300);
INSERT INTO `rf_komplektacija` VALUES (631, 'полиця для взуття висувна, нижнє кріплення, 375х500х150, хром (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (632, 'полиця для взуття висувна, нижнє кріплення, 425х500х150, хром (шт)', '5', 281);
INSERT INTO `rf_komplektacija` VALUES (633, 'полиця для взуття висувна, нижнє кріплення, 475х500х150, хром (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (634, 'полиця для взуття дворядна, поворотна, на фасад 60см, 530х230х220, хром (шт)', '5', 232);
INSERT INTO `rf_komplektacija` VALUES (635, 'полиця для взуття дворядна, поворотна, на фасад 70см, 630х230х220, хром (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (636, 'полиця для взуття дворядна, поворотна, на фасад 80см, 730х230х220, хром (шт)', '5', 271);
INSERT INTO `rf_komplektacija` VALUES (637, 'полиця для взуття дворядна, поворотна, на фасад 90см, 830х230х220, хром (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (638, 'полиця для взуття однорядна, поворотна, на фасад 60см, 530х120х220, хром (шт)', '5', 155);
INSERT INTO `rf_komplektacija` VALUES (639, 'полиця для взуття однорядна, поворотна, на фасад 70см, 630х120х220, хром (шт)', '5', 170);
INSERT INTO `rf_komplektacija` VALUES (640, 'полиця для взуття однорядна, поворотна, на фасад 80см, 730х120х220, хром (шт)', '5', 184);
INSERT INTO `rf_komplektacija` VALUES (641, 'полиця для взуття однорядна, поворотна, на фасад 90см, 830х120х220, хром (шт)', '5', 197);
INSERT INTO `rf_komplektacija` VALUES (642, 'полиця з боковим кріпленням 350-380х520х160 в секцию400мм алюмінієва (шт)', '5', 484);
INSERT INTO `rf_komplektacija` VALUES (643, 'полиця з боковим кріпленням 400-430х520х160 в секцию450мм алюмінієва (шт)', '5', 504);
INSERT INTO `rf_komplektacija` VALUES (644, 'полиця з боковим кріпленням 450-480х520х160 в секцию500мм алюмінієва (шт)', '5', 523);
INSERT INTO `rf_komplektacija` VALUES (645, 'полиця з боковим кріпленням 500-530х520х160 в секцию550мм алюмінієва (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (646, 'полиця з боковим кріпленням 550-580х520х160 в секцию600мм алюмінієва (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (647, 'полиця з боковим кріпленням 650-680х520х160 в секцию700мм алюмінієва (шт)', '5', 581);
INSERT INTO `rf_komplektacija` VALUES (648, 'полиця з боковим кріпленням 750-780х520х160 в секцию800мм алюмінієва (шт)', '5', 600);
INSERT INTO `rf_komplektacija` VALUES (649, 'полиця з боковим кріпленням 850-880х520х160 в секцию900мм алюмінієва (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (650, 'полиця з верхнім кріпленням 475-505х520х160 в секцию550мм алюмінієва (шт)', '5', 542);
INSERT INTO `rf_komplektacija` VALUES (651, 'полиця з верхнім кріпленням 525-555х520х160 в секцию600мм алюмінієва (шт)', '5', 562);
INSERT INTO `rf_komplektacija` VALUES (652, 'полиця з верхнім кріпленням 825-855х520х160 в секцию900мм алюмінієва (шт)', '5', 620);
INSERT INTO `rf_komplektacija` VALUES (653, 'секція для взуття висувна, 500х500х470, хром (шт)', '5', 1027);
INSERT INTO `rf_komplektacija` VALUES (654, 'секція для взуття висувна, 550х500х470, хром (шт)', '5', 1046);
INSERT INTO `rf_komplektacija` VALUES (655, 'сітка повного висунення в корпус 35см (шт)', '5', 368);
INSERT INTO `rf_komplektacija` VALUES (656, 'сітка повного висунення в корпус 40см (шт)', '5', 377);
INSERT INTO `rf_komplektacija` VALUES (657, 'сітка повного висунення в корпус 45см (шт)', '5', 387);
INSERT INTO `rf_komplektacija` VALUES (658, 'сітка повного висунення в корпус 50см (шт)', '5', 406);
INSERT INTO `rf_komplektacija` VALUES (659, 'сітка повного висунення в корпус 55см (шт)', '5', 416);
INSERT INTO `rf_komplektacija` VALUES (660, 'сітка повного висунення в корпус 60см (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (661, 'сітка повного висунення в корпус 70см (шт)', '5', 465);
INSERT INTO `rf_komplektacija` VALUES (662, 'сітка повного висунення в корпус 80см (шт)', '5', 474);
INSERT INTO `rf_komplektacija` VALUES (663, 'сітка повного висунення в корпус 90см (шт)', '5', 503);
INSERT INTO `rf_komplektacija` VALUES (664, 'стійка для газет, 375х270х430, хром (шт)', '5', 242);
INSERT INTO `rf_komplektacija` VALUES (665, 'тримач брюк і краваток висувний, поворотний 215х460х90, алюміній (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (666, 'тримач брюк і ременів висувний, поворотний 215х460х90, алюміній (шт)', '5', 290);
INSERT INTO `rf_komplektacija` VALUES (667, 'тримач взуття висувний 2 полиці лівий алюм. (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (668, 'тримач взуття висувний 2 полиці правий алюміній (шт)', '5', 445);
INSERT INTO `rf_komplektacija` VALUES (669, 'тримач краваткокі ременів 120х460х110, на спрямовуючій хром лівий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (670, 'тримач краваток 120х460х85, на спрямовуючій хром лівий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (671, 'тримач краваток120х460х85, на спрямовуючій хром правий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (672, 'тримач краваток 85х460х90, алюміній (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (673, 'тримач краваток 85х460х90, хром (шт)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (674, 'тримач краваток і ременів 120х460х110, на спрямовуючій хром правий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (675, 'тримач краваток і ременів 85х460х125, хром (шт.)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (676, 'тримач краваток і ременів 85х460х90, алюміній (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (677, 'тримач краваток і ременів з петельным кріпленням 350х50х350 (шт)', '5', 116);
INSERT INTO `rf_komplektacija` VALUES (678, 'тримач ременів  35х460х125, хром (шт)', '5', 135);
INSERT INTO `rf_komplektacija` VALUES (679, 'тримач ременів 85х460х90, алюміній (шт)', '5', 252);
INSERT INTO `rf_komplektacija` VALUES (680, 'тримач ременів висувний  120х460х110 лівий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (681, 'тримач ременів висувний 120х460х110 правий (шт)', '5', 193);
INSERT INTO `rf_komplektacija` VALUES (682, 'Карго 2-х уровневое боковое в верхний модуль 150 с доводчиком (гл.280мм)', '6', 460);
INSERT INTO `rf_komplektacija` VALUES (683, 'Карго 2-х уровневое боковое (без доводчика)', '6', 430);
INSERT INTO `rf_komplektacija` VALUES (684, 'Карго 3-х уровневое боковое 150 (без доводчика)', '6', 470);
INSERT INTO `rf_komplektacija` VALUES (685, 'Карго 2-х уровневое боковое со скрытой направляющей  150 (с доводчиком)', '6', 550);
INSERT INTO `rf_komplektacija` VALUES (686, 'Карго 2-х уровневое боковое со скрытой направляющей  200 (с доводчиком)', '6', 640);
INSERT INTO `rf_komplektacija` VALUES (687, 'Карго 3-х уровневое боковое со скрытой направляющей 300 (с доводчиком)', '6', 680);
INSERT INTO `rf_komplektacija` VALUES (688, 'Сушка 2-х уровневая для посуды выдвижная нижняя 300 (с доводчиком)', '6', 900);
INSERT INTO `rf_komplektacija` VALUES (689, 'Разделитель-фиксатор для бутылок (комплект сост. из 4 шт)', '6', 50);
INSERT INTO `rf_komplektacija` VALUES (690, 'Комплект креплений фасада на карго', '6', 42);
INSERT INTO `rf_komplektacija` VALUES (691, 'Комплект креплений фасада на карго NEW', '6', 55);
INSERT INTO `rf_komplektacija` VALUES (692, 'Карусель 3/4  ?  815', '6', 1360);
INSERT INTO `rf_komplektacija` VALUES (693, 'Труба для карусели ? 25', '6', 155);
INSERT INTO `rf_komplektacija` VALUES (694, 'Рамка для двери модуля H=1740                          (рамка крепится на дверь, max 6 корзин, 450 или 600 мм)', '6', 960);
INSERT INTO `rf_komplektacija` VALUES (695, 'Рамка для двойной выдвижной корзины H=1740 (рамки для внутр. корзин и дверных, max кол-во 6 шт одного вида,450 или 600мм )', '6', 2120);
INSERT INTO `rf_komplektacija` VALUES (696, 'Газовая пружина для рамки                (рекомендовано использовать со структурой для двойной выдвижной корзины)  ', '6', 160);
INSERT INTO `rf_komplektacija` VALUES (697, 'Внутреняя решетчатая корзина                                                 (для двери модуля)', '6', 210);
INSERT INTO `rf_komplektacija` VALUES (698, 'Решетчатая корзина                                             (подходит для структуры для дверей модуля и для структуры для двойной выдвижной корзины)', '6', 195);
INSERT INTO `rf_komplektacija` VALUES (699, 'Выдвижной механизм для базы со створкой 450 правая', '6', 3200);
INSERT INTO `rf_komplektacija` VALUES (700, 'Газовая пружина для выдвижного механизма(рекомендовано использовать с выдвижным механизмом для базы со створкой  45)', '6', 132);
INSERT INTO `rf_komplektacija` VALUES (701, 'Механизм Fly Moon для угл.базы 600мм на 2 полки прав/лев', '6', 1575);
INSERT INTO `rf_komplektacija` VALUES (702, 'Механизм Fly Moon для угл.базы 600мм на 4 полки прав/лев', '6', 2225);
INSERT INTO `rf_komplektacija` VALUES (703, 'Выдвижные полки Fly Moon для механизма 600мм, H=60мм прав/лев', '6', 1745);
INSERT INTO `rf_komplektacija` VALUES (704, 'Выдвижные полки  Fly Moon для механизма 600мм, H=40мм прав/лев', '6', 1730);
INSERT INTO `rf_komplektacija` VALUES (705, 'Ящик выдвижной H=160', '6', 525);
INSERT INTO `rf_komplektacija` VALUES (706, 'Ящик выдвижной H=160', '6', 550);
INSERT INTO `rf_komplektacija` VALUES (707, 'Ящик выдвижной H=160', '6', 655);
INSERT INTO `rf_komplektacija` VALUES (708, 'Ящик выдвижной H=150 межмодульный', '6', 550);
INSERT INTO `rf_komplektacija` VALUES (709, 'Ящик выдвижной H=150 межмодульный', '6', 655);
INSERT INTO `rf_komplektacija` VALUES (710, 'Ящик выдвижной для посуды H=159', '6', 745);
INSERT INTO `rf_komplektacija` VALUES (711, 'Ящик выдвижной для посуды H=159', '6', 935);
INSERT INTO `rf_komplektacija` VALUES (712, 'Корзина под мойку', '6', 755);
INSERT INTO `rf_komplektacija` VALUES (713, 'Комплект креплений фасада на ящик выдвижной', '6', 30);
INSERT INTO `rf_komplektacija` VALUES (714, 'Подьемно-спускная полка для  шкафа 277', '6', 910);
INSERT INTO `rf_komplektacija` VALUES (715, 'Подьемно-спускная полка для  шкафа 277', '6', 1280);
INSERT INTO `rf_komplektacija` VALUES (716, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', 90);
INSERT INTO `rf_komplektacija` VALUES (717, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', 210);
INSERT INTO `rf_komplektacija` VALUES (718, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', 260);
INSERT INTO `rf_komplektacija` VALUES (719, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', 335);
INSERT INTO `rf_komplektacija` VALUES (720, 'Полка стеклянная для кухонных шкафов 480', '6', 215);
INSERT INTO `rf_komplektacija` VALUES (721, 'Полка стеклянная для кухонных шкафов 480', '6', 280);
INSERT INTO `rf_komplektacija` VALUES (722, 'Полка стеклянная для кухонных шкафов 480', '6', 460);
INSERT INTO `rf_komplektacija` VALUES (723, 'Полка решетчатая для кухонных шкафов 479', '6', 230);
INSERT INTO `rf_komplektacija` VALUES (724, 'Полка решетчатая для кухонных шкафов 480', '6', 300);
INSERT INTO `rf_komplektacija` VALUES (725, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', 220);
INSERT INTO `rf_komplektacija` VALUES (726, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', 270);
INSERT INTO `rf_komplektacija` VALUES (727, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', 355);
INSERT INTO `rf_komplektacija` VALUES (728, 'Комплект креплений для полок решетчатых       (4 крепежных элемента + евровинты)', '6', 15);
INSERT INTO `rf_komplektacija` VALUES (729, 'Подставка для бутылок , для крепления к полке решетчетой (подходит только к полке решетчатой)', '6', 385);
INSERT INTO `rf_komplektacija` VALUES (730, 'Корзина для крепления к полке решетчетой (подходит только к полке решетчатой)', '6', 170);
INSERT INTO `rf_komplektacija` VALUES (731, 'Сортер для мусора', '6', 480);
INSERT INTO `rf_komplektacija` VALUES (732, 'Сортер с крышкой для модуля 450 правая (с опрокидывающимися крышками)', '6', 1730);
INSERT INTO `rf_komplektacija` VALUES (733, 'Сортер с крышкой для модуля 450 левая (с опрокидывающимися крышками)', '6', 1730);
INSERT INTO `rf_komplektacija` VALUES (734, 'Сортер с поворотной системой                                          (для углового модудуля 900х900, с амортизированным возвратом)', '6', 2180);
INSERT INTO `rf_komplektacija` VALUES (735, ' Cортер в модуль 450 на 3 ведра с доводчиком', '6', 1025);
INSERT INTO `rf_komplektacija` VALUES (736, ' Cортер в модуль 300 на 2 ведра с доводчиком', '6', 665);
INSERT INTO `rf_komplektacija` VALUES (737, 'Сортер в модуль 600мм, на 4 ведра с доводчиком', '6', 1265);
INSERT INTO `rf_komplektacija` VALUES (738, 'Поворотная система для угловой базы (для углового модудуля 900х900, 2 алюминиевых конетейнера, 2 решетчатых контейнера и карусель 3/4)', '6', 3850);
INSERT INTO `rf_komplektacija` VALUES (739, 'Гофрированный коврик 1,5мм', '6', 75);
INSERT INTO `rf_komplektacija` VALUES (740, 'Гофрированный коврик 2,5мм', '6', 100);
INSERT INTO `rf_komplektacija` VALUES (741, 'Релинг d=16 с квадратными опорами (цвет алюминий)', '6', 155);
INSERT INTO `rf_komplektacija` VALUES (742, 'Заглушка для релинга, плоская', '6', 11);
INSERT INTO `rf_komplektacija` VALUES (743, 'Крючки одинарные для релинга (в к-те 5шт.) ', '6', 40);
INSERT INTO `rf_komplektacija` VALUES (744, 'Полка угловая (устанавливается с любой стороны: прав.=лев.)', '6', 400);
INSERT INTO `rf_komplektacija` VALUES (745, 'Полка', '6', 355);
INSERT INTO `rf_komplektacija` VALUES (746, 'Комплект для специй', '6', 415);
INSERT INTO `rf_komplektacija` VALUES (747, 'Держатель для рулонов', '6', 365);
INSERT INTO `rf_komplektacija` VALUES (748, 'Держатель для бумаги', '6', 265);
INSERT INTO `rf_komplektacija` VALUES (749, 'Релинг d=16 с круглыми опорами (цвет хром)', '6', 160);
INSERT INTO `rf_komplektacija` VALUES (750, 'Заглушка для релинга ', '6', 25);
INSERT INTO `rf_komplektacija` VALUES (751, 'Заглушка для релинга, плоская', '6', 10);
INSERT INTO `rf_komplektacija` VALUES (752, 'Крючки одинарные для релинга (в к-те 5шт.) ', '6', 40);
INSERT INTO `rf_komplektacija` VALUES (753, ' Крючок-мини несъемный для релинга (в к-те 5шт.) ', '6', 50);
INSERT INTO `rf_komplektacija` VALUES (754, 'Полка угловая решетчатая (устанавливается с любой стороны: прав.=лев.)', '6', 180);
INSERT INTO `rf_komplektacija` VALUES (755, 'Полка решетчатая ', '6', 195);
INSERT INTO `rf_komplektacija` VALUES (756, 'Комплект для специй решетчатый', '6', 135);
INSERT INTO `rf_komplektacija` VALUES (757, 'Держатель для рулонов', '6', 180);
INSERT INTO `rf_komplektacija` VALUES (758, 'Держатель для бумаги', '6', 160);
INSERT INTO `rf_komplektacija` VALUES (759, 'Держатель для бокалов', '6', 145);
INSERT INTO `rf_komplektacija` VALUES (760, 'Подставка для бутылок', '6', 145);
INSERT INTO `rf_komplektacija` VALUES (761, 'Полка для аксессуаров', '6', 215);
INSERT INTO `rf_komplektacija` VALUES (762, 'Навесная сушка для посуды (с поддоном из нержавеющей стали AISI 304)', '6', 570);
INSERT INTO `rf_komplektacija` VALUES (763, 'Подставка для столовых приборов c контейнером из нержавеющей стали AISI 304', '6', 420);
INSERT INTO `rf_komplektacija` VALUES (764, 'Плінтус RAUWALON MAGIC 127 алюмінієвий', '7', 190);
INSERT INTO `rf_komplektacija` VALUES (765, 'Плінтус RAUWALON 116 алюмінієвий ', '7', 120);
INSERT INTO `rf_komplektacija` VALUES (766, 'Плінтус пустишка RAUWALON Basic 104', '7', 130);
INSERT INTO `rf_komplektacija` VALUES (767, 'Плінтус RAUWALON 118 (колір стільниці EGGER)', '7', 160);
INSERT INTO `rf_komplektacija` VALUES (768, 'Кути (закінчення) плінтуса RAUWALON ', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (769, 'Плінтус SCILM алюміній Н=45мм', '7', 155);
INSERT INTO `rf_komplektacija` VALUES (770, 'Плінтус SCILM алюміній Н=45мм рифлений', '7', 170);
INSERT INTO `rf_komplektacija` VALUES (771, 'Кути зовн/внутр 90/135?  плінтуса алюмінієвого SCILM Н=45мм (сірі)', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (772, 'Закінчення  плінтуса алюмінієвого SCILM Н=45мм (сірі)', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (773, 'Плінтус SCILM алюміній прямокутний Н=30мм колір: алюм, алюм браш, золото, білий, чорний браш. Основа-біла, сіра.', '7', 140);
INSERT INTO `rf_komplektacija` VALUES (774, 'Плінтус SCILM алюміній прямокутний рифлений Н=30мм', '7', 150);
INSERT INTO `rf_komplektacija` VALUES (775, 'Отбортовка под пластиковую вставку Н=30 L=4000 (основа - біла, коричнева)', '7', 100);
INSERT INTO `rf_komplektacija` VALUES (776, 'Кути зовн/внутр 90/135  плінтуса Н=30мм SCILM (сірі, білі, коричневі)', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (777, 'Кути, закінчення  плінтуса SCILM Н=30мм (сірі, білі, чорні)', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (778, 'Цоколь пластиковий Н=100мм під алюміній', '7', 150);
INSERT INTO `rf_komplektacija` VALUES (779, 'Цоколь пластиковий Н=100мм під алюміній рифлений', '7', 150);
INSERT INTO `rf_komplektacija` VALUES (780, 'Цоколь пластиковий Н=150мм', '7', 250);
INSERT INTO `rf_komplektacija` VALUES (781, 'Цоколь алюмінієвий Н=100мм (колір алюміній, алюміній BRUSH)', '7', 200);
INSERT INTO `rf_komplektacija` VALUES (782, 'Цоколь алюмінієвий Н=100мм (колір золото, глянцеве золото)', '7', 200);
INSERT INTO `rf_komplektacija` VALUES (783, 'Цоколь алюмінієвий Н=100мм (колір білий, чорний BRUSH)', '7', 250);
INSERT INTO `rf_komplektacija` VALUES (784, 'Кут до цоколя алюм. Н=100мм (90°, 270°)', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (785, 'Заглушка до цоколя алюм Н=100мм', '7', 15);
INSERT INTO `rf_komplektacija` VALUES (786, 'Кутик регулюємий для алюмінієвого цоколя Н=100мм new', '7', 30);
INSERT INTO `rf_komplektacija` VALUES (787, 'Кутик 90°, 270° універсальний до пластикового цоколя', '7', 20);
INSERT INTO `rf_komplektacija` VALUES (788, 'Кутик 135°, 315° універсальний до пластикового цоколя', '7', 25);
INSERT INTO `rf_komplektacija` VALUES (789, 'Заглушка до пластикового цоколя', '7', 5);
INSERT INTO `rf_komplektacija` VALUES (790, 'Решітка в цоколь вентиляційна алюм(пластик)', '7', 25);
INSERT INTO `rf_komplektacija` VALUES (791, 'Ущільнювач під цоколь ДСП пластиковий (прозорий)', '7', 25);
INSERT INTO `rf_komplektacija` VALUES (792, 'Торцовка стільниці профіль подвійний Н=40 L=3900 (алюміній)', '7', 350);
INSERT INTO `rf_komplektacija` VALUES (793, 'Торцовка стільниці профіль подвійний Н=40 L=3900 (під нержавійку)', '7', 500);
INSERT INTO `rf_komplektacija` VALUES (794, 'Торцовка стільниці кут внутрішній 90°, 135°, зовнішній 270°, 315°', '7', 150);
INSERT INTO `rf_komplektacija` VALUES (795, 'Торцовка стільниці кут внутрішній 90°, 135°', '7', 190);
INSERT INTO `rf_komplektacija` VALUES (796, ' VIBO cушка 45см з регул рамою', '8', 340);
INSERT INTO `rf_komplektacija` VALUES (797, 'VIBO cушка 50 см з регул рамою (1 піддон металевий)', '8', 770);
INSERT INTO `rf_komplektacija` VALUES (798, 'VIBO cушка 60 см з регул рамою', '8', 360);
INSERT INTO `rf_komplektacija` VALUES (799, 'VIBO cушка 80 см з регул рамою', '8', 410);
INSERT INTO `rf_komplektacija` VALUES (800, 'VIBO cушка 90 см з регул рамою', '8', 420);
INSERT INTO `rf_komplektacija` VALUES (801, 'VIBO cушка 50 см', '8', 770);
INSERT INTO `rf_komplektacija` VALUES (802, 'VIBO cушка 60 см', '8', 300);
INSERT INTO `rf_komplektacija` VALUES (803, 'VIBO cушка 80 см', '8', 325);
INSERT INTO `rf_komplektacija` VALUES (804, 'VIBO cушка 90 см', '8', 340);
INSERT INTO `rf_komplektacija` VALUES (805, 'VIBO cушка 1200мм з регул рамою (1 піддон металевий)', '8', 925);
INSERT INTO `rf_komplektacija` VALUES (806, 'VIBO cушка кутова рама (верхня кутова секція 600*600мм)', '8', 1360);
INSERT INTO `rf_komplektacija` VALUES (807, 'REJS cушка 40 см рама регульована', '8', 130);
INSERT INTO `rf_komplektacija` VALUES (808, 'REJS cушка 50 см рама регульована', '8', 150);
INSERT INTO `rf_komplektacija` VALUES (809, 'REJS cушка 60 см рама регульована', '8', 160);
INSERT INTO `rf_komplektacija` VALUES (810, 'REJS cушка 70 см рама регульована', '8', 250);
INSERT INTO `rf_komplektacija` VALUES (811, 'REJS cушка 80 см рама регульована', '8', 180);
INSERT INTO `rf_komplektacija` VALUES (812, 'REJS cушка 90 см рама регульована', '8', 205);
INSERT INTO `rf_komplektacija` VALUES (813, 'РГ 32', '10', 27);
INSERT INTO `rf_komplektacija` VALUES (814, 'РГ 31', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (815, 'РГ 50', '10', 16);
INSERT INTO `rf_komplektacija` VALUES (816, 'РГ 49', '10', 18);
INSERT INTO `rf_komplektacija` VALUES (817, 'РГ 30', '10', 43);
INSERT INTO `rf_komplektacija` VALUES (818, 'РГ 285', '10', 43);
INSERT INTO `rf_komplektacija` VALUES (819, 'РГ 51', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (820, 'РГ 282', '10', 60);
INSERT INTO `rf_komplektacija` VALUES (821, 'РГ 283', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (822, 'РГ 284', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (823, 'РГ 34', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (824, 'РГ 33', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (825, 'РГ 54', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (826, 'РГ 53', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (827, 'РГ 35', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (828, 'РГ 295', '10', 28);
INSERT INTO `rf_komplektacija` VALUES (829, 'РГ 55', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (830, 'РГ 36', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (831, 'РГ 296', '10', 28);
INSERT INTO `rf_komplektacija` VALUES (832, 'РГ 56', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (833, 'РГ 210', '10', 50);
INSERT INTO `rf_komplektacija` VALUES (834, 'РГ 211', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (835, 'РГ 212', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (836, 'РГ 286', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (837, 'РГ 287', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (838, 'РГ 288', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (839, 'РГ 289', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (840, 'РГ 290', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (841, 'РГ 291', '10', 27);
INSERT INTO `rf_komplektacija` VALUES (842, 'РГ 292', '10', 46);
INSERT INTO `rf_komplektacija` VALUES (843, 'РГ 293', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (844, 'РГ 294', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (845, 'РГ 38', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (846, 'РГ 331', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (847, 'РГ 58', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (848, 'РГ 37', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (849, 'РГ 332', '10', 42);
INSERT INTO `rf_komplektacija` VALUES (850, 'РГ 57', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (851, 'РГ 322', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (852, 'РГ 323', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (853, 'РГ 324', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (854, 'РГ 325', '10', 43);
INSERT INTO `rf_komplektacija` VALUES (855, 'РГ 326', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (856, 'РГ 327', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (857, 'РГ 328', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (858, 'РГ 329', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (859, 'РГ 330', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (860, 'РГ 40', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (861, 'РГ 276', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (862, 'РГ 42', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (863, 'РГ 60', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (864, 'РГ 39', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (865, 'РГ 277', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (866, 'РГ 41', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (867, 'РГ 59', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (868, 'РГ 278 ', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (869, 'РГ 279 ', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (870, 'РГ 281 ', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (871, 'РГ 280 ', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (872, 'РГ 302', '10', 49);
INSERT INTO `rf_komplektacija` VALUES (873, 'РГ 303', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (874, 'РГ 304', '10', 28);
INSERT INTO `rf_komplektacija` VALUES (875, 'РГ 305', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (876, 'РГ 306', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (877, 'РГ 307', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (878, 'РГ 308', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (879, 'РГ 309', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (880, 'РГ 310', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (881, 'РГ 311', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (882, 'РГ 312', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (883, 'РГ 313', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (884, 'РГ 411', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (885, 'РГ 412', '10', 27);
INSERT INTO `rf_komplektacija` VALUES (886, 'РГ 413', '10', 22);
INSERT INTO `rf_komplektacija` VALUES (887, 'РГ 414', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (888, 'РГ 4', '10', 63);
INSERT INTO `rf_komplektacija` VALUES (889, 'РГ 22', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (890, 'РГ 5', '10', 67);
INSERT INTO `rf_komplektacija` VALUES (891, 'РГ 24', '10', 52);
INSERT INTO `rf_komplektacija` VALUES (892, 'РГ 121', '10', 75);
INSERT INTO `rf_komplektacija` VALUES (893, 'РГ 122', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (894, 'РГ 7', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (895, 'РГ 21', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (896, 'РГ 6', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (897, 'РГ 20', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (898, 'РГ 8', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (899, 'РГ 23', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (900, 'РГ 9', '10', 65);
INSERT INTO `rf_komplektacija` VALUES (901, 'РГ 25', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (902, 'РГ 10', '10', 66);
INSERT INTO `rf_komplektacija` VALUES (903, 'РГ 16', '10', 22);
INSERT INTO `rf_komplektacija` VALUES (904, 'РГ 11', '10', 65);
INSERT INTO `rf_komplektacija` VALUES (905, 'РГ 17', '10', 19);
INSERT INTO `rf_komplektacija` VALUES (906, 'РГ 18', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (907, 'РГ 165 ', '10', 22);
INSERT INTO `rf_komplektacija` VALUES (908, 'РГ 12', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (909, 'РГ 14', '10', 19);
INSERT INTO `rf_komplektacija` VALUES (910, 'РГ 19', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (911, 'РГ 13', '10', 61);
INSERT INTO `rf_komplektacija` VALUES (912, 'РГ 15', '10', 18);
INSERT INTO `rf_komplektacija` VALUES (913, 'РГ 26', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (914, 'РГ 27', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (915, 'РГ 28', '10', 29);
INSERT INTO `rf_komplektacija` VALUES (916, 'РГ 29', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (917, 'РГ 115', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (918, 'РГ 116', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (919, 'РГ 117', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (920, 'РГ 118', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (921, 'РГ 409', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (922, 'РГ 410', '10', 91);
INSERT INTO `rf_komplektacija` VALUES (923, 'РГ 52', '10', 13);
INSERT INTO `rf_komplektacija` VALUES (924, 'РГ 73', '10', 76);
INSERT INTO `rf_komplektacija` VALUES (925, 'РГ 74', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (926, 'РГ 77', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (927, 'РГ 78', '10', 49);
INSERT INTO `rf_komplektacija` VALUES (928, 'РГ 75', '10', 65);
INSERT INTO `rf_komplektacija` VALUES (929, 'РГ 76', '10', 58);
INSERT INTO `rf_komplektacija` VALUES (930, 'РГ 79', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (931, 'РГ 80', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (932, 'РГ 81', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (933, 'РГ 82', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (934, 'РГ 83', '10', 85);
INSERT INTO `rf_komplektacija` VALUES (935, 'РГ 84', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (936, 'РГ 85', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (937, 'РГ 86', '10', 49);
INSERT INTO `rf_komplektacija` VALUES (938, 'РГ 87', '10', 73);
INSERT INTO `rf_komplektacija` VALUES (939, 'РГ 108', '10', 92);
INSERT INTO `rf_komplektacija` VALUES (940, 'РГ 109', '10', 79);
INSERT INTO `rf_komplektacija` VALUES (941, 'РГ 110', '10', 43);
INSERT INTO `rf_komplektacija` VALUES (942, 'РГ 113', '10', 46);
INSERT INTO `rf_komplektacija` VALUES (943, 'РГ 114', '10', 62);
INSERT INTO `rf_komplektacija` VALUES (944, 'РГ 172', '10', 44);
INSERT INTO `rf_komplektacija` VALUES (945, 'РГ 173', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (946, 'РГ 2', '10', 27);
INSERT INTO `rf_komplektacija` VALUES (947, 'РГ 1', '10', 12);
INSERT INTO `rf_komplektacija` VALUES (948, 'РГ 3', '10', 7);
INSERT INTO `rf_komplektacija` VALUES (949, 'РГ 333', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (950, 'РГ 334', '10', 23);
INSERT INTO `rf_komplektacija` VALUES (951, 'РГ 335', '10', 15);
INSERT INTO `rf_komplektacija` VALUES (952, 'РГ 336', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (953, 'РГ 337', '10', 9);
INSERT INTO `rf_komplektacija` VALUES (954, 'РГ 338', '10', 11);
INSERT INTO `rf_komplektacija` VALUES (955, 'РГ 340', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (956, 'РГ 341', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (957, 'РГ 107', '10', 22);
INSERT INTO `rf_komplektacija` VALUES (958, 'РГ 99', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (959, 'РГ 92', '10', 12);
INSERT INTO `rf_komplektacija` VALUES (960, 'РГ 102', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (961, 'РГ 90', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (962, 'РГ 91', '10', 29);
INSERT INTO `rf_komplektacija` VALUES (963, 'РГ 95', '10', 15);
INSERT INTO `rf_komplektacija` VALUES (964, 'РГ 96', '10', 14);
INSERT INTO `rf_komplektacija` VALUES (965, 'РГ 97', '10', 42);
INSERT INTO `rf_komplektacija` VALUES (966, 'РГ 98', '10', 42);
INSERT INTO `rf_komplektacija` VALUES (967, 'РГ 105', '10', 16);
INSERT INTO `rf_komplektacija` VALUES (968, 'РГ 106', '10', 13);
INSERT INTO `rf_komplektacija` VALUES (969, 'РГ 100', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (970, 'РГ 101', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (971, 'РГ 88', '10', 23);
INSERT INTO `rf_komplektacija` VALUES (972, 'РГ 89', '10', 18);
INSERT INTO `rf_komplektacija` VALUES (973, 'РГ 111', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (974, 'РГ 112', '10', 17);
INSERT INTO `rf_komplektacija` VALUES (975, 'РГ 119', '10', 29);
INSERT INTO `rf_komplektacija` VALUES (976, 'РГ 320', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (977, 'РГ 120', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (978, 'РГ 321', '10', 50);
INSERT INTO `rf_komplektacija` VALUES (979, 'РГ 314 ', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (980, 'РГ 315', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (981, 'РГ 316', '10', 31);
INSERT INTO `rf_komplektacija` VALUES (982, 'РГ 317', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (983, 'РГ 318', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (984, 'РГ 319', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (985, 'РГ 253 ', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (986, 'РГ 254 ', '10', 63);
INSERT INTO `rf_komplektacija` VALUES (987, 'РГ 255 ', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (988, 'РГ 256', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (989, 'РГ 257 ', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (990, 'РГ 258 ', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (991, 'РГ 259 ', '10', 44);
INSERT INTO `rf_komplektacija` VALUES (992, 'РГ 260 ', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (993, 'РГ 261', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (994, 'РГ 262', '10', 20);
INSERT INTO `rf_komplektacija` VALUES (995, 'РГ 263', '10', 123);
INSERT INTO `rf_komplektacija` VALUES (996, 'РГ 264', '10', 98);
INSERT INTO `rf_komplektacija` VALUES (997, 'РГ 265', '10', 89);
INSERT INTO `rf_komplektacija` VALUES (998, 'РГ 266', '10', 73);
INSERT INTO `rf_komplektacija` VALUES (999, 'РГ 267', '10', 53);
INSERT INTO `rf_komplektacija` VALUES (1000, 'РГ 103', '10', 10);
INSERT INTO `rf_komplektacija` VALUES (1001, 'РГ 93', '10', 9);
INSERT INTO `rf_komplektacija` VALUES (1002, 'РГ 104', '10', 7);
INSERT INTO `rf_komplektacija` VALUES (1003, 'РГ 94', '10', 9);
INSERT INTO `rf_komplektacija` VALUES (1004, 'РГ 123', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (1005, 'РГ 300', '10', 52);
INSERT INTO `rf_komplektacija` VALUES (1006, 'РГ 125', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (1007, 'РГ 124', '10', 61);
INSERT INTO `rf_komplektacija` VALUES (1008, 'РГ 301', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (1009, 'РГ 126', '10', 36);
INSERT INTO `rf_komplektacija` VALUES (1010, 'РГ 297', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (1011, 'РГ 298', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (1012, 'РГ 299', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (1013, 'РГ 127', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (1014, 'РГ 128', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (1015, 'РГ 129', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1016, 'РГ 133', '10', 99);
INSERT INTO `rf_komplektacija` VALUES (1017, 'РГ 130', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (1018, 'РГ 134', '10', 70);
INSERT INTO `rf_komplektacija` VALUES (1019, 'РГ 131', '10', 96);
INSERT INTO `rf_komplektacija` VALUES (1020, 'РГ 135', '10', 107);
INSERT INTO `rf_komplektacija` VALUES (1021, 'РГ 132', '10', 60);
INSERT INTO `rf_komplektacija` VALUES (1022, 'РГ 136', '10', 75);
INSERT INTO `rf_komplektacija` VALUES (1023, 'РГ 192', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1024, 'РГ 193', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (1025, 'РГ 194', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (1026, 'РГ 195', '10', 62);
INSERT INTO `rf_komplektacija` VALUES (1027, 'РГ 196', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (1028, 'РГ 197', '10', 57);
INSERT INTO `rf_komplektacija` VALUES (1029, 'РГ 198', '10', 77);
INSERT INTO `rf_komplektacija` VALUES (1030, 'РГ 199', '10', 68);
INSERT INTO `rf_komplektacija` VALUES (1031, 'РГ 200', '10', 67);
INSERT INTO `rf_komplektacija` VALUES (1032, 'РГ 201', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (1033, 'РГ 202', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (1034, 'РГ 203', '10', 23);
INSERT INTO `rf_komplektacija` VALUES (1035, 'РГ 204', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1036, 'РГ 205', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (1037, 'РГ 206', '10', 24);
INSERT INTO `rf_komplektacija` VALUES (1038, 'РГ 207', '10', 16);
INSERT INTO `rf_komplektacija` VALUES (1039, 'РГ 208', '10', 36);
INSERT INTO `rf_komplektacija` VALUES (1040, 'РГ 209', '10', 25);
INSERT INTO `rf_komplektacija` VALUES (1041, 'РГ 213', '10', 42);
INSERT INTO `rf_komplektacija` VALUES (1042, 'РГ 214', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (1043, 'РГ 215', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (1044, 'РГ 216', '10', 18);
INSERT INTO `rf_komplektacija` VALUES (1045, 'РГ 217', '10', 88);
INSERT INTO `rf_komplektacija` VALUES (1046, 'РГ 218', '10', 71);
INSERT INTO `rf_komplektacija` VALUES (1047, 'РГ 219', '10', 55);
INSERT INTO `rf_komplektacija` VALUES (1048, 'РГ 220', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (1049, 'РГ 248', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1050, 'РГ 249', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1051, 'РГ 250', '10', 66);
INSERT INTO `rf_komplektacija` VALUES (1052, 'РГ 251', '10', 66);
INSERT INTO `rf_komplektacija` VALUES (1053, 'РГ 252', '10', 44);
INSERT INTO `rf_komplektacija` VALUES (1054, 'РГ 221', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (1055, 'РГ 222', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (1056, 'РГ 223', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (1057, 'РГ 224', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (1058, 'РГ 225', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (1059, 'РГ 226', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (1060, 'РГ 227', '10', 62);
INSERT INTO `rf_komplektacija` VALUES (1061, 'РГ 228', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (1062, 'РГ 229', '10', 30);
INSERT INTO `rf_komplektacija` VALUES (1063, 'РГ 230', '10', 32);
INSERT INTO `rf_komplektacija` VALUES (1064, 'РГ 231', '10', 61);
INSERT INTO `rf_komplektacija` VALUES (1065, 'РГ 232', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (1066, 'РГ 239', '10', 49);
INSERT INTO `rf_komplektacija` VALUES (1067, 'РГ 240', '10', 41);
INSERT INTO `rf_komplektacija` VALUES (1068, 'РГ 241', '10', 19);
INSERT INTO `rf_komplektacija` VALUES (1069, 'РГ 242', '10', 128);
INSERT INTO `rf_komplektacija` VALUES (1070, 'РГ 243', '10', 107);
INSERT INTO `rf_komplektacija` VALUES (1071, 'РГ 244', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (1072, 'РГ 245', '10', 140);
INSERT INTO `rf_komplektacija` VALUES (1073, 'РГ 246', '10', 112);
INSERT INTO `rf_komplektacija` VALUES (1074, 'РГ 247', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (1075, 'РГ 345', '10', 53);
INSERT INTO `rf_komplektacija` VALUES (1076, 'РГ 346', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (1077, 'РГ 347', '10', 26);
INSERT INTO `rf_komplektacija` VALUES (1078, 'РГ 348', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1079, 'РГ 349', '10', 56);
INSERT INTO `rf_komplektacija` VALUES (1080, 'РГ 350', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (1081, 'РГ 342', '10', 98);
INSERT INTO `rf_komplektacija` VALUES (1082, 'РГ 343', '10', 74);
INSERT INTO `rf_komplektacija` VALUES (1083, 'РГ 344', '10', 54);
INSERT INTO `rf_komplektacija` VALUES (1084, 'РГ 366', '10', 74);
INSERT INTO `rf_komplektacija` VALUES (1085, 'РГ 367', '10', 50);
INSERT INTO `rf_komplektacija` VALUES (1086, 'РГ 368', '10', 37);
INSERT INTO `rf_komplektacija` VALUES (1087, 'РГ 369', '10', 36);
INSERT INTO `rf_komplektacija` VALUES (1088, 'РГ 370', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (1089, 'РГ 351', '10', 160);
INSERT INTO `rf_komplektacija` VALUES (1090, 'РГ 352', '10', 109);
INSERT INTO `rf_komplektacija` VALUES (1091, 'РГ 353', '10', 73);
INSERT INTO `rf_komplektacija` VALUES (1092, 'РГ 354', '10', 73);
INSERT INTO `rf_komplektacija` VALUES (1093, 'РГ 355', '10', 53);
INSERT INTO `rf_komplektacija` VALUES (1094, 'РГ 356', '10', 177);
INSERT INTO `rf_komplektacija` VALUES (1095, 'РГ 357', '10', 118);
INSERT INTO `rf_komplektacija` VALUES (1096, 'РГ 358', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1097, 'РГ 359', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1098, 'РГ 360', '10', 58);
INSERT INTO `rf_komplektacija` VALUES (1099, 'РГ 361', '10', 76);
INSERT INTO `rf_komplektacija` VALUES (1100, 'РГ 362', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (1101, 'РГ 363', '10', 36);
INSERT INTO `rf_komplektacija` VALUES (1102, 'РГ 365', '10', 21);
INSERT INTO `rf_komplektacija` VALUES (1103, 'РГ 406', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (1104, 'РГ 407', '10', 33);
INSERT INTO `rf_komplektacija` VALUES (1105, 'РГ 408', '10', 23);
INSERT INTO `rf_komplektacija` VALUES (1106, 'РГ 400', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (1107, 'РГ 401', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (1108, 'РГ 402', '10', 29);
INSERT INTO `rf_komplektacija` VALUES (1109, 'РГ 403', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (1110, 'РГ 404', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (1111, 'РГ 405', '10', 29);
INSERT INTO `rf_komplektacija` VALUES (1112, 'РГ 233', '10', 313);
INSERT INTO `rf_komplektacija` VALUES (1113, 'РГ 234', '10', 222);
INSERT INTO `rf_komplektacija` VALUES (1114, 'РГ 155', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1115, 'РГ 157', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1116, 'РГ 158', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1117, 'РГ 156', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1118, 'РГ 159', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (1119, 'РГ 162', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (1120, 'РГ 161', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (1121, 'РГ 160', '10', 38);
INSERT INTO `rf_komplektacija` VALUES (1122, 'РГ 147', '10', 111);
INSERT INTO `rf_komplektacija` VALUES (1123, 'РГ 151', '10', 111);
INSERT INTO `rf_komplektacija` VALUES (1124, 'РГ 149', '10', 111);
INSERT INTO `rf_komplektacija` VALUES (1125, 'РГ 153', '10', 111);
INSERT INTO `rf_komplektacija` VALUES (1126, 'РГ 148', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1127, 'РГ 152', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1128, 'РГ 150', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1129, 'РГ 154', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1130, 'РГ 145 ', '10', 141);
INSERT INTO `rf_komplektacija` VALUES (1131, 'РГ 146 ', '10', 141);
INSERT INTO `rf_komplektacija` VALUES (1132, 'РГ 163', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (1133, 'РГ 164 ', '10', 59);
INSERT INTO `rf_komplektacija` VALUES (1134, 'РГ 44', '10', 210);
INSERT INTO `rf_komplektacija` VALUES (1135, 'РГ 167', '10', 287);
INSERT INTO `rf_komplektacija` VALUES (1136, 'РГ 68', '10', 235);
INSERT INTO `rf_komplektacija` VALUES (1137, 'РГ 62', '10', 96);
INSERT INTO `rf_komplektacija` VALUES (1138, 'РГ 175', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (1139, 'РГ 43', '10', 202);
INSERT INTO `rf_komplektacija` VALUES (1140, 'РГ 166', '10', 281);
INSERT INTO `rf_komplektacija` VALUES (1141, 'РГ 67', '10', 248);
INSERT INTO `rf_komplektacija` VALUES (1142, 'РГ 61', '10', 101);
INSERT INTO `rf_komplektacija` VALUES (1143, 'РГ 174', '10', 42);
INSERT INTO `rf_komplektacija` VALUES (1144, 'РГ 45', '10', 199);
INSERT INTO `rf_komplektacija` VALUES (1145, 'РГ 168', '10', 279);
INSERT INTO `rf_komplektacija` VALUES (1146, 'РГ 69', '10', 241);
INSERT INTO `rf_komplektacija` VALUES (1147, 'РГ 63', '10', 101);
INSERT INTO `rf_komplektacija` VALUES (1148, 'РГ 176', '10', 43);
INSERT INTO `rf_komplektacija` VALUES (1149, 'РГ 46', '10', 214);
INSERT INTO `rf_komplektacija` VALUES (1150, 'РГ 169', '10', 289);
INSERT INTO `rf_komplektacija` VALUES (1151, 'РГ 70', '10', 238);
INSERT INTO `rf_komplektacija` VALUES (1152, 'РГ 64', '10', 99);
INSERT INTO `rf_komplektacija` VALUES (1153, 'РГ 177', '10', 44);
INSERT INTO `rf_komplektacija` VALUES (1154, 'РГ 48', '10', 102);
INSERT INTO `rf_komplektacija` VALUES (1155, 'РГ 171', '10', 120);
INSERT INTO `rf_komplektacija` VALUES (1156, 'РГ 72', '10', 94);
INSERT INTO `rf_komplektacija` VALUES (1157, 'РГ 66', '10', 45);
INSERT INTO `rf_komplektacija` VALUES (1158, 'РГ 179', '10', 34);
INSERT INTO `rf_komplektacija` VALUES (1159, 'РГ 47', '10', 80);
INSERT INTO `rf_komplektacija` VALUES (1160, 'РГ 170', '10', 92);
INSERT INTO `rf_komplektacija` VALUES (1161, 'РГ 71', '10', 84);
INSERT INTO `rf_komplektacija` VALUES (1162, 'РГ 65', '10', 53);
INSERT INTO `rf_komplektacija` VALUES (1163, 'РГ 178', '10', 35);
INSERT INTO `rf_komplektacija` VALUES (1164, 'РГ 137', '10', 1934);
INSERT INTO `rf_komplektacija` VALUES (1165, 'РГ 138', '10', 942);
INSERT INTO `rf_komplektacija` VALUES (1166, 'РГ 139', '10', 1934);
INSERT INTO `rf_komplektacija` VALUES (1167, 'РГ 140', '10', 942);
INSERT INTO `rf_komplektacija` VALUES (1168, 'РГ 141', '10', 2975);
INSERT INTO `rf_komplektacija` VALUES (1169, 'РГ 142', '10', 1041);
INSERT INTO `rf_komplektacija` VALUES (1170, 'РГ 143', '10', 2975);
INSERT INTO `rf_komplektacija` VALUES (1171, 'РГ 144', '10', 1041);
INSERT INTO `rf_komplektacija` VALUES (1172, 'РГ 180', '10', 347);
INSERT INTO `rf_komplektacija` VALUES (1173, 'РГ 181', '10', 292);
INSERT INTO `rf_komplektacija` VALUES (1174, 'РГ 182', '10', 262);
INSERT INTO `rf_komplektacija` VALUES (1175, 'РГ 183', '10', 230);
INSERT INTO `rf_komplektacija` VALUES (1176, 'РГ 184', '10', 454);
INSERT INTO `rf_komplektacija` VALUES (1177, 'РГ 185', '10', 418);
INSERT INTO `rf_komplektacija` VALUES (1178, 'РГ 186', '10', 244);
INSERT INTO `rf_komplektacija` VALUES (1179, 'РГ 187', '10', 205);
INSERT INTO `rf_komplektacija` VALUES (1180, 'РГ 188', '10', 146);
INSERT INTO `rf_komplektacija` VALUES (1181, 'РГ 189', '10', 121);
INSERT INTO `rf_komplektacija` VALUES (1182, 'РГ 190', '10', 74);
INSERT INTO `rf_komplektacija` VALUES (1183, 'РГ 191', '10', 51);
INSERT INTO `rf_komplektacija` VALUES (1184, 'РГ 235', '10', 244);
INSERT INTO `rf_komplektacija` VALUES (1185, 'РГ 236', '10', 196);
INSERT INTO `rf_komplektacija` VALUES (1186, 'РГ 237', '10', 125);
INSERT INTO `rf_komplektacija` VALUES (1187, 'РГ 238', '10', 79);
INSERT INTO `rf_komplektacija` VALUES (1188, 'РГ 268 ', '10', 176);
INSERT INTO `rf_komplektacija` VALUES (1189, 'РГ 269', '10', 179);
INSERT INTO `rf_komplektacija` VALUES (1190, 'РГ 270', '10', 135);
INSERT INTO `rf_komplektacija` VALUES (1191, 'РГ 271', '10', 137);
INSERT INTO `rf_komplektacija` VALUES (1192, 'РГ 272', '10', 113);
INSERT INTO `rf_komplektacija` VALUES (1193, 'РГ 273', '10', 117);
INSERT INTO `rf_komplektacija` VALUES (1194, 'РГ 274', '10', 84);
INSERT INTO `rf_komplektacija` VALUES (1195, 'РГ 275', '10', 86);
INSERT INTO `rf_komplektacija` VALUES (1196, 'РГ 374', '10', 151);
INSERT INTO `rf_komplektacija` VALUES (1197, 'РГ 375', '10', 143);
INSERT INTO `rf_komplektacija` VALUES (1198, 'РГ 376', '10', 78);
INSERT INTO `rf_komplektacija` VALUES (1199, 'РГ 377', '10', 103);
INSERT INTO `rf_komplektacija` VALUES (1200, 'РГ 378', '10', 93);
INSERT INTO `rf_komplektacija` VALUES (1201, 'РГ 379', '10', 47);
INSERT INTO `rf_komplektacija` VALUES (1202, 'РГ 380', '10', 83);
INSERT INTO `rf_komplektacija` VALUES (1203, 'РГ 381', '10', 77);
INSERT INTO `rf_komplektacija` VALUES (1204, 'РГ 382', '10', 40);
INSERT INTO `rf_komplektacija` VALUES (1205, 'РГ 371', '10', 139);
INSERT INTO `rf_komplektacija` VALUES (1206, 'РГ 372', '10', 132);
INSERT INTO `rf_komplektacija` VALUES (1207, 'РГ 373', '10', 73);
INSERT INTO `rf_komplektacija` VALUES (1208, 'РГ 383', '10', 137);
INSERT INTO `rf_komplektacija` VALUES (1209, 'РГ 384', '10', 129);
INSERT INTO `rf_komplektacija` VALUES (1210, 'РГ 385', '10', 71);
INSERT INTO `rf_komplektacija` VALUES (1211, 'РГ 392', '10', 100);
INSERT INTO `rf_komplektacija` VALUES (1212, 'РГ 394', '10', 39);
INSERT INTO `rf_komplektacija` VALUES (1213, 'РГ 386', '10', 176);
INSERT INTO `rf_komplektacija` VALUES (1214, 'РГ 387', '10', 138);
INSERT INTO `rf_komplektacija` VALUES (1215, 'РГ 388', '10', 69);
INSERT INTO `rf_komplektacija` VALUES (1216, 'РГ 389', '10', 126);
INSERT INTO `rf_komplektacija` VALUES (1217, 'РГ 390', '10', 108);
INSERT INTO `rf_komplektacija` VALUES (1218, 'РГ 391', '10', 48);
INSERT INTO `rf_komplektacija` VALUES (1219, 'РГ 395', '10', 158);
INSERT INTO `rf_komplektacija` VALUES (1220, 'РГ 396', '10', 124);
INSERT INTO `rf_komplektacija` VALUES (1221, 'РГ 397', '10', 61);
INSERT INTO `rf_komplektacija` VALUES (1222, 'РГ 398', '10', 242);
INSERT INTO `rf_komplektacija` VALUES (1223, 'РГ 399', '10', 193);
INSERT INTO `rf_komplektacija` VALUES (1224, 'КК-1', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1225, 'КК-2', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1226, 'КК-3', '11', 39);
INSERT INTO `rf_komplektacija` VALUES (1227, 'КК-4', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1228, 'КК-5', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1229, 'КК-6', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1230, 'КК-7', '11', 42);
INSERT INTO `rf_komplektacija` VALUES (1231, 'КК-8', '11', 55);
INSERT INTO `rf_komplektacija` VALUES (1232, 'НК-1', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1233, 'НК-2', '11', 31);
INSERT INTO `rf_komplektacija` VALUES (1234, 'НК-3', '11', 36);
INSERT INTO `rf_komplektacija` VALUES (1235, 'НК-4', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1236, 'ПК-1', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1237, 'ПК-2', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1238, 'ПК-3', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1239, 'ПК-17', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1240, 'ПК-4', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1241, 'ПК-5', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1242, 'ПК-6', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1243, 'ПК-18', '11', 29);
INSERT INTO `rf_komplektacija` VALUES (1244, 'ПК-7', '11', 31);
INSERT INTO `rf_komplektacija` VALUES (1245, 'ПК-8', '11', 33);
INSERT INTO `rf_komplektacija` VALUES (1246, 'ПК-9', '11', 34);
INSERT INTO `rf_komplektacija` VALUES (1247, 'ПК-19', '11', 31);
INSERT INTO `rf_komplektacija` VALUES (1248, 'ПК-10', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1249, 'ПК-11', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1250, 'ПК-12', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1251, 'ПК-20', '11', 29);
INSERT INTO `rf_komplektacija` VALUES (1252, 'ПК-13', '11', 32);
INSERT INTO `rf_komplektacija` VALUES (1253, 'ПК-14', '11', 34);
INSERT INTO `rf_komplektacija` VALUES (1254, 'ПК-15', '11', 39);
INSERT INTO `rf_komplektacija` VALUES (1255, 'ПК-16', '11', 41);
INSERT INTO `rf_komplektacija` VALUES (1256, 'РК-522', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1257, 'РК-523', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1258, 'РК-524', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1259, 'РК-525', '11', 28);
INSERT INTO `rf_komplektacija` VALUES (1260, 'РК-526', '11', 38);
INSERT INTO `rf_komplektacija` VALUES (1261, 'РК-527', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1262, 'РК-528', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1263, 'РК-529', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1264, 'РК-530', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1265, 'РК-531', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1266, 'РК-532', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1267, 'РК-533', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1268, 'РК-534', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1269, 'РК-535', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1270, 'РК-536', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1271, 'РК-537', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1272, 'РК-538', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1273, 'РК-539', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1274, 'РК-540', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1275, 'РК-541', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1276, 'РК-581', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1277, 'РК-582', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1278, 'РК-583', '11', 28);
INSERT INTO `rf_komplektacija` VALUES (1279, 'РК-584', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1280, 'РК-585', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1281, 'РК-586', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1282, 'РК-587', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1283, 'РК-588', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1284, 'РК-590', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1285, 'РК-589', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1286, 'РК-591', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1287, 'РК-592', '11', 33);
INSERT INTO `rf_komplektacija` VALUES (1288, 'РК-594', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1289, 'РК-593', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1290, 'РК-595', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1291, 'РК-596', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1292, 'РК-598', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1293, 'РК-597', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1294, 'РК-599', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1295, 'РК-600', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1296, 'РК-602', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1297, 'РК-601', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1298, 'РК-603', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1299, 'РК-604', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1300, 'РК-606', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1301, 'РК-605', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1302, 'РК-607', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1303, 'РК-608', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1304, 'РК-609', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1305, 'РК-610', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1306, 'РК-611', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1307, 'РК-612', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1308, 'РК-613', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1309, 'РК-614', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1310, 'РК-615', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1311, 'РК-616', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1312, 'РК-617', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1313, 'РК-618', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1314, 'РК-619', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1315, 'РК-620', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1316, 'РК-621', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1317, 'РК-622', '11', 27);
INSERT INTO `rf_komplektacija` VALUES (1318, 'РК-623', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1319, 'РК-624', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1320, 'РК-226', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1321, 'РК-227', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1322, 'РК-228', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1323, 'РК-229', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1324, 'РК-295', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1325, 'РК-296', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1326, 'РК-297', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1327, 'РК-289', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1328, 'РК-290', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1329, 'РК-291', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1330, 'РК-292', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1331, 'РК-293', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1332, 'РК-294', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1333, 'РК-160', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1334, 'РК-163', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1335, 'РК-164', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1336, 'РК-273', '11', 6);
INSERT INTO `rf_komplektacija` VALUES (1337, 'РК-274', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1338, 'РК-275', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1339, 'РК-335', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1340, 'РК-276', '11', 27);
INSERT INTO `rf_komplektacija` VALUES (1341, 'РК-277', '11', 66);
INSERT INTO `rf_komplektacija` VALUES (1342, 'РК-338', '11', 29);
INSERT INTO `rf_komplektacija` VALUES (1343, 'РК-339', '11', 52);
INSERT INTO `rf_komplektacija` VALUES (1344, 'РК-348', '11', 69);
INSERT INTO `rf_komplektacija` VALUES (1345, 'РК-278', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1346, 'РК-279', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1347, 'РК-280', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1348, 'РК-281', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1349, 'РК-282', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1350, 'РК-283', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1351, 'РК-284', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1352, 'РК-285', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1353, 'РК-173', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1354, 'РК-174', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1355, 'РК-175', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1356, 'РК-176', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1357, 'РК-309', '11', 7);
INSERT INTO `rf_komplektacija` VALUES (1358, 'РК-310', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1359, 'РК-311', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1360, 'РК-312', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1361, 'РК-177', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1362, 'РК-178', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1363, 'РК-179', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1364, 'РК-180', '11', 28);
INSERT INTO `rf_komplektacija` VALUES (1365, 'РК-313', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1366, 'РК-314', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1367, 'РК-315', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1368, 'РК-316', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1369, 'РК-181', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1370, 'РК-317', '11', 0);
INSERT INTO `rf_komplektacija` VALUES (1371, 'РК-182', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1372, 'РК-318', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1373, 'РК-434', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1374, 'РК-435', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1375, 'РК-436', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1376, 'РК-437', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1377, 'РК-517', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1378, 'РК-518', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1379, 'РК-519', '11', 7);
INSERT INTO `rf_komplektacija` VALUES (1380, 'РК-490', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1381, 'РК-491', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1382, 'РК-492', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1383, 'РК-183', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1384, 'РК-184', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1385, 'РК-185', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1386, 'РК-186', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1387, 'РК-187', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1388, 'РК-188', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1389, 'РК-189', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1390, 'РК-190', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1391, 'РК-191', '11', 28);
INSERT INTO `rf_komplektacija` VALUES (1392, 'РК-286', '11', 31);
INSERT INTO `rf_komplektacija` VALUES (1393, 'РК-287', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1394, 'РК-288', '11', 37);
INSERT INTO `rf_komplektacija` VALUES (1395, 'РК-625', '11', 44);
INSERT INTO `rf_komplektacija` VALUES (1396, 'РК-626', '11', 86);
INSERT INTO `rf_komplektacija` VALUES (1397, 'РК-627', '11', 58);
INSERT INTO `rf_komplektacija` VALUES (1398, 'РК-628', '11', 119);
INSERT INTO `rf_komplektacija` VALUES (1399, 'РК-629', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1400, 'РК-630', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1401, 'РК-631', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1402, 'РК-632', '11', 41);
INSERT INTO `rf_komplektacija` VALUES (1403, 'РК-152', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1404, 'РК-153', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1405, 'РК-192', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1406, 'РК-298', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1407, 'РК-299', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1408, 'РК-300', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1409, 'РК-301', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1410, 'РК-302', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1411, 'РК-319', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1412, 'РК-320', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1413, 'РК-322', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1414, 'РК-323', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1415, 'РК-324', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1416, 'РК-328', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1417, 'РК-329', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1418, 'РК-330', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1419, 'РК-331', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1420, 'РК-332', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1421, 'РК-321', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1422, 'РК-333', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1423, 'РК-373', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1424, 'РК-334', '11', 7);
INSERT INTO `rf_komplektacija` VALUES (1425, 'РК-346', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1426, 'РК-422', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1427, 'РК-423', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1428, 'РК-424', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1429, 'РК-425', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1430, 'РК-426', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1431, 'РК-427', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1432, 'РК-428', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1433, 'РК-429', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1434, 'РК-430', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1435, 'РК-431', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1436, 'РК-432', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1437, 'РК-433', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1438, 'РК-154', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1439, 'РК-155', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1440, 'РК-156', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1441, 'РК-157', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1442, 'РК-158', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1443, 'РК-159', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1444, 'РК-236', '11', 7);
INSERT INTO `rf_komplektacija` VALUES (1445, 'РК-237', '11', 5);
INSERT INTO `rf_komplektacija` VALUES (1446, 'РК-238', '11', 17);
INSERT INTO `rf_komplektacija` VALUES (1447, 'РК-239', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1448, 'РК-240', '11', 3);
INSERT INTO `rf_komplektacija` VALUES (1449, 'РК-241', '11', 6);
INSERT INTO `rf_komplektacija` VALUES (1450, 'РК-242', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1451, 'РК-243', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1452, 'РК-303', '11', 7);
INSERT INTO `rf_komplektacija` VALUES (1453, 'РК-304', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1454, 'РК-305', '11', 5);
INSERT INTO `rf_komplektacija` VALUES (1455, 'РК-306', '11', 3);
INSERT INTO `rf_komplektacija` VALUES (1456, 'РК-307', '11', 6);
INSERT INTO `rf_komplektacija` VALUES (1457, 'РК-308', '11', 8);
INSERT INTO `rf_komplektacija` VALUES (1458, 'РК-361', '11', 3);
INSERT INTO `rf_komplektacija` VALUES (1459, 'РК-362', '11', 5);
INSERT INTO `rf_komplektacija` VALUES (1460, 'РК-325', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1461, 'РК-326', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1462, 'РК-327', '11', 30);
INSERT INTO `rf_komplektacija` VALUES (1463, 'РК-255', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1464, 'РК-256', '11', 36);
INSERT INTO `rf_komplektacija` VALUES (1465, 'РК-257', '11', 49);
INSERT INTO `rf_komplektacija` VALUES (1466, 'РК-161', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1467, 'РК-162', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1468, 'РК-263', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1469, 'РК-264', '11', 13);
INSERT INTO `rf_komplektacija` VALUES (1470, 'РК-265', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1471, 'РК-166', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1472, 'РК-165', '11', 34);
INSERT INTO `rf_komplektacija` VALUES (1473, 'РК-169', '11', 15);
INSERT INTO `rf_komplektacija` VALUES (1474, 'РК-168', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1475, 'РК-167', '11', 39);
INSERT INTO `rf_komplektacija` VALUES (1476, 'РК-170', '11', 39);
INSERT INTO `rf_komplektacija` VALUES (1477, 'РК-171', '11', 44);
INSERT INTO `rf_komplektacija` VALUES (1478, 'РК-172', '11', 51);
INSERT INTO `rf_komplektacija` VALUES (1479, 'РК-144', '11', 26);
INSERT INTO `rf_komplektacija` VALUES (1480, 'РК-145', '11', 33);
INSERT INTO `rf_komplektacija` VALUES (1481, 'РК-146', '11', 22);
INSERT INTO `rf_komplektacija` VALUES (1482, 'РК-147', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1483, 'РК-150', '11', 24);
INSERT INTO `rf_komplektacija` VALUES (1484, 'РК-151', '11', 25);
INSERT INTO `rf_komplektacija` VALUES (1485, 'РК-363 ножка ', '11', 5);
INSERT INTO `rf_komplektacija` VALUES (1486, 'РК-364 ножка', '11', 6);
INSERT INTO `rf_komplektacija` VALUES (1487, 'РК-365 ножка', '11', 5);
INSERT INTO `rf_komplektacija` VALUES (1488, 'РК-366 ножка', '11', 4);
INSERT INTO `rf_komplektacija` VALUES (1489, 'РК-142', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1490, 'РК-141', '11', 9);
INSERT INTO `rf_komplektacija` VALUES (1491, 'РК-143', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1492, 'РК-230', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1493, 'РК-231', '11', 10);
INSERT INTO `rf_komplektacija` VALUES (1494, 'РК-232', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1495, 'РК-233', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1496, 'РК-234', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1497, 'РК-235', '11', 23);
INSERT INTO `rf_komplektacija` VALUES (1498, 'РК-357', '11', 16);
INSERT INTO `rf_komplektacija` VALUES (1499, 'РК-358', '11', 21);
INSERT INTO `rf_komplektacija` VALUES (1500, 'РК-359', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1501, 'РК-360', '11', 28);
INSERT INTO `rf_komplektacija` VALUES (1502, 'РК-482', '11', 11);
INSERT INTO `rf_komplektacija` VALUES (1503, 'РК-483', '11', 12);
INSERT INTO `rf_komplektacija` VALUES (1504, 'РК-484', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1505, 'РК-485', '11', 14);
INSERT INTO `rf_komplektacija` VALUES (1506, 'РК-486', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1507, 'РК-487', '11', 20);
INSERT INTO `rf_komplektacija` VALUES (1508, 'РК-488', '11', 18);
INSERT INTO `rf_komplektacija` VALUES (1509, 'РК-489', '11', 19);
INSERT INTO `rf_komplektacija` VALUES (1510, 'РК-1', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1511, 'РК-2', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1512, 'РК-38', '12', 26);
INSERT INTO `rf_komplektacija` VALUES (1513, 'РК-39', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1514, 'РК-40', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1515, 'РК-41', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1516, 'РК-42', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1517, 'РК-43', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1518, 'РК-244', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1519, 'РК-245', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1520, 'РК-69', '12', 45);
INSERT INTO `rf_komplektacija` VALUES (1521, 'РК-70', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1522, 'РК-71', '12', 62);
INSERT INTO `rf_komplektacija` VALUES (1523, 'РК-72', '12', 48);
INSERT INTO `rf_komplektacija` VALUES (1524, 'РК-73', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1525, 'РК-74', '12', 36);
INSERT INTO `rf_komplektacija` VALUES (1526, 'РК-75', '12', 32);
INSERT INTO `rf_komplektacija` VALUES (1527, 'РК-76', '12', 33);
INSERT INTO `rf_komplektacija` VALUES (1528, 'РК-77', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1529, 'РК-78', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1530, 'РК-79', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1531, 'РК-80', '12', 38);
INSERT INTO `rf_komplektacija` VALUES (1532, 'РК-81', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1533, 'РК-82', '12', 33);
INSERT INTO `rf_komplektacija` VALUES (1534, 'РК-100', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1535, 'РК-101', '12', 44);
INSERT INTO `rf_komplektacija` VALUES (1536, 'РК-15', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1537, 'РК-16', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1538, 'РК-17', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1539, 'РК-18', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1540, 'РК-19', '12', 26);
INSERT INTO `rf_komplektacija` VALUES (1541, 'РК-20', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1542, 'РК-21', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1543, 'РК-22', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1544, 'РК-23', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1545, 'РК-24', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1546, 'РК-25', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1547, 'РК-26', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1548, 'РК-27', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1549, 'РК-28', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1550, 'РК-29', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1551, 'РК-93', '12', 36);
INSERT INTO `rf_komplektacija` VALUES (1552, 'РК-94', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1553, 'РК-95', '12', 59);
INSERT INTO `rf_komplektacija` VALUES (1554, 'РК-96', '12', 55);
INSERT INTO `rf_komplektacija` VALUES (1555, 'РК-85', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1556, 'РК-86', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1557, 'РК-87', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1558, 'РК-88', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1559, 'РК-347 профиль, 3м', '12', 96);
INSERT INTO `rf_komplektacija` VALUES (1560, 'РК-352', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1561, 'РК-353', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1562, 'РК-354', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1563, 'РК-355', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1564, 'РК-369', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1565, 'РК-370', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1566, 'РК-371', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1567, 'РК-372', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1568, 'РК-374', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1569, 'РК-375', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1570, 'РК-376', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1571, 'РК-377', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1572, 'РК-378', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1573, 'РК-379', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1574, 'РК-380', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1575, 'РК-381', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1576, 'РК-382', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1577, 'РК-383', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1578, 'РК-384', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1579, 'РК-385', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1580, 'РК-386', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1581, 'РК-387', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1582, 'РК-388', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1583, 'РК-389', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1584, 'РК-390', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1585, 'РК-391', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1586, 'РК-392', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1587, 'РК-393', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1588, 'РК-394', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1589, 'РК-395', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1590, 'РК-396', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1591, 'РК-397', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1592, 'РК-398', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1593, 'РК-399', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1594, 'РК-400', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1595, 'РК-401', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1596, 'РК-402', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1597, 'РК-403', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1598, 'РК-404', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1599, 'РК-405', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1600, 'РК-406', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1601, 'РК-407', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1602, 'РК-408', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1603, 'РК-409', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1604, 'РК-410', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1605, 'РК-411', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1606, 'РК-412', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1607, 'РК-413', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1608, 'РК-414', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1609, 'РК-421', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1610, 'РК-415', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1611, 'РК-416', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1612, 'РК-417', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1613, 'РК-418', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1614, 'РК-419', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1615, 'РК-420', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1616, 'РК-193', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1617, 'РК-194', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1618, 'РК-195', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1619, 'РК-196', '12', 33);
INSERT INTO `rf_komplektacija` VALUES (1620, 'РК-197', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1621, 'РК-198', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1622, 'РК-199', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1623, 'РК-200', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1624, 'РК-201', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1625, 'РК-202', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1626, 'РК-203', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1627, 'РК-204', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1628, 'РК-205', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1629, 'РК-206', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1630, 'РК-207', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1631, 'РК-208', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1632, 'РК-209', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1633, 'РК-367', '12', 53);
INSERT INTO `rf_komplektacija` VALUES (1634, 'РК-368', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1635, 'РК-210', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1636, 'РК-211', '12', 48);
INSERT INTO `rf_komplektacija` VALUES (1637, 'РК-212', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1638, 'РК-213', '12', 45);
INSERT INTO `rf_komplektacija` VALUES (1639, 'РК-214', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1640, 'РК-215', '12', 49);
INSERT INTO `rf_komplektacija` VALUES (1641, 'РК-216', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1642, 'РК-217', '12', 52);
INSERT INTO `rf_komplektacija` VALUES (1643, 'РК-258', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1644, 'РК-259', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1645, 'РК-260', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1646, 'РК-261', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1647, 'РК-262', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1648, 'РК-11', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1649, 'РК-12', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1650, 'РК-13', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1651, 'РК-14', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1652, 'РК-336', '12', 70);
INSERT INTO `rf_komplektacija` VALUES (1653, 'РК-337', '12', 109);
INSERT INTO `rf_komplektacija` VALUES (1654, 'РК-474', '12', 41);
INSERT INTO `rf_komplektacija` VALUES (1655, 'РК-475', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1656, 'РК-476', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1657, 'РК-477', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1658, 'РК-266', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1659, 'РК-267', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1660, 'РК-108', '12', 36);
INSERT INTO `rf_komplektacija` VALUES (1661, 'РК-109', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1662, 'РК-3', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1663, 'РК-83', '12', 53);
INSERT INTO `rf_komplektacija` VALUES (1664, 'РК-84', '12', 56);
INSERT INTO `rf_komplektacija` VALUES (1665, 'РК-98', '12', 62);
INSERT INTO `rf_komplektacija` VALUES (1666, 'РК-99', '12', 62);
INSERT INTO `rf_komplektacija` VALUES (1667, 'РК-110', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1668, 'РК-30', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1669, 'РК-31', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1670, 'РК-32', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1671, 'РК-91', '12', 41);
INSERT INTO `rf_komplektacija` VALUES (1672, 'РК-92', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1673, 'РК-53', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1674, 'РК-54', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1675, 'РК-97', '12', 46);
INSERT INTO `rf_komplektacija` VALUES (1676, 'РК-111', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1677, 'РК-112', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1678, 'РК-50', '12', 45);
INSERT INTO `rf_komplektacija` VALUES (1679, 'РК-51', '12', 44);
INSERT INTO `rf_komplektacija` VALUES (1680, 'РК-52', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1681, 'РК-59', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1682, 'РК-60', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1683, 'РК-55', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1684, 'РК-56', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1685, 'РК-57', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1686, 'РК-58', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1687, 'РК-102', '12', 49);
INSERT INTO `rf_komplektacija` VALUES (1688, 'РК-103', '12', 41);
INSERT INTO `rf_komplektacija` VALUES (1689, 'РК-7', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1690, 'РК-8', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1691, 'РК-9', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1692, 'РК-33', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1693, 'РК-10', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1694, 'РК-34', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1695, 'РК-35', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1696, 'РК-36', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1697, 'РК-37', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1698, 'РК-44', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1699, 'РК-45', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1700, 'РК-46', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1701, 'РК-47', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1702, 'РК-48', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1703, 'РК-49', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1704, 'РК-106', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1705, 'РК-107', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1706, 'РК-218', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1707, 'РК-219', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1708, 'РК-220', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1709, 'РК-221', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1710, 'РК-222', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1711, 'РК-223', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1712, 'РК-224', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1713, 'РК-225', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1714, 'РК-63', '12', 26);
INSERT INTO `rf_komplektacija` VALUES (1715, 'РК-64', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1716, 'РК-65', '12', 27);
INSERT INTO `rf_komplektacija` VALUES (1717, 'РК-4', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1718, 'РК-5', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1719, 'РК-6', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1720, 'РК-148', '12', 46);
INSERT INTO `rf_komplektacija` VALUES (1721, 'РК-149', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1722, 'РК-66', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1723, 'РК-67', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1724, 'РК-68', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1725, 'РК-89', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1726, 'РК-90', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1727, 'РК-114', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1728, 'РК-115', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1729, 'РК-116', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1730, 'РК-117', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1731, 'РК-268', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1732, 'РК-119', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1733, 'РК-120', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1734, 'РК-121', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1735, 'РК-122', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1736, 'РК-272', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1737, 'РК-123', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1738, 'РК-124', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1739, 'РК-269', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1740, 'РК-270', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1741, 'РК-271', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1742, 'РК-113', '12', 69);
INSERT INTO `rf_komplektacija` VALUES (1743, 'РК-104', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1744, 'РК-105', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1745, 'РК-118', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1746, 'РК-125', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1747, 'РК-126', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1748, 'РК-127', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1749, 'РК-128', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1750, 'РК-137 ', '12', 151);
INSERT INTO `rf_komplektacija` VALUES (1751, '', '12', 15);
INSERT INTO `rf_komplektacija` VALUES (1752, 'РК-356', '12', 112);
INSERT INTO `rf_komplektacija` VALUES (1753, '', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1754, '', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1755, 'РК-493', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1756, 'РК-494', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1757, 'РК-495', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1758, 'РК-496', '12', 4);
INSERT INTO `rf_komplektacija` VALUES (1759, 'РК-497', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1760, 'РК-498', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1761, 'РК-499', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1762, 'РК-500', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1763, 'РК-501', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1764, 'РК-502', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1765, 'РК-503', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1766, 'РК-504', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1767, 'РК-505', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1768, 'РК-506', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1769, 'РК-507', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1770, 'РК-508', '12', 5);
INSERT INTO `rf_komplektacija` VALUES (1771, 'РК-509', '12', 6);
INSERT INTO `rf_komplektacija` VALUES (1772, 'РК-510', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1773, 'РК-511', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1774, 'РК-512', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1775, 'РК-513', '12', 8);
INSERT INTO `rf_komplektacija` VALUES (1776, 'РК-514', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1777, 'РК-515', '12', 10);
INSERT INTO `rf_komplektacija` VALUES (1778, 'РК-516', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1779, 'РК-633', '12', 42);
INSERT INTO `rf_komplektacija` VALUES (1780, 'РК-634', '12', 43);
INSERT INTO `rf_komplektacija` VALUES (1781, 'РК-635', '12', 69);
INSERT INTO `rf_komplektacija` VALUES (1782, 'РК-636', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1783, 'РК-637', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1784, 'РК-638', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1785, 'РК-639', '12', 73);
INSERT INTO `rf_komplektacija` VALUES (1786, 'РК-640', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1787, 'РК-641', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1788, 'РК-642', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1789, 'РК-643', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1790, 'РК-644', '12', 27);
INSERT INTO `rf_komplektacija` VALUES (1791, 'РК-645', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1792, 'РК-646', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1793, 'РК-647', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1794, 'РК-648', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1795, 'РК-649', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1796, 'РК-650', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1797, 'РК-651', '12', 41);
INSERT INTO `rf_komplektacija` VALUES (1798, 'РК-652', '12', 32);
INSERT INTO `rf_komplektacija` VALUES (1799, 'РК-653', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1800, 'РК-654', '12', 24);
INSERT INTO `rf_komplektacija` VALUES (1801, 'РК-655', '12', 34);
INSERT INTO `rf_komplektacija` VALUES (1802, 'РК-656', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1803, 'РК-657', '12', 34);
INSERT INTO `rf_komplektacija` VALUES (1804, 'РК-658', '12', 32);
INSERT INTO `rf_komplektacija` VALUES (1805, 'РК-659', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1806, 'РК-660', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1807, 'РК-661', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1808, 'РК-662', '12', 27);
INSERT INTO `rf_komplektacija` VALUES (1809, 'РК-663', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1810, 'РК-664', '12', 34);
INSERT INTO `rf_komplektacija` VALUES (1811, 'РК-665', '12', 49);
INSERT INTO `rf_komplektacija` VALUES (1812, 'РК-666', '12', 54);
INSERT INTO `rf_komplektacija` VALUES (1813, 'РК-129', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1814, 'РК-130', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1815, 'РК-131', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1816, 'РК-132', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1817, 'РК-133', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1818, 'РК-134', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1819, 'РК-135', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1820, 'РК-136', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1821, 'РК-138', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1822, 'РК-139', '12', 32);
INSERT INTO `rf_komplektacija` VALUES (1823, 'РК-140', '12', 26);
INSERT INTO `rf_komplektacija` VALUES (1824, 'РК-246', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1825, 'РК-247', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1826, 'РК-248', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1827, 'РК-249', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1828, 'РК-250', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1829, 'РК-251', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1830, 'РК-252', '12', 21);
INSERT INTO `rf_komplektacija` VALUES (1831, 'РК-253', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1832, 'РК-254', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1833, 'РК-340', '12', 42);
INSERT INTO `rf_komplektacija` VALUES (1834, 'РК-341', '12', 43);
INSERT INTO `rf_komplektacija` VALUES (1835, 'РК-342', '12', 43);
INSERT INTO `rf_komplektacija` VALUES (1836, 'РК-343', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1837, 'РК-344', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1838, 'РК-345', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1839, 'РК-349', '12', 7);
INSERT INTO `rf_komplektacija` VALUES (1840, 'РК-351', '12', 9);
INSERT INTO `rf_komplektacija` VALUES (1841, 'РК-350', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1842, 'РК-438', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1843, 'РК-439', '12', 67);
INSERT INTO `rf_komplektacija` VALUES (1844, 'РК-440', '12', 33);
INSERT INTO `rf_komplektacija` VALUES (1845, 'РК-441', '12', 85);
INSERT INTO `rf_komplektacija` VALUES (1846, 'РК-442', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1847, 'РК-443', '12', 57);
INSERT INTO `rf_komplektacija` VALUES (1848, 'РК-444', '12', 30);
INSERT INTO `rf_komplektacija` VALUES (1849, 'РК-445', '12', 69);
INSERT INTO `rf_komplektacija` VALUES (1850, 'РК-446', '12', 38);
INSERT INTO `rf_komplektacija` VALUES (1851, 'РК-447', '12', 92);
INSERT INTO `rf_komplektacija` VALUES (1852, 'РК-448', '12', 32);
INSERT INTO `rf_komplektacija` VALUES (1853, 'РК-449', '12', 73);
INSERT INTO `rf_komplektacija` VALUES (1854, 'РК-450', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1855, 'РК-451', '12', 57);
INSERT INTO `rf_komplektacija` VALUES (1856, 'РК-452', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1857, 'РК-453', '12', 60);
INSERT INTO `rf_komplektacija` VALUES (1858, 'РК-61', '12', 12);
INSERT INTO `rf_komplektacija` VALUES (1859, 'РК-62', '12', 11);
INSERT INTO `rf_komplektacija` VALUES (1860, 'РК-454', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1861, 'РК-520', '12', 45);
INSERT INTO `rf_komplektacija` VALUES (1862, 'РК-455', '12', 103);
INSERT INTO `rf_komplektacija` VALUES (1863, 'РК-521', '12', 62);
INSERT INTO `rf_komplektacija` VALUES (1864, 'РК-456', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1865, 'РК-457', '12', 128);
INSERT INTO `rf_komplektacija` VALUES (1866, 'РК-458', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1867, 'РК-459', '12', 53);
INSERT INTO `rf_komplektacija` VALUES (1868, 'РК-460', '12', 27);
INSERT INTO `rf_komplektacija` VALUES (1869, 'РК-461', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1870, 'РК-462', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1871, 'РК-463', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1872, 'РК-464', '12', 70);
INSERT INTO `rf_komplektacija` VALUES (1873, 'РК-465', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1874, 'РК-466', '12', 27);
INSERT INTO `rf_komplektacija` VALUES (1875, 'РК-467', '12', 38);
INSERT INTO `rf_komplektacija` VALUES (1876, 'РК-468', '12', 26);
INSERT INTO `rf_komplektacija` VALUES (1877, 'РК-469', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1878, 'РК-470', '12', 13);
INSERT INTO `rf_komplektacija` VALUES (1879, 'РК-471', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1880, 'РК-472', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1881, 'РК-473', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1882, 'РК-478', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1883, 'РК-480', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1884, 'РК-479', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1885, 'РК-481', '12', 23);
INSERT INTO `rf_komplektacija` VALUES (1886, 'РК-542', '12', 60);
INSERT INTO `rf_komplektacija` VALUES (1887, 'РК-543', '12', 42);
INSERT INTO `rf_komplektacija` VALUES (1888, 'РК-544', '12', 31);
INSERT INTO `rf_komplektacija` VALUES (1889, 'РК-545', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1890, 'РК-546', '12', 65);
INSERT INTO `rf_komplektacija` VALUES (1891, 'РК-547', '12', 46);
INSERT INTO `rf_komplektacija` VALUES (1892, 'РК-548', '12', 33);
INSERT INTO `rf_komplektacija` VALUES (1893, 'РК-549', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1894, 'РК-550', '12', 71);
INSERT INTO `rf_komplektacija` VALUES (1895, 'РК-551', '12', 49);
INSERT INTO `rf_komplektacija` VALUES (1896, 'РК-552', '12', 35);
INSERT INTO `rf_komplektacija` VALUES (1897, 'РК-553', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1898, 'РК-554', '12', 57);
INSERT INTO `rf_komplektacija` VALUES (1899, 'РК-555', '12', 39);
INSERT INTO `rf_komplektacija` VALUES (1900, 'РК-556', '12', 29);
INSERT INTO `rf_komplektacija` VALUES (1901, 'РК-557', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1902, 'РК-558', '12', 79);
INSERT INTO `rf_komplektacija` VALUES (1903, 'РК-559', '12', 69);
INSERT INTO `rf_komplektacija` VALUES (1904, 'РК-560', '12', 40);
INSERT INTO `rf_komplektacija` VALUES (1905, 'РК-561', '12', 19);
INSERT INTO `rf_komplektacija` VALUES (1906, 'РК-562', '12', 74);
INSERT INTO `rf_komplektacija` VALUES (1907, 'РК-563', '12', 65);
INSERT INTO `rf_komplektacija` VALUES (1908, 'РК-564', '12', 37);
INSERT INTO `rf_komplektacija` VALUES (1909, 'РК-565', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1910, 'РК-566', '12', 76);
INSERT INTO `rf_komplektacija` VALUES (1911, 'РК-567', '12', 51);
INSERT INTO `rf_komplektacija` VALUES (1912, 'РК-568', '12', 28);
INSERT INTO `rf_komplektacija` VALUES (1913, 'РК-569', '12', 20);
INSERT INTO `rf_komplektacija` VALUES (1914, 'РК-570', '12', 16);
INSERT INTO `rf_komplektacija` VALUES (1915, 'РК-571', '12', 78);
INSERT INTO `rf_komplektacija` VALUES (1916, 'РК-572', '12', 55);
INSERT INTO `rf_komplektacija` VALUES (1917, 'РК-573', '12', 30);
INSERT INTO `rf_komplektacija` VALUES (1918, 'РК-574', '12', 22);
INSERT INTO `rf_komplektacija` VALUES (1919, 'РК-575', '12', 17);
INSERT INTO `rf_komplektacija` VALUES (1920, 'РК-576', '12', 69);
INSERT INTO `rf_komplektacija` VALUES (1921, 'РК-577', '12', 47);
INSERT INTO `rf_komplektacija` VALUES (1922, 'РК-578', '12', 25);
INSERT INTO `rf_komplektacija` VALUES (1923, 'РК-579', '12', 18);
INSERT INTO `rf_komplektacija` VALUES (1924, 'РК-580', '12', 14);
INSERT INTO `rf_komplektacija` VALUES (1925, 'Стільниця планка  кутова 28мм/38мм', '13', 20);
INSERT INTO `rf_komplektacija` VALUES (1926, 'Стільниця Планка з+B2014єднувальна 28мм/38мм', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (1927, 'Стільниця Планка від плити 28мм/38мм', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1928, 'Стільниця стяжка для стільниці 60мм/150мм', '13', 6);
INSERT INTO `rf_komplektacija` VALUES (1929, 'Ніжка SCILM монтажна Н=80,100,130,150мм', '13', 4);
INSERT INTO `rf_komplektacija` VALUES (1930, 'Ніжка SCILM монтажна універсальна Н=80,100,130,150мм', '13', 5);
INSERT INTO `rf_komplektacija` VALUES (1931, 'НІжка монтажна Н=100мм', '13', 1);
INSERT INTO `rf_komplektacija` VALUES (1932, 'Кліпса до цоколя ДСП, пластикового', '13', 1);
INSERT INTO `rf_komplektacija` VALUES (1933, 'Ніжка пластикова Н=27мм', '13', 3);
INSERT INTO `rf_komplektacija` VALUES (1934, 'Ніжка декоративна (алюм) Н=100мм', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (1935, 'Ніжка декоративна (ст.золото) Н=100мм', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (1936, 'Ніжка нерегулюєма врізна (хром, ст.золото) Н=60мм', '13', 20);
INSERT INTO `rf_komplektacija` VALUES (1937, 'Ніжка нерегулюєма врізна (хром) Н=100мм', '13', 25);
INSERT INTO `rf_komplektacija` VALUES (1938, 'Нога для відкидного столу (сіра) Н=720мм', '13', 100);
INSERT INTO `rf_komplektacija` VALUES (1939, 'Нога для столу (хром) Н=720мм', '13', 60);
INSERT INTO `rf_komplektacija` VALUES (1940, 'Нога для столу (хром) Н=850мм', '13', 85);
INSERT INTO `rf_komplektacija` VALUES (1941, 'Механізм для відкидного столу', '13', 85);
INSERT INTO `rf_komplektacija` VALUES (1942, 'Ролік меблевий резиновий з площадкою', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1943, 'Планка монтажна  2м', '13', 25);
INSERT INTO `rf_komplektacija` VALUES (1944, 'Командорики малі (1 отвір)', '13', 1);
INSERT INTO `rf_komplektacija` VALUES (1945, 'Консоль з накладкою 77*120мм', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1946, 'Кут з накладкою регул.', '13', 3);
INSERT INTO `rf_komplektacija` VALUES (1947, 'Кутник металевий великий (хром, сатин, алюм) 222*222мм', '13', 60);
INSERT INTO `rf_komplektacija` VALUES (1948, 'Кутник металевий 30*20мм', '13', 1);
INSERT INTO `rf_komplektacija` VALUES (1949, 'Стяжка міжсекційна хром, мідь, золото', '13', 0);
INSERT INTO `rf_komplektacija` VALUES (1950, 'Планка пряма зєднювальна для деталей ДСП', '13', 2);
INSERT INTO `rf_komplektacija` VALUES (1951, 'Підвіска шафи регульована', '13', 2);
INSERT INTO `rf_komplektacija` VALUES (1952, 'Підвіска шафи нерегульована  (вухо)', '13', 2);
INSERT INTO `rf_komplektacija` VALUES (1953, 'Підвіска нижньої тумби (ліва+права)', '13', 50);
INSERT INTO `rf_komplektacija` VALUES (1954, 'Планка монтажна  для кріплення нижньої тумби', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1955, 'полицетримач L240мм на дюбелі скритого монтажу)', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1956, 'Тримач полки ДСП, скло, середній (пелікан), сатин, ст.золото, золото, хром', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (1957, 'Тримач полки ДСП, скло, великий (пелікан), сатин, ст.золото, золото, хром', '13', 20);
INSERT INTO `rf_komplektacija` VALUES (1958, 'Підйомник пневматичний N60, N80, N100', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (1959, 'Підйомник механічний ', '13', 30);
INSERT INTO `rf_komplektacija` VALUES (1960, 'Барна Труба барна  хром ?50 мм (3м)', '13', 130);
INSERT INTO `rf_komplektacija` VALUES (1961, 'Барні фланці на трубу барну верхній, нижній', '13', 16);
INSERT INTO `rf_komplektacija` VALUES (1962, 'Барна Корзина центральна (хром) D=360мм', '13', 130);
INSERT INTO `rf_komplektacija` VALUES (1963, 'Барна Корзина центральна висока (хром) D=360мм', '13', 190);
INSERT INTO `rf_komplektacija` VALUES (1964, 'Барний Бокалотримач (хром)', '13', 120);
INSERT INTO `rf_komplektacija` VALUES (1965, 'Барний Пляшкотримач (хром)', '13', 120);
INSERT INTO `rf_komplektacija` VALUES (1966, 'Барна склополиця центральна хром', '13', 150);
INSERT INTO `rf_komplektacija` VALUES (1967, 'Барна Труба барна ст.золото/сатин ?50 мм (3м)', '13', 500);
INSERT INTO `rf_komplektacija` VALUES (1968, 'Барні фланці на трубу барну ст.золото/сатин верхній, нижній', '13', 25);
INSERT INTO `rf_komplektacija` VALUES (1969, 'Барна склополиця центральна ст.золото/сатин', '13', 200);
INSERT INTO `rf_komplektacija` VALUES (1970, 'Барна Корзина центральна (хром)', '13', 170);
INSERT INTO `rf_komplektacija` VALUES (1971, 'Труба  хром  ?25 мм, 3м ', '13', 50);
INSERT INTO `rf_komplektacija` VALUES (1972, 'Тримачі на трубу  ?25 мм', '13', 3);
INSERT INTO `rf_komplektacija` VALUES (1973, 'Штанга  на одяг овальна, 3м ', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (1974, 'Штанга  на одяг кріплення', '13', 2);
INSERT INTO `rf_komplektacija` VALUES (1975, 'Труба 25мм Муфта хрестоподібна', '13', 35);
INSERT INTO `rf_komplektacija` VALUES (1976, 'Труба 25мм Муфта Т-подібна', '13', 30);
INSERT INTO `rf_komplektacija` VALUES (1977, 'Барна  труба коса', '13', 110);
INSERT INTO `rf_komplektacija` VALUES (1978, 'Підсвітка галогенна нижня трикутна 1 лампа з вим / без вим, матов хром/ст.золото', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (1979, 'Підсвітка галогенна верхня 1 лампа', '13', 25);
INSERT INTO `rf_komplektacija` VALUES (1980, 'Підсвітка  галогенна верхня 3 лампи', '13', 110);
INSERT INTO `rf_komplektacija` VALUES (1981, 'Підсвітка  галогенна верхня 5 ламп', '13', 140);
INSERT INTO `rf_komplektacija` VALUES (1982, 'Підсвітка кольорова для склополиць на 2 полиці+трансформатор', '13', 150);
INSERT INTO `rf_komplektacija` VALUES (1983, 'Підсвітка світодіодна верхня накладна, хром/алюм, тепле/холодне біле світло', '13', 60);
INSERT INTO `rf_komplektacija` VALUES (1984, 'Підсвітка верхня світодіодна накладна, хром/алюм, тепле/холодне біле світло (компл. 3шт+трансформатор)', '13', 240);
INSERT INTO `rf_komplektacija` VALUES (1985, 'Підсвітка нижня світодіодна (трикутник) хром, алюм', '13', 70);
INSERT INTO `rf_komplektacija` VALUES (1986, 'Профіль накладний для стрічки світодіодної+ пластик прозорий (розсіювач світла) (2м)', '13', 80);
INSERT INTO `rf_komplektacija` VALUES (1987, 'Стрічка світодіодна120 діодів (10 Вт), тепле/холодне біле світло', '13', 80);
INSERT INTO `rf_komplektacija` VALUES (1988, 'Трансформатор 30 Вт для світодіодних підсвіток (пост.струму)', '13', 80);
INSERT INTO `rf_komplektacija` VALUES (1989, 'Трансформатор 60 Вт для світодіодних підсвіток (пост.струму)', '13', 135);
INSERT INTO `rf_komplektacija` VALUES (1990, 'Люмінісцентна лампа 34см', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (1991, 'Люмінісцентна лампа 54см', '13', 60);
INSERT INTO `rf_komplektacija` VALUES (1992, 'Вимикач ланцюжковий хром/золото', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (1993, 'Вимикач кнопковий врізний HAFELE', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (1994, 'Вилка електрична', '13', 8);
INSERT INTO `rf_komplektacija` VALUES (1995, 'Трансформатор  60 W', '13', 35);
INSERT INTO `rf_komplektacija` VALUES (1996, 'Трансформатор  105 W', '13', 50);
INSERT INTO `rf_komplektacija` VALUES (1997, 'Трансформатор 150  W', '13', 100);
INSERT INTO `rf_komplektacija` VALUES (1998, 'Релінг  100 см сатин, т.золото, золото, хром', '13', 25);
INSERT INTO `rf_komplektacija` VALUES (1999, 'Релінг Гачок до релінга', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (2000, 'Релінг Кут релінгу', '13', 15);
INSERT INTO `rf_komplektacija` VALUES (2001, 'Релінг Тримач на релінг', '13', 12);
INSERT INTO `rf_komplektacija` VALUES (2002, 'Релінг Закінчення на релінг', '13', 10);
INSERT INTO `rf_komplektacija` VALUES (2003, 'Лоток д/ложок 30 см, сірий', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (2004, 'Лоток д/ложок 35 см, сірий', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (2005, 'р', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (2006, 'страда', '13', 40);
INSERT INTO `rf_komplektacija` VALUES (2007, 'Лоток д/ложок 50 см, сірий', '13', 50);
INSERT INTO `rf_komplektacija` VALUES (2008, 'Лоток д/ложок 55 см, сірий', '13', 50);
INSERT INTO `rf_komplektacija` VALUES (2009, 'Лоток д/ложок 60 см, сірий', '13', 60);
INSERT INTO `rf_komplektacija` VALUES (2010, 'Лоток д/ложок 70 см, сірий', '13', 70);
INSERT INTO `rf_komplektacija` VALUES (2011, 'Лоток д/ложок 80 см, сірий', '13', 80);
INSERT INTO `rf_komplektacija` VALUES (2012, 'Лоток д/ложок 90 см, сірий', '13', 90);
INSERT INTO `rf_komplektacija` VALUES (2013, 'Дно металізоване (алюм) 600мм', '13', 90);
INSERT INTO `rf_komplektacija` VALUES (2014, 'Дно металізоване (алюм) 800мм', '13', 110);
INSERT INTO `rf_komplektacija` VALUES (2015, 'Дно металізоване (алюм) 900мм', '13', 120);
INSERT INTO `rf_komplektacija` VALUES (2016, 'Дно металізоване (алюм) 1200мм', '13', 140);
INSERT INTO `rf_komplektacija` VALUES (2017, 'Контейнер під сміття VIBO (2 секції) на направляючих', '13', 450);
INSERT INTO `rf_komplektacija` VALUES (2018, 'Декор Решітка фасадна (кратка) дерево', '13', 500);
INSERT INTO `rf_komplektacija` VALUES (2019, 'Декор Балюстрада пряма з балясинами дерево', '13', 300);
INSERT INTO `rf_komplektacija` VALUES (2020, 'Декор Балюстрада радіусна з балясинами', '13', 400);
INSERT INTO `rf_komplektacija` VALUES (2021, 'Декор Закінчення радіусне на стільницю (клюшка)', '13', 400);
INSERT INTO `rf_komplektacija` VALUES (2022, 'Декор Балюстрада пряма з балясинами МДФ', '13', 125);
INSERT INTO `rf_komplektacija` VALUES (2023, 'Декор Бал гнута з балясинами МДФ', '13', 160);
INSERT INTO `rf_komplektacija` VALUES (2024, 'Шафа Пантограф GTV 450-600мм', '13', 210);
INSERT INTO `rf_komplektacija` VALUES (2025, 'Шафа Пантограф GTV 600-830мм', '13', 210);
INSERT INTO `rf_komplektacija` VALUES (2026, 'Шафа Пантограф GTV 30-1150мм', '13', 210);
INSERT INTO `rf_komplektacija` VALUES (2027, 'Шафа Штанга під одяг з підсвіткою LED та сенсорним вимикачем 600мм (522-567мм)', '13', 160);
INSERT INTO `rf_komplektacija` VALUES (2028, 'Шафа Штанга під одяг з підсвіткою LED та сенсорним вимикачем 800мм (722-767мм)', '13', 175);
INSERT INTO `rf_komplektacija` VALUES (2029, 'Шафа Вішак висувний (тромбон) 300мм,410мм,450мм,500мм', '13', 30);
INSERT INTO `rf_komplektacija` VALUES (2030, 'Шафа Фіксатор до дверей шафи-купе', '13', 5);
INSERT INTO `rf_komplektacija` VALUES (2031, 'Шафа Щітка до дверей шафи-купе', '13', 2);
INSERT INTO `rf_komplektacija` VALUES (2032, 'Світополка (алюм, золото, золото полироване, чорний браш 450мм', '13', 220);
INSERT INTO `rf_komplektacija` VALUES (2033, 'UA-AA-А3 алюм 128 / 160 mm', '9', 13);
INSERT INTO `rf_komplektacija` VALUES (2034, 'UA-AA-А3 алюм 160 / 192 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2035, 'UA-AA-А3 алюм 224 / 256 mm', '9', 18);
INSERT INTO `rf_komplektacija` VALUES (2036, 'RE - AL / SAT 96 / 176 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2037, 'RE - AL / SAT 128 / 188 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2038, 'RE - AL / SAT 160 / 220 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2039, 'RE - AL / SAT 192 / 272 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2040, 'RE - AL / SAT 224 / 304 mm', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2041, 'RE - AL / SAT 352 / 432 mm', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2042, 'RE - AL / SAT 384 / 464 mm', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2043, 'RE - AL / SAT 416 / 496 mm', '9', 24);
INSERT INTO `rf_komplektacija` VALUES (2044, 'RE - AL / SAT 448 / 528 mm', '9', 24);
INSERT INTO `rf_komplektacija` VALUES (2045, 'RE - AL / SAT 448 / 528 mm', '9', 24);
INSERT INTO `rf_komplektacija` VALUES (2046, 'RE - AL / SAT 512 / 592 mm', '9', 30);
INSERT INTO `rf_komplektacija` VALUES (2047, 'UA-AA18(алюм) 512 / 592 mm', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2048, 'UA-AA18(алюм) 512 / 592 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2049, 'UA-AA18(алюм) 192 / 210 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2050, 'UA-AA18(алюм) 192 / 210 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2051, 'UA-AA18(алюм) 192 / 210 mm', '9', 17);
INSERT INTO `rf_komplektacija` VALUES (2052, 'UA-AA18(іnox) 192 / 210 mm', '9', 18);
INSERT INTO `rf_komplektacija` VALUES (2053, 'UA-AA18(іnox) 192 / 210 mm', '9', 19);
INSERT INTO `rf_komplektacija` VALUES (2054, 'UA-B0-311 алюм 96 / 116 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2055, 'UA-B0-311 алюм 128 / 148 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2056, 'UA-B0-311 алюм 160 / 180 mm', '9', 11);
INSERT INTO `rf_komplektacija` VALUES (2057, 'UA-B0-311 алюм 160 / 180 mm', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2058, 'UA-B0-311 алюм 224 / 244 mm', '9', 13);
INSERT INTO `rf_komplektacija` VALUES (2059, 'UA-B0-311 алюм 256 / 276 mm', '9', 14);
INSERT INTO `rf_komplektacija` VALUES (2060, 'UA-B0-311 алюм 320 / 340 mm', '9', 16);
INSERT INTO `rf_komplektacija` VALUES (2061, 'UA-00-315 алюм 320 / 340 mm', '9', 21);
INSERT INTO `rf_komplektacija` VALUES (2062, 'UA-00-315 алюм 320 / 340 mm', '9', 24);
INSERT INTO `rf_komplektacija` VALUES (2063, 'UA-00-315 алюм 320 / 340 mm', '9', 25);
INSERT INTO `rf_komplektacija` VALUES (2064, 'UA-00-315 алюм 224 / 263 mm', '9', 27);
INSERT INTO `rf_komplektacija` VALUES (2065, 'UA-00-315 алюм 256 / 295 mm', '9', 30);
INSERT INTO `rf_komplektacija` VALUES (2066, 'UA-00-315 алюм 320 / 359 mm', '9', 33);
INSERT INTO `rf_komplektacija` VALUES (2067, 'UA-00-319 алюм 320 / 359 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2068, 'UA-00-319 алюм 128 / 168 mm', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2069, 'UA-00-319 алюм 160 / 200 mm', '9', 13);
INSERT INTO `rf_komplektacija` VALUES (2070, 'UA-A0-330 алюм 128 / 168 mm', '9', 17);
INSERT INTO `rf_komplektacija` VALUES (2071, 'UA-00-333 алюм 96 mm', '9', 7);
INSERT INTO `rf_komplektacija` VALUES (2072, 'UA-00-333 алюм 128 mm', '9', 8);
INSERT INTO `rf_komplektacija` VALUES (2073, 'UA-B0-337 алюм 128 / 168 mm', '9', 16);
INSERT INTO `rf_komplektacija` VALUES (2074, 'UA-B0-337 алюм 128 / 168 mm', '9', 18);
INSERT INTO `rf_komplektacija` VALUES (2075, 'UA-00-340 алюм 96 / 136 mm', '9', 10);
INSERT INTO `rf_komplektacija` VALUES (2076, 'UA-00-340 алюм 128 / 168 mm', '9', 14);
INSERT INTO `rf_komplektacija` VALUES (2077, 'UA-00-340 алюм 128 / 168 mm', '9', 16);
INSERT INTO `rf_komplektacija` VALUES (2078, 'UA-00-340 алюм 192 / 232 mm', '9', 18);
INSERT INTO `rf_komplektacija` VALUES (2079, 'UA-00-340 алюм 224 / 264 mm', '9', 18);
INSERT INTO `rf_komplektacija` VALUES (2080, 'UA-00-340 алюм 256 / 296 mm', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2081, 'UA-00-340 алюм 320 / 360 mm', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2082, 'UA-00-358 алюм 128 / 168 mm', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2083, 'UA-00-358 алюм 160 / 200 mm', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2084, 'UA-00-338 алюм 140мм', '9', 11);
INSERT INTO `rf_komplektacija` VALUES (2085, 'UA-00-338 алюм 160мм', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2086, 'RR0804 сатин 160 / 200 mm', '9', 12);
INSERT INTO `rf_komplektacija` VALUES (2088, 'RG0806', '9', 8);
INSERT INTO `rf_komplektacija` VALUES (2089, 'РК-11', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2090, 'РК-12', '9', 20);
INSERT INTO `rf_komplektacija` VALUES (2091, 'РК-13', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2092, 'РК-14', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2093, 'UN7108 сат/алюм', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2094, 'UN7106сат/алюм', '9', 25);
INSERT INTO `rf_komplektacija` VALUES (2095, 'UN7106 сатин', '9', 11);
INSERT INTO `rf_komplektacija` VALUES (2096, 'UN4907 сатин', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2097, 'GAL 1', '9', 61);
INSERT INTO `rf_komplektacija` VALUES (2098, 'GAL 2', '9', 61);
INSERT INTO `rf_komplektacija` VALUES (2099, 'VETRO', '9', 33);
INSERT INTO `rf_komplektacija` VALUES (2100, 'MODENA хром/сатин', '9', 21);
INSERT INTO `rf_komplektacija` VALUES (2101, 'LOTOS', '9', 26);
INSERT INTO `rf_komplektacija` VALUES (2102, 'LOTOS малий', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2103, 'TULIPAN', '9', 30);
INSERT INTO `rf_komplektacija` VALUES (2104, 'TULIPAN малий', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2105, 'FLOWER', '9', 33);
INSERT INTO `rf_komplektacija` VALUES (2106, 'FLOWER малий', '9', 22);
INSERT INTO `rf_komplektacija` VALUES (2107, 'NARCYZ 96 / - mm', '9', 30);
INSERT INTO `rf_komplektacija` VALUES (2108, 'NARCYZ', '9', 19);
INSERT INTO `rf_komplektacija` VALUES (2109, 'DP 211', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2110, 'DP 212', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2111, 'DP 213', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2112, 'DP 215', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2113, 'DP 217', '9', 15);
INSERT INTO `rf_komplektacija` VALUES (2114, 'DP 191', '9', 15);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_korpus`
-- 

CREATE TABLE `rf_korpus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- 
-- Дамп даних таблиці `rf_korpus`
-- 

INSERT INTO `rf_korpus` VALUES (1, 'I  K 110 S        белый 110    ');
INSERT INTO `rf_korpus` VALUES (2, '1301 PE        ваниль ');
INSERT INTO `rf_korpus` VALUES (3, '1937 PR        кальвадос ');
INSERT INTO `rf_korpus` VALUES (4, ' 1972 PR        яблоня локарно ');
INSERT INTO `rf_korpus` VALUES (5, '2251 PR        орех экко ');
INSERT INTO `rf_korpus` VALUES (6, '2330 PR        ольха медовая ');
INSERT INTO `rf_korpus` VALUES (7, '8622 PR        дуб молочный ');
INSERT INTO `rf_korpus` VALUES (8, ' 9103 PR        дуб светлый ');
INSERT INTO `rf_korpus` VALUES (9, '9200 PR        бук бовария ');
INSERT INTO `rf_korpus` VALUES (10, '9310 PR        ольха ');
INSERT INTO `rf_korpus` VALUES (11, '9311 PR        ольха горная ');
INSERT INTO `rf_korpus` VALUES (12, '9502 S         мрамор оникс ');
INSERT INTO `rf_korpus` VALUES (13, 'D 071 PR       дуб седан ');
INSERT INTO `rf_korpus` VALUES (14, 'D 079 S        мрамор каррара ');
INSERT INTO `rf_korpus` VALUES (15, 'D 088 PR       вишня оксфорд');
INSERT INTO `rf_korpus` VALUES (16, 'D 344 PR       вишня двиркова');
INSERT INTO `rf_korpus` VALUES (17, 'D 381 PR       бук');
INSERT INTO `rf_korpus` VALUES (18, 'D 466 PR       красное дерево того');
INSERT INTO `rf_korpus` VALUES (19, 'D 722 PR       орех');
INSERT INTO `rf_korpus` VALUES (20, 'K110SM,PR,PE   белый 101');
INSERT INTO `rf_korpus` VALUES (21, '2227 PR        венге');
INSERT INTO `rf_korpus` VALUES (22, '2260 PR        береза майнау');
INSERT INTO `rf_korpus` VALUES (23, '2427 PR        венге светлый');
INSERT INTO `rf_korpus` VALUES (24, '9404 PR        орех итальянский');
INSERT INTO `rf_korpus` VALUES (25, 'D 665 PR       рустикаль');
INSERT INTO `rf_korpus` VALUES (26, 'D 740 PR       дуб горный светлый');
INSERT INTO `rf_korpus` VALUES (27, 'D 775 PR       красное дерево');
INSERT INTO `rf_korpus` VALUES (28, 'D 375PR        клен танзау ');
INSERT INTO `rf_korpus` VALUES (29, 'U 112 PE       пепельный');
INSERT INTO `rf_korpus` VALUES (30, 'U 190 PR,PE    черный');
INSERT INTO `rf_korpus` VALUES (31, ' 2226 PR        венге магия');
INSERT INTO `rf_korpus` VALUES (32, '9260 PR        каштан благородный');
INSERT INTO `rf_korpus` VALUES (33, '9450 PR        орех темный ');
INSERT INTO `rf_korpus` VALUES (34, '9451 PR        орех болонья');
INSERT INTO `rf_korpus` VALUES (35, '2617 PR        бук шоколадный');
INSERT INTO `rf_korpus` VALUES (36, ' 1354 PR        груша рома');
INSERT INTO `rf_korpus` VALUES (37, '2031 PR        орех амадо');
INSERT INTO `rf_korpus` VALUES (38, '2032 PR        вишня авиола');
INSERT INTO `rf_korpus` VALUES (39, '2033 PR        груша фавио');
INSERT INTO `rf_korpus` VALUES (40, '2197 PR        орех вичита');
INSERT INTO `rf_korpus` VALUES (41, '2296 PR        зебрано африканское');
INSERT INTO `rf_korpus` VALUES (42, '2361 PR        черешня');
INSERT INTO `rf_korpus` VALUES (43, '2374 PR        клен торонто');
INSERT INTO `rf_korpus` VALUES (44, '2378 PR        дуб старый');
INSERT INTO `rf_korpus` VALUES (45, '2380 PR        лимба шоколадная');
INSERT INTO `rf_korpus` VALUES (46, '2381 PR        лимба');
INSERT INTO `rf_korpus` VALUES (47, '2419 PR        дуб античный');
INSERT INTO `rf_korpus` VALUES (48, '2446 PR        макассар');
INSERT INTO `rf_korpus` VALUES (49, '2619 PR        дуб феррара');
INSERT INTO `rf_korpus` VALUES (50, '2672 PR        орех вирджиния');
INSERT INTO `rf_korpus` VALUES (51, '8567 PR        дуб борас светлый');
INSERT INTO `rf_korpus` VALUES (52, '8568 PR        дуб борас темный');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_kraine`
-- 

CREATE TABLE `rf_kraine` (
  `id` varchar(5) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_kraine`
-- 

INSERT INTO `rf_kraine` VALUES ('Bil', 'Білорусь');
INSERT INTO `rf_kraine` VALUES ('Ita', 'Італія');
INSERT INTO `rf_kraine` VALUES ('Pol', 'Польша');
INSERT INTO `rf_kraine` VALUES ('Ukr', 'Україна');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_modelj_fasad`
-- 

CREATE TABLE `rf_modelj_fasad` (
  `id` varchar(10) NOT NULL,
  `name` text NOT NULL,
  `fasad_id` text NOT NULL,
  `firma_id` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_modelj_fasad`
-- 

INSERT INTO `rf_modelj_fasad` VALUES ('gljan', 'Глянцева', 'mdf_pliv', 'Edi', 400);
INSERT INTO `rf_modelj_fasad` VALUES ('mat', 'Матова', 'mdf_pliv', 'Edi', 400);
INSERT INTO `rf_modelj_fasad` VALUES ('met', 'Металік', 'mdf_pliv', 'Edi', 400);
INSERT INTO `rf_modelj_fasad` VALUES ('vin', 'Віноріт', 'mdf_pliv', 'Edi', 400);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_modelj_plintus`
-- 

CREATE TABLE `rf_modelj_plintus` (
  `id` int(11) NOT NULL,
  `fir_pl_id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_modelj_plintus`
-- 

INSERT INTO `rf_modelj_plintus` VALUES (1, 1, 'в колір стільниці');
INSERT INTO `rf_modelj_plintus` VALUES (2, 1, '127 (алюміній)');
INSERT INTO `rf_modelj_plintus` VALUES (3, 2, 'алюміній h=30мм');
INSERT INTO `rf_modelj_plintus` VALUES (4, 2, 'пустишка h=30мм');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_modelj_sklo`
-- 

CREATE TABLE `rf_modelj_sklo` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_modelj_sklo`
-- 

INSERT INTO `rf_modelj_sklo` VALUES (1, 0xd0b1d0b5d0b2d0b5d0bbd181);
INSERT INTO `rf_modelj_sklo` VALUES (2, 0xd0bfd196d181d0bad0bed181d182d180d183d0b9);
INSERT INTO `rf_modelj_sklo` VALUES (3, 0xd0bfd181d0b5d0b2d0b4d0bed0b2d196d182d180d0b0d0b6);
INSERT INTO `rf_modelj_sklo` VALUES (4, 0xd0bfd180d0bed0b7d0bed180d0b5);
INSERT INTO `rf_modelj_sklo` VALUES (5, 0xd0bad180d0b0d182d0bad0b020d0b4d0b5d180d0b5d0b2d0be);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_modelj_stiljnycja`
-- 

CREATE TABLE `rf_modelj_stiljnycja` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_modelj_stiljnycja`
-- 

INSERT INTO `rf_modelj_stiljnycja` VALUES (1, 'LuxeForm-28');
INSERT INTO `rf_modelj_stiljnycja` VALUES (2, 'LuxeForm-38');
INSERT INTO `rf_modelj_stiljnycja` VALUES (3, 'EGGER-38');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_obrobka_stiljnycja`
-- 

CREATE TABLE `rf_obrobka_stiljnycja` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_obrobka_stiljnycja`
-- 

INSERT INTO `rf_obrobka_stiljnycja` VALUES (1, 'Єврозарез', 30);
INSERT INTO `rf_obrobka_stiljnycja` VALUES (2, 'Фрезеровка', 35);
INSERT INTO `rf_obrobka_stiljnycja` VALUES (3, 'Фрезеровка капля', 40);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_odyn_vymir`
-- 

CREATE TABLE `rf_odyn_vymir` (
  `id` varchar(5) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_odyn_vymir`
-- 

INSERT INTO `rf_odyn_vymir` VALUES ('1', 'м/кв');
INSERT INTO `rf_odyn_vymir` VALUES ('2', 'м/п');
INSERT INTO `rf_odyn_vymir` VALUES ('3', 'шт');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_operation`
-- 

CREATE TABLE `rf_operation` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_operation`
-- 

INSERT INTO `rf_operation` VALUES (1, 'поклейка ПВХ', 20);
INSERT INTO `rf_operation` VALUES (2, 'поклейка ABS алюміній', 10);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_plintus`
-- 

CREATE TABLE `rf_plintus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_pl_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- 
-- Дамп даних таблиці `rf_plintus`
-- 

INSERT INTO `rf_plintus` VALUES (1, 1, 'Мозаїка', 20);
INSERT INTO `rf_plintus` VALUES (2, 1, 'Равена', 15);
INSERT INTO `rf_plintus` VALUES (3, 1, 'Травертин', 10);
INSERT INTO `rf_plintus` VALUES (4, 2, '127 (алюміній)', 15);
INSERT INTO `rf_plintus` VALUES (5, 3, 'алюміній', 30);
INSERT INTO `rf_plintus` VALUES (6, 4, 'білий', 10);
INSERT INTO `rf_plintus` VALUES (7, 4, 'коричневий', 10);
INSERT INTO `rf_plintus` VALUES (8, 1, 'в колір стільниці', 10);
INSERT INTO `rf_plintus` VALUES (10, 2, 'в колір стільниці', 10);
INSERT INTO `rf_plintus` VALUES (11, 3, 'в колір стільниці', 10);
INSERT INTO `rf_plintus` VALUES (12, 4, 'в колір стільниці', 10);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_shyryna`
-- 

CREATE TABLE `rf_shyryna` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_shyryna`
-- 

INSERT INTO `rf_shyryna` VALUES (150);
INSERT INTO `rf_shyryna` VALUES (200);
INSERT INTO `rf_shyryna` VALUES (250);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_sklo`
-- 

CREATE TABLE `rf_sklo` (
  `id` int(11) NOT NULL,
  `modelj_sklo_id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_sklo`
-- 

INSERT INTO `rf_sklo` VALUES (1, 3, 0xd0bfd181d0b5d0b2d0b4d0bed0b2d196d182d180d0b0d0b6, 20);
INSERT INTO `rf_sklo` VALUES (2, 4, 0xd0bfd180d0bed0b7d0bed180d0b5, 25);
INSERT INTO `rf_sklo` VALUES (3, 1, 0xd0b1d0b5d0b2d0b5d0bbd1812d31, 30);
INSERT INTO `rf_sklo` VALUES (4, 1, 0xd0b1d0b5d0b2d0b5d0bbd1812d32, 35);
INSERT INTO `rf_sklo` VALUES (5, 1, 0xd0b1d0b5d0b2d0b5d0bbd1812d33, 37);
INSERT INTO `rf_sklo` VALUES (6, 1, 0xd0b1d0b5d0b2d0b5d0bbd1812d34, 44);
INSERT INTO `rf_sklo` VALUES (7, 1, 0xd0b1d0b5d0b2d0b5d0bbd1812d35, 47);
INSERT INTO `rf_sklo` VALUES (8, 2, 0xd0bfd196d181d0bad0bed181d182d180d183d0b92d31, 30);
INSERT INTO `rf_sklo` VALUES (9, 5, 0xd184d0b0d181d0b0d0b4d0bdd0b020d180d0b5d188d196d182d0bad0b0, 30);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_stiljnycja`
-- 

CREATE TABLE `rf_stiljnycja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelj_stiljnycja_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Дамп даних таблиці `rf_stiljnycja`
-- 

INSERT INTO `rf_stiljnycja` VALUES (1, 1, 'L406 (матова)', 20);
INSERT INTO `rf_stiljnycja` VALUES (2, 3, 'L003 (матова)', 100);
INSERT INTO `rf_stiljnycja` VALUES (3, 2, 'F005 (мозаїка)', 200);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_stiljnycja_glybyna`
-- 

CREATE TABLE `rf_stiljnycja_glybyna` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_stiljnycja_glybyna`
-- 

INSERT INTO `rf_stiljnycja_glybyna` VALUES (1, 'гл 60см');
INSERT INTO `rf_stiljnycja_glybyna` VALUES (2, 'гл 80см');
INSERT INTO `rf_stiljnycja_glybyna` VALUES (3, 'гл 90см');
INSERT INTO `rf_stiljnycja_glybyna` VALUES (4, 'гл 120см');
INSERT INTO `rf_stiljnycja_glybyna` VALUES (5, 'кут 90*90');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_stiln`
-- 

CREATE TABLE `rf_stiln` (
  `id` int(11) NOT NULL,
  `modelj_stiljnycja_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_stiln`
-- 

INSERT INTO `rf_stiln` VALUES (1, 2, 'ed', 432);
INSERT INTO `rf_stiln` VALUES (2, 1, 'weq', 3);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_tovar`
-- 

CREATE TABLE `rf_tovar` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_tovar`
-- 

INSERT INTO `rf_tovar` VALUES (1, 'ДСП', 80);
INSERT INTO `rf_tovar` VALUES (2, 'ДВП', 22);
INSERT INTO `rf_tovar` VALUES (3, 'ПВЦ', 10);
INSERT INTO `rf_tovar` VALUES (4, 'Завешки', 2);
INSERT INTO `rf_tovar` VALUES (5, 'Полкотримачі', 1);
INSERT INTO `rf_tovar` VALUES (6, 'Кути внутрішні (плінтус)', 1);
INSERT INTO `rf_tovar` VALUES (7, 'Кути зовнішні (плінтус)', 2);
INSERT INTO `rf_tovar` VALUES (8, 'Закінчення ліве (плінтус)', 3);
INSERT INTO `rf_tovar` VALUES (9, 'Закінчення праве (плінтус)', 2);
INSERT INTO `rf_tovar` VALUES (10, 'Кути внутрішні (цоколь)', 10);
INSERT INTO `rf_tovar` VALUES (11, 'Кути зовнішні (цоколь)', 50);
INSERT INTO `rf_tovar` VALUES (12, 'Кути універсальні (цоколь)', 20);
INSERT INTO `rf_tovar` VALUES (13, 'Заглушка (цоколь)', 20);
INSERT INTO `rf_tovar` VALUES (14, 'Алюміній', 100);
INSERT INTO `rf_tovar` VALUES (15, 'ДСП Н=100', 15);
INSERT INTO `rf_tovar` VALUES (16, 'ДСП Н=150', 40);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_tumba`
-- 

CREATE TABLE `rf_tumba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL COMMENT 'Назва тумбочки',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8 AUTO_INCREMENT=268 ;

-- 
-- Дамп даних таблиці `rf_tumba`
-- 

INSERT INTO `rf_tumba` VALUES (1, 'ШНС90');
INSERT INTO `rf_tumba` VALUES (2, 'ШНС80');
INSERT INTO `rf_tumba` VALUES (3, 'ШНС70');
INSERT INTO `rf_tumba` VALUES (4, 'ШНС60');
INSERT INTO `rf_tumba` VALUES (5, 'ШНС50/2');
INSERT INTO `rf_tumba` VALUES (6, 'ШНС60/1');
INSERT INTO `rf_tumba` VALUES (7, 'ШНС50');
INSERT INTO `rf_tumba` VALUES (8, 'ШНС40');
INSERT INTO `rf_tumba` VALUES (9, 'ШН100');
INSERT INTO `rf_tumba` VALUES (10, 'ШН90');
INSERT INTO `rf_tumba` VALUES (11, 'ШН80');
INSERT INTO `rf_tumba` VALUES (12, 'ШН70');
INSERT INTO `rf_tumba` VALUES (13, 'ШН60');
INSERT INTO `rf_tumba` VALUES (14, 'ШН50/2');
INSERT INTO `rf_tumba` VALUES (15, 'ШН60/1');
INSERT INTO `rf_tumba` VALUES (16, 'ШН50');
INSERT INTO `rf_tumba` VALUES (17, 'ШН45');
INSERT INTO `rf_tumba` VALUES (18, 'ШН40');
INSERT INTO `rf_tumba` VALUES (19, 'ШН35');
INSERT INTO `rf_tumba` VALUES (20, 'ШН30');
INSERT INTO `rf_tumba` VALUES (21, 'ШН25');
INSERT INTO `rf_tumba` VALUES (22, 'ШН20');
INSERT INTO `rf_tumba` VALUES (23, 'ШН15');
INSERT INTO `rf_tumba` VALUES (24, 'ШНУ60/60');
INSERT INTO `rf_tumba` VALUES (25, 'ШНУГ60/60');
INSERT INTO `rf_tumba` VALUES (26, 'ШН40скос');
INSERT INTO `rf_tumba` VALUES (27, 'ШН30скос');
INSERT INTO `rf_tumba` VALUES (28, 'ШН20скос');
INSERT INTO `rf_tumba` VALUES (29, 'ШН100Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (30, 'ШН100Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (31, 'ШН90Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (32, 'ШН80Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (33, 'ШН70Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (34, 'ШН60Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (35, 'ШН50Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (36, 'ШН40Т/2вверх(355,355)');
INSERT INTO `rf_tumba` VALUES (37, 'ШН80 трио');
INSERT INTO `rf_tumba` VALUES (38, 'ШН60 трио');
INSERT INTO `rf_tumba` VALUES (39, 'ШН40 арка');
INSERT INTO `rf_tumba` VALUES (40, 'ШН30 арка');
INSERT INTO `rf_tumba` VALUES (41, 'ШНС90(915)');
INSERT INTO `rf_tumba` VALUES (42, 'ШНС80(915)');
INSERT INTO `rf_tumba` VALUES (43, 'ШНС70(915)');
INSERT INTO `rf_tumba` VALUES (44, 'ШНС60(915)');
INSERT INTO `rf_tumba` VALUES (45, 'ШНС50/2(915)');
INSERT INTO `rf_tumba` VALUES (46, 'ШНС60/1(915)');
INSERT INTO `rf_tumba` VALUES (47, 'ШНС50(915)');
INSERT INTO `rf_tumba` VALUES (48, 'ШНС40(915)');
INSERT INTO `rf_tumba` VALUES (49, 'ШН100(915)');
INSERT INTO `rf_tumba` VALUES (50, 'ШН90(915)');
INSERT INTO `rf_tumba` VALUES (51, 'ШН80(915)');
INSERT INTO `rf_tumba` VALUES (52, 'ШН70(915)');
INSERT INTO `rf_tumba` VALUES (53, 'ШН60(915)');
INSERT INTO `rf_tumba` VALUES (54, 'ШН50/2(915)');
INSERT INTO `rf_tumba` VALUES (55, 'ШН60/1(915)');
INSERT INTO `rf_tumba` VALUES (56, 'ШН50(915)');
INSERT INTO `rf_tumba` VALUES (57, 'ШН45(915)');
INSERT INTO `rf_tumba` VALUES (58, 'ШН40(915)');
INSERT INTO `rf_tumba` VALUES (59, 'ШН35(915)');
INSERT INTO `rf_tumba` VALUES (60, 'ШН30(915)');
INSERT INTO `rf_tumba` VALUES (61, 'ШН25(915)');
INSERT INTO `rf_tumba` VALUES (62, 'ШН20(915)');
INSERT INTO `rf_tumba` VALUES (63, 'ШН15(915)');
INSERT INTO `rf_tumba` VALUES (64, 'ШНУ60/60(915)');
INSERT INTO `rf_tumba` VALUES (65, 'ШНУГ60/60(915)');
INSERT INTO `rf_tumba` VALUES (66, 'ШН40скос(915)');
INSERT INTO `rf_tumba` VALUES (67, 'ШН30скос(915)');
INSERT INTO `rf_tumba` VALUES (68, 'ШН20скос(915)');
INSERT INTO `rf_tumba` VALUES (69, 'ШН100Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (70, 'ШН900Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (71, 'ШН80Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (72, 'ШН70Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (73, 'ШН60Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (74, 'ШН50Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (75, 'ШН40Т/2вверх(915)(ниша,355,355)');
INSERT INTO `rf_tumba` VALUES (76, 'ШН100Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (77, 'ШН90Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (78, 'ШН80Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (79, 'ШН70Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (80, 'ШН60Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (81, 'ШН50Т/2(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (82, 'ШН50Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (83, 'ШН45Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (84, 'ШН40Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (85, 'ШН35Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (86, 'ШН30Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (87, 'ШН25Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (88, 'ШН20Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (89, 'ШН15Т(915)ф.713 сн');
INSERT INTO `rf_tumba` VALUES (90, 'ШН80 трио(915)');
INSERT INTO `rf_tumba` VALUES (91, 'ШН60 трио(915)');
INSERT INTO `rf_tumba` VALUES (92, 'ШН40 арка(915)');
INSERT INTO `rf_tumba` VALUES (93, 'ШН30 арка(915)');
INSERT INTO `rf_tumba` VALUES (94, 'ШНГ 100/1');
INSERT INTO `rf_tumba` VALUES (95, 'ШНГ 90/1');
INSERT INTO `rf_tumba` VALUES (96, 'ШНГ 80/1');
INSERT INTO `rf_tumba` VALUES (97, 'ШНГ 70/1');
INSERT INTO `rf_tumba` VALUES (98, 'ШНГ 60/1');
INSERT INTO `rf_tumba` VALUES (99, 'ШНГ 50');
INSERT INTO `rf_tumba` VALUES (100, 'ШНГ 60');
INSERT INTO `rf_tumba` VALUES (101, 'ШНГ 100/1(360)');
INSERT INTO `rf_tumba` VALUES (102, 'ШНГ 90/1(360)');
INSERT INTO `rf_tumba` VALUES (103, 'ШНГ 80/1(360)');
INSERT INTO `rf_tumba` VALUES (104, 'ШНГ 70/1(360)');
INSERT INTO `rf_tumba` VALUES (105, 'ШНГ 60/1(360)');
INSERT INTO `rf_tumba` VALUES (106, 'ШНГ 50(360)');
INSERT INTO `rf_tumba` VALUES (107, 'ШНС80(600)');
INSERT INTO `rf_tumba` VALUES (108, 'ШНС60(600)');
INSERT INTO `rf_tumba` VALUES (109, 'ШНС50/2(600)');
INSERT INTO `rf_tumba` VALUES (110, 'ШНС50(600)');
INSERT INTO `rf_tumba` VALUES (111, 'ШН80(600)');
INSERT INTO `rf_tumba` VALUES (112, 'ШН60(600)');
INSERT INTO `rf_tumba` VALUES (113, 'ШН50/2(600)');
INSERT INTO `rf_tumba` VALUES (114, 'ШН60/1(600)');
INSERT INTO `rf_tumba` VALUES (115, 'ШН50(600)');
INSERT INTO `rf_tumba` VALUES (116, 'ШН40(600)');
INSERT INTO `rf_tumba` VALUES (117, 'ШН30(600)');
INSERT INTO `rf_tumba` VALUES (118, 'ШНУ60/60(600)');
INSERT INTO `rf_tumba` VALUES (119, 'ШНС80(960фр)');
INSERT INTO `rf_tumba` VALUES (120, 'ШНС60(960фр)');
INSERT INTO `rf_tumba` VALUES (121, 'ШНС50/2(960фр)');
INSERT INTO `rf_tumba` VALUES (122, 'ШНС50(960фр)');
INSERT INTO `rf_tumba` VALUES (123, 'ШН80(960фр)');
INSERT INTO `rf_tumba` VALUES (124, 'ШН60(960фр)');
INSERT INTO `rf_tumba` VALUES (125, 'ШН50/2(960фр)');
INSERT INTO `rf_tumba` VALUES (126, 'ШН50(960фр)');
INSERT INTO `rf_tumba` VALUES (127, 'ШН40(960фр)');
INSERT INTO `rf_tumba` VALUES (128, 'ШН30(960фр)');
INSERT INTO `rf_tumba` VALUES (129, 'ШНЗ 30(720)');
INSERT INTO `rf_tumba` VALUES (130, 'ШНЗ 30(915)');
INSERT INTO `rf_tumba` VALUES (131, 'ШЛЗ 30(база 560 мм), дв 713х270');
INSERT INTO `rf_tumba` VALUES (132, 'ШНЗУ 60/60(720)');
INSERT INTO `rf_tumba` VALUES (133, 'ШНЗУ 60/60(915)');
INSERT INTO `rf_tumba` VALUES (134, 'ШЛЗУ 90/90(база 530 мм)');
INSERT INTO `rf_tumba` VALUES (135, 'ШНП40(720)');
INSERT INTO `rf_tumba` VALUES (136, 'ШНП35(720)');
INSERT INTO `rf_tumba` VALUES (137, 'ШНП30(720)');
INSERT INTO `rf_tumba` VALUES (138, 'ШНП25(720)');
INSERT INTO `rf_tumba` VALUES (139, 'ШНП20(720)');
INSERT INTO `rf_tumba` VALUES (140, 'ШНП15(720)');
INSERT INTO `rf_tumba` VALUES (141, 'ШНП40(915)');
INSERT INTO `rf_tumba` VALUES (142, 'ШНП35(915)');
INSERT INTO `rf_tumba` VALUES (143, 'ШНП30(915)');
INSERT INTO `rf_tumba` VALUES (144, 'ШНП25(915)');
INSERT INTO `rf_tumba` VALUES (145, 'ШНП20(915)');
INSERT INTO `rf_tumba` VALUES (146, 'ШНП15(915)');
INSERT INTO `rf_tumba` VALUES (147, 'ШЛП40(база560мм)');
INSERT INTO `rf_tumba` VALUES (148, 'ШЛП35(база560мм)');
INSERT INTO `rf_tumba` VALUES (149, 'ШЛП30(база560мм)');
INSERT INTO `rf_tumba` VALUES (150, 'ШЛП25(база560мм)');
INSERT INTO `rf_tumba` VALUES (151, 'ШЛП20(база560мм)');
INSERT INTO `rf_tumba` VALUES (152, 'ШЛП15(база560мм)');
INSERT INTO `rf_tumba` VALUES (153, 'ШЛМ100');
INSERT INTO `rf_tumba` VALUES (154, 'ШЛМ90');
INSERT INTO `rf_tumba` VALUES (155, 'ШЛМ80');
INSERT INTO `rf_tumba` VALUES (156, 'ШЛМ70');
INSERT INTO `rf_tumba` VALUES (157, 'ШЛМ60');
INSERT INTO `rf_tumba` VALUES (158, 'ШЛМ50/2');
INSERT INTO `rf_tumba` VALUES (159, 'ШЛМ60/1');
INSERT INTO `rf_tumba` VALUES (160, 'ШЛМ50');
INSERT INTO `rf_tumba` VALUES (161, 'ШЛМ40');
INSERT INTO `rf_tumba` VALUES (162, 'ШЛ100');
INSERT INTO `rf_tumba` VALUES (163, 'ШЛ90');
INSERT INTO `rf_tumba` VALUES (164, 'ШЛ80');
INSERT INTO `rf_tumba` VALUES (165, 'ШЛ70');
INSERT INTO `rf_tumba` VALUES (166, 'ШЛ60');
INSERT INTO `rf_tumba` VALUES (167, 'ШЛ50/2');
INSERT INTO `rf_tumba` VALUES (168, 'ШЛ60/1');
INSERT INTO `rf_tumba` VALUES (169, 'ШЛ50');
INSERT INTO `rf_tumba` VALUES (170, 'ШЛ45');
INSERT INTO `rf_tumba` VALUES (171, 'ШЛ40');
INSERT INTO `rf_tumba` VALUES (172, 'ШЛ35');
INSERT INTO `rf_tumba` VALUES (173, 'ШЛ30');
INSERT INTO `rf_tumba` VALUES (174, 'ШЛ25');
INSERT INTO `rf_tumba` VALUES (175, 'ШЛ20');
INSERT INTO `rf_tumba` VALUES (176, 'ШЛ15');
INSERT INTO `rf_tumba` VALUES (177, 'ШЛ80 трио');
INSERT INTO `rf_tumba` VALUES (178, 'ШЛ60 трио');
INSERT INTO `rf_tumba` VALUES (179, 'ШЛ40 скос(база 560мм)');
INSERT INTO `rf_tumba` VALUES (180, 'ШЛ30 скос(база 560мм)');
INSERT INTO `rf_tumba` VALUES (181, 'ШЛ20 скос(база 560мм)');
INSERT INTO `rf_tumba` VALUES (182, 'ШЛК120,1дв.60');
INSERT INTO `rf_tumba` VALUES (183, 'ШЛК110,1дв.50');
INSERT INTO `rf_tumba` VALUES (184, 'ШЛК105,1дв.45');
INSERT INTO `rf_tumba` VALUES (185, 'ШЛК100,1дв.40');
INSERT INTO `rf_tumba` VALUES (186, 'ПН80(1300)');
INSERT INTO `rf_tumba` VALUES (187, 'ПН60(1300)');
INSERT INTO `rf_tumba` VALUES (188, 'ПН50(1300)');
INSERT INTO `rf_tumba` VALUES (189, 'ПН40(1300)');
INSERT INTO `rf_tumba` VALUES (190, 'ПН30(1300)');
INSERT INTO `rf_tumba` VALUES (191, 'ПЛ80(1300)');
INSERT INTO `rf_tumba` VALUES (192, 'ПЛ60(1300)');
INSERT INTO `rf_tumba` VALUES (193, 'ПЛ50(1300)');
INSERT INTO `rf_tumba` VALUES (194, 'ПЛ40(1300)');
INSERT INTO `rf_tumba` VALUES (195, 'ПЛШ40(1300,176х4)');
INSERT INTO `rf_tumba` VALUES (196, 'ПЛ80(910,713)');
INSERT INTO `rf_tumba` VALUES (197, 'ПЛ60(910,713)');
INSERT INTO `rf_tumba` VALUES (198, 'ПЛ50(910,713)');
INSERT INTO `rf_tumba` VALUES (199, 'ПЛ40(910,713)');
INSERT INTO `rf_tumba` VALUES (200, 'ПЛ80(1300,713)');
INSERT INTO `rf_tumba` VALUES (201, 'ПЛ60(1300,713)');
INSERT INTO `rf_tumba` VALUES (202, 'ПЛ60/1(1300,713)');
INSERT INTO `rf_tumba` VALUES (203, 'ПЛ50(1300,713)');
INSERT INTO `rf_tumba` VALUES (204, 'ПЛ40(1300,713)');
INSERT INTO `rf_tumba` VALUES (205, 'ПЛ30(1300,713)');
INSERT INTO `rf_tumba` VALUES (206, 'ПЛ80(713,713,713)');
INSERT INTO `rf_tumba` VALUES (207, 'ПЛ80/1(713,713,713)');
INSERT INTO `rf_tumba` VALUES (208, 'ПЛ50(713,713,713)');
INSERT INTO `rf_tumba` VALUES (209, 'ПЛ40(713,713,713)');
INSERT INTO `rf_tumba` VALUES (210, 'ПЛ30(713,713,713)');
INSERT INTO `rf_tumba` VALUES (211, 'ПЛ60(910,713,713)');
INSERT INTO `rf_tumba` VALUES (212, 'ПЛ50(910,713,713)');
INSERT INTO `rf_tumba` VALUES (213, 'ПЛ40(910,713,713)');
INSERT INTO `rf_tumba` VALUES (214, 'ПЛ60(910,570,713)');
INSERT INTO `rf_tumba` VALUES (215, 'ПЛ50(910,570,713)');
INSERT INTO `rf_tumba` VALUES (216, 'ПЛ40(910,570,713)');
INSERT INTO `rf_tumba` VALUES (217, 'ПЛХ60(450,1100,713)');
INSERT INTO `rf_tumba` VALUES (218, 'ПЛД60(713,ниша580,713)');
INSERT INTO `rf_tumba` VALUES (219, 'ПЛД60(ниша580,713)');
INSERT INTO `rf_tumba` VALUES (220, 'ПЛШ60(ниша580,140,283,283)(база560мм)');
INSERT INTO `rf_tumba` VALUES (221, 'ПЛШ60(ниша580,355,355)(база560мм)');
INSERT INTO `rf_tumba` VALUES (222, 'ПЛШ60(ниша580,355)(база560мм)');
INSERT INTO `rf_tumba` VALUES (223, 'ШЛШ80');
INSERT INTO `rf_tumba` VALUES (224, 'ШЛШ70');
INSERT INTO `rf_tumba` VALUES (225, 'ШЛШ60');
INSERT INTO `rf_tumba` VALUES (226, 'ШЛШ50(ш140х496,дв570х246 2шт');
INSERT INTO `rf_tumba` VALUES (227, 'ШЛШ50');
INSERT INTO `rf_tumba` VALUES (228, 'ШЛШ40');
INSERT INTO `rf_tumba` VALUES (229, 'ШЛШ30');
INSERT INTO `rf_tumba` VALUES (230, 'ШЛШ80/2-2');
INSERT INTO `rf_tumba` VALUES (231, 'ШЛШ70/2-2');
INSERT INTO `rf_tumba` VALUES (232, 'ШЛШ60/2-2');
INSERT INTO `rf_tumba` VALUES (233, 'ШЛШ80/2(355,355)');
INSERT INTO `rf_tumba` VALUES (234, 'ШЛШ70/2(355,355)');
INSERT INTO `rf_tumba` VALUES (235, 'ШЛШ60/2(355,355)');
INSERT INTO `rf_tumba` VALUES (236, 'ШЛШ50/2(355,355)');
INSERT INTO `rf_tumba` VALUES (237, 'ШЛШ40/2(355,355)');
INSERT INTO `rf_tumba` VALUES (238, 'ШЛШ30/2(355,355)');
INSERT INTO `rf_tumba` VALUES (239, 'ШЛШ80/2-1');
INSERT INTO `rf_tumba` VALUES (240, 'ШЛШ70/2-1');
INSERT INTO `rf_tumba` VALUES (241, 'ШЛШ60/2-1');
INSERT INTO `rf_tumba` VALUES (242, 'ШЛШ50/2-1');
INSERT INTO `rf_tumba` VALUES (243, 'ШЛШ40/2-1');
INSERT INTO `rf_tumba` VALUES (244, 'ШЛШ30/2-1');
INSERT INTO `rf_tumba` VALUES (245, 'ШЛШ80/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (246, 'ШЛШ70/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (247, 'ШЛШ60/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (248, 'ШЛШ50/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (249, 'ШЛШ40/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (250, 'ШЛШ30/3(176,176,355)');
INSERT INTO `rf_tumba` VALUES (251, 'ШЛШ80/3-1');
INSERT INTO `rf_tumba` VALUES (252, 'ШЛШ60/3-1');
INSERT INTO `rf_tumba` VALUES (253, 'ШЛШ50/3-1');
INSERT INTO `rf_tumba` VALUES (254, 'ШЛШ40/3-1');
INSERT INTO `rf_tumba` VALUES (255, 'ШЛШ30/3-1');
INSERT INTO `rf_tumba` VALUES (256, 'ШЛШ80/4');
INSERT INTO `rf_tumba` VALUES (257, 'ШЛШ60/4');
INSERT INTO `rf_tumba` VALUES (258, 'ШЛШ50/4');
INSERT INTO `rf_tumba` VALUES (259, 'ШЛШ40/4');
INSERT INTO `rf_tumba` VALUES (260, 'ШЛШ30/4');
INSERT INTO `rf_tumba` VALUES (261, 'ШЛШ80 Т(ниша,355)');
INSERT INTO `rf_tumba` VALUES (262, 'ШЛШ70 Т(ниша,355)');
INSERT INTO `rf_tumba` VALUES (263, 'ШЛШ60 Т(ниша,355)');
INSERT INTO `rf_tumba` VALUES (264, 'ШЛШ50 Т(ниша,355)');
INSERT INTO `rf_tumba` VALUES (265, 'ШЛГП60');
INSERT INTO `rf_tumba` VALUES (266, 'ШЛУГ 90/90,дв.713х310,(база 530мм)');
INSERT INTO `rf_tumba` VALUES (267, 'ШЛУГ 90/90Н,дв.713х446,(база 530мм)');

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_vyb_glub`
-- 

CREATE TABLE `rf_vyb_glub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `glub_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- 
-- Дамп даних таблиці `rf_vyb_glub`
-- 

INSERT INTO `rf_vyb_glub` VALUES (1, 1, 1, 15);
INSERT INTO `rf_vyb_glub` VALUES (2, 1, 2, 25);
INSERT INTO `rf_vyb_glub` VALUES (3, 1, 3, 30);
INSERT INTO `rf_vyb_glub` VALUES (4, 1, 4, 35);
INSERT INTO `rf_vyb_glub` VALUES (5, 1, 5, 40);
INSERT INTO `rf_vyb_glub` VALUES (6, 2, 1, 20);
INSERT INTO `rf_vyb_glub` VALUES (7, 2, 2, 25);
INSERT INTO `rf_vyb_glub` VALUES (8, 2, 3, 35);
INSERT INTO `rf_vyb_glub` VALUES (9, 2, 4, 45);
INSERT INTO `rf_vyb_glub` VALUES (10, 2, 5, 55);
INSERT INTO `rf_vyb_glub` VALUES (11, 3, 1, 30);

-- --------------------------------------------------------

-- 
-- Структура таблиці `rf_vysota`
-- 

CREATE TABLE `rf_vysota` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `rf_vysota`
-- 

INSERT INTO `rf_vysota` VALUES (601);
INSERT INTO `rf_vysota` VALUES (718);
INSERT INTO `rf_vysota` VALUES (915);

-- --------------------------------------------------------

-- 
-- Структура таблиці `test1`
-- 

CREATE TABLE `test1` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `firma_name` text NOT NULL,
  `price` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Дамп даних таблиці `test1`
-- 

INSERT INTO `test1` VALUES (1, 'METALLA A петля 110 ° накладна сталева нікельована \r\n55.5х13.1мм шаблон свердління: 48/6 глибина: 12мм + Монтажна планка METALLA  А хрестова, \r\nпід шуруп, 0 мм, 37/32, сталева, нікельована', '1', '3');
INSERT INTO `test1` VALUES (2, 'METALLA A петля 45° сталь нікельована шаблон: 48/6 під шуруп + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', '5');
INSERT INTO `test1` VALUES (3, 'METALLA A петля 30° сталь нікельована шаблон: 48/6 під шуруп + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', '3');
INSERT INTO `test1` VALUES (4, 'METALLA A петля 180° рівнолегла + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', '5');
INSERT INTO `test1` VALUES (5, 'METALLA A Петля 165 ° (крокодил) + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', '12');
INSERT INTO `test1` VALUES (6, 'METALLAMAT A петля 60° (допоміжна до крокодилу) + Монтажна планка METALLA  А хрестова, під шуруп, 0 мм, 37/32, сталева, нікельована', '1', '23');
INSERT INTO `test1` VALUES (7, 'METALLA D SM петля  110° з дотягом + Монтажна планка METALLA SM хрестова 0мм сталева оцинкована під шуруп з регулюванням', '1', '14');
INSERT INTO `test1` VALUES (8, 'SALICE SILENTIA петля 180° (рівнолегла) з дотягом + Планка монтажна DUOMATIC SM хрестовидна 3 мм \r\nцамак нікельована під шуруп', '1', '38');
INSERT INTO `test1` VALUES (9, 'SILENTIA Петля 45° з дотягом + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', '32');
INSERT INTO `test1` VALUES (10, 'Петля SALICE SILENTIA 700 30° сталь нікельована шаблон: 45/9.5 під шуруп + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', '32');
INSERT INTO `test1` VALUES (11, 'SILENTIA Петля 155° (крокодил) з дотягом + Монтажна планка DUOMATIC SL хрестова сталева 0мм під шуруп', '1', '57');
INSERT INTO `test1` VALUES (12, 'PUSH DUOMATIC петля 110° накладна сталь нікельована шаблон: 45/9.5мм під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', '32');
INSERT INTO `test1` VALUES (13, 'PUSH DUOMATIC петля 110° внутрішня сталева нікельована шаблон свердління 45/9.5 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', '52');
INSERT INTO `test1` VALUES (14, 'PUSH DUOMATIC петля  94° пряма сталева нікельована шаблон свердління 48/6 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 3 мм цамак нікельована під шуруп', '1', '67');
INSERT INTO `test1` VALUES (15, 'PUSH DUOMATIC петля  165° накладна шаблон свердління 48/6 під шуруп + Планка монтажна DUOMATIC SM хрестовидна 0 мм цамак нікельована під шуруп', '1', '67');
INSERT INTO `test1` VALUES (16, 'PUSH Магнітний тримач для петель Push колір \r\nсірий довжина 40мм зі штирем', '1', '25');
INSERT INTO `test1` VALUES (17, 'PUSH Адаптер для магнітної защіпки з позиціонуванням пластиковий бежевий', '1', '5');
INSERT INTO `test1` VALUES (18, ' FREE 3677 Повний комплект кут розкриття 100° модель В, С, D, E', '1', '320');
INSERT INTO `test1` VALUES (19, ' FREE 3677 Повний комплект кут розкриття 100° модель F', '1', '422');
INSERT INTO `test1` VALUES (20, ' FREE 3677 Кріплення до фасаду з алюмінієвими рамами 20мм товщина бокової стінки 19мм цамак хромоване матове', '1', '105');
INSERT INTO `test1` VALUES (21, 'STRATO SM 3685 Повний комплект висота фасаду: від 342-420мм вага фасаду: 4-7.2кг до 500-550мм вага фасаду: 3.8-10кг', '1', '850');
INSERT INTO `test1` VALUES (22, 'STRATO SM 3685 Повний комплект висота фасаду: від 500-550мм вага фасаду: 6-15кг до 550-650 мм вага фасаду: 6-14,5 кг', '1', '920');
INSERT INTO `test1` VALUES (23, 'STRATO Кріплення до фасадів з алюмінієвими рамами 20мм товщина бокової стінки 19мм сталь хромована матова', '1', '130');
INSERT INTO `test1` VALUES (24, 'Ewiva Ком.підй.фур. для лиц.пан. дер.з ал.проф.зручк.шир.до 600мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+аморт+з''єдн.для лиц.пан+синхр.штанга (0015600050)', '1', '733');
INSERT INTO `test1` VALUES (25, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев..шир.до 900мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (2 шт.) (0015570050)', '1', '579');
INSERT INTO `test1` VALUES (26, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев.. шир.до 1200мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (0015580050 (2 шт.)', '1', '591');
INSERT INTO `test1` VALUES (27, 'Ewiva Ком.підй.фурн. для лиц.пан.дерев.. шир.до 1500мм,Н=320мм,ал.оцинк.(бок.секц.+кришка+з''єдн.для лиц.пан. (0015650050 (2 шт.)', '1', '604');
INSERT INTO `test1` VALUES (28, 'Ewiva Набір амортизаторів (2 пар лів.х прав.) до підйомної фурнітури EWIVA колір сірий вага панелі 2.5-4.8 кг з ручкою (0015627931)', '1', '141');
INSERT INTO `test1` VALUES (29, 'Ewiva Набір амортизаторів (пар лів.х прав.)  до підйомної фурнітури колір сірий вага панелі 3.3-6.6 кг з ручкою (0015637931)', '1', '149');
INSERT INTO `test1` VALUES (30, 'Набір амортизаторів (2 пар лів.х прав.) до підйомної фурнітури EWIVA колір сірий вага панелі 5.3-8.0 кг з ручкою (0015647931)', '1', '145');
INSERT INTO `test1` VALUES (31, 'Ewiva Опора для алюмінієвого профілю лиц.панелі підйомного механізму (2 пари) (0015450050)', '1', '131');
INSERT INTO `test1` VALUES (32, 'SENSO 3674 повний комплект висота фасаду: 540-580мм вага фасаду: 3-6.5 кг', '1', '550');
INSERT INTO `test1` VALUES (33, 'VERSO SM 3684 Повний комплект модель А,B,C,D', '1', '690');
INSERT INTO `test1` VALUES (34, 'WOODPRO Направляючі прихованого монтажу , 450мм, част. витяг. сталь оцинк., м’яке закриття, пласт. кріплення,  товщина боковини 16-19мм', '1', '75');
INSERT INTO `test1` VALUES (35, 'WOODPRO Направляючі прихованого монтажу , 450мм, повний витяг. сталь оцинк., м’яке закриття, пласт. кріплення,  товщина боковини 16-19мм', '1', '125');
INSERT INTO `test1` VALUES (36, ' PUSH, Направляючі ролик. прихов. монтажу 460мм  повний. витяг, навантаж. до 25 кг  частков. висув.сталь оцинков.', '1', '152');
INSERT INTO `test1` VALUES (37, 'PUSH, Направляючі кульк. прихов. монтажу 452мм  част. витяг навантаж. до 25 кг  частков. висув.сталь оцинков.', '1', '80');
INSERT INTO `test1` VALUES (38, 'Метал. бокс 450 мм, Н=54 мм, сталь, білий', '1', '40');
INSERT INTO `test1` VALUES (39, 'Метал. бокс 450 мм, Н=86 мм, сталь, білий', '1', '45');
INSERT INTO `test1` VALUES (40, 'Метал.бокс 450 мм, Н=150 мм, сталь, білий', '1', '60');
INSERT INTO `test1` VALUES (41, 'Метал.бокс система поздовжньої огорожі 450 мм, ст., білий', '1', '21');
INSERT INTO `test1` VALUES (42, 'Метал бокс Демпфер пластиковий сірий для метал.бокс', '1', '15');
INSERT INTO `test1` VALUES (43, 'MOOVIT Система для висувних ящиків з доводчиком сіра 92/450мм 50кг', '1', '235');
INSERT INTO `test1` VALUES (44, 'MOOVIT Система для висувних ящиків  з доводчиком біла 92/450мм 50кг', '1', '235');
INSERT INTO `test1` VALUES (45, 'MOOVIT Релінги для MOOVIT сталеві сірі/білі 450мм', '1', '40');
INSERT INTO `test1` VALUES (46, 'MOOVIT Фасад для внутрішнього висувного ящика алюмінієвий сірий/білий 600мм, 900мм, 1200мм + тримач фасаду + накладка для стабілізації', '1', '225');
INSERT INTO `test1` VALUES (47, 'Classic Карго 150мм 2-рівневе 90° з доводжувачем, сталь. хром.', '2', '860');
INSERT INTO `test1` VALUES (48, ' STYLE Карго 150мм 2-рівневе 90° з доводжувачем, сталь. хром.', '2', '950');
INSERT INTO `test1` VALUES (49, ' CLASSIC Карго для рушників 150мм  90° з доводжувачем, сталь. хром.', '2', '950');
INSERT INTO `test1` VALUES (50, 'STYLE Карго для рушників 150мм 90° з доводжувачем, сталь. хром.', '2', '990');
INSERT INTO `test1` VALUES (51, ' CLASSIC Карго для піддону для випічки, 150мм 90° з доводжувачем, сталь. хром.', '2', '930');
INSERT INTO `test1` VALUES (52, 'STYLE Карго для піддону для випічки, 150мм 90° з доводжувачем, сталь. хром.', '2', '980');
INSERT INTO `test1` VALUES (53, 'CLASSIC Розділювач до висувної корзини 150мм сталь хром.', '2', '30');
INSERT INTO `test1` VALUES (54, 'Розділювач STYLE до висувної корзини 150мм сталь хром.', '2', '35');
INSERT INTO `test1` VALUES (55, 'Комплект дротяних висувних полиць 400мм\r\n', '2', '750');
INSERT INTO `test1` VALUES (56, 'Комплект дротяних висувних полиць 500мм', '2', '780');
INSERT INTO `test1` VALUES (57, 'Комплект дротяних висувних полиць 600мм', '2', '800');
INSERT INTO `test1` VALUES (58, 'Comfort 30 Повний комплект карго, праве/ліве, з трьома дротяними корзинами на 110 мм ', '2', '1150');
INSERT INTO `test1` VALUES (59, 'Comfort 30 Повний комплект карго, ліве/праве, з двома дротяними корзинами на 210 мм ', '2', '1050');
INSERT INTO `test1` VALUES (60, 'Comfort 30 Повний комплект карго Comfort 30, ліве/праве, з трьома дротяними корзинами на 210 мм ', '2', '1220');
INSERT INTO `test1` VALUES (61, 'Comfort 30 Повний комплект карго, ліве/праве, з двома  корзинами Arena Classic на 228 мм ', '2', '1617');
INSERT INTO `test1` VALUES (62, ' PORTERO Висувний механізм з доводжувачем хром.без покр/срібл.Н=400мм з 1 нав. корз.161x495x395мм\r\n', '2', '990');
INSERT INTO `test1` VALUES (63, 'PORTERO Висувний механізм з доводжувачем, хром.без покр/срібл.Н=400мм з 2 нав.корз.без руч274x495x395мм\r\n', '2', '1360');
INSERT INTO `test1` VALUES (64, 'PORTERO Висувний механізм з доводжувачем, хром.без покр/срібл.Н=511мм з 3 нав.корз\r\n', '2', '1954');
INSERT INTO `test1` VALUES (65, 'TOPFLEX Фурнітура для висувного столу  довж. висування 810 мм глиб.внутр.ящика 500мм навантаження до 30кг', '2', '1235');
INSERT INTO `test1` VALUES (66, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 450 мм, правий на 2 полиці', '2', '2300');
INSERT INTO `test1` VALUES (67, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 600 мм, правий на 2 полиці', '2', '3315');
INSERT INTO `test1` VALUES (68, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 450 мм, правий на 4 полиці', '2', '6025');
INSERT INTO `test1` VALUES (69, 'Le Mans 2 Plus Classic Повний комплект, правий/лівий для висувних кутових шаф , білий , 600 мм, правий на 4 полиці', '2', '6690');
INSERT INTO `test1` VALUES (70, 'Magic Corner Повний комплект висувний поворотний для кутових шаф\r\n(поворотно висувний блок та набір корзин 2+2), серія Arena\r\nCLASSIC, правий/лівий', '2', '4855');
INSERT INTO `test1` VALUES (71, 'Magic Corner Повний комплект висувний поворотний для кутових шаф\r\n(поворотно висувний блок та набір корзин 2+2), серія Arena\r\nSTYLE, правий/лівий', '2', '4980');
INSERT INTO `test1` VALUES (72, 'Комплект поворот. фурнітури 900х900 мм\r\nна 3/4 кола D820 мм, з двома полицями, дротяні полиці', '2', '1890');
INSERT INTO `test1` VALUES (73, 'Комплект поворот. фурнітури 900х900 мм\r\nна 3/4 кола D820 мм, з двома полицями, серія Arena CLASSIC', '2', '2145');
INSERT INTO `test1` VALUES (74, 'Dispensa Повний комплект шафи 300 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма корзинами Arena Classic 250x462x106 мм', '2', '3890');
INSERT INTO `test1` VALUES (75, ' Dispensa Dispensa Повний комплект шафи 300 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма дротяними корзинами 250x467x110 мм', '2', '3370');
INSERT INTO `test1` VALUES (76, ' Dispensa Повний комплект шафи 400 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма корзинами Arena Classic 250x462x106 мм', '2', '4275');
INSERT INTO `test1` VALUES (77, ' Dispensa Повний комплект шафи 400 мм висота 1600 - 2000 мм з функцією ClickFixx та SoftStopp з 5-ма дротяними корзинами 250x467x110 мм', '2', '3655');
INSERT INTO `test1` VALUES (78, ' Dispensa Набір ємкостей  для кутової шафи по 500мл 4 шт., прозорий, сірі кришки ', '2', '375');
INSERT INTO `test1` VALUES (79, ' Dispensa Набір лотків  2 ємкості для кутової шафи, пластм..,тримач алюм. ', '2', '250');
INSERT INTO `test1` VALUES (80, 'Tandem із SoftStopp Повний комплект для ширини шафи 600 мм висота 1700 мм колір рами алюміній (рама, 6 поличок), Arena CLASSIC', '2', '6475');
INSERT INTO `test1` VALUES (81, 'Tandem із SoftStopp Повний комплект TANDEM Arena Classic із SoftStopp для ширини шафи 600 мм  висота 1100 мм колір рами алюміній ', '2', '5160');
INSERT INTO `test1` VALUES (82, 'Tandem для ванни 570 мм три яруси', '2', '1405');
INSERT INTO `test1` VALUES (83, 'Tandem для ванни 360 мм два яруси', '2', '1125');
INSERT INTO `test1` VALUES (84, 'Світильник LED 2018 срібно-сірий 12V/2W тепле біле світло бокове кріплення', '3', '285');
INSERT INTO `test1` VALUES (85, 'Світильник LED 2018 срібно-сірий 12V/2W тепле біле світло нижнє кріплення', '3', '285');
INSERT INTO `test1` VALUES (86, 'Світильник LED 2001 колір: срібний 12V/1.7W холодне біле світло', '3', '122');
INSERT INTO `test1` VALUES (87, 'Світильник LED 2001 колір: білий 12V/1.7W холодне біле світло', '3', '122');
INSERT INTO `test1` VALUES (88, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W холодне біле світло', '3', '105');
INSERT INTO `test1` VALUES (89, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W тепле біле світло', '3', '105');
INSERT INTO `test1` VALUES (90, 'Світильник LED 2002 нержавіюча сталь 12V/1.5W голубе світло', '3', '105');
INSERT INTO `test1` VALUES (91, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W холодне біле світло 575мм', '3', '190');
INSERT INTO `test1` VALUES (92, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W холодне біле світло 820мм', '3', '295');
INSERT INTO `test1` VALUES (93, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W тепле біле світло 575мм', '3', '190');
INSERT INTO `test1` VALUES (94, 'Світильник LED 2004 алюмінієвий колір: срібний 12V/1.2W тепле біле світло 820мм', '3', '295');
INSERT INTO `test1` VALUES (95, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W тепле біле світло 575мм', '3', '190');
INSERT INTO `test1` VALUES (96, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W тепле біле світло 820мм', '3', '295');
INSERT INTO `test1` VALUES (97, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W холодне біле світло 575мм', '3', '190');
INSERT INTO `test1` VALUES (98, 'Світильник LED 2005 врізний алюмінієвий колір: срібний 12V/1.2W холодне біле світло 820мм', '3', '310');
INSERT INTO `test1` VALUES (99, 'Полиця скляна з LED 2006 12V/2-4W RGB 70х175х600мм', '3', '620');
INSERT INTO `test1` VALUES (100, 'Полиця скляна з LED 2006 12V/2-6W RGB 70x175x900мм', '3', '800');
INSERT INTO `test1` VALUES (101, 'Полиця скляна з LED 2006 12V/4W голубе світло 70х175х600мм', '3', '480');
INSERT INTO `test1` VALUES (102, 'Полиця скляна з LED 2006 12V/5W голубе світло 70х175х900мм', '3', '640');
INSERT INTO `test1` VALUES (103, 'Полиця скляна з LED 2006 12V/4W холодне біле світло 70х175х600мм', '3', '455');
INSERT INTO `test1` VALUES (104, 'Полиця скляна з LED 2006 12V/5W холодне біле світло 70х175х900мм', '3', '610');
INSERT INTO `test1` VALUES (105, 'Профіль LED 2007 12V/2.5W алюміній колір: срібний тепле біле світло 12х15х563мм', '3', '180');
INSERT INTO `test1` VALUES (106, 'Профіль LED 2007 12V/4W алюміній колір: срібний тепле біле світло 12х15х863мм', '3', '257');
INSERT INTO `test1` VALUES (107, 'Профіль LED 2007 12V/2.5W алюміній колір: срібний голубе світло 12х15х563мм', '3', '180');
INSERT INTO `test1` VALUES (108, 'Профіль LED 2007 12V/4W алюміній колір: срібний голубе світло 12х15х863мм', '3', '270');
INSERT INTO `test1` VALUES (109, 'Світильник LED 2003 з сенсорним вимикачем 12V/3.2W алюміній колір: срібний холодне біле світло 20х30х600мм', '3', '356');
INSERT INTO `test1` VALUES (110, 'Світильник LED 2003 з сенсорним вимикачем 12V/3.2W алюміній колір: срібний холодне біле світло 20х30х900мм', '3', '490');
INSERT INTO `test1` VALUES (111, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/1.6W холодне біле світло 405мм', '3', '300');
INSERT INTO `test1` VALUES (112, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/2.4W холодне біле світло 555мм', '3', '370');
INSERT INTO `test1` VALUES (113, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/3.2W холодне біле світло 755мм', '3', '425');
INSERT INTO `test1` VALUES (114, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/3.2W холодне біле світло 855мм', '3', '440');
INSERT INTO `test1` VALUES (115, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/4W холодне біле світло 955мм', '3', '490');
INSERT INTO `test1` VALUES (116, 'Світильник LED 2008 для підсвітки шухляд алюмінієвий колір: срібний 12V/4.8W холодне біле світло 1155мм', '3', '565');
INSERT INTO `test1` VALUES (117, 'Світильник LED 2009 нижній 12V/1.2W RGB нержавіюча сталь матова D58мм', '3', '135');
INSERT INTO `test1` VALUES (118, 'Світильник LED 2010 нижній 12V/1.7W RGB нержавіюча сталь матова 52х52мм', '3', '170');
INSERT INTO `test1` VALUES (119, 'Стрічка LED 2011 пластикова біла 12V/0.8W холодне біле світло 300мм', '3', '90');
INSERT INTO `test1` VALUES (120, 'Стрічка LED 2011 пластикова біла 12V/5W холодне біле світло 2000мм', '3', '500');
INSERT INTO `test1` VALUES (121, 'Стрічка LED 2011 пластикова біла 12V/0.8W тепле біле світло 300мм', '3', '90');
INSERT INTO `test1` VALUES (122, 'Стрічка LED 2011 пластикова біла 12V/5W тепле біле світло 2000мм', '3', '500');
INSERT INTO `test1` VALUES (123, 'З''єднувач Т-подібний LED 2011 12V/0.2W холодне біле світло 75мм', '3', '23');
INSERT INTO `test1` VALUES (124, 'З''єднувач Т-подібний LED 2011 12V/0.2W тепле біле світло 75мм', '3', '23');
INSERT INTO `test1` VALUES (125, 'Кабель LED стрічка/блок живлення білий 2500мм', '3', '23');
INSERT INTO `test1` VALUES (126, 'Кабель LED для кутових з''єднань білий 50мм', '3', '6');
INSERT INTO `test1` VALUES (127, 'Стрічка LED 2012 пластикова біла 12V/2W RGB 300мм', '3', '171');
INSERT INTO `test1` VALUES (128, 'Стрічка LED 2012 пластикова біла 12V/12W RGB 2000мм', '3', '1141');
INSERT INTO `test1` VALUES (129, 'Мікшер RGB з пультом дистанційного управління 12V/30W пластиковий білий', '3', '462');
INSERT INTO `test1` VALUES (130, 'Кабель LED стрічка/мікшер RGB білий 2500мм', '3', '34');
INSERT INTO `test1` VALUES (131, 'Кабель LED для кутових з''єднань RGB білий 50мм', '3', '19');
INSERT INTO `test1` VALUES (132, 'Стрічка LED 2013 пластикова біла 12V/24W тепле біле світло 5000мм (300)', '3', '703');
INSERT INTO `test1` VALUES (133, 'Стрічка LED 2013 пластикова біла 12V/24W холодне біле світло 5000мм (300)', '3', '703');
INSERT INTO `test1` VALUES (134, 'Стрічка LED 2013 пластикова біла 12V/24W голубе світло 5000мм (300)', '3', '703');
INSERT INTO `test1` VALUES (135, 'Стрічка LED 2013 пластикова біла 12V/24W червоне світло 5000мм (300)', '3', '660');
INSERT INTO `test1` VALUES (136, 'Розподілювач RGB на 9 виходів пластиковий білий', '3', '28');
INSERT INTO `test1` VALUES (137, 'Кабель LED стрічка/блок живлення 2000мм', '3', '15');
INSERT INTO `test1` VALUES (138, 'Кабель LED стрічка/стрічка 500мм', '3', '15');
INSERT INTO `test1` VALUES (139, 'Роз''єм для стрічок LED', '3', '14');
INSERT INTO `test1` VALUES (140, 'Стрічка LED 2014 пластикова біла 12V/24W RGB 5000мм (300)', '3', '703');
INSERT INTO `test1` VALUES (141, 'Стрічка LED 2015 пластикова біла 12V/36W тепле біле світло 5000мм (150)', '3', '970');
INSERT INTO `test1` VALUES (142, 'Стрічка LED 2015 пластикова біла 12V/36W холодне біле світло 5000мм (150)', '3', '970');
INSERT INTO `test1` VALUES (143, 'Стрічка LED 2015 пластикова біла 12V/36W голубе світло 5000мм (150)', '3', '1014');
INSERT INTO `test1` VALUES (144, 'Стрічка LED 2015 пластикова біла 12V/36W червоне світло 5000мм (150)', '3', '1014');
INSERT INTO `test1` VALUES (145, 'Стрічка LED 2016 пластикова біла 12V/36W RGB 5000мм (150)', '3', '1012');
INSERT INTO `test1` VALUES (146, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 8х17х2500мм нижнє кріплення', '3', '138');
INSERT INTO `test1` VALUES (147, 'Заглушка пластикова колір: срібний (до 833.74.729)', '3', '8');
INSERT INTO `test1` VALUES (148, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 8х17х2500мм врізний', '3', '150');
INSERT INTO `test1` VALUES (149, 'Заглушка пластикова колір: срібний (до 833.72.705)', '3', '6');
INSERT INTO `test1` VALUES (150, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 13х18х2500мм кутовий', '3', '173');
INSERT INTO `test1` VALUES (151, 'Заглушка пластикова колір: срібний (до 833.72.715)', '3', '10');
INSERT INTO `test1` VALUES (152, 'Профіль для стрічок LED 2013/2015 алюміній колір: срібний 26х17х2500мм врізний', '3', '202');
INSERT INTO `test1` VALUES (153, 'Заглушка пластикова колір: срібний (до 833.72.725)', '3', '11');
INSERT INTO `test1` VALUES (154, 'Штанга гардеробна для стрічок LED 2013/2015 алюміній колір: срібний 2500мм', '3', '210');
INSERT INTO `test1` VALUES (155, 'Штанготримач для кріплення до бокової стінки, цинк', '3', '1');
INSERT INTO `test1` VALUES (156, 'Середній кронштейн д/шафної штанги 85-105 мм, хром', '3', '30');
INSERT INTO `test1` VALUES (157, 'Комплект стрічки LED2017 12V/2.16W холодне біле світло 300мм (3 частини)', '3', '15');
INSERT INTO `test1` VALUES (158, 'Комплект стрічки LED2017 12V/2.16W тепле біле світло 300мм (3 частини)', '3', '160');
INSERT INTO `test1` VALUES (159, 'Розгалужувач на 3 світильники для LED 12V 34х45мм', '3', '34');
INSERT INTO `test1` VALUES (160, 'Блок живлення для LED 12V/15W пластиковий чорний на 6 виходів 20х45х155мм', '3', '146');
INSERT INTO `test1` VALUES (161, 'Блок живлення для LED 12V/30W пластиковий чорний на 6 виходів 28х50х165мм', '3', '189');
INSERT INTO `test1` VALUES (162, 'Шнур з вилкою EU 250V 2000мм', '3', '21');
INSERT INTO `test1` VALUES (163, 'DP 192', '9', '15');
INSERT INTO `test1` VALUES (164, 'DP 193', '9', '15');
INSERT INTO `test1` VALUES (165, 'DP 195', '9', '15');
INSERT INTO `test1` VALUES (166, 'DP 197', '9', '15');
INSERT INTO `test1` VALUES (167, 'DG 191', '9', '10');
INSERT INTO `test1` VALUES (168, 'DG 192', '9', '10');
INSERT INTO `test1` VALUES (169, 'DG 193', '9', '10');
INSERT INTO `test1` VALUES (170, 'DG 195', '9', '10');
INSERT INTO `test1` VALUES (171, 'DG 197', '9', '10');
INSERT INTO `test1` VALUES (172, 'Г-7125', '9', '10');
INSERT INTO `test1` VALUES (173, 'UG1705', '9', '16');
INSERT INTO `test1` VALUES (174, 'UR0705 бронза', '9', '13');
INSERT INTO `test1` VALUES (175, 'UR2005', '9', '8');
INSERT INTO `test1` VALUES (176, 'UR1005', '9', '5');
INSERT INTO `test1` VALUES (177, 'UG3205', '9', '6');
INSERT INTO `test1` VALUES (178, 'UN8803 сатин', '9', '7');
INSERT INTO `test1` VALUES (180, 'UN8303 сатин, золото', '9', '8');
INSERT INTO `test1` VALUES (182, 'Hafele 121.XX.10 ', '9', '12');
INSERT INTO `test1` VALUES (183, 'Hafele 125.XX.1  ', '9', '6');
INSERT INTO `test1` VALUES (184, 'UA-326 врізна (алюм+хром)', '9', '15');
INSERT INTO `test1` VALUES (185, 'UA-326 врізна (хром)', '9', '25');
INSERT INTO `test1` VALUES (186, 'Світильник LED 3001 врізний алюмінієвий колір: срібний 24V/1.7W холодне біле світло D65мм', '3', '76');
INSERT INTO `test1` VALUES (187, 'Світильник LED 3001 врізний алюмінієвий колір: срібний 24V/1.7W тепле біле світло D65мм', '3', '76');
INSERT INTO `test1` VALUES (188, 'Монтажне кільце сталеве колір: срібний D65мм (до 833.75.000/010)', '3', '18');
INSERT INTO `test1` VALUES (189, 'Світильник LED 3002 врізний алюмінієвий колір: срібний 24V/4.4W холодне біле світло D127мм', '3', '393');
INSERT INTO `test1` VALUES (190, 'Світильник LED 3002 врізний алюмінієвий колір: срібний 24V/4.4W тепле біле світло D127мм', '3', '411');
INSERT INTO `test1` VALUES (191, 'Світильник LED 3004 пластиковий нікельований матовий 24V/0.8W тепле біле світло D70мм', '3', '92');
INSERT INTO `test1` VALUES (192, 'Світильник LED 3005 пластиковий нікельований матовий 24V/0.8W тепле біле світло D58мм', '3', '90');
INSERT INTO `test1` VALUES (193, 'Комплект світильників LED 3003 24V/1.65W пластиковий колір: срібний тепле біле світло (3 штуки)', '3', '690');
INSERT INTO `test1` VALUES (194, 'Комплект світильників LED3006 24V/5.4W рухомих алюміній колір: срібний тепле біле світло 900мм (3 штуки)', '3', '592');
INSERT INTO `test1` VALUES (195, 'Світильник LED 3006 24V/1.8W тепле біле світло 55х55х15мм (до 833.76.040)', '3', '95');
INSERT INTO `test1` VALUES (196, 'Розгалужувач на 3 світильники для LED 24V 34х45мм', '3', '34');
INSERT INTO `test1` VALUES (197, 'Блок живлення для LED 24V/15W на 6 виходів пластиковий чорний 20х45х155мм', '3', '147');
INSERT INTO `test1` VALUES (198, 'Блок живлення для LED 24V/30W на 6 виходів пластиковий чорний 28х50х165мм', '3', '190');
INSERT INTO `test1` VALUES (199, 'Шнур з вилкою EU 250V 2000мм', '3', '21');
INSERT INTO `test1` VALUES (200, 'Світильник LED 4009 350mA/3W врізний алюміній нікельований матовий тепле біле світло D65мм', '3', '3');
INSERT INTO `test1` VALUES (201, 'Світильник LED 4009 350mA/3W врізний алюміній нікельований матовий холодне біле світло D65мм', '3', '3');
INSERT INTO `test1` VALUES (202, 'Монтажне кільце сталеве нікельоване D65мм (до 833.78.140/150)', '3', '12');
INSERT INTO `test1` VALUES (203, 'Монтажний трикутник сталевий оцинкований 135х118х40мм (до 833.78.140/150)', '3', '25');
INSERT INTO `test1` VALUES (204, 'Світильник LED 4005 350mA/1W врізний пластиковий колір: срібний тепле біле світло D30мм', '3', '64');
INSERT INTO `test1` VALUES (205, 'Світильник LED 4004 350mA/1W врізний пластиковий колір: срібний тепле біле світло D30мм', '3', '62');
INSERT INTO `test1` VALUES (206, 'Світильник LED 4003 350mA/1W пластиковий колір: срібний тепле біле світло 34х34мм (3 штуки)', '3', '62');
INSERT INTO `test1` VALUES (207, 'Світильник LED 4007 350mA/1W пластиковий колір: срібний тепле біле світло 34х34мм (3 штуки)', '3', '62');
INSERT INTO `test1` VALUES (208, 'Світильник LED 4006 350mA/1W 6-кутний пластиковий колір: срібний тепле біле світло 38мм (3 штуки)', '3', '62');
INSERT INTO `test1` VALUES (209, 'Блок живлення для LED 350mA/1-4W на 4 виходи пластиковий чорний 20х45х155мм', '3', '142');
INSERT INTO `test1` VALUES (210, 'Блок живлення для LED 350mA/5-10W на 10 виходів пластиковий чорний 28х50х165мм', '3', '166');
INSERT INTO `test1` VALUES (211, 'Шнур з вилкою EU 250V 2000мм', '3', '21');
INSERT INTO `test1` VALUES (212, 'Сенсорний вимикач пластиковий колір: срібний 60мм D13мм', '3', '170');
INSERT INTO `test1` VALUES (213, 'Сенсорний вимикач для дверей пластиковий колір: срібний 60мм D13мм', '3', '162');
INSERT INTO `test1` VALUES (214, 'Детектор руху пластиковий колір: срібний 60мм D13мм', '3', '112');
INSERT INTO `test1` VALUES (215, 'Регулятор яскравості пластиковий колір: срібний 60мм D13мм', '3', '107');
INSERT INTO `test1` VALUES (216, 'Вимикач кнопковий пластиковий чорний 30мм D13мм', '3', '28');
INSERT INTO `test1` VALUES (217, 'Кріплення для вимикачів пластикове колір: срібний 20х50х57мм', '3', '8');
INSERT INTO `test1` VALUES (218, 'Здовжувач для LED 12V чорний 500мм', '3', '8');
INSERT INTO `test1` VALUES (219, 'Здовжувач для LED 12V чорний 1000мм', '3', '9');
INSERT INTO `test1` VALUES (220, 'Здовжувач для LED 12V чорний 2000мм', '3', '11');
INSERT INTO `test1` VALUES (221, 'Здовжувач для LED 24V чорний 500мм', '3', '7');
INSERT INTO `test1` VALUES (222, 'Здовжувач для LED 24V чорний 1000мм', '3', '9');
INSERT INTO `test1` VALUES (223, 'Здовжувач для LED 24V чорний 2000мм', '3', '12');
INSERT INTO `test1` VALUES (224, 'Здовжувач для LED 350mA чорний 500мм', '3', '8');
INSERT INTO `test1` VALUES (225, 'Здовжувач для LED 350mA чорний 1000мм', '3', '10');
INSERT INTO `test1` VALUES (226, 'Здовжувач для LED 350mA чорний 2000мм', '3', '12');
INSERT INTO `test1` VALUES (227, 'Захисний профіль самоклеючий для кабелів LED пластиковий білий RAL 9010 2500мм', '3', '60');
INSERT INTO `test1` VALUES (228, 'Захисний профіль самоклеючий для кабелів LED пластиковий коричневий RAL 8007 2500мм', '3', '60');
INSERT INTO `test1` VALUES (229, 'Захисний профіль самоклеючий для кабелів LED пластиковий чорний RAL 9005 2500мм', '3', '60');
INSERT INTO `test1` VALUES (230, 'Світильник LED 9001 на 4 батарейки AAA/1.2W для шухляд пластиковий нікельований матовий холодне біле світло 303мм', '3', '179');
INSERT INTO `test1` VALUES (231, 'CLIP top Завіса для накладних дверцят, відчинення 100?+Опорна планка хрестоподібна під саморізи+ Декор.заглушка для завіс', '4', '9');
INSERT INTO `test1` VALUES (232, 'CLIP top Амортизатор накладний на завісу BLUMOTION', '4', '12');
INSERT INTO `test1` VALUES (233, 'CLIP top Амортизатор врізний (монтаж у ручки), св.-сірий', '4', '12');
INSERT INTO `test1` VALUES (234, 'CLIP top Завіса пряма під фальшпанель, накладна, відч.95? (рівнолегла 180?)+Опорна планка хрестоподібна під саморізи для завіси під фальшпанель, підйом 3мм', '4', '32');
INSERT INTO `test1` VALUES (235, 'CLIP top Завіса кутова +45? І напівнакладна (відчинення 95?)+Опорна планка хрестоподібна під саморізи для завіси +45?, регулюв.', '4', '23');
INSERT INTO `test1` VALUES (236, 'CLIP top Завіса для накладних дверцят, відчинення 170? (крокодил)', '4', '35');
INSERT INTO `test1` VALUES (237, 'CLIP top Завіса для складаних дверцят, відчинення 60?  (допоміжна)', '4', '40');
INSERT INTO `test1` VALUES (238, 'CLIP top Завіса для алюм.рамок Z-1, накладна, відч.95?', '4', '25');
INSERT INTO `test1` VALUES (239, 'CLIP top BLUMOTION Завіса накладна, відч. 110?', '4', '22');
INSERT INTO `test1` VALUES (240, 'CLIP top BLUMOTION Завіса пряма під фальшпанель, відч. 95? (рівнолегла 180?)', '4', '45');
INSERT INTO `test1` VALUES (241, 'CLIP top BLUMOTION Кутова завіса напівнакладна +45?, відч.95?', '4', '40');
INSERT INTO `test1` VALUES (242, 'CLIP top BLUMOTION Завіса для алюм рамок Z-1,накладна, відч. 95?', '4', '40');
INSERT INTO `test1` VALUES (243, 'TIP-ON Завіса без пружини - накладна, відч. 100?', '4', '8');
INSERT INTO `test1` VALUES (244, 'TIP-ON Завіса без пружини - накладна, відч. 120?', '4', '12');
INSERT INTO `test1` VALUES (245, 'TIP-ON Завіса без пружини - під фальшпанель, відч. 95? (рівнолегла 180?)', '4', '28');
INSERT INTO `test1` VALUES (246, 'TIP-ON Завіса без пружини - кутова завіса +45?, відч. 95?', '4', '23');
INSERT INTO `test1` VALUES (247, 'TIP-ON Завіса без пружини - кутова завіса +30?, відч. 95?', '4', '23');
INSERT INTO `test1` VALUES (248, 'TIP-ON стандартний для накладних дверцят', '4', '30');
INSERT INTO `test1` VALUES (249, 'TIP-ON довгий для вкладних і високих дверцят', '4', '35');
INSERT INTO `test1` VALUES (250, 'TIP-ON Пластина під саморіз', '4', '1');
INSERT INTO `test1` VALUES (251, 'TIP-ON Прямий тримач ', '4', '13');
INSERT INTO `test1` VALUES (252, 'HF Підйомник (2 дверки) коеф.потужності F-22 (легкий)', '4', '590');
INSERT INTO `test1` VALUES (253, 'HF Підйомник (2 дверки) коеф.потужності F-25 (середній)', '4', '620');
INSERT INTO `test1` VALUES (254, 'HF Підйомник HF (2 дверки) коеф.потужності F-28 (сильний)', '4', '690');
INSERT INTO `test1` VALUES (255, 'HF Комплект кріплень до алюм.фасаду Z-1', '4', '150');
INSERT INTO `test1` VALUES (256, 'HS Підйомник (1 дверка) коеф.потужності (легкий), Нфас=350-525мм', '4', '630');
INSERT INTO `test1` VALUES (257, 'HS Підйомник (1 дверка)  коеф.потужності (середній), Нфас=526-675мм', '4', '670');
INSERT INTO `test1` VALUES (258, 'HS Підйомник (1 дверка)  коеф.потужності (сильний), Нфас=676-800мм', '4', '910');
INSERT INTO `test1` VALUES (259, 'HS Комплект кріплень до алюм.фасаду Z-1', '4', '60');
INSERT INTO `test1` VALUES (260, 'HL Підйомник (1 дверка) коеф.потужності (легкий), Нфас=300-400мм', '4', '640');
INSERT INTO `test1` VALUES (261, 'HL Підйомник (1 дверка) коеф.потужності (середній), Нфас=400-550мм', '4', '690');
INSERT INTO `test1` VALUES (262, 'HL Підйомник  1 дверка) коеф.потужності (сильний), Нфас=450-580мм', '4', '960');
INSERT INTO `test1` VALUES (263, 'HL Підйомник комплект кріплень  до алюм.фасаду Z-1', '4', '60');
INSERT INTO `test1` VALUES (264, 'HK Підйомник (1 дверка) коеф.потужності (легкий, середній)', '4', '300');
INSERT INTO `test1` VALUES (265, 'HK Підйомник (1 дверка) коеф.потужності (сильний)', '4', '470');
INSERT INTO `test1` VALUES (266, 'HK Підйомник доплата за кріплення до алюм.рамки Z-1', '4', '70');
INSERT INTO `test1` VALUES (267, 'HK Підйомник доплата за ТІР-ОN', '4', '100');
INSERT INTO `test1` VALUES (268, 'HK Підйомник комплект кріплень до алюм.фасаду Z-1', '4', '60');
INSERT INTO `test1` VALUES (269, 'HK S Підйомник (1 дверка) коеф.потужності (легкий)', '4', '150');
INSERT INTO `test1` VALUES (270, 'HK S Підйомник (1 дверка) коеф.потужності (середній, сильний)', '4', '170');
INSERT INTO `test1` VALUES (271, 'HK S Підйомник доплата за  TIP-ON', '4', '35');
INSERT INTO `test1` VALUES (272, 'HK S Підйомник доплата за кріплення до алюм.рамки Z-1', '4', '30');
INSERT INTO `test1` VALUES (273, 'TANDEMBOX M (висота M) 83*450мм(колір сірий, білий)', '4', '220');
INSERT INTO `test1` VALUES (274, 'TANDEMBOX M (висота D) 204*450мм(колір сірий, білий) з подвійним релінгом', '4', '300');
INSERT INTO `test1` VALUES (275, 'TANDEMBOX N (висота N) 68*450мм (колір сірий, білий) під духовку', '4', '260');
INSERT INTO `test1` VALUES (276, 'TANDEMBOX M (висота M) внутрішній 83*450мм(колір сірий, білий) + передня металева панель', '4', '335');
INSERT INTO `test1` VALUES (277, 'TANDEMBOX M (висота M) під мийку 96,5*450мм', '4', '325');
INSERT INTO `test1` VALUES (278, 'TANDEMBOX ANTARO M (висота M) 83*450мм(колір білий шовк)', '4', '220');
INSERT INTO `test1` VALUES (279, 'TANDEMBOX ANTARO M (висота D) 204*450мм(колір білий шовк) з одинарним релінгом прямокутної форми', '4', '270');
INSERT INTO `test1` VALUES (280, 'TANDEMBOX ANTARO (висота N) 68*450мм (колір  білий шовк) під духовку', '4', '260');
INSERT INTO `test1` VALUES (281, 'TANDEMBOX INTIVO M (висота M) внутрішній 83*450мм(колір  білий шовк) + передня металева панель', '4', '350');
INSERT INTO `test1` VALUES (282, 'TANDEM повний висув 450мм, навантаження 30кг', '4', '175');
INSERT INTO `test1` VALUES (283, 'TANDEM повний висув 450мм, навантаження 50кг', '4', '210');
INSERT INTO `test1` VALUES (284, 'TANDEM частковий висув 450мм, навантаження 30кг', '4', '85');
INSERT INTO `test1` VALUES (285, 'TANDEM повний висув 450мм, навантаження 30кг з оранжевими кріпленнями замками', '4', '120');
INSERT INTO `test1` VALUES (286, 'TANDEM частковий висув 450мм, навантаження 30кг з оранжевими кріпленнями замками', '4', '60');
INSERT INTO `test1` VALUES (287, 'TANDEM TIP-ON для TANDEM для повного висування', '4', '60');
INSERT INTO `test1` VALUES (288, 'TANDEM TIP-ON для TANDEM для часткового висування', '4', '60');
INSERT INTO `test1` VALUES (289, 'TANDEM частковий висув 435мм, навантаження 30кг кріплення під фіксатор', '4', '70');
INSERT INTO `test1` VALUES (290, 'TANDEM частковий висув 460мм, навантаження 30кг кріплення під фіксатор', '4', '70');
INSERT INTO `test1` VALUES (291, 'TANDEM TIP ON  частковий висув 435мм, навантаження 30кг', '4', '110');
INSERT INTO `test1` VALUES (292, 'TANDEM TIP ON частковий висув 460мм, навантаження 30кг', '4', '110');
INSERT INTO `test1` VALUES (293, 'MOVENTO повний висув 450мм, навантаження 40кг', '4', '220');
INSERT INTO `test1` VALUES (294, 'MOVENTO повний висув 450мм, навантаження 60кг', '4', '260');
INSERT INTO `test1` VALUES (295, 'MOVENTO TIP ON повний висув 450мм, навантаження 40кг (TIP-ON вмонтований)', '4', '290');
INSERT INTO `test1` VALUES (296, 'MOVENTO TIP ON повний висув 450мм, навантаження 60кг   (TIP-ON вмонтований)', '4', '330');
INSERT INTO `test1` VALUES (297, 'METABOX (висота M) 86*450мм, навантаження 25кг', '4', '65');
INSERT INTO `test1` VALUES (298, 'METABOX (висота Н) 150*450мм, навантаження 25кг', '4', '90');
INSERT INTO `test1` VALUES (299, 'METABOX (висота N) під духовку 54*450мм, навантаження 30кг', '4', '90');
INSERT INTO `test1` VALUES (300, 'METABOX (висота M) внутрішня шуфляда 86*450мм, навантаження 30кг', '4', '70');
INSERT INTO `test1` VALUES (301, 'METABOX комплект релінгів 450мм', '4', '40');
INSERT INTO `test1` VALUES (302, 'METABOX Blumotion для шуфляд метабокс зменшує навантажння на шуфляду на 5кг', '4', '25');
INSERT INTO `test1` VALUES (303, 'вкладиш для столових приладів 240х490х45, секція 300мм (шт)', '5', '46');
INSERT INTO `test1` VALUES (304, 'вкладиш для столових приладів 290х490х45, секція 350мм (шт)', '5', '48');
INSERT INTO `test1` VALUES (305, 'вкладиш для столових приладів 340х490х45, секція 400мм (шт)', '5', '50');
INSERT INTO `test1` VALUES (306, 'вкладиш для столових приладів 390х490х45, секція 450мм (шт)', '5', '52');
INSERT INTO `test1` VALUES (307, 'вкладиш для столових приладів 440х490х45, секція 500мм (шт)', '5', '61');
INSERT INTO `test1` VALUES (308, 'вкладиш для столових приладів 490х490х45, секція 550мм (шт)', '5', '65');
INSERT INTO `test1` VALUES (309, 'вкладиш для столових приладів 540х490х45, секція 600мм (шт)', '5', '69');
INSERT INTO `test1` VALUES (310, 'вкладиш для столових приладів 640х490х45, секція 700мм (шт)', '5', '125');
INSERT INTO `test1` VALUES (311, 'вкладиш для столових приладів 740х490х45, секція 800мм (шт)', '5', '129');
INSERT INTO `test1` VALUES (312, 'вкладиш для столових приладів 840х490х45, секція 900мм (шт)', '5', '133');
INSERT INTO `test1` VALUES (313, 'Килимок антиковзаючий(бухта-10м) (м)', '5', '81');
INSERT INTO `test1` VALUES (314, 'консоль барна вертикальна, d42х200, хром, (дерево-дерево) (шт)', '5', '348');
INSERT INTO `test1` VALUES (315, 'консоль барна вертикальна, d42х200, хром, (дерево-скло) (шт)', '5', '348');
INSERT INTO `test1` VALUES (316, 'консоль барна похила, d42х200, хром, (дерево-дерево) (шт)', '5', '348');
INSERT INTO `test1` VALUES (317, 'консоль барна похила, d42х200, хром, (дерево-скло) (шт)', '5', '368');
INSERT INTO `test1` VALUES (318, 'Набір для ванної кімнати з круглими кошиками (шт)', '5', '852');
INSERT INTO `test1` VALUES (319, 'Набір для ванної кімнати установка на фасад (шт)', '5', '969');
INSERT INTO `test1` VALUES (320, 'Піддон алюмінієвий 1164мм секція 1200мм (шт)', '5', '76');
INSERT INTO `test1` VALUES (321, 'Піддон алюмінієвий 464мм секція 500мм (шт)', '5', '47');
INSERT INTO `test1` VALUES (322, 'Піддон алюмінієвий 564мм секція 600мм (шт)', '5', '50');
INSERT INTO `test1` VALUES (323, 'Піддон алюмінієвий 564мм секція 600мм низький (шт)', '5', '50');
INSERT INTO `test1` VALUES (324, 'Піддон алюмінієвий 664мм секція 700мм (шт)', '5', '53');
INSERT INTO `test1` VALUES (325, 'Піддон алюмінієвий 664мм секція 700мм низький (шт)', '5', '52');
INSERT INTO `test1` VALUES (326, 'Піддон алюмінієвий 764 мм секція 800мм (шт)', '5', '56');
INSERT INTO `test1` VALUES (327, 'Піддон алюмінієвий 764 мм секція 800мм низький (шт)', '5', '56');
INSERT INTO `test1` VALUES (328, 'Піддон алюмінієвий 864мм секція 900мм (шт)', '5', '61');
INSERT INTO `test1` VALUES (329, 'Піддон алюмінієвий 964мм секція 1000мм (шт)', '5', '66');
INSERT INTO `test1` VALUES (330, 'Піддон для газового балона на напрямних (шт)', '5', '252');
INSERT INTO `test1` VALUES (331, 'Сітка повного висунення під мийку  80см (шт)', '5', '503');
INSERT INTO `test1` VALUES (332, 'Сітка повного висунення під мийку  80см з крепл.фасада (шт)', '5', '533');
INSERT INTO `test1` VALUES (333, 'Сітка повного висунення під мийку  80см з крепл.фасада алюміній (шт)', '5', '813');
INSERT INTO `test1` VALUES (334, 'Сітка повного висунення під мийку  90см (шт)', '5', '523');
INSERT INTO `test1` VALUES (335, 'Сітка повного висунення під мийку  90см з крепл.фасада (шт)', '5', '516');
INSERT INTO `test1` VALUES (336, 'Сітка повного висунення під мийку  90см з крепл.фасада алюміній (шт)', '5', '833');
INSERT INTO `test1` VALUES (337, 'сміттєве відро на напрямних 16л (шт) (шт)', '5', '465');
INSERT INTO `test1` VALUES (338, 'сміттєве відро на напрямних 24л (шт) (шт)', '5', '658');
INSERT INTO `test1` VALUES (339, 'сміттєве відро на напрямних 32л (шт) (шт)', '5', '736');
INSERT INTO `test1` VALUES (340, 'сміттєве відро на напрямних 32л крепл.на фасад (шт) (шт)', '5', '1056');
INSERT INTO `test1` VALUES (341, 'сміттєве відро нержавіюча сталь (шт.)', '5', '132');
INSERT INTO `test1` VALUES (342, 'функціональний ящик  під фасад в корпус 600мм, алюміній (шт)', '5', '717');
INSERT INTO `test1` VALUES (343, 'функціональний ящик  під фасад в корпус 700мм, алюміній (шт)', '5', '736');
INSERT INTO `test1` VALUES (344, 'функціональний ящик  під фасад в корпус 800мм, алюміній (шт)', '5', '755');
INSERT INTO `test1` VALUES (345, 'функціональний ящик  під фасад в корпус 900мм, алюміній (шт)', '5', '775');
INSERT INTO `test1` VALUES (346, 'функціональний ящик в корпус 600мм, алюміній (шт)', '5', '657');
INSERT INTO `test1` VALUES (347, 'функціональний ящик в корпус 700мм, алюміній (шт)', '5', '682');
INSERT INTO `test1` VALUES (348, 'функціональний ящик в корпус 800мм, алюміній (шт)', '5', '701');
INSERT INTO `test1` VALUES (349, 'функціональний ящик в корпус 900мм, алюміній (шт)', '5', '719');
INSERT INTO `test1` VALUES (350, 'функціональний ящик-сушка  в корпус 600мм, алюміній (шт)', '5', '701');
INSERT INTO `test1` VALUES (351, 'функціональний ящик-сушка  в корпус 700мм, алюміній (шт)', '5', '719');
INSERT INTO `test1` VALUES (352, 'функціональний ящик-сушка  в корпус 800мм, алюміній (шт)', '5', '738');
INSERT INTO `test1` VALUES (353, 'функціональний ящик-сушка  в корпус 900мм, алюміній (шт)', '5', '756');
INSERT INTO `test1` VALUES (354, 'функціональний ящик-сушка під фасад в корпус 600мм, алюміній (шт)', '5', '813');
INSERT INTO `test1` VALUES (355, 'функціональний ящик-сушка під фасад в корпус 700мм, алюміній (шт)', '5', '833');
INSERT INTO `test1` VALUES (356, 'функціональний ящик-сушка під фасад в корпус 800мм, алюміній (шт)', '5', '852');
INSERT INTO `test1` VALUES (357, 'функціональний ящик-сушка під фасад в корпус 900мм, алюміній (шт)', '5', '872');
INSERT INTO `test1` VALUES (358, 'Вішалка висувна 30см (шт) (шт)', '5', '50');
INSERT INTO `test1` VALUES (359, 'Вішалка висувна 35см (шт) (шт)', '5', '54');
INSERT INTO `test1` VALUES (360, 'Вішалка висувна 40см (шт) (шт)', '5', '58');
INSERT INTO `test1` VALUES (361, 'Вішалка висувна 45см (шт) (шт)', '5', '61');
INSERT INTO `test1` VALUES (362, 'Вішалка висувна широка 30см (шт) (шт)', '5', '83');
INSERT INTO `test1` VALUES (363, 'Вішалка висувна широка 35см (шт) (шт)', '5', '85');
INSERT INTO `test1` VALUES (364, 'Вішалка висувна широка 40см (шт) (шт)', '5', '87');
INSERT INTO `test1` VALUES (365, 'Вішалка висувна широка 45см (шт) (шт)', '5', '89');
INSERT INTO `test1` VALUES (366, 'висувний кутовий механізм Secret - лівий 400мм (шт)', '5', '2228');
INSERT INTO `test1` VALUES (367, 'висувний кутовий механізм Secret - лівий (шт)', '5', '2286');
INSERT INTO `test1` VALUES (368, 'висувний кутовий механізм Secret - правий 400мм (шт)', '5', '2228');
INSERT INTO `test1` VALUES (369, 'висувний кутовий механізм Secret - правий (шт)', '5', '2286');
INSERT INTO `test1` VALUES (370, 'висувний кутовий механізм кріплення до фасаду Secret - лівий (шт)', '5', '2131');
INSERT INTO `test1` VALUES (371, 'висувний кутовий механізм кріплення до фасаду Secret - правий (шт)', '5', '2131');
INSERT INTO `test1` VALUES (372, 'Галерея висувна  200-215 45см (шт)', '5', '1259');
INSERT INTO `test1` VALUES (373, 'Галерея висувна  200-215 50см (шт)', '5', '1259');
INSERT INTO `test1` VALUES (374, 'Галерея висувна  85-110  55см (шт)', '5', '813');
INSERT INTO `test1` VALUES (375, 'Галерея висувна  85-110 45см (шт)', '5', '775');
INSERT INTO `test1` VALUES (376, 'Галерея висувна  85-110 50см (шт)', '5', '813');
INSERT INTO `test1` VALUES (377, 'Галерея висувна 110-125 30см (шт)', '5', '891');
INSERT INTO `test1` VALUES (378, 'Галерея висувна 110-125 40см (шт)', '5', '891');
INSERT INTO `test1` VALUES (379, 'Галерея висувна 125-140 30см (шт)', '5', '969');
INSERT INTO `test1` VALUES (380, 'Галерея висувна 125-140 40см (шт)', '5', '969');
INSERT INTO `test1` VALUES (381, 'Галерея висувна 125-140 45см (шт)', '5', '922');
INSERT INTO `test1` VALUES (382, 'Галерея висувна 155-170 30см (шт)', '5', '1085');
INSERT INTO `test1` VALUES (383, 'Галерея висувна 155-170 35см (шт)', '5', '1085');
INSERT INTO `test1` VALUES (384, 'Галерея висувна 155-170 40см (шт)', '5', '1085');
INSERT INTO `test1` VALUES (385, 'Галерея висувна 155-170 50см (шт)', '5', '1085');
INSERT INTO `test1` VALUES (386, 'Галерея висувна 170-185 30см (шт)', '5', '1162');
INSERT INTO `test1` VALUES (387, 'Галерея висувна 170-185 45см (шт)', '5', '1162');
INSERT INTO `test1` VALUES (388, 'Галерея висувна 185-200 30см (шт)', '5', '1201');
INSERT INTO `test1` VALUES (389, 'Галерея висувна 185-200 35см (шт)', '5', '1201');
INSERT INTO `test1` VALUES (390, 'Галерея висувна 185-200 40см (шт)', '5', '1144');
INSERT INTO `test1` VALUES (391, 'Галерея висувна 200-215 30см (шт)', '5', '1259');
INSERT INTO `test1` VALUES (392, 'Галерея висувна 200-215 35см (шт)', '5', '1259');
INSERT INTO `test1` VALUES (393, 'Галерея висувна 200-215 40см (шт)', '5', '1259');
INSERT INTO `test1` VALUES (394, 'Галерея висувна 230х500х1250-1400, 30см с доводч (шт)', '5', '3197');
INSERT INTO `test1` VALUES (395, 'Галерея висувна 230х500х1700-1850, 30см с доводч (шт)', '5', '3972');
INSERT INTO `test1` VALUES (396, 'Галерея висувна 230х500х1850-2000, 30см с доводч (шт)', '5', '4205');
INSERT INTO `test1` VALUES (397, 'Галерея висувна 230х500х1850-2000, 30см с доводч (шт)', '5', '5620');
INSERT INTO `test1` VALUES (398, 'Галерея висувна 330х500х1250-1400, 40см с доводч (шт)', '5', '3197');
INSERT INTO `test1` VALUES (399, 'Галерея висувна 330х500х1400-1550, 40см с доводч (шт)', '5', '3488');
INSERT INTO `test1` VALUES (400, 'Галерея висувна 330х500х1700-1850, 40см с доводч (шт)', '5', '3972');
INSERT INTO `test1` VALUES (401, 'Галерея висувна 330х500х1850-2000, 40см с доводч (шт)', '5', '4205');
INSERT INTO `test1` VALUES (402, 'Галерея висувна 330х500х1850-2000, 40см с доводч (шт)', '5', '5620');
INSERT INTO `test1` VALUES (403, 'Галерея висувна 380х500х1400-1550, 45см с доводч (шт)', '5', '4903');
INSERT INTO `test1` VALUES (404, 'Галерея висувна 380х500х1700-1850, 45см c доводч (шт)', '5', '3972');
INSERT INTO `test1` VALUES (405, 'Галерея висувна 380х500х1850-2000, 45см с доводч (шт)', '5', '4205');
INSERT INTO `test1` VALUES (406, 'Галерея висувна 430х500х1250-1400, 50см с доводч (шт)', '5', '3197');
INSERT INTO `test1` VALUES (407, 'Галерея висувна 430х500х1700-1850, 50см c доводч (шт)', '5', '3972');
INSERT INTO `test1` VALUES (408, 'Галерея висувна 430х500х1850-2000, 50см с доводч (шт)', '5', '4205');
INSERT INTO `test1` VALUES (409, 'Галерея висувна 430х500х1850-2000, 50см с доводч (шт)', '5', '5620');
INSERT INTO `test1` VALUES (410, 'Галерея висувна 480х500х1550-1700  55см (шт)', '5', '1085');
INSERT INTO `test1` VALUES (411, 'Галерея висувна 60-80 30см (шт)', '5', '736');
INSERT INTO `test1` VALUES (412, 'Галерея висувна 60-80 40см (шт)', '5', '736');
INSERT INTO `test1` VALUES (413, 'Галерея висувна 60-80 45см (шт)', '5', '701');
INSERT INTO `test1` VALUES (414, 'Галерея висувна 85-110  40см (шт)', '5', '813');
INSERT INTO `test1` VALUES (415, 'Галерея подвійна Classic 45см (шт)', '5', '1550');
INSERT INTO `test1` VALUES (416, 'Галерея подвійна Luxury 45см (шт)', '5', '2170');
INSERT INTO `test1` VALUES (417, 'Карго 2-х рівневе з посудодержателем в секцію 450мм, хром 360х500х520 с доводч (шт)', '5', '562');
INSERT INTO `test1` VALUES (418, 'Карго 2-х рівневе з посудодержателем в секцію 500мм, хром 410х500х520 с доводч (шт)', '5', '571');
INSERT INTO `test1` VALUES (419, 'Карго 2-х рівневе з посудодержателем в секцію 600мм, хром 510х500х520 с доводч (шт)', '5', '600');
INSERT INTO `test1` VALUES (420, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм сіре 210х500х520 с доводч (шт)', '5', '445');
INSERT INTO `test1` VALUES (421, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм хром (шт)', '5', '355');
INSERT INTO `test1` VALUES (422, 'Карго 2-х рівневе з сіткою для пляшок в секцію 300мм хром 210х500х520 с довод (шт)', '5', '484');
INSERT INTO `test1` VALUES (423, 'Карго 2-х рівневе з сіткою для пляшок в секцію 350мм хром 260х500х520 с доводч (шт)', '5', '503');
INSERT INTO `test1` VALUES (424, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм сіре (шт)', '5', '317');
INSERT INTO `test1` VALUES (425, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм сіре 310х500х520 с доводч (шт)', '5', '484');
INSERT INTO `test1` VALUES (426, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм хром (шт)', '5', '377');
INSERT INTO `test1` VALUES (427, 'Карго 2-х рівневе з сіткою для пляшок в секцію 400мм хром 310х500х520 с довод (шт)', '5', '523');
INSERT INTO `test1` VALUES (428, 'Карго 2-х рівневе з сіткою для пляшок в секцію 450мм хром 360х500х520 с доводч (шт)', '5', '542');
INSERT INTO `test1` VALUES (429, 'Карго 2-х рівневе з сіткою для пляшок в секцію 450мм, сіре 360х500х520 с доводч (шт)', '5', '503');
INSERT INTO `test1` VALUES (430, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм сіре 410х500х520 с доводч (шт)', '5', '523');
INSERT INTO `test1` VALUES (431, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм хром 410х500х520 с доводч (шт)', '5', '562');
INSERT INTO `test1` VALUES (432, 'Карго 2-х рівневе з сіткою для пляшок в секцію 500мм, сіре (шт)', '5', '346');
INSERT INTO `test1` VALUES (433, 'Карго 2-х рівневе з сіткою для пляшок в секцію 600мм хром 510х500х520 с доводч (шт)', '5', '600');
INSERT INTO `test1` VALUES (434, 'Карго 2-х рівневе з тарілкодерж. в секцію 600мм, хром 510х500х520 с доводч (шт)', '5', '600');
INSERT INTO `test1` VALUES (435, 'Карго 2-х рівневе с хлібницею в секцію 300мм 210х500х520 (шт)', '5', '620');
INSERT INTO `test1` VALUES (436, 'Карго 2-х рівневе с хлібницею в секцію 350мм 260х500х520 (шт)', '5', '639');
INSERT INTO `test1` VALUES (437, 'Карго 2-х рівневе с хлібницею в секцію 400мм 310х500х520 (шт)', '5', '658');
INSERT INTO `test1` VALUES (438, 'Карго 2-х рівневе с хлібницею в секцію 450мм 360х500х520 (шт)', '5', '678');
INSERT INTO `test1` VALUES (439, 'Карго 3-х рівневе з відділеннями для ножів та пляшок в секцію 300мм алюмін.240х500х520 (шт)', '5', '969');
INSERT INTO `test1` VALUES (440, 'Карго 3-х рівневе з сіткою для пляшок в секцію 300мм хром 210х500х520 с дов. (шт)', '5', '542');
INSERT INTO `test1` VALUES (441, 'Карго 3-х рівневе з сіткою для пляшок в секцію 350мм хром 260х500х520 с дов. (шт)', '5', '562');
INSERT INTO `test1` VALUES (442, 'Карго 3-х рівневе з сіткою для пляшок в секцію 400мм хром 310х500х520 с дов. (шт)', '5', '581');
INSERT INTO `test1` VALUES (443, 'Карго 3-х рівневе з сіткою для пляшок в секцію 450мм хром 360х500х520 с дов. (шт)', '5', '600');
INSERT INTO `test1` VALUES (444, 'Карго 3-х рівневе з сіткою для пляшок в секцію 500мм хром 410х500х520 с дов. (шт)', '5', '620');
INSERT INTO `test1` VALUES (445, 'карусель 1/2 d=740х420x 650-700 хром 450mm (шт)', '5', '639');
INSERT INTO `test1` VALUES (446, 'карусель 3/4 d=600 х 650-700 хром (шт)', '5', '736');
INSERT INTO `test1` VALUES (447, 'карусель 3/4 d=740 х 650-700 хром (шт)', '5', '775');
INSERT INTO `test1` VALUES (448, 'карусель 3/4 d=830 х 650-700 хром (шт)', '5', '813');
INSERT INTO `test1` VALUES (449, 'карусель 3/4 висувна (шт)', '5', '3488');
INSERT INTO `test1` VALUES (450, 'карусель 4/4 D600*700 (шт)', '5', '1007');
INSERT INTO `test1` VALUES (451, 'карусель 4/4 D750*700 (шт)', '5', '1201');
INSERT INTO `test1` VALUES (452, 'кошик алюмінієвий нижнього кріплення в секцію 150мм с доводч (шт)', '5', '578');
INSERT INTO `test1` VALUES (453, 'кошик алюмінієвий нижнього кріплення в секцію 200мм 150х500х520 с доводч (шт)', '5', '620');
INSERT INTO `test1` VALUES (454, 'Кошик бічного кріплення , 3-рівневий, лівий,в секцію 250 мм, хром 185*510*615 (шт)', '5', '542');
INSERT INTO `test1` VALUES (455, 'Кошик бічного кріплення , 3-рівневий, лівий,в секцію 300 мм, хром 240*510*615 (шт)', '5', '562');
INSERT INTO `test1` VALUES (456, 'Кошик бічного кріплення , 3-рівневий, правий,в секцію 250 мм, хром 185*510*615 (шт)', '5', '542');
INSERT INTO `test1` VALUES (457, 'Кошик бічного кріплення , 3-рівневий, правий,в секцію 300 мм, хром 240*510*615 (шт)', '5', '562');
INSERT INTO `test1` VALUES (458, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію 150 мм,ліва, сірий (шт)', '5', '429');
INSERT INTO `test1` VALUES (459, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію 150 мм,ліва, хром (шт)', '5', '547');
INSERT INTO `test1` VALUES (460, 'кошик бічного кріплення 11см + Тандем напрямні, в секцію150 мм,права, сірий (шт)', '5', '429');
INSERT INTO `test1` VALUES (461, 'кошик бічного кріплення 11см + Тандем напрямні, в секцшю 150 мм,права, хром (шт)', '5', '547');
INSERT INTO `test1` VALUES (462, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, сірий, ліве (шт)', '5', '432');
INSERT INTO `test1` VALUES (463, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, сірий, праве (шт)', '5', '432');
INSERT INTO `test1` VALUES (464, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, хром, ліве (шт)', '5', '557');
INSERT INTO `test1` VALUES (465, 'кошик бічного кріплення 16см + Тандем напрямні, в секцію 200 мм, хром, праве (шт)', '5', '557');
INSERT INTO `test1` VALUES (466, 'Кошик бічного кріплення 45°, ліва,в секцію 250 мм, хром (шт)', '5', '523');
INSERT INTO `test1` VALUES (467, 'Кошик бічного кріплення 45°, права ,в секцію 250 мм, хром (шт)', '5', '523');
INSERT INTO `test1` VALUES (468, 'кошик бічного кріплення, в секцію 150мм,сірий 105х500х470 с доводч (шт)', '5', '310');
INSERT INTO `test1` VALUES (469, 'кошик бічного кріплення, в секцію 150мм,хром 105х500х470 с доводч (шт)', '5', '329');
INSERT INTO `test1` VALUES (470, 'кошик бічного кріплення, в секцію 200 мм, сірий 155х500х470 с доводч (шт)', '5', '329');
INSERT INTO `test1` VALUES (471, 'кошик бічного кріплення, в секцію 200 мм, хром 155х500х470 с доводч (шт)', '5', '348');
INSERT INTO `test1` VALUES (472, 'кошик бічного кріплення, в секцію 260 мм, хром 225/155х500х470 с доводч (шт)', '5', '358');
INSERT INTO `test1` VALUES (473, 'кошик бічного кріплення, в секцію 260 мм, хром 225х500х470 с доводч (шт)', '5', '368');
INSERT INTO `test1` VALUES (474, 'кошик бічного кріплення, в секцію 260 мм,сірий 225/155х500х470 с доводч (шт)', '5', '339');
INSERT INTO `test1` VALUES (475, 'кріплення фасаду для висувною сітки (шт)', '5', '48');
INSERT INTO `test1` VALUES (476, 'поворотний кутовий механізм Secret - лівий (шт)', '5', '2131');
INSERT INTO `test1` VALUES (477, 'поворотний кутовий механізм Secret - правий (шт)', '5', '2131');
INSERT INTO `test1` VALUES (478, 'Сітка для галереї 230х440х75 хром (шт)', '5', '125');
INSERT INTO `test1` VALUES (479, 'піддон пласт. для сушки  90см (шт)', '5', '46');
INSERT INTO `test1` VALUES (480, 'піддон прозорий для сушки 60см (шт)', '5', '38');
INSERT INTO `test1` VALUES (481, 'піддон прозорий для сушки 70см (шт)', '5', '42');
INSERT INTO `test1` VALUES (482, 'піддон прозорий для сушки 80см (шт)', '5', '54');
INSERT INTO `test1` VALUES (483, 'піддон прозорий для сушки 90см (шт)', '5', '58');
INSERT INTO `test1` VALUES (484, 'полиця барная підвісна 1000х360х480, хром (шт)', '5', '833');
INSERT INTO `test1` VALUES (485, 'сушка для посуду висувна  60 см хром (шт)', '5', '387');
INSERT INTO `test1` VALUES (486, 'сушка для посуду висувна  60 см хром  з крепл.фасада (шт)', '5', '445');
INSERT INTO `test1` VALUES (487, 'сушка для посуду з пласт. піддоном 45см хром (шт)', '5', '199');
INSERT INTO `test1` VALUES (488, 'сушка для посуду з прозорим піддоном 60см хром і алюм.рамкой (шт)', '5', '252');
INSERT INTO `test1` VALUES (489, 'сушка для посуду з прозорим піддоном 60см, алюм.рамкой сіра (шт)', '5', '244');
INSERT INTO `test1` VALUES (490, 'сушка для посуду з прозорим піддоном 60см, алюм.рамкой хром (шт)', '5', '252');
INSERT INTO `test1` VALUES (491, 'сушка для посуду з прозорим піддоном 70см, алюм.рамкой сіра (шт)', '5', '253');
INSERT INTO `test1` VALUES (492, 'сушка для посуду з прозорим піддоном 70см, алюм.рамкой хром (шт)', '5', '261');
INSERT INTO `test1` VALUES (493, 'сушка для посуду з прозорим піддоном 80см, алюм.рамкой сіра (шт)', '5', '263');
INSERT INTO `test1` VALUES (494, 'сушка для посуду з прозорим піддоном 80см, алюм.рамкой хром (шт)', '5', '271');
INSERT INTO `test1` VALUES (495, 'сушка для посуду з прозорим піддоном 90см, алюм.рамкой сіра (шт)', '5', '273');
INSERT INTO `test1` VALUES (496, 'сушка для посуду з прозорим піддоном 90см, алюм.рамкой хром (шт)', '5', '281');
INSERT INTO `test1` VALUES (497, 'сушка-нержавіюча сталь з прозорим піддоном 60см, алюм.рамкой (шт)', '5', '465');
INSERT INTO `test1` VALUES (498, 'сушка-нержавіюча сталь з прозорим піддоном 70см, алюм.рамкой (шт)', '5', '484');
INSERT INTO `test1` VALUES (499, 'сушка-нержавіюча сталь з прозорим піддоном 80см, алюм.рамкой (шт)', '5', '503');
INSERT INTO `test1` VALUES (500, 'сушка-нержавіюча сталь з прозорим піддоном 90см, алюм.рамкой (шт)', '5', '523');
INSERT INTO `test1` VALUES (501, 'сушка-полиця однорядна  80см сіра (шт)', '5', '102');
INSERT INTO `test1` VALUES (502, 'сушка-полиця однорядна  80см хром (шт)', '5', '106');
INSERT INTO `test1` VALUES (503, 'сушка-полиця однорядна  80см хром з прозорим піддоном (шт)', '5', '141');
INSERT INTO `test1` VALUES (504, 'сушка-полиця однорядна  90см  хром (шт)', '5', '108');
INSERT INTO `test1` VALUES (505, 'сушка-полиця однорядна  90см сіра (шт)', '5', '104');
INSERT INTO `test1` VALUES (506, 'тримач келихів 1- рядний 120*355*70 (шт)', '5', '54');
INSERT INTO `test1` VALUES (507, 'тримач келихів 2- рядний 235х355х70 (шт)', '5', '125');
INSERT INTO `test1` VALUES (508, 'тримач келихів 2- рядний 235х500х70 (шт)', '5', '125');
INSERT INTO `test1` VALUES (509, 'тримач келихів 4- рядний 460х355х70 (шт)', '5', '193');
INSERT INTO `test1` VALUES (510, 'тримач келихів 4- рядний 460х500х70 (шт)', '5', '213');
INSERT INTO `test1` VALUES (511, 'тримач келихів 5- рядний 460*250*160 (шт)', '5', '135');
INSERT INTO `test1` VALUES (512, 'дошка обробна з тримачем на релінг, дерево-хром (шт)', '5', '155');
INSERT INTO `test1` VALUES (513, 'полиця -сушка навісна (шт)', '5', '290');
INSERT INTO `test1` VALUES (514, 'полиця велика алюміній (шт)', '5', '319');
INSERT INTO `test1` VALUES (515, 'полиця для cпецій вузька подвійна (шт)', '5', '252');
INSERT INTO `test1` VALUES (516, 'полиця для зберігання спецій (8 предметів) (шт)', '5', '290');
INSERT INTO `test1` VALUES (517, 'полиця для келихів 5-рядна (шт)', '5', '145');
INSERT INTO `test1` VALUES (518, 'полиця для пляшок (шт)', '5', '145');
INSERT INTO `test1` VALUES (519, 'полиця для рушників (шт)', '5', '125');
INSERT INTO `test1` VALUES (520, 'полиця для спецій з тримачем ножів, одинарна, алюміній (шт)', '5', '261');
INSERT INTO `test1` VALUES (521, 'полиця для спецій одинарна вузька (шт)', '5', '116');
INSERT INTO `test1` VALUES (522, 'полиця для спецій одинарна широка (шт)', '5', '120');
INSERT INTO `test1` VALUES (523, 'полиця для спецій та рушників подвійна (шт)', '5', '162');
INSERT INTO `test1` VALUES (524, 'полиця для спецій, вузька, подвійна, дерево (шт)', '5', '290');
INSERT INTO `test1` VALUES (525, 'полиця для спецій, двойная.алюміній (шт)', '5', '281');
INSERT INTO `test1` VALUES (526, 'полиця для спецій, метал. подвійна вузька / широка (шт)', '5', '271');
INSERT INTO `test1` VALUES (527, 'полиця для спецій, одинарна вузька, дерево-хром (шт)', '5', '175');
INSERT INTO `test1` VALUES (528, 'полиця для спецій, одинарна, алюміній (шт)', '5', '213');
INSERT INTO `test1` VALUES (529, 'полиця для спецій, подвійна вузька (шт)', '5', '174');
INSERT INTO `test1` VALUES (530, 'полиця для спецій, подвійна вузька / широка, дерево-хром (шт)', '5', '308');
INSERT INTO `test1` VALUES (531, 'полиця для спецій, широка, подвійна, дерево (шт)', '5', '308');
INSERT INTO `test1` VALUES (532, 'полиця для спецій,, велика, одинарна, дерево (шт)', '5', '199');
INSERT INTO `test1` VALUES (533, 'полиця кутова класична (шт)', '5', '135');
INSERT INTO `test1` VALUES (534, 'полиця-сушка настольная (шт)', '5', '213');
INSERT INTO `test1` VALUES (535, 'полиця-тримач кулінарної книги (шт)', '5', '174');
INSERT INTO `test1` VALUES (536, 'профіль алюмінієвий для вішалок 120 мм (шт)', '5', '43');
INSERT INTO `test1` VALUES (537, 'сушка для посуду однорядна на релінг (шт)', '5', '426');
INSERT INTO `test1` VALUES (538, 'тримач для ножів алюміній (шт)', '5', '252');
INSERT INTO `test1` VALUES (539, 'тримач для ножів, дерево-хром (шт)', '5', '193');
INSERT INTO `test1` VALUES (540, 'тримач рушників (шт)', '5', '106');
INSERT INTO `test1` VALUES (541, 'тримач рушників настольній (шт)', '5', '125');
INSERT INTO `test1` VALUES (542, 'тримач фольги (шт)', '5', '174');
INSERT INTO `test1` VALUES (543, 'чаша для ложок/вилок, дерево-хром (шт)', '5', '213');
INSERT INTO `test1` VALUES (544, 'чаша для ложок/вилок, метал. (шт)', '5', '232');
INSERT INTO `test1` VALUES (545, 'вішалка платтяна потрійна d 25см (шт)', '5', '232');
INSERT INTO `test1` VALUES (546, 'вішалка платтяна потрійна d 50см (шт)', '5', '271');
INSERT INTO `test1` VALUES (547, 'комплект на трубу d =50см (шт)', '5', '1938');
INSERT INTO `test1` VALUES (548, 'кошик бічний з кріпленням 360х100 (шт)', '5', '426');
INSERT INTO `test1` VALUES (549, 'кронштейн скляной полици  для труби d-50 (шт)', '5', '174');
INSERT INTO `test1` VALUES (550, 'набір сполучних елементів для труби d-50 (шт)', '5', '155');
INSERT INTO `test1` VALUES (551, 'підставка для ніг  360х100 d=50mm (шт)', '5', '319');
INSERT INTO `test1` VALUES (552, 'полиця для келихів (шт)', '5', '319');
INSERT INTO `test1` VALUES (553, 'полиця центральна  скляна 360 мм (шт)', '5', '304');
INSERT INTO `test1` VALUES (554, 'полиця центральна 360 мм (шт)', '5', '319');
INSERT INTO `test1` VALUES (555, 'стійка кухонна ( комплект) (шт)', '5', '3120');
INSERT INTO `test1` VALUES (556, 'тримач келихив  D=50mm (шт)', '5', '319');
INSERT INTO `test1` VALUES (557, 'тримач парасольок D=50mm (шт)', '5', '319');
INSERT INTO `test1` VALUES (558, 'тримач труби D=50мм (шт)', '5', '387');
INSERT INTO `test1` VALUES (559, 'борт для полиці 300х15х65 хром (шт)', '5', '58');
INSERT INTO `test1` VALUES (560, 'борт для полиці 400х15х65 хром (шт)', '5', '61');
INSERT INTO `test1` VALUES (561, 'борт для полиці 500х15х65 хром (шт)', '5', '65');
INSERT INTO `test1` VALUES (562, 'брючніца висувна 230х450х80, хром (шт.)', '5', '222');
INSERT INTO `test1` VALUES (563, 'брючніца висувна 360х500х85, в секцію 400мм, хром (шт)', '5', '261');
INSERT INTO `test1` VALUES (564, 'брючніца висувна 418х500х85, в секцію 450мм, хром (шт)', '5', '271');
INSERT INTO `test1` VALUES (565, 'брючніца висувна 468х500х85, в секцію 500мм, хром (шт)', '5', '281');
INSERT INTO `test1` VALUES (566, 'брючніца висувна 518х500х85, в секцію 550мм, хром (шт)', '5', '290');
INSERT INTO `test1` VALUES (567, 'брючніца висувна 568х500х85, в секцію 600мм, хром (шт)', '5', '300');
INSERT INTO `test1` VALUES (568, 'брючніца висувна бок. кріплення 215х500х90 алюм. (шт)', '5', '290');
INSERT INTO `test1` VALUES (569, 'брючніца висувна бок.кріплення 380х520х95, в секц. 40см, алюм. (шт)', '5', '310');
INSERT INTO `test1` VALUES (570, 'брючніца висувна бок.кріплення 430х470х95, в секц. 45см, алюм. (шт)', '5', '329');
INSERT INTO `test1` VALUES (571, 'брючніца висувна бок.кріплення 480х520х95, в секц. 50см, алюм. (шт)', '5', '348');
INSERT INTO `test1` VALUES (572, 'брючніца висувна бок.кріплення 530х520х95, в секц. 55см, алюм. (шт)', '5', '368');
INSERT INTO `test1` VALUES (573, 'брючніца висувна бок.кріплення 580х470х95, в секц. 60см, алюм. (шт)', '5', '387');
INSERT INTO `test1` VALUES (574, 'брючніца висувна бок.кріплення 680х520х95, в секц. 70см, алюм. (шт)', '5', '406');
INSERT INTO `test1` VALUES (575, 'брючніца висувна бок.кріплення 780х470х95, в секц. 80см, алюм. (шт)', '5', '426');
INSERT INTO `test1` VALUES (576, 'брючніца висувна бок.кріплення 880х520х95, в секц. 90см, алюм. (шт)', '5', '445');
INSERT INTO `test1` VALUES (577, 'брючніца висувна верхнього кріплення 325-355х520х80мм в секцию 400 мм алюм. (шт)', '5', '310');
INSERT INTO `test1` VALUES (578, 'брючніца висувна верхнього кріплення 375-405х520х80мм в секцию 450мм  алюм. (шт)', '5', '329');
INSERT INTO `test1` VALUES (579, 'брючніца висувна верхнього кріплення  425-455х520х80мм в секцию 500 мм  алюм. (шт)', '5', '348');
INSERT INTO `test1` VALUES (580, 'брючніца висувна верхнього кріплення 475-505х520х80мм в секцию 550 мм  алюм. (шт)', '5', '368');
INSERT INTO `test1` VALUES (581, 'брючніца висувна верхнього кріплення340х450х90 алюм. (шт)', '5', '348');
INSERT INTO `test1` VALUES (582, 'брючніца висувна з галст. верхнього кріплення 725-755х470х80мм в секцію 800 мм алюм. (шт)', '5', '426');
INSERT INTO `test1` VALUES (583, 'брючніца висувна з краватками, верхнього кріплення 525-555х520х80мм в секцію 600мм алюм. (шт)', '5', '387');
INSERT INTO `test1` VALUES (584, 'брючніца висувна з краватками, верхнього кріплення 625-655х520х80мм в секцію 700мм алюм. (шт)', '5', '406');
INSERT INTO `test1` VALUES (585, 'брючніца висувна з краватками, верхнього кріплення 825-855х470х80мм в секцію 900мм алюм. (шт)', '5', '445');
INSERT INTO `test1` VALUES (586, 'брючніца висувна, бічне кріплення 364х500х45, в секцію 400мм, хром (шт)', '5', '213');
INSERT INTO `test1` VALUES (587, 'брючніца висувна, бічне кріплення 414х500х45, в секцію 450мм, хром (шт)', '5', '222');
INSERT INTO `test1` VALUES (588, 'брючніца висувна, бічне кріплення 464х500х45, в секцію 500мм, хром (шт)', '5', '232');
INSERT INTO `test1` VALUES (589, 'брючніца висувна, бічне кріплення 514х500х45, в секцію 550мм, хром (шт)', '5', '242');
INSERT INTO `test1` VALUES (590, 'брючніца висувна, бічне кріплення 564х500х45, в секцію 600мм, хром (шт)', '5', '252');
INSERT INTO `test1` VALUES (591, 'брючніца і 2 сітки висувна325х500х1120, хром (шт)', '5', '716');
INSERT INTO `test1` VALUES (592, 'брючніца на напрямной  485*425*100 хром (шт)', '5', '368');
INSERT INTO `test1` VALUES (593, 'брючніца поворотна 460х460х80, хром (шт.)', '5', '290');
INSERT INTO `test1` VALUES (594, 'брючніца-полиця з боковим кріпленням 650-680х520х160 в секцию700мм алюмінієва (шт)', '5', '581');
INSERT INTO `test1` VALUES (595, 'брючніца-полиця з боковим кріпленням 750-780х520х160 в секцию800мм алюмінієва (шт)', '5', '600');
INSERT INTO `test1` VALUES (596, 'брючніца-полиця з боковим кріпленням 850-880х520х160 в секцию900мм алюмінієва (шт)', '5', '620');
INSERT INTO `test1` VALUES (597, 'брючніца-полиця з верхним кріпленням 625-655х520х160 в секцию700мм алюмінієва (шт)', '5', '610');
INSERT INTO `test1` VALUES (598, 'брючніца-полиця з верхним кріпленням 725-755х520х160 в секцию800мм алюмінієва (шт)', '5', '620');
INSERT INTO `test1` VALUES (599, 'брючніця-полиця з верхним кріпленням 825-855х520х160 в секцию900мм алюмінієва (шт)', '5', '629');
INSERT INTO `test1` VALUES (600, 'галерея підвісна (4 сітки хром), 235х450х1300, хром (шт)', '5', '511');
INSERT INTO `test1` VALUES (601, 'дошка прасувальна для гардероба розкладна сіра 150-340х1360 (шт)', '5', '2325');
INSERT INTO `test1` VALUES (602, 'комплект для панелі, що обертається, 5 полиць хром (шт)', '5', '775');
INSERT INTO `test1` VALUES (603, 'корзина для білизни з тримачем 280х290х500, в корпус 350мм, хром (шт)', '5', '242');
INSERT INTO `test1` VALUES (604, 'корзина для білизни з тримачем 330х290х500, в корпус 400мм, хром (шт)', '5', '252');
INSERT INTO `test1` VALUES (605, 'корзина для білизни з тримачем 380х290х500, в корпус 450мм, хром (шт)', '5', '261');
INSERT INTO `test1` VALUES (606, 'корзина для білизни з тримачем 430х290х500, в корпус 500мм, хром (шт)', '5', '271');
INSERT INTO `test1` VALUES (607, 'корзина для білизни з тримачем 480х290х500, в корпус 550мм, хром (шт)', '5', '281');
INSERT INTO `test1` VALUES (608, 'корзина для білизни з тримачем 530х290х500, в корпус 600мм, хром (шт)', '5', '290');
INSERT INTO `test1` VALUES (609, 'корзина для білизни кріплення на фасад 230х250х500, корпус 300мм, хром (шт)', '5', '106');
INSERT INTO `test1` VALUES (610, 'корзина для білизни кріплення на фасад 230х290х500, корпус 300мм, хром (шт)', '5', '232');
INSERT INTO `test1` VALUES (611, 'корзина для білизни кріплення на фасад 280х250х500, корпус 350мм, хром (шт)', '5', '116');
INSERT INTO `test1` VALUES (612, 'корзина для білизни кріплення на фасад 330х250х500, корпус 400мм, хром (шт)', '5', '125');
INSERT INTO `test1` VALUES (613, 'корзина для білизни кріплення на фасад 380х250х500, корпус 450мм, хром (шт)', '5', '135');
INSERT INTO `test1` VALUES (614, 'корзина для білизни кріплення на фасад 430х250х500, корпус 500мм, хром (шт)', '5', '145');
INSERT INTO `test1` VALUES (615, 'кошик 3-х уровн. алюмін.., бічного кріплення, лівий 280х485х920 (шт)', '5', '872');
INSERT INTO `test1` VALUES (616, 'кошик 3-х уровн. алюмін.., бічного кріплення, правий 280х485х920 (шт)', '5', '872');
INSERT INTO `test1` VALUES (617, 'кошик висувний 210х500х90мм, хром (шт)', '5', '116');
INSERT INTO `test1` VALUES (618, 'ліфт пантограф 600-900х140х850, сірий/хром (шт)', '5', '348');
INSERT INTO `test1` VALUES (619, 'ліфт пантограф 800-1150х140х850, сірий/хром (шт.)', '5', '348');
INSERT INTO `test1` VALUES (620, 'ліфт пантограф 800-1150х140х850, сірий/хром на 15 кг (шт)', '5', '348');
INSERT INTO `test1` VALUES (621, 'полиця висувна 600 мм, хром (шт)', '5', '308');
INSERT INTO `test1` VALUES (622, 'полиця висувна 600 мм, хром (шт)', '5', '310');
INSERT INTO `test1` VALUES (623, 'полиця висувна 900 мм, хром (шт)', '5', '368');
INSERT INTO `test1` VALUES (624, 'полиця для взуття 420-700х200х95 (шт.)', '5', '87');
INSERT INTO `test1` VALUES (625, 'полиця для взуття 900-1100х200х95 (шт)', '5', '106');
INSERT INTO `test1` VALUES (626, 'полиця для взуття висувна 550-580х470х170 в секцію 600 мм хром/алюміній (шт)', '5', '465');
INSERT INTO `test1` VALUES (627, 'полиця для взуття висувна 650-680х470х170 в секцію 700 мм хром/алюміній (шт)', '5', '484');
INSERT INTO `test1` VALUES (628, 'полиця для взуття висувна 750-780х470х170 в секцію 800 мм хром/алюміній (шт)', '5', '503');
INSERT INTO `test1` VALUES (629, 'полиця для взуття висувна 850-880х470х170 в секцію 900 мм, хром/алюміній (шт)', '5', '523');
INSERT INTO `test1` VALUES (630, 'полиця для взуття висувна нижнє кріплення 525х500х150, хром (шт)', '5', '300');
INSERT INTO `test1` VALUES (631, 'полиця для взуття висувна, нижнє кріплення, 375х500х150, хром (шт)', '5', '271');
INSERT INTO `test1` VALUES (632, 'полиця для взуття висувна, нижнє кріплення, 425х500х150, хром (шт)', '5', '281');
INSERT INTO `test1` VALUES (633, 'полиця для взуття висувна, нижнє кріплення, 475х500х150, хром (шт)', '5', '290');
INSERT INTO `test1` VALUES (634, 'полиця для взуття дворядна, поворотна, на фасад 60см, 530х230х220, хром (шт)', '5', '232');
INSERT INTO `test1` VALUES (635, 'полиця для взуття дворядна, поворотна, на фасад 70см, 630х230х220, хром (шт)', '5', '252');
INSERT INTO `test1` VALUES (636, 'полиця для взуття дворядна, поворотна, на фасад 80см, 730х230х220, хром (шт)', '5', '271');
INSERT INTO `test1` VALUES (637, 'полиця для взуття дворядна, поворотна, на фасад 90см, 830х230х220, хром (шт)', '5', '290');
INSERT INTO `test1` VALUES (638, 'полиця для взуття однорядна, поворотна, на фасад 60см, 530х120х220, хром (шт)', '5', '155');
INSERT INTO `test1` VALUES (639, 'полиця для взуття однорядна, поворотна, на фасад 70см, 630х120х220, хром (шт)', '5', '170');
INSERT INTO `test1` VALUES (640, 'полиця для взуття однорядна, поворотна, на фасад 80см, 730х120х220, хром (шт)', '5', '184');
INSERT INTO `test1` VALUES (641, 'полиця для взуття однорядна, поворотна, на фасад 90см, 830х120х220, хром (шт)', '5', '197');
INSERT INTO `test1` VALUES (642, 'полиця з боковим кріпленням 350-380х520х160 в секцию400мм алюмінієва (шт)', '5', '484');
INSERT INTO `test1` VALUES (643, 'полиця з боковим кріпленням 400-430х520х160 в секцию450мм алюмінієва (шт)', '5', '504');
INSERT INTO `test1` VALUES (644, 'полиця з боковим кріпленням 450-480х520х160 в секцию500мм алюмінієва (шт)', '5', '523');
INSERT INTO `test1` VALUES (645, 'полиця з боковим кріпленням 500-530х520х160 в секцию550мм алюмінієва (шт)', '5', '542');
INSERT INTO `test1` VALUES (646, 'полиця з боковим кріпленням 550-580х520х160 в секцию600мм алюмінієва (шт)', '5', '562');
INSERT INTO `test1` VALUES (647, 'полиця з боковим кріпленням 650-680х520х160 в секцию700мм алюмінієва (шт)', '5', '581');
INSERT INTO `test1` VALUES (648, 'полиця з боковим кріпленням 750-780х520х160 в секцию800мм алюмінієва (шт)', '5', '600');
INSERT INTO `test1` VALUES (649, 'полиця з боковим кріпленням 850-880х520х160 в секцию900мм алюмінієва (шт)', '5', '620');
INSERT INTO `test1` VALUES (650, 'полиця з верхнім кріпленням 475-505х520х160 в секцию550мм алюмінієва (шт)', '5', '542');
INSERT INTO `test1` VALUES (651, 'полиця з верхнім кріпленням 525-555х520х160 в секцию600мм алюмінієва (шт)', '5', '562');
INSERT INTO `test1` VALUES (652, 'полиця з верхнім кріпленням 825-855х520х160 в секцию900мм алюмінієва (шт)', '5', '620');
INSERT INTO `test1` VALUES (653, 'секція для взуття висувна, 500х500х470, хром (шт)', '5', '1027');
INSERT INTO `test1` VALUES (654, 'секція для взуття висувна, 550х500х470, хром (шт)', '5', '1046');
INSERT INTO `test1` VALUES (655, 'сітка повного висунення в корпус 35см (шт)', '5', '368');
INSERT INTO `test1` VALUES (656, 'сітка повного висунення в корпус 40см (шт)', '5', '377');
INSERT INTO `test1` VALUES (657, 'сітка повного висунення в корпус 45см (шт)', '5', '387');
INSERT INTO `test1` VALUES (658, 'сітка повного висунення в корпус 50см (шт)', '5', '406');
INSERT INTO `test1` VALUES (659, 'сітка повного висунення в корпус 55см (шт)', '5', '416');
INSERT INTO `test1` VALUES (660, 'сітка повного висунення в корпус 60см (шт)', '5', '445');
INSERT INTO `test1` VALUES (661, 'сітка повного висунення в корпус 70см (шт)', '5', '465');
INSERT INTO `test1` VALUES (662, 'сітка повного висунення в корпус 80см (шт)', '5', '474');
INSERT INTO `test1` VALUES (663, 'сітка повного висунення в корпус 90см (шт)', '5', '503');
INSERT INTO `test1` VALUES (664, 'стійка для газет, 375х270х430, хром (шт)', '5', '242');
INSERT INTO `test1` VALUES (665, 'тримач брюк і краваток висувний, поворотний 215х460х90, алюміній (шт)', '5', '290');
INSERT INTO `test1` VALUES (666, 'тримач брюк і ременів висувний, поворотний 215х460х90, алюміній (шт)', '5', '290');
INSERT INTO `test1` VALUES (667, 'тримач взуття висувний 2 полиці лівий алюм. (шт)', '5', '445');
INSERT INTO `test1` VALUES (668, 'тримач взуття висувний 2 полиці правий алюміній (шт)', '5', '445');
INSERT INTO `test1` VALUES (669, 'тримач краваткокі ременів 120х460х110, на спрямовуючій хром лівий (шт)', '5', '193');
INSERT INTO `test1` VALUES (670, 'тримач краваток 120х460х85, на спрямовуючій хром лівий (шт)', '5', '193');
INSERT INTO `test1` VALUES (671, 'тримач краваток120х460х85, на спрямовуючій хром правий (шт)', '5', '193');
INSERT INTO `test1` VALUES (672, 'тримач краваток 85х460х90, алюміній (шт)', '5', '252');
INSERT INTO `test1` VALUES (673, 'тримач краваток 85х460х90, хром (шт)', '5', '135');
INSERT INTO `test1` VALUES (674, 'тримач краваток і ременів 120х460х110, на спрямовуючій хром правий (шт)', '5', '193');
INSERT INTO `test1` VALUES (675, 'тримач краваток і ременів 85х460х125, хром (шт.)', '5', '135');
INSERT INTO `test1` VALUES (676, 'тримач краваток і ременів 85х460х90, алюміній (шт)', '5', '252');
INSERT INTO `test1` VALUES (677, 'тримач краваток і ременів з петельным кріпленням 350х50х350 (шт)', '5', '116');
INSERT INTO `test1` VALUES (678, 'тримач ременів  35х460х125, хром (шт)', '5', '135');
INSERT INTO `test1` VALUES (679, 'тримач ременів 85х460х90, алюміній (шт)', '5', '252');
INSERT INTO `test1` VALUES (680, 'тримач ременів висувний  120х460х110 лівий (шт)', '5', '193');
INSERT INTO `test1` VALUES (681, 'тримач ременів висувний 120х460х110 правий (шт)', '5', '193');
INSERT INTO `test1` VALUES (682, 'Карго 2-х уровневое боковое в верхний модуль 150 с доводчиком (гл.280мм)', '6', '460');
INSERT INTO `test1` VALUES (683, 'Карго 2-х уровневое боковое (без доводчика)', '6', '430');
INSERT INTO `test1` VALUES (684, 'Карго 3-х уровневое боковое 150 (без доводчика)', '6', '470');
INSERT INTO `test1` VALUES (685, 'Карго 2-х уровневое боковое со скрытой направляющей  150 (с доводчиком)', '6', '550');
INSERT INTO `test1` VALUES (686, 'Карго 2-х уровневое боковое со скрытой направляющей  200 (с доводчиком)', '6', '640');
INSERT INTO `test1` VALUES (687, 'Карго 3-х уровневое боковое со скрытой направляющей 300 (с доводчиком)', '6', '680');
INSERT INTO `test1` VALUES (688, 'Сушка 2-х уровневая для посуды выдвижная нижняя 300 (с доводчиком)', '6', '900');
INSERT INTO `test1` VALUES (689, 'Разделитель-фиксатор для бутылок (комплект сост. из 4 шт)', '6', '50');
INSERT INTO `test1` VALUES (690, 'Комплект креплений фасада на карго', '6', '42');
INSERT INTO `test1` VALUES (691, 'Комплект креплений фасада на карго NEW', '6', '55');
INSERT INTO `test1` VALUES (692, 'Карусель 3/4  ?  815', '6', '1360');
INSERT INTO `test1` VALUES (693, 'Труба для карусели ? 25', '6', '155');
INSERT INTO `test1` VALUES (694, 'Рамка для двери модуля H=1740                          (рамка крепится на дверь, max 6 корзин, 450 или 600 мм)', '6', '960');
INSERT INTO `test1` VALUES (695, 'Рамка для двойной выдвижной корзины H=1740 (рамки для внутр. корзин и дверных, max кол-во 6 шт одного вида,450 или 600мм )', '6', '2120');
INSERT INTO `test1` VALUES (696, 'Газовая пружина для рамки                (рекомендовано использовать со структурой для двойной выдвижной корзины)  ', '6', '160');
INSERT INTO `test1` VALUES (697, 'Внутреняя решетчатая корзина                                                 (для двери модуля)', '6', '210');
INSERT INTO `test1` VALUES (698, 'Решетчатая корзина                                             (подходит для структуры для дверей модуля и для структуры для двойной выдвижной корзины)', '6', '195');
INSERT INTO `test1` VALUES (699, 'Выдвижной механизм для базы со створкой 450 правая', '6', '3200');
INSERT INTO `test1` VALUES (700, 'Газовая пружина для выдвижного механизма(рекомендовано использовать с выдвижным механизмом для базы со створкой  45)', '6', '132');
INSERT INTO `test1` VALUES (701, 'Механизм Fly Moon для угл.базы 600мм на 2 полки прав/лев', '6', '1575');
INSERT INTO `test1` VALUES (702, 'Механизм Fly Moon для угл.базы 600мм на 4 полки прав/лев', '6', '2225');
INSERT INTO `test1` VALUES (703, 'Выдвижные полки Fly Moon для механизма 600мм, H=60мм прав/лев', '6', '1745');
INSERT INTO `test1` VALUES (704, 'Выдвижные полки  Fly Moon для механизма 600мм, H=40мм прав/лев', '6', '1730');
INSERT INTO `test1` VALUES (705, 'Ящик выдвижной H=160', '6', '525');
INSERT INTO `test1` VALUES (706, 'Ящик выдвижной H=160', '6', '550');
INSERT INTO `test1` VALUES (707, 'Ящик выдвижной H=160', '6', '655');
INSERT INTO `test1` VALUES (708, 'Ящик выдвижной H=150 межмодульный', '6', '550');
INSERT INTO `test1` VALUES (709, 'Ящик выдвижной H=150 межмодульный', '6', '655');
INSERT INTO `test1` VALUES (710, 'Ящик выдвижной для посуды H=159', '6', '745');
INSERT INTO `test1` VALUES (711, 'Ящик выдвижной для посуды H=159', '6', '935');
INSERT INTO `test1` VALUES (712, 'Корзина под мойку', '6', '755');
INSERT INTO `test1` VALUES (713, 'Комплект креплений фасада на ящик выдвижной', '6', '30');
INSERT INTO `test1` VALUES (714, 'Подьемно-спускная полка для  шкафа 277', '6', '910');
INSERT INTO `test1` VALUES (715, 'Подьемно-спускная полка для  шкафа 277', '6', '1280');
INSERT INTO `test1` VALUES (716, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', '90');
INSERT INTO `test1` VALUES (717, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', '210');
INSERT INTO `test1` VALUES (718, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', '260');
INSERT INTO `test1` VALUES (719, 'Полка стеклянная для навесных кухонных шкафов 260 ', '6', '335');
INSERT INTO `test1` VALUES (720, 'Полка стеклянная для кухонных шкафов 480', '6', '215');
INSERT INTO `test1` VALUES (721, 'Полка стеклянная для кухонных шкафов 480', '6', '280');
INSERT INTO `test1` VALUES (722, 'Полка стеклянная для кухонных шкафов 480', '6', '460');
INSERT INTO `test1` VALUES (723, 'Полка решетчатая для кухонных шкафов 479', '6', '230');
INSERT INTO `test1` VALUES (724, 'Полка решетчатая для кухонных шкафов 480', '6', '300');
INSERT INTO `test1` VALUES (725, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', '220');
INSERT INTO `test1` VALUES (726, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', '270');
INSERT INTO `test1` VALUES (727, 'Полка решетчатая  для навесных кухонных шкафов 260', '6', '355');
INSERT INTO `test1` VALUES (728, 'Комплект креплений для полок решетчатых       (4 крепежных элемента + евровинты)', '6', '15');
INSERT INTO `test1` VALUES (729, 'Подставка для бутылок , для крепления к полке решетчетой (подходит только к полке решетчатой)', '6', '385');
INSERT INTO `test1` VALUES (730, 'Корзина для крепления к полке решетчетой (подходит только к полке решетчатой)', '6', '170');
INSERT INTO `test1` VALUES (731, 'Сортер для мусора', '6', '480');
INSERT INTO `test1` VALUES (732, 'Сортер с крышкой для модуля 450 правая (с опрокидывающимися крышками)', '6', '1730');
INSERT INTO `test1` VALUES (733, 'Сортер с крышкой для модуля 450 левая (с опрокидывающимися крышками)', '6', '1730');
INSERT INTO `test1` VALUES (734, 'Сортер с поворотной системой                                          (для углового модудуля 900х900, с амортизированным возвратом)', '6', '2180');
INSERT INTO `test1` VALUES (735, ' Cортер в модуль 450 на 3 ведра с доводчиком', '6', '1025');
INSERT INTO `test1` VALUES (736, ' Cортер в модуль 300 на 2 ведра с доводчиком', '6', '665');
INSERT INTO `test1` VALUES (737, 'Сортер в модуль 600мм, на 4 ведра с доводчиком', '6', '1265');
INSERT INTO `test1` VALUES (738, 'Поворотная система для угловой базы (для углового модудуля 900х900, 2 алюминиевых конетейнера, 2 решетчатых контейнера и карусель 3/4)', '6', '3850');
INSERT INTO `test1` VALUES (739, 'Гофрированный коврик 1,5мм', '6', '75');
INSERT INTO `test1` VALUES (740, 'Гофрированный коврик 2,5мм', '6', '100');
INSERT INTO `test1` VALUES (741, 'Релинг d=16 с квадратными опорами (цвет алюминий)', '6', '155');
INSERT INTO `test1` VALUES (742, 'Заглушка для релинга, плоская', '6', '11');
INSERT INTO `test1` VALUES (743, 'Крючки одинарные для релинга (в к-те 5шт.) ', '6', '40');
INSERT INTO `test1` VALUES (744, 'Полка угловая (устанавливается с любой стороны: прав.=лев.)', '6', '400');
INSERT INTO `test1` VALUES (745, 'Полка', '6', '355');
INSERT INTO `test1` VALUES (746, 'Комплект для специй', '6', '415');
INSERT INTO `test1` VALUES (747, 'Держатель для рулонов', '6', '365');
INSERT INTO `test1` VALUES (748, 'Держатель для бумаги', '6', '265');
INSERT INTO `test1` VALUES (749, 'Релинг d=16 с круглыми опорами (цвет хром)', '6', '160');
INSERT INTO `test1` VALUES (750, 'Заглушка для релинга ', '6', '25');
INSERT INTO `test1` VALUES (751, 'Заглушка для релинга, плоская', '6', '10');
INSERT INTO `test1` VALUES (752, 'Крючки одинарные для релинга (в к-те 5шт.) ', '6', '40');
INSERT INTO `test1` VALUES (753, ' Крючок-мини несъемный для релинга (в к-те 5шт.) ', '6', '50');
INSERT INTO `test1` VALUES (754, 'Полка угловая решетчатая (устанавливается с любой стороны: прав.=лев.)', '6', '180');
INSERT INTO `test1` VALUES (755, 'Полка решетчатая ', '6', '195');
INSERT INTO `test1` VALUES (756, 'Комплект для специй решетчатый', '6', '135');
INSERT INTO `test1` VALUES (757, 'Держатель для рулонов', '6', '180');
INSERT INTO `test1` VALUES (758, 'Держатель для бумаги', '6', '160');
INSERT INTO `test1` VALUES (759, 'Держатель для бокалов', '6', '145');
INSERT INTO `test1` VALUES (760, 'Подставка для бутылок', '6', '145');
INSERT INTO `test1` VALUES (761, 'Полка для аксессуаров', '6', '215');
INSERT INTO `test1` VALUES (762, 'Навесная сушка для посуды (с поддоном из нержавеющей стали AISI 304)', '6', '570');
INSERT INTO `test1` VALUES (763, 'Подставка для столовых приборов c контейнером из нержавеющей стали AISI 304', '6', '420');
INSERT INTO `test1` VALUES (764, 'Плінтус RAUWALON MAGIC 127 алюмінієвий', '7', '190');
INSERT INTO `test1` VALUES (765, 'Плінтус RAUWALON 116 алюмінієвий ', '7', '120');
INSERT INTO `test1` VALUES (766, 'Плінтус пустишка RAUWALON Basic 104', '7', '130');
INSERT INTO `test1` VALUES (767, 'Плінтус RAUWALON 118 (колір стільниці EGGER)', '7', '160');
INSERT INTO `test1` VALUES (768, 'Кути (закінчення) плінтуса RAUWALON ', '7', '5');
INSERT INTO `test1` VALUES (769, 'Плінтус SCILM алюміній Н=45мм', '7', '155');
INSERT INTO `test1` VALUES (770, 'Плінтус SCILM алюміній Н=45мм рифлений', '7', '170');
INSERT INTO `test1` VALUES (771, 'Кути зовн/внутр 90/135?  плінтуса алюмінієвого SCILM Н=45мм (сірі)', '7', '5');
INSERT INTO `test1` VALUES (772, 'Закінчення  плінтуса алюмінієвого SCILM Н=45мм (сірі)', '7', '5');
INSERT INTO `test1` VALUES (773, 'Плінтус SCILM алюміній прямокутний Н=30мм колір: алюм, алюм браш, золото, білий, чорний браш. Основа-біла, сіра.', '7', '140');
INSERT INTO `test1` VALUES (774, 'Плінтус SCILM алюміній прямокутний рифлений Н=30мм', '7', '150');
INSERT INTO `test1` VALUES (775, 'Отбортовка под пластиковую вставку Н=30 L=4000 (основа - біла, коричнева)', '7', '100');
INSERT INTO `test1` VALUES (776, 'Кути зовн/внутр 90/135  плінтуса Н=30мм SCILM (сірі, білі, коричневі)', '7', '5');
INSERT INTO `test1` VALUES (777, 'Кути, закінчення  плінтуса SCILM Н=30мм (сірі, білі, чорні)', '7', '5');
INSERT INTO `test1` VALUES (778, 'Цоколь пластиковий Н=100мм під алюміній', '7', '150');
INSERT INTO `test1` VALUES (779, 'Цоколь пластиковий Н=100мм під алюміній рифлений', '7', '150');
INSERT INTO `test1` VALUES (780, 'Цоколь пластиковий Н=150мм', '7', '250');
INSERT INTO `test1` VALUES (781, 'Цоколь алюмінієвий Н=100мм (колір алюміній, алюміній BRUSH)', '7', '200');
INSERT INTO `test1` VALUES (782, 'Цоколь алюмінієвий Н=100мм (колір золото, глянцеве золото)', '7', '200');
INSERT INTO `test1` VALUES (783, 'Цоколь алюмінієвий Н=100мм (колір білий, чорний BRUSH)', '7', '250');
INSERT INTO `test1` VALUES (784, 'Кут до цоколя алюм. Н=100мм (90°, 270°)', '7', '5');
INSERT INTO `test1` VALUES (785, 'Заглушка до цоколя алюм Н=100мм', '7', '15');
INSERT INTO `test1` VALUES (786, 'Кутик регулюємий для алюмінієвого цоколя Н=100мм new', '7', '30');
INSERT INTO `test1` VALUES (787, 'Кутик 90°, 270° універсальний до пластикового цоколя', '7', '20');
INSERT INTO `test1` VALUES (788, 'Кутик 135°, 315° універсальний до пластикового цоколя', '7', '25');
INSERT INTO `test1` VALUES (789, 'Заглушка до пластикового цоколя', '7', '5');
INSERT INTO `test1` VALUES (790, 'Решітка в цоколь вентиляційна алюм(пластик)', '7', '25');
INSERT INTO `test1` VALUES (791, 'Ущільнювач під цоколь ДСП пластиковий (прозорий)', '7', '25');
INSERT INTO `test1` VALUES (792, 'Торцовка стільниці профіль подвійний Н=40 L=3900 (алюміній)', '7', '350');
INSERT INTO `test1` VALUES (793, 'Торцовка стільниці профіль подвійний Н=40 L=3900 (під нержавійку)', '7', '500');
INSERT INTO `test1` VALUES (794, 'Торцовка стільниці кут внутрішній 90°, 135°, зовнішній 270°, 315°', '7', '150');
INSERT INTO `test1` VALUES (795, 'Торцовка стільниці кут внутрішній 90°, 135°', '7', '190');
INSERT INTO `test1` VALUES (796, ' VIBO cушка 45см з регул рамою', '8', '340');
INSERT INTO `test1` VALUES (797, 'VIBO cушка 50 см з регул рамою (1 піддон металевий)', '8', '770');
INSERT INTO `test1` VALUES (798, 'VIBO cушка 60 см з регул рамою', '8', '360');
INSERT INTO `test1` VALUES (799, 'VIBO cушка 80 см з регул рамою', '8', '410');
INSERT INTO `test1` VALUES (800, 'VIBO cушка 90 см з регул рамою', '8', '420');
INSERT INTO `test1` VALUES (801, 'VIBO cушка 50 см', '8', '770');
INSERT INTO `test1` VALUES (802, 'VIBO cушка 60 см', '8', '300');
INSERT INTO `test1` VALUES (803, 'VIBO cушка 80 см', '8', '325');
INSERT INTO `test1` VALUES (804, 'VIBO cушка 90 см', '8', '340');
INSERT INTO `test1` VALUES (805, 'VIBO cушка 1200мм з регул рамою (1 піддон металевий)', '8', '925');
INSERT INTO `test1` VALUES (806, 'VIBO cушка кутова рама (верхня кутова секція 600*600мм)', '8', '1360');
INSERT INTO `test1` VALUES (807, 'REJS cушка 40 см рама регульована', '8', '130');
INSERT INTO `test1` VALUES (808, 'REJS cушка 50 см рама регульована', '8', '150');
INSERT INTO `test1` VALUES (809, 'REJS cушка 60 см рама регульована', '8', '160');
INSERT INTO `test1` VALUES (810, 'REJS cушка 70 см рама регульована', '8', '250');
INSERT INTO `test1` VALUES (811, 'REJS cушка 80 см рама регульована', '8', '180');
INSERT INTO `test1` VALUES (812, 'REJS cушка 90 см рама регульована', '8', '205');
INSERT INTO `test1` VALUES (813, 'РГ 32', '10', '27');
INSERT INTO `test1` VALUES (814, 'РГ 31', '10', '35');
INSERT INTO `test1` VALUES (815, 'РГ 50', '10', '16');
INSERT INTO `test1` VALUES (816, 'РГ 49', '10', '18');
INSERT INTO `test1` VALUES (817, 'РГ 30', '10', '43');
INSERT INTO `test1` VALUES (818, 'РГ 285', '10', '43');
INSERT INTO `test1` VALUES (819, 'РГ 51', '10', '26');
INSERT INTO `test1` VALUES (820, 'РГ 282', '10', '60');
INSERT INTO `test1` VALUES (821, 'РГ 283', '10', '57');
INSERT INTO `test1` VALUES (822, 'РГ 284', '10', '32');
INSERT INTO `test1` VALUES (823, 'РГ 34', '10', '39');
INSERT INTO `test1` VALUES (824, 'РГ 33', '10', '41');
INSERT INTO `test1` VALUES (825, 'РГ 54', '10', '20');
INSERT INTO `test1` VALUES (826, 'РГ 53', '10', '24');
INSERT INTO `test1` VALUES (827, 'РГ 35', '10', '38');
INSERT INTO `test1` VALUES (828, 'РГ 295', '10', '28');
INSERT INTO `test1` VALUES (829, 'РГ 55', '10', '20');
INSERT INTO `test1` VALUES (830, 'РГ 36', '10', '38');
INSERT INTO `test1` VALUES (831, 'РГ 296', '10', '28');
INSERT INTO `test1` VALUES (832, 'РГ 56', '10', '20');
INSERT INTO `test1` VALUES (833, 'РГ 210', '10', '50');
INSERT INTO `test1` VALUES (834, 'РГ 211', '10', '32');
INSERT INTO `test1` VALUES (835, 'РГ 212', '10', '24');
INSERT INTO `test1` VALUES (836, 'РГ 286', '10', '59');
INSERT INTO `test1` VALUES (837, 'РГ 287', '10', '40');
INSERT INTO `test1` VALUES (838, 'РГ 288', '10', '32');
INSERT INTO `test1` VALUES (839, 'РГ 289', '10', '48');
INSERT INTO `test1` VALUES (840, 'РГ 290', '10', '31');
INSERT INTO `test1` VALUES (841, 'РГ 291', '10', '27');
INSERT INTO `test1` VALUES (842, 'РГ 292', '10', '46');
INSERT INTO `test1` VALUES (843, 'РГ 293', '10', '30');
INSERT INTO `test1` VALUES (844, 'РГ 294', '10', '26');
INSERT INTO `test1` VALUES (845, 'РГ 38', '10', '37');
INSERT INTO `test1` VALUES (846, 'РГ 331', '10', '31');
INSERT INTO `test1` VALUES (847, 'РГ 58', '10', '25');
INSERT INTO `test1` VALUES (848, 'РГ 37', '10', '47');
INSERT INTO `test1` VALUES (849, 'РГ 332', '10', '42');
INSERT INTO `test1` VALUES (850, 'РГ 57', '10', '33');
INSERT INTO `test1` VALUES (851, 'РГ 322', '10', '54');
INSERT INTO `test1` VALUES (852, 'РГ 323', '10', '47');
INSERT INTO `test1` VALUES (853, 'РГ 324', '10', '41');
INSERT INTO `test1` VALUES (854, 'РГ 325', '10', '43');
INSERT INTO `test1` VALUES (855, 'РГ 326', '10', '39');
INSERT INTO `test1` VALUES (856, 'РГ 327', '10', '33');
INSERT INTO `test1` VALUES (857, 'РГ 328', '10', '41');
INSERT INTO `test1` VALUES (858, 'РГ 329', '10', '37');
INSERT INTO `test1` VALUES (859, 'РГ 330', '10', '33');
INSERT INTO `test1` VALUES (860, 'РГ 40', '10', '38');
INSERT INTO `test1` VALUES (861, 'РГ 276', '10', '24');
INSERT INTO `test1` VALUES (862, 'РГ 42', '10', '20');
INSERT INTO `test1` VALUES (863, 'РГ 60', '10', '20');
INSERT INTO `test1` VALUES (864, 'РГ 39', '10', '47');
INSERT INTO `test1` VALUES (865, 'РГ 277', '10', '35');
INSERT INTO `test1` VALUES (866, 'РГ 41', '10', '24');
INSERT INTO `test1` VALUES (867, 'РГ 59', '10', '21');
INSERT INTO `test1` VALUES (868, 'РГ 278 ', '10', '54');
INSERT INTO `test1` VALUES (869, 'РГ 279 ', '10', '34');
INSERT INTO `test1` VALUES (870, 'РГ 281 ', '10', '31');
INSERT INTO `test1` VALUES (871, 'РГ 280 ', '10', '32');
INSERT INTO `test1` VALUES (872, 'РГ 302', '10', '49');
INSERT INTO `test1` VALUES (873, 'РГ 303', '10', '32');
INSERT INTO `test1` VALUES (874, 'РГ 304', '10', '28');
INSERT INTO `test1` VALUES (875, 'РГ 305', '10', '31');
INSERT INTO `test1` VALUES (876, 'РГ 306', '10', '41');
INSERT INTO `test1` VALUES (877, 'РГ 307', '10', '26');
INSERT INTO `test1` VALUES (878, 'РГ 308', '10', '24');
INSERT INTO `test1` VALUES (879, 'РГ 309', '10', '26');
INSERT INTO `test1` VALUES (880, 'РГ 310', '10', '39');
INSERT INTO `test1` VALUES (881, 'РГ 311', '10', '25');
INSERT INTO `test1` VALUES (882, 'РГ 312', '10', '24');
INSERT INTO `test1` VALUES (883, 'РГ 313', '10', '25');
INSERT INTO `test1` VALUES (884, 'РГ 411', '10', '40');
INSERT INTO `test1` VALUES (885, 'РГ 412', '10', '27');
INSERT INTO `test1` VALUES (886, 'РГ 413', '10', '22');
INSERT INTO `test1` VALUES (887, 'РГ 414', '10', '26');
INSERT INTO `test1` VALUES (888, 'РГ 4', '10', '63');
INSERT INTO `test1` VALUES (889, 'РГ 22', '10', '47');
INSERT INTO `test1` VALUES (890, 'РГ 5', '10', '67');
INSERT INTO `test1` VALUES (891, 'РГ 24', '10', '52');
INSERT INTO `test1` VALUES (892, 'РГ 121', '10', '75');
INSERT INTO `test1` VALUES (893, 'РГ 122', '10', '30');
INSERT INTO `test1` VALUES (894, 'РГ 7', '10', '57');
INSERT INTO `test1` VALUES (895, 'РГ 21', '10', '40');
INSERT INTO `test1` VALUES (896, 'РГ 6', '10', '59');
INSERT INTO `test1` VALUES (897, 'РГ 20', '10', '47');
INSERT INTO `test1` VALUES (898, 'РГ 8', '10', '54');
INSERT INTO `test1` VALUES (899, 'РГ 23', '10', '45');
INSERT INTO `test1` VALUES (900, 'РГ 9', '10', '65');
INSERT INTO `test1` VALUES (901, 'РГ 25', '10', '54');
INSERT INTO `test1` VALUES (902, 'РГ 10', '10', '66');
INSERT INTO `test1` VALUES (903, 'РГ 16', '10', '22');
INSERT INTO `test1` VALUES (904, 'РГ 11', '10', '65');
INSERT INTO `test1` VALUES (905, 'РГ 17', '10', '19');
INSERT INTO `test1` VALUES (906, 'РГ 18', '10', '26');
INSERT INTO `test1` VALUES (907, 'РГ 165 ', '10', '22');
INSERT INTO `test1` VALUES (908, 'РГ 12', '10', '51');
INSERT INTO `test1` VALUES (909, 'РГ 14', '10', '19');
INSERT INTO `test1` VALUES (910, 'РГ 19', '10', '32');
INSERT INTO `test1` VALUES (911, 'РГ 13', '10', '61');
INSERT INTO `test1` VALUES (912, 'РГ 15', '10', '18');
INSERT INTO `test1` VALUES (913, 'РГ 26', '10', '33');
INSERT INTO `test1` VALUES (914, 'РГ 27', '10', '45');
INSERT INTO `test1` VALUES (915, 'РГ 28', '10', '29');
INSERT INTO `test1` VALUES (916, 'РГ 29', '10', '34');
INSERT INTO `test1` VALUES (917, 'РГ 115', '10', '31');
INSERT INTO `test1` VALUES (918, 'РГ 116', '10', '30');
INSERT INTO `test1` VALUES (919, 'РГ 117', '10', '31');
INSERT INTO `test1` VALUES (920, 'РГ 118', '10', '31');
INSERT INTO `test1` VALUES (921, 'РГ 409', '10', '80');
INSERT INTO `test1` VALUES (922, 'РГ 410', '10', '91');
INSERT INTO `test1` VALUES (923, 'РГ 52', '10', '13');
INSERT INTO `test1` VALUES (924, 'РГ 73', '10', '76');
INSERT INTO `test1` VALUES (925, 'РГ 74', '10', '54');
INSERT INTO `test1` VALUES (926, 'РГ 77', '10', '34');
INSERT INTO `test1` VALUES (927, 'РГ 78', '10', '49');
INSERT INTO `test1` VALUES (928, 'РГ 75', '10', '65');
INSERT INTO `test1` VALUES (929, 'РГ 76', '10', '58');
INSERT INTO `test1` VALUES (930, 'РГ 79', '10', '32');
INSERT INTO `test1` VALUES (931, 'РГ 80', '10', '47');
INSERT INTO `test1` VALUES (932, 'РГ 81', '10', '41');
INSERT INTO `test1` VALUES (933, 'РГ 82', '10', '40');
INSERT INTO `test1` VALUES (934, 'РГ 83', '10', '85');
INSERT INTO `test1` VALUES (935, 'РГ 84', '10', '30');
INSERT INTO `test1` VALUES (936, 'РГ 85', '10', '45');
INSERT INTO `test1` VALUES (937, 'РГ 86', '10', '49');
INSERT INTO `test1` VALUES (938, 'РГ 87', '10', '73');
INSERT INTO `test1` VALUES (939, 'РГ 108', '10', '92');
INSERT INTO `test1` VALUES (940, 'РГ 109', '10', '79');
INSERT INTO `test1` VALUES (941, 'РГ 110', '10', '43');
INSERT INTO `test1` VALUES (942, 'РГ 113', '10', '46');
INSERT INTO `test1` VALUES (943, 'РГ 114', '10', '62');
INSERT INTO `test1` VALUES (944, 'РГ 172', '10', '44');
INSERT INTO `test1` VALUES (945, 'РГ 173', '10', '32');
INSERT INTO `test1` VALUES (946, 'РГ 2', '10', '27');
INSERT INTO `test1` VALUES (947, 'РГ 1', '10', '12');
INSERT INTO `test1` VALUES (948, 'РГ 3', '10', '7');
INSERT INTO `test1` VALUES (949, 'РГ 333', '10', '51');
INSERT INTO `test1` VALUES (950, 'РГ 334', '10', '23');
INSERT INTO `test1` VALUES (951, 'РГ 335', '10', '15');
INSERT INTO `test1` VALUES (952, 'РГ 336', '10', '35');
INSERT INTO `test1` VALUES (953, 'РГ 337', '10', '9');
INSERT INTO `test1` VALUES (954, 'РГ 338', '10', '11');
INSERT INTO `test1` VALUES (955, 'РГ 340', '10', '57');
INSERT INTO `test1` VALUES (956, 'РГ 341', '10', '41');
INSERT INTO `test1` VALUES (957, 'РГ 107', '10', '22');
INSERT INTO `test1` VALUES (958, 'РГ 99', '10', '20');
INSERT INTO `test1` VALUES (959, 'РГ 92', '10', '12');
INSERT INTO `test1` VALUES (960, 'РГ 102', '10', '20');
INSERT INTO `test1` VALUES (961, 'РГ 90', '10', '37');
INSERT INTO `test1` VALUES (962, 'РГ 91', '10', '29');
INSERT INTO `test1` VALUES (963, 'РГ 95', '10', '15');
INSERT INTO `test1` VALUES (964, 'РГ 96', '10', '14');
INSERT INTO `test1` VALUES (965, 'РГ 97', '10', '42');
INSERT INTO `test1` VALUES (966, 'РГ 98', '10', '42');
INSERT INTO `test1` VALUES (967, 'РГ 105', '10', '16');
INSERT INTO `test1` VALUES (968, 'РГ 106', '10', '13');
INSERT INTO `test1` VALUES (969, 'РГ 100', '10', '48');
INSERT INTO `test1` VALUES (970, 'РГ 101', '10', '34');
INSERT INTO `test1` VALUES (971, 'РГ 88', '10', '23');
INSERT INTO `test1` VALUES (972, 'РГ 89', '10', '18');
INSERT INTO `test1` VALUES (973, 'РГ 111', '10', '25');
INSERT INTO `test1` VALUES (974, 'РГ 112', '10', '17');
INSERT INTO `test1` VALUES (975, 'РГ 119', '10', '29');
INSERT INTO `test1` VALUES (976, 'РГ 320', '10', '35');
INSERT INTO `test1` VALUES (977, 'РГ 120', '10', '37');
INSERT INTO `test1` VALUES (978, 'РГ 321', '10', '50');
INSERT INTO `test1` VALUES (979, 'РГ 314 ', '10', '39');
INSERT INTO `test1` VALUES (980, 'РГ 315', '10', '40');
INSERT INTO `test1` VALUES (981, 'РГ 316', '10', '31');
INSERT INTO `test1` VALUES (982, 'РГ 317', '10', '35');
INSERT INTO `test1` VALUES (983, 'РГ 318', '10', '30');
INSERT INTO `test1` VALUES (984, 'РГ 319', '10', '33');
INSERT INTO `test1` VALUES (985, 'РГ 253 ', '10', '78');
INSERT INTO `test1` VALUES (986, 'РГ 254 ', '10', '63');
INSERT INTO `test1` VALUES (987, 'РГ 255 ', '10', '57');
INSERT INTO `test1` VALUES (988, 'РГ 256', '10', '37');
INSERT INTO `test1` VALUES (989, 'РГ 257 ', '10', '25');
INSERT INTO `test1` VALUES (990, 'РГ 258 ', '10', '54');
INSERT INTO `test1` VALUES (991, 'РГ 259 ', '10', '44');
INSERT INTO `test1` VALUES (992, 'РГ 260 ', '10', '38');
INSERT INTO `test1` VALUES (993, 'РГ 261', '10', '26');
INSERT INTO `test1` VALUES (994, 'РГ 262', '10', '20');
INSERT INTO `test1` VALUES (995, 'РГ 263', '10', '123');
INSERT INTO `test1` VALUES (996, 'РГ 264', '10', '98');
INSERT INTO `test1` VALUES (997, 'РГ 265', '10', '89');
INSERT INTO `test1` VALUES (998, 'РГ 266', '10', '73');
INSERT INTO `test1` VALUES (999, 'РГ 267', '10', '53');
INSERT INTO `test1` VALUES (1000, 'РГ 103', '10', '10');
INSERT INTO `test1` VALUES (1001, 'РГ 93', '10', '9');
INSERT INTO `test1` VALUES (1002, 'РГ 104', '10', '7');
INSERT INTO `test1` VALUES (1003, 'РГ 94', '10', '9');
INSERT INTO `test1` VALUES (1004, 'РГ 123', '10', '54');
INSERT INTO `test1` VALUES (1005, 'РГ 300', '10', '52');
INSERT INTO `test1` VALUES (1006, 'РГ 125', '10', '32');
INSERT INTO `test1` VALUES (1007, 'РГ 124', '10', '61');
INSERT INTO `test1` VALUES (1008, 'РГ 301', '10', '57');
INSERT INTO `test1` VALUES (1009, 'РГ 126', '10', '36');
INSERT INTO `test1` VALUES (1010, 'РГ 297', '10', '37');
INSERT INTO `test1` VALUES (1011, 'РГ 298', '10', '30');
INSERT INTO `test1` VALUES (1012, 'РГ 299', '10', '21');
INSERT INTO `test1` VALUES (1013, 'РГ 127', '10', '38');
INSERT INTO `test1` VALUES (1014, 'РГ 128', '10', '51');
INSERT INTO `test1` VALUES (1015, 'РГ 129', '10', '78');
INSERT INTO `test1` VALUES (1016, 'РГ 133', '10', '99');
INSERT INTO `test1` VALUES (1017, 'РГ 130', '10', '51');
INSERT INTO `test1` VALUES (1018, 'РГ 134', '10', '70');
INSERT INTO `test1` VALUES (1019, 'РГ 131', '10', '96');
INSERT INTO `test1` VALUES (1020, 'РГ 135', '10', '107');
INSERT INTO `test1` VALUES (1021, 'РГ 132', '10', '60');
INSERT INTO `test1` VALUES (1022, 'РГ 136', '10', '75');
INSERT INTO `test1` VALUES (1023, 'РГ 192', '10', '35');
INSERT INTO `test1` VALUES (1024, 'РГ 193', '10', '26');
INSERT INTO `test1` VALUES (1025, 'РГ 194', '10', '24');
INSERT INTO `test1` VALUES (1026, 'РГ 195', '10', '62');
INSERT INTO `test1` VALUES (1027, 'РГ 196', '10', '59');
INSERT INTO `test1` VALUES (1028, 'РГ 197', '10', '57');
INSERT INTO `test1` VALUES (1029, 'РГ 198', '10', '77');
INSERT INTO `test1` VALUES (1030, 'РГ 199', '10', '68');
INSERT INTO `test1` VALUES (1031, 'РГ 200', '10', '67');
INSERT INTO `test1` VALUES (1032, 'РГ 201', '10', '33');
INSERT INTO `test1` VALUES (1033, 'РГ 202', '10', '30');
INSERT INTO `test1` VALUES (1034, 'РГ 203', '10', '23');
INSERT INTO `test1` VALUES (1035, 'РГ 204', '10', '35');
INSERT INTO `test1` VALUES (1036, 'РГ 205', '10', '26');
INSERT INTO `test1` VALUES (1037, 'РГ 206', '10', '24');
INSERT INTO `test1` VALUES (1038, 'РГ 207', '10', '16');
INSERT INTO `test1` VALUES (1039, 'РГ 208', '10', '36');
INSERT INTO `test1` VALUES (1040, 'РГ 209', '10', '25');
INSERT INTO `test1` VALUES (1041, 'РГ 213', '10', '42');
INSERT INTO `test1` VALUES (1042, 'РГ 214', '10', '34');
INSERT INTO `test1` VALUES (1043, 'РГ 215', '10', '26');
INSERT INTO `test1` VALUES (1044, 'РГ 216', '10', '18');
INSERT INTO `test1` VALUES (1045, 'РГ 217', '10', '88');
INSERT INTO `test1` VALUES (1046, 'РГ 218', '10', '71');
INSERT INTO `test1` VALUES (1047, 'РГ 219', '10', '55');
INSERT INTO `test1` VALUES (1048, 'РГ 220', '10', '51');
INSERT INTO `test1` VALUES (1049, 'РГ 248', '10', '78');
INSERT INTO `test1` VALUES (1050, 'РГ 249', '10', '78');
INSERT INTO `test1` VALUES (1051, 'РГ 250', '10', '66');
INSERT INTO `test1` VALUES (1052, 'РГ 251', '10', '66');
INSERT INTO `test1` VALUES (1053, 'РГ 252', '10', '44');
INSERT INTO `test1` VALUES (1054, 'РГ 221', '10', '40');
INSERT INTO `test1` VALUES (1055, 'РГ 222', '10', '33');
INSERT INTO `test1` VALUES (1056, 'РГ 223', '10', '21');
INSERT INTO `test1` VALUES (1057, 'РГ 224', '10', '21');
INSERT INTO `test1` VALUES (1058, 'РГ 225', '10', '39');
INSERT INTO `test1` VALUES (1059, 'РГ 226', '10', '34');
INSERT INTO `test1` VALUES (1060, 'РГ 227', '10', '62');
INSERT INTO `test1` VALUES (1061, 'РГ 228', '10', '45');
INSERT INTO `test1` VALUES (1062, 'РГ 229', '10', '30');
INSERT INTO `test1` VALUES (1063, 'РГ 230', '10', '32');
INSERT INTO `test1` VALUES (1064, 'РГ 231', '10', '61');
INSERT INTO `test1` VALUES (1065, 'РГ 232', '10', '54');
INSERT INTO `test1` VALUES (1066, 'РГ 239', '10', '49');
INSERT INTO `test1` VALUES (1067, 'РГ 240', '10', '41');
INSERT INTO `test1` VALUES (1068, 'РГ 241', '10', '19');
INSERT INTO `test1` VALUES (1069, 'РГ 242', '10', '128');
INSERT INTO `test1` VALUES (1070, 'РГ 243', '10', '107');
INSERT INTO `test1` VALUES (1071, 'РГ 244', '10', '48');
INSERT INTO `test1` VALUES (1072, 'РГ 245', '10', '140');
INSERT INTO `test1` VALUES (1073, 'РГ 246', '10', '112');
INSERT INTO `test1` VALUES (1074, 'РГ 247', '10', '51');
INSERT INTO `test1` VALUES (1075, 'РГ 345', '10', '53');
INSERT INTO `test1` VALUES (1076, 'РГ 346', '10', '37');
INSERT INTO `test1` VALUES (1077, 'РГ 347', '10', '26');
INSERT INTO `test1` VALUES (1078, 'РГ 348', '10', '78');
INSERT INTO `test1` VALUES (1079, 'РГ 349', '10', '56');
INSERT INTO `test1` VALUES (1080, 'РГ 350', '10', '37');
INSERT INTO `test1` VALUES (1081, 'РГ 342', '10', '98');
INSERT INTO `test1` VALUES (1082, 'РГ 343', '10', '74');
INSERT INTO `test1` VALUES (1083, 'РГ 344', '10', '54');
INSERT INTO `test1` VALUES (1084, 'РГ 366', '10', '74');
INSERT INTO `test1` VALUES (1085, 'РГ 367', '10', '50');
INSERT INTO `test1` VALUES (1086, 'РГ 368', '10', '37');
INSERT INTO `test1` VALUES (1087, 'РГ 369', '10', '36');
INSERT INTO `test1` VALUES (1088, 'РГ 370', '10', '21');
INSERT INTO `test1` VALUES (1089, 'РГ 351', '10', '160');
INSERT INTO `test1` VALUES (1090, 'РГ 352', '10', '109');
INSERT INTO `test1` VALUES (1091, 'РГ 353', '10', '73');
INSERT INTO `test1` VALUES (1092, 'РГ 354', '10', '73');
INSERT INTO `test1` VALUES (1093, 'РГ 355', '10', '53');
INSERT INTO `test1` VALUES (1094, 'РГ 356', '10', '177');
INSERT INTO `test1` VALUES (1095, 'РГ 357', '10', '118');
INSERT INTO `test1` VALUES (1096, 'РГ 358', '10', '80');
INSERT INTO `test1` VALUES (1097, 'РГ 359', '10', '78');
INSERT INTO `test1` VALUES (1098, 'РГ 360', '10', '58');
INSERT INTO `test1` VALUES (1099, 'РГ 361', '10', '76');
INSERT INTO `test1` VALUES (1100, 'РГ 362', '10', '48');
INSERT INTO `test1` VALUES (1101, 'РГ 363', '10', '36');
INSERT INTO `test1` VALUES (1102, 'РГ 365', '10', '21');
INSERT INTO `test1` VALUES (1103, 'РГ 406', '10', '39');
INSERT INTO `test1` VALUES (1104, 'РГ 407', '10', '33');
INSERT INTO `test1` VALUES (1105, 'РГ 408', '10', '23');
INSERT INTO `test1` VALUES (1106, 'РГ 400', '10', '59');
INSERT INTO `test1` VALUES (1107, 'РГ 401', '10', '48');
INSERT INTO `test1` VALUES (1108, 'РГ 402', '10', '29');
INSERT INTO `test1` VALUES (1109, 'РГ 403', '10', '59');
INSERT INTO `test1` VALUES (1110, 'РГ 404', '10', '48');
INSERT INTO `test1` VALUES (1111, 'РГ 405', '10', '29');
INSERT INTO `test1` VALUES (1112, 'РГ 233', '10', '313');
INSERT INTO `test1` VALUES (1113, 'РГ 234', '10', '222');
INSERT INTO `test1` VALUES (1114, 'РГ 155', '10', '35');
INSERT INTO `test1` VALUES (1115, 'РГ 157', '10', '35');
INSERT INTO `test1` VALUES (1116, 'РГ 158', '10', '35');
INSERT INTO `test1` VALUES (1117, 'РГ 156', '10', '35');
INSERT INTO `test1` VALUES (1118, 'РГ 159', '10', '38');
INSERT INTO `test1` VALUES (1119, 'РГ 162', '10', '38');
INSERT INTO `test1` VALUES (1120, 'РГ 161', '10', '38');
INSERT INTO `test1` VALUES (1121, 'РГ 160', '10', '38');
INSERT INTO `test1` VALUES (1122, 'РГ 147', '10', '111');
INSERT INTO `test1` VALUES (1123, 'РГ 151', '10', '111');
INSERT INTO `test1` VALUES (1124, 'РГ 149', '10', '111');
INSERT INTO `test1` VALUES (1125, 'РГ 153', '10', '111');
INSERT INTO `test1` VALUES (1126, 'РГ 148', '10', '80');
INSERT INTO `test1` VALUES (1127, 'РГ 152', '10', '80');
INSERT INTO `test1` VALUES (1128, 'РГ 150', '10', '80');
INSERT INTO `test1` VALUES (1129, 'РГ 154', '10', '80');
INSERT INTO `test1` VALUES (1130, 'РГ 145 ', '10', '141');
INSERT INTO `test1` VALUES (1131, 'РГ 146 ', '10', '141');
INSERT INTO `test1` VALUES (1132, 'РГ 163', '10', '59');
INSERT INTO `test1` VALUES (1133, 'РГ 164 ', '10', '59');
INSERT INTO `test1` VALUES (1134, 'РГ 44', '10', '210');
INSERT INTO `test1` VALUES (1135, 'РГ 167', '10', '287');
INSERT INTO `test1` VALUES (1136, 'РГ 68', '10', '235');
INSERT INTO `test1` VALUES (1137, 'РГ 62', '10', '96');
INSERT INTO `test1` VALUES (1138, 'РГ 175', '10', '45');
INSERT INTO `test1` VALUES (1139, 'РГ 43', '10', '202');
INSERT INTO `test1` VALUES (1140, 'РГ 166', '10', '281');
INSERT INTO `test1` VALUES (1141, 'РГ 67', '10', '248');
INSERT INTO `test1` VALUES (1142, 'РГ 61', '10', '101');
INSERT INTO `test1` VALUES (1143, 'РГ 174', '10', '42');
INSERT INTO `test1` VALUES (1144, 'РГ 45', '10', '199');
INSERT INTO `test1` VALUES (1145, 'РГ 168', '10', '279');
INSERT INTO `test1` VALUES (1146, 'РГ 69', '10', '241');
INSERT INTO `test1` VALUES (1147, 'РГ 63', '10', '101');
INSERT INTO `test1` VALUES (1148, 'РГ 176', '10', '43');
INSERT INTO `test1` VALUES (1149, 'РГ 46', '10', '214');
INSERT INTO `test1` VALUES (1150, 'РГ 169', '10', '289');
INSERT INTO `test1` VALUES (1151, 'РГ 70', '10', '238');
INSERT INTO `test1` VALUES (1152, 'РГ 64', '10', '99');
INSERT INTO `test1` VALUES (1153, 'РГ 177', '10', '44');
INSERT INTO `test1` VALUES (1154, 'РГ 48', '10', '102');
INSERT INTO `test1` VALUES (1155, 'РГ 171', '10', '120');
INSERT INTO `test1` VALUES (1156, 'РГ 72', '10', '94');
INSERT INTO `test1` VALUES (1157, 'РГ 66', '10', '45');
INSERT INTO `test1` VALUES (1158, 'РГ 179', '10', '34');
INSERT INTO `test1` VALUES (1159, 'РГ 47', '10', '80');
INSERT INTO `test1` VALUES (1160, 'РГ 170', '10', '92');
INSERT INTO `test1` VALUES (1161, 'РГ 71', '10', '84');
INSERT INTO `test1` VALUES (1162, 'РГ 65', '10', '53');
INSERT INTO `test1` VALUES (1163, 'РГ 178', '10', '35');
INSERT INTO `test1` VALUES (1164, 'РГ 137', '10', '1934');
INSERT INTO `test1` VALUES (1165, 'РГ 138', '10', '942');
INSERT INTO `test1` VALUES (1166, 'РГ 139', '10', '1934');
INSERT INTO `test1` VALUES (1167, 'РГ 140', '10', '942');
INSERT INTO `test1` VALUES (1168, 'РГ 141', '10', '2975');
INSERT INTO `test1` VALUES (1169, 'РГ 142', '10', '1041');
INSERT INTO `test1` VALUES (1170, 'РГ 143', '10', '2975');
INSERT INTO `test1` VALUES (1171, 'РГ 144', '10', '1041');
INSERT INTO `test1` VALUES (1172, 'РГ 180', '10', '347');
INSERT INTO `test1` VALUES (1173, 'РГ 181', '10', '292');
INSERT INTO `test1` VALUES (1174, 'РГ 182', '10', '262');
INSERT INTO `test1` VALUES (1175, 'РГ 183', '10', '230');
INSERT INTO `test1` VALUES (1176, 'РГ 184', '10', '454');
INSERT INTO `test1` VALUES (1177, 'РГ 185', '10', '418');
INSERT INTO `test1` VALUES (1178, 'РГ 186', '10', '244');
INSERT INTO `test1` VALUES (1179, 'РГ 187', '10', '205');
INSERT INTO `test1` VALUES (1180, 'РГ 188', '10', '146');
INSERT INTO `test1` VALUES (1181, 'РГ 189', '10', '121');
INSERT INTO `test1` VALUES (1182, 'РГ 190', '10', '74');
INSERT INTO `test1` VALUES (1183, 'РГ 191', '10', '51');
INSERT INTO `test1` VALUES (1184, 'РГ 235', '10', '244');
INSERT INTO `test1` VALUES (1185, 'РГ 236', '10', '196');
INSERT INTO `test1` VALUES (1186, 'РГ 237', '10', '125');
INSERT INTO `test1` VALUES (1187, 'РГ 238', '10', '79');
INSERT INTO `test1` VALUES (1188, 'РГ 268 ', '10', '176');
INSERT INTO `test1` VALUES (1189, 'РГ 269', '10', '179');
INSERT INTO `test1` VALUES (1190, 'РГ 270', '10', '135');
INSERT INTO `test1` VALUES (1191, 'РГ 271', '10', '137');
INSERT INTO `test1` VALUES (1192, 'РГ 272', '10', '113');
INSERT INTO `test1` VALUES (1193, 'РГ 273', '10', '117');
INSERT INTO `test1` VALUES (1194, 'РГ 274', '10', '84');
INSERT INTO `test1` VALUES (1195, 'РГ 275', '10', '86');
INSERT INTO `test1` VALUES (1196, 'РГ 374', '10', '151');
INSERT INTO `test1` VALUES (1197, 'РГ 375', '10', '143');
INSERT INTO `test1` VALUES (1198, 'РГ 376', '10', '78');
INSERT INTO `test1` VALUES (1199, 'РГ 377', '10', '103');
INSERT INTO `test1` VALUES (1200, 'РГ 378', '10', '93');
INSERT INTO `test1` VALUES (1201, 'РГ 379', '10', '47');
INSERT INTO `test1` VALUES (1202, 'РГ 380', '10', '83');
INSERT INTO `test1` VALUES (1203, 'РГ 381', '10', '77');
INSERT INTO `test1` VALUES (1204, 'РГ 382', '10', '40');
INSERT INTO `test1` VALUES (1205, 'РГ 371', '10', '139');
INSERT INTO `test1` VALUES (1206, 'РГ 372', '10', '132');
INSERT INTO `test1` VALUES (1207, 'РГ 373', '10', '73');
INSERT INTO `test1` VALUES (1208, 'РГ 383', '10', '137');
INSERT INTO `test1` VALUES (1209, 'РГ 384', '10', '129');
INSERT INTO `test1` VALUES (1210, 'РГ 385', '10', '71');
INSERT INTO `test1` VALUES (1211, 'РГ 392', '10', '100');
INSERT INTO `test1` VALUES (1212, 'РГ 394', '10', '39');
INSERT INTO `test1` VALUES (1213, 'РГ 386', '10', '176');
INSERT INTO `test1` VALUES (1214, 'РГ 387', '10', '138');
INSERT INTO `test1` VALUES (1215, 'РГ 388', '10', '69');
INSERT INTO `test1` VALUES (1216, 'РГ 389', '10', '126');
INSERT INTO `test1` VALUES (1217, 'РГ 390', '10', '108');
INSERT INTO `test1` VALUES (1218, 'РГ 391', '10', '48');
INSERT INTO `test1` VALUES (1219, 'РГ 395', '10', '158');
INSERT INTO `test1` VALUES (1220, 'РГ 396', '10', '124');
INSERT INTO `test1` VALUES (1221, 'РГ 397', '10', '61');
INSERT INTO `test1` VALUES (1222, 'РГ 398', '10', '242');
INSERT INTO `test1` VALUES (1223, 'РГ 399', '10', '193');
INSERT INTO `test1` VALUES (1224, 'КК-1', '11', '19');
INSERT INTO `test1` VALUES (1225, 'КК-2', '11', '30');
INSERT INTO `test1` VALUES (1226, 'КК-3', '11', '39');
INSERT INTO `test1` VALUES (1227, 'КК-4', '11', '19');
INSERT INTO `test1` VALUES (1228, 'КК-5', '11', '23');
INSERT INTO `test1` VALUES (1229, 'КК-6', '11', '26');
INSERT INTO `test1` VALUES (1230, 'КК-7', '11', '42');
INSERT INTO `test1` VALUES (1231, 'КК-8', '11', '55');
INSERT INTO `test1` VALUES (1232, 'НК-1', '11', '30');
INSERT INTO `test1` VALUES (1233, 'НК-2', '11', '31');
INSERT INTO `test1` VALUES (1234, 'НК-3', '11', '36');
INSERT INTO `test1` VALUES (1235, 'НК-4', '11', '30');
INSERT INTO `test1` VALUES (1236, 'ПК-1', '11', '22');
INSERT INTO `test1` VALUES (1237, 'ПК-2', '11', '23');
INSERT INTO `test1` VALUES (1238, 'ПК-3', '11', '19');
INSERT INTO `test1` VALUES (1239, 'ПК-17', '11', '19');
INSERT INTO `test1` VALUES (1240, 'ПК-4', '11', '22');
INSERT INTO `test1` VALUES (1241, 'ПК-5', '11', '23');
INSERT INTO `test1` VALUES (1242, 'ПК-6', '11', '24');
INSERT INTO `test1` VALUES (1243, 'ПК-18', '11', '29');
INSERT INTO `test1` VALUES (1244, 'ПК-7', '11', '31');
INSERT INTO `test1` VALUES (1245, 'ПК-8', '11', '33');
INSERT INTO `test1` VALUES (1246, 'ПК-9', '11', '34');
INSERT INTO `test1` VALUES (1247, 'ПК-19', '11', '31');
INSERT INTO `test1` VALUES (1248, 'ПК-10', '11', '21');
INSERT INTO `test1` VALUES (1249, 'ПК-11', '11', '22');
INSERT INTO `test1` VALUES (1250, 'ПК-12', '11', '23');
INSERT INTO `test1` VALUES (1251, 'ПК-20', '11', '29');
INSERT INTO `test1` VALUES (1252, 'ПК-13', '11', '32');
INSERT INTO `test1` VALUES (1253, 'ПК-14', '11', '34');
INSERT INTO `test1` VALUES (1254, 'ПК-15', '11', '39');
INSERT INTO `test1` VALUES (1255, 'ПК-16', '11', '41');
INSERT INTO `test1` VALUES (1256, 'РК-522', '11', '19');
INSERT INTO `test1` VALUES (1257, 'РК-523', '11', '26');
INSERT INTO `test1` VALUES (1258, 'РК-524', '11', '14');
INSERT INTO `test1` VALUES (1259, 'РК-525', '11', '28');
INSERT INTO `test1` VALUES (1260, 'РК-526', '11', '38');
INSERT INTO `test1` VALUES (1261, 'РК-527', '11', '22');
INSERT INTO `test1` VALUES (1262, 'РК-528', '11', '16');
INSERT INTO `test1` VALUES (1263, 'РК-529', '11', '20');
INSERT INTO `test1` VALUES (1264, 'РК-530', '11', '10');
INSERT INTO `test1` VALUES (1265, 'РК-531', '11', '16');
INSERT INTO `test1` VALUES (1266, 'РК-532', '11', '20');
INSERT INTO `test1` VALUES (1267, 'РК-533', '11', '10');
INSERT INTO `test1` VALUES (1268, 'РК-534', '11', '23');
INSERT INTO `test1` VALUES (1269, 'РК-535', '11', '9');
INSERT INTO `test1` VALUES (1270, 'РК-536', '11', '16');
INSERT INTO `test1` VALUES (1271, 'РК-537', '11', '22');
INSERT INTO `test1` VALUES (1272, 'РК-538', '11', '9');
INSERT INTO `test1` VALUES (1273, 'РК-539', '11', '22');
INSERT INTO `test1` VALUES (1274, 'РК-540', '11', '30');
INSERT INTO `test1` VALUES (1275, 'РК-541', '11', '14');
INSERT INTO `test1` VALUES (1276, 'РК-581', '11', '19');
INSERT INTO `test1` VALUES (1277, 'РК-582', '11', '13');
INSERT INTO `test1` VALUES (1278, 'РК-583', '11', '28');
INSERT INTO `test1` VALUES (1279, 'РК-584', '11', '19');
INSERT INTO `test1` VALUES (1280, 'РК-585', '11', '18');
INSERT INTO `test1` VALUES (1281, 'РК-586', '11', '13');
INSERT INTO `test1` VALUES (1282, 'РК-587', '11', '12');
INSERT INTO `test1` VALUES (1283, 'РК-588', '11', '24');
INSERT INTO `test1` VALUES (1284, 'РК-590', '11', '12');
INSERT INTO `test1` VALUES (1285, 'РК-589', '11', '14');
INSERT INTO `test1` VALUES (1286, 'РК-591', '11', '20');
INSERT INTO `test1` VALUES (1287, 'РК-592', '11', '33');
INSERT INTO `test1` VALUES (1288, 'РК-594', '11', '20');
INSERT INTO `test1` VALUES (1289, 'РК-593', '11', '20');
INSERT INTO `test1` VALUES (1290, 'РК-595', '11', '12');
INSERT INTO `test1` VALUES (1291, 'РК-596', '11', '24');
INSERT INTO `test1` VALUES (1292, 'РК-598', '11', '12');
INSERT INTO `test1` VALUES (1293, 'РК-597', '11', '14');
INSERT INTO `test1` VALUES (1294, 'РК-599', '11', '13');
INSERT INTO `test1` VALUES (1295, 'РК-600', '11', '24');
INSERT INTO `test1` VALUES (1296, 'РК-602', '11', '13');
INSERT INTO `test1` VALUES (1297, 'РК-601', '11', '14');
INSERT INTO `test1` VALUES (1298, 'РК-603', '11', '13');
INSERT INTO `test1` VALUES (1299, 'РК-604', '11', '24');
INSERT INTO `test1` VALUES (1300, 'РК-606', '11', '13');
INSERT INTO `test1` VALUES (1301, 'РК-605', '11', '14');
INSERT INTO `test1` VALUES (1302, 'РК-607', '11', '22');
INSERT INTO `test1` VALUES (1303, 'РК-608', '11', '16');
INSERT INTO `test1` VALUES (1304, 'РК-609', '11', '11');
INSERT INTO `test1` VALUES (1305, 'РК-610', '11', '22');
INSERT INTO `test1` VALUES (1306, 'РК-611', '11', '16');
INSERT INTO `test1` VALUES (1307, 'РК-612', '11', '11');
INSERT INTO `test1` VALUES (1308, 'РК-613', '11', '22');
INSERT INTO `test1` VALUES (1309, 'РК-614', '11', '16');
INSERT INTO `test1` VALUES (1310, 'РК-615', '11', '12');
INSERT INTO `test1` VALUES (1311, 'РК-616', '11', '22');
INSERT INTO `test1` VALUES (1312, 'РК-617', '11', '16');
INSERT INTO `test1` VALUES (1313, 'РК-618', '11', '12');
INSERT INTO `test1` VALUES (1314, 'РК-619', '11', '17');
INSERT INTO `test1` VALUES (1315, 'РК-620', '11', '12');
INSERT INTO `test1` VALUES (1316, 'РК-621', '11', '9');
INSERT INTO `test1` VALUES (1317, 'РК-622', '11', '27');
INSERT INTO `test1` VALUES (1318, 'РК-623', '11', '20');
INSERT INTO `test1` VALUES (1319, 'РК-624', '11', '14');
INSERT INTO `test1` VALUES (1320, 'РК-226', '11', '12');
INSERT INTO `test1` VALUES (1321, 'РК-227', '11', '9');
INSERT INTO `test1` VALUES (1322, 'РК-228', '11', '19');
INSERT INTO `test1` VALUES (1323, 'РК-229', '11', '19');
INSERT INTO `test1` VALUES (1324, 'РК-295', '11', '16');
INSERT INTO `test1` VALUES (1325, 'РК-296', '11', '20');
INSERT INTO `test1` VALUES (1326, 'РК-297', '11', '24');
INSERT INTO `test1` VALUES (1327, 'РК-289', '11', '9');
INSERT INTO `test1` VALUES (1328, 'РК-290', '11', '8');
INSERT INTO `test1` VALUES (1329, 'РК-291', '11', '8');
INSERT INTO `test1` VALUES (1330, 'РК-292', '11', '8');
INSERT INTO `test1` VALUES (1331, 'РК-293', '11', '12');
INSERT INTO `test1` VALUES (1332, 'РК-294', '11', '17');
INSERT INTO `test1` VALUES (1333, 'РК-160', '11', '16');
INSERT INTO `test1` VALUES (1334, 'РК-163', '11', '18');
INSERT INTO `test1` VALUES (1335, 'РК-164', '11', '8');
INSERT INTO `test1` VALUES (1336, 'РК-273', '11', '6');
INSERT INTO `test1` VALUES (1337, 'РК-274', '11', '9');
INSERT INTO `test1` VALUES (1338, 'РК-275', '11', '16');
INSERT INTO `test1` VALUES (1339, 'РК-335', '11', '9');
INSERT INTO `test1` VALUES (1340, 'РК-276', '11', '27');
INSERT INTO `test1` VALUES (1341, 'РК-277', '11', '66');
INSERT INTO `test1` VALUES (1342, 'РК-338', '11', '29');
INSERT INTO `test1` VALUES (1343, 'РК-339', '11', '52');
INSERT INTO `test1` VALUES (1344, 'РК-348', '11', '69');
INSERT INTO `test1` VALUES (1345, 'РК-278', '11', '8');
INSERT INTO `test1` VALUES (1346, 'РК-279', '11', '9');
INSERT INTO `test1` VALUES (1347, 'РК-280', '11', '13');
INSERT INTO `test1` VALUES (1348, 'РК-281', '11', '13');
INSERT INTO `test1` VALUES (1349, 'РК-282', '11', '17');
INSERT INTO `test1` VALUES (1350, 'РК-283', '11', '17');
INSERT INTO `test1` VALUES (1351, 'РК-284', '11', '13');
INSERT INTO `test1` VALUES (1352, 'РК-285', '11', '19');
INSERT INTO `test1` VALUES (1353, 'РК-173', '11', '10');
INSERT INTO `test1` VALUES (1354, 'РК-174', '11', '13');
INSERT INTO `test1` VALUES (1355, 'РК-175', '11', '13');
INSERT INTO `test1` VALUES (1356, 'РК-176', '11', '14');
INSERT INTO `test1` VALUES (1357, 'РК-309', '11', '7');
INSERT INTO `test1` VALUES (1358, 'РК-310', '11', '10');
INSERT INTO `test1` VALUES (1359, 'РК-311', '11', '12');
INSERT INTO `test1` VALUES (1360, 'РК-312', '11', '14');
INSERT INTO `test1` VALUES (1361, 'РК-177', '11', '13');
INSERT INTO `test1` VALUES (1362, 'РК-178', '11', '19');
INSERT INTO `test1` VALUES (1363, 'РК-179', '11', '23');
INSERT INTO `test1` VALUES (1364, 'РК-180', '11', '28');
INSERT INTO `test1` VALUES (1365, 'РК-313', '11', '9');
INSERT INTO `test1` VALUES (1366, 'РК-314', '11', '17');
INSERT INTO `test1` VALUES (1367, 'РК-315', '11', '18');
INSERT INTO `test1` VALUES (1368, 'РК-316', '11', '25');
INSERT INTO `test1` VALUES (1369, 'РК-181', '11', '10');
INSERT INTO `test1` VALUES (1370, 'РК-317', '11', '0');
INSERT INTO `test1` VALUES (1371, 'РК-182', '11', '14');
INSERT INTO `test1` VALUES (1372, 'РК-318', '11', '14');
INSERT INTO `test1` VALUES (1373, 'РК-434', '11', '12');
INSERT INTO `test1` VALUES (1374, 'РК-435', '11', '15');
INSERT INTO `test1` VALUES (1375, 'РК-436', '11', '11');
INSERT INTO `test1` VALUES (1376, 'РК-437', '11', '13');
INSERT INTO `test1` VALUES (1377, 'РК-517', '11', '15');
INSERT INTO `test1` VALUES (1378, 'РК-518', '11', '10');
INSERT INTO `test1` VALUES (1379, 'РК-519', '11', '7');
INSERT INTO `test1` VALUES (1380, 'РК-490', '11', '19');
INSERT INTO `test1` VALUES (1381, 'РК-491', '11', '18');
INSERT INTO `test1` VALUES (1382, 'РК-492', '11', '12');
INSERT INTO `test1` VALUES (1383, 'РК-183', '11', '17');
INSERT INTO `test1` VALUES (1384, 'РК-184', '11', '12');
INSERT INTO `test1` VALUES (1385, 'РК-185', '11', '22');
INSERT INTO `test1` VALUES (1386, 'РК-186', '11', '26');
INSERT INTO `test1` VALUES (1387, 'РК-187', '11', '21');
INSERT INTO `test1` VALUES (1388, 'РК-188', '11', '30');
INSERT INTO `test1` VALUES (1389, 'РК-189', '11', '23');
INSERT INTO `test1` VALUES (1390, 'РК-190', '11', '18');
INSERT INTO `test1` VALUES (1391, 'РК-191', '11', '28');
INSERT INTO `test1` VALUES (1392, 'РК-286', '11', '31');
INSERT INTO `test1` VALUES (1393, 'РК-287', '11', '22');
INSERT INTO `test1` VALUES (1394, 'РК-288', '11', '37');
INSERT INTO `test1` VALUES (1395, 'РК-625', '11', '44');
INSERT INTO `test1` VALUES (1396, 'РК-626', '11', '86');
INSERT INTO `test1` VALUES (1397, 'РК-627', '11', '58');
INSERT INTO `test1` VALUES (1398, 'РК-628', '11', '119');
INSERT INTO `test1` VALUES (1399, 'РК-629', '11', '17');
INSERT INTO `test1` VALUES (1400, 'РК-630', '11', '30');
INSERT INTO `test1` VALUES (1401, 'РК-631', '11', '25');
INSERT INTO `test1` VALUES (1402, 'РК-632', '11', '41');
INSERT INTO `test1` VALUES (1403, 'РК-152', '11', '11');
INSERT INTO `test1` VALUES (1404, 'РК-153', '11', '12');
INSERT INTO `test1` VALUES (1405, 'РК-192', '11', '26');
INSERT INTO `test1` VALUES (1406, 'РК-298', '11', '15');
INSERT INTO `test1` VALUES (1407, 'РК-299', '11', '10');
INSERT INTO `test1` VALUES (1408, 'РК-300', '11', '16');
INSERT INTO `test1` VALUES (1409, 'РК-301', '11', '21');
INSERT INTO `test1` VALUES (1410, 'РК-302', '11', '13');
INSERT INTO `test1` VALUES (1411, 'РК-319', '11', '11');
INSERT INTO `test1` VALUES (1412, 'РК-320', '11', '16');
INSERT INTO `test1` VALUES (1413, 'РК-322', '11', '21');
INSERT INTO `test1` VALUES (1414, 'РК-323', '11', '11');
INSERT INTO `test1` VALUES (1415, 'РК-324', '11', '8');
INSERT INTO `test1` VALUES (1416, 'РК-328', '11', '17');
INSERT INTO `test1` VALUES (1417, 'РК-329', '11', '16');
INSERT INTO `test1` VALUES (1418, 'РК-330', '11', '13');
INSERT INTO `test1` VALUES (1419, 'РК-331', '11', '15');
INSERT INTO `test1` VALUES (1420, 'РК-332', '11', '17');
INSERT INTO `test1` VALUES (1421, 'РК-321', '11', '23');
INSERT INTO `test1` VALUES (1422, 'РК-333', '11', '16');
INSERT INTO `test1` VALUES (1423, 'РК-373', '11', '19');
INSERT INTO `test1` VALUES (1424, 'РК-334', '11', '7');
INSERT INTO `test1` VALUES (1425, 'РК-346', '11', '8');
INSERT INTO `test1` VALUES (1426, 'РК-422', '11', '22');
INSERT INTO `test1` VALUES (1427, 'РК-423', '11', '26');
INSERT INTO `test1` VALUES (1428, 'РК-424', '11', '26');
INSERT INTO `test1` VALUES (1429, 'РК-425', '11', '25');
INSERT INTO `test1` VALUES (1430, 'РК-426', '11', '22');
INSERT INTO `test1` VALUES (1431, 'РК-427', '11', '22');
INSERT INTO `test1` VALUES (1432, 'РК-428', '11', '20');
INSERT INTO `test1` VALUES (1433, 'РК-429', '11', '14');
INSERT INTO `test1` VALUES (1434, 'РК-430', '11', '21');
INSERT INTO `test1` VALUES (1435, 'РК-431', '11', '18');
INSERT INTO `test1` VALUES (1436, 'РК-432', '11', '22');
INSERT INTO `test1` VALUES (1437, 'РК-433', '11', '30');
INSERT INTO `test1` VALUES (1438, 'РК-154', '11', '11');
INSERT INTO `test1` VALUES (1439, 'РК-155', '11', '25');
INSERT INTO `test1` VALUES (1440, 'РК-156', '11', '13');
INSERT INTO `test1` VALUES (1441, 'РК-157', '11', '25');
INSERT INTO `test1` VALUES (1442, 'РК-158', '11', '11');
INSERT INTO `test1` VALUES (1443, 'РК-159', '11', '25');
INSERT INTO `test1` VALUES (1444, 'РК-236', '11', '7');
INSERT INTO `test1` VALUES (1445, 'РК-237', '11', '5');
INSERT INTO `test1` VALUES (1446, 'РК-238', '11', '17');
INSERT INTO `test1` VALUES (1447, 'РК-239', '11', '16');
INSERT INTO `test1` VALUES (1448, 'РК-240', '11', '3');
INSERT INTO `test1` VALUES (1449, 'РК-241', '11', '6');
INSERT INTO `test1` VALUES (1450, 'РК-242', '11', '10');
INSERT INTO `test1` VALUES (1451, 'РК-243', '11', '11');
INSERT INTO `test1` VALUES (1452, 'РК-303', '11', '7');
INSERT INTO `test1` VALUES (1453, 'РК-304', '11', '8');
INSERT INTO `test1` VALUES (1454, 'РК-305', '11', '5');
INSERT INTO `test1` VALUES (1455, 'РК-306', '11', '3');
INSERT INTO `test1` VALUES (1456, 'РК-307', '11', '6');
INSERT INTO `test1` VALUES (1457, 'РК-308', '11', '8');
INSERT INTO `test1` VALUES (1458, 'РК-361', '11', '3');
INSERT INTO `test1` VALUES (1459, 'РК-362', '11', '5');
INSERT INTO `test1` VALUES (1460, 'РК-325', '11', '15');
INSERT INTO `test1` VALUES (1461, 'РК-326', '11', '21');
INSERT INTO `test1` VALUES (1462, 'РК-327', '11', '30');
INSERT INTO `test1` VALUES (1463, 'РК-255', '11', '19');
INSERT INTO `test1` VALUES (1464, 'РК-256', '11', '36');
INSERT INTO `test1` VALUES (1465, 'РК-257', '11', '49');
INSERT INTO `test1` VALUES (1466, 'РК-161', '11', '9');
INSERT INTO `test1` VALUES (1467, 'РК-162', '11', '11');
INSERT INTO `test1` VALUES (1468, 'РК-263', '11', '12');
INSERT INTO `test1` VALUES (1469, 'РК-264', '11', '13');
INSERT INTO `test1` VALUES (1470, 'РК-265', '11', '21');
INSERT INTO `test1` VALUES (1471, 'РК-166', '11', '18');
INSERT INTO `test1` VALUES (1472, 'РК-165', '11', '34');
INSERT INTO `test1` VALUES (1473, 'РК-169', '11', '15');
INSERT INTO `test1` VALUES (1474, 'РК-168', '11', '21');
INSERT INTO `test1` VALUES (1475, 'РК-167', '11', '39');
INSERT INTO `test1` VALUES (1476, 'РК-170', '11', '39');
INSERT INTO `test1` VALUES (1477, 'РК-171', '11', '44');
INSERT INTO `test1` VALUES (1478, 'РК-172', '11', '51');
INSERT INTO `test1` VALUES (1479, 'РК-144', '11', '26');
INSERT INTO `test1` VALUES (1480, 'РК-145', '11', '33');
INSERT INTO `test1` VALUES (1481, 'РК-146', '11', '22');
INSERT INTO `test1` VALUES (1482, 'РК-147', '11', '11');
INSERT INTO `test1` VALUES (1483, 'РК-150', '11', '24');
INSERT INTO `test1` VALUES (1484, 'РК-151', '11', '25');
INSERT INTO `test1` VALUES (1485, 'РК-363 ножка ', '11', '5');
INSERT INTO `test1` VALUES (1486, 'РК-364 ножка', '11', '6');
INSERT INTO `test1` VALUES (1487, 'РК-365 ножка', '11', '5');
INSERT INTO `test1` VALUES (1488, 'РК-366 ножка', '11', '4');
INSERT INTO `test1` VALUES (1489, 'РК-142', '11', '19');
INSERT INTO `test1` VALUES (1490, 'РК-141', '11', '9');
INSERT INTO `test1` VALUES (1491, 'РК-143', '11', '12');
INSERT INTO `test1` VALUES (1492, 'РК-230', '11', '10');
INSERT INTO `test1` VALUES (1493, 'РК-231', '11', '10');
INSERT INTO `test1` VALUES (1494, 'РК-232', '11', '18');
INSERT INTO `test1` VALUES (1495, 'РК-233', '11', '20');
INSERT INTO `test1` VALUES (1496, 'РК-234', '11', '21');
INSERT INTO `test1` VALUES (1497, 'РК-235', '11', '23');
INSERT INTO `test1` VALUES (1498, 'РК-357', '11', '16');
INSERT INTO `test1` VALUES (1499, 'РК-358', '11', '21');
INSERT INTO `test1` VALUES (1500, 'РК-359', '11', '12');
INSERT INTO `test1` VALUES (1501, 'РК-360', '11', '28');
INSERT INTO `test1` VALUES (1502, 'РК-482', '11', '11');
INSERT INTO `test1` VALUES (1503, 'РК-483', '11', '12');
INSERT INTO `test1` VALUES (1504, 'РК-484', '11', '14');
INSERT INTO `test1` VALUES (1505, 'РК-485', '11', '14');
INSERT INTO `test1` VALUES (1506, 'РК-486', '11', '20');
INSERT INTO `test1` VALUES (1507, 'РК-487', '11', '20');
INSERT INTO `test1` VALUES (1508, 'РК-488', '11', '18');
INSERT INTO `test1` VALUES (1509, 'РК-489', '11', '19');
INSERT INTO `test1` VALUES (1510, 'РК-1', '12', '10');
INSERT INTO `test1` VALUES (1511, 'РК-2', '12', '10');
INSERT INTO `test1` VALUES (1512, 'РК-38', '12', '26');
INSERT INTO `test1` VALUES (1513, 'РК-39', '12', '40');
INSERT INTO `test1` VALUES (1514, 'РК-40', '12', '40');
INSERT INTO `test1` VALUES (1515, 'РК-41', '12', '11');
INSERT INTO `test1` VALUES (1516, 'РК-42', '12', '14');
INSERT INTO `test1` VALUES (1517, 'РК-43', '12', '21');
INSERT INTO `test1` VALUES (1518, 'РК-244', '12', '19');
INSERT INTO `test1` VALUES (1519, 'РК-245', '12', '22');
INSERT INTO `test1` VALUES (1520, 'РК-69', '12', '45');
INSERT INTO `test1` VALUES (1521, 'РК-70', '12', '39');
INSERT INTO `test1` VALUES (1522, 'РК-71', '12', '62');
INSERT INTO `test1` VALUES (1523, 'РК-72', '12', '48');
INSERT INTO `test1` VALUES (1524, 'РК-73', '12', '37');
INSERT INTO `test1` VALUES (1525, 'РК-74', '12', '36');
INSERT INTO `test1` VALUES (1526, 'РК-75', '12', '32');
INSERT INTO `test1` VALUES (1527, 'РК-76', '12', '33');
INSERT INTO `test1` VALUES (1528, 'РК-77', '12', '18');
INSERT INTO `test1` VALUES (1529, 'РК-78', '12', '18');
INSERT INTO `test1` VALUES (1530, 'РК-79', '12', '16');
INSERT INTO `test1` VALUES (1531, 'РК-80', '12', '38');
INSERT INTO `test1` VALUES (1532, 'РК-81', '12', '37');
INSERT INTO `test1` VALUES (1533, 'РК-82', '12', '33');
INSERT INTO `test1` VALUES (1534, 'РК-100', '12', '24');
INSERT INTO `test1` VALUES (1535, 'РК-101', '12', '44');
INSERT INTO `test1` VALUES (1536, 'РК-15', '12', '16');
INSERT INTO `test1` VALUES (1537, 'РК-16', '12', '16');
INSERT INTO `test1` VALUES (1538, 'РК-17', '12', '20');
INSERT INTO `test1` VALUES (1539, 'РК-18', '12', '25');
INSERT INTO `test1` VALUES (1540, 'РК-19', '12', '26');
INSERT INTO `test1` VALUES (1541, 'РК-20', '12', '15');
INSERT INTO `test1` VALUES (1542, 'РК-21', '12', '15');
INSERT INTO `test1` VALUES (1543, 'РК-22', '12', '19');
INSERT INTO `test1` VALUES (1544, 'РК-23', '12', '20');
INSERT INTO `test1` VALUES (1545, 'РК-24', '12', '25');
INSERT INTO `test1` VALUES (1546, 'РК-25', '12', '14');
INSERT INTO `test1` VALUES (1547, 'РК-26', '12', '14');
INSERT INTO `test1` VALUES (1548, 'РК-27', '12', '15');
INSERT INTO `test1` VALUES (1549, 'РК-28', '12', '19');
INSERT INTO `test1` VALUES (1550, 'РК-29', '12', '20');
INSERT INTO `test1` VALUES (1551, 'РК-93', '12', '36');
INSERT INTO `test1` VALUES (1552, 'РК-94', '12', '35');
INSERT INTO `test1` VALUES (1553, 'РК-95', '12', '59');
INSERT INTO `test1` VALUES (1554, 'РК-96', '12', '55');
INSERT INTO `test1` VALUES (1555, 'РК-85', '12', '14');
INSERT INTO `test1` VALUES (1556, 'РК-86', '12', '9');
INSERT INTO `test1` VALUES (1557, 'РК-87', '12', '4');
INSERT INTO `test1` VALUES (1558, 'РК-88', '12', '6');
INSERT INTO `test1` VALUES (1559, 'РК-347 профиль, 3м', '12', '96');
INSERT INTO `test1` VALUES (1560, 'РК-352', '12', '10');
INSERT INTO `test1` VALUES (1561, 'РК-353', '12', '12');
INSERT INTO `test1` VALUES (1562, 'РК-354', '12', '12');
INSERT INTO `test1` VALUES (1563, 'РК-355', '12', '12');
INSERT INTO `test1` VALUES (1564, 'РК-369', '12', '11');
INSERT INTO `test1` VALUES (1565, 'РК-370', '12', '16');
INSERT INTO `test1` VALUES (1566, 'РК-371', '12', '18');
INSERT INTO `test1` VALUES (1567, 'РК-372', '12', '19');
INSERT INTO `test1` VALUES (1568, 'РК-374', '12', '6');
INSERT INTO `test1` VALUES (1569, 'РК-375', '12', '9');
INSERT INTO `test1` VALUES (1570, 'РК-376', '12', '6');
INSERT INTO `test1` VALUES (1571, 'РК-377', '12', '8');
INSERT INTO `test1` VALUES (1572, 'РК-378', '12', '15');
INSERT INTO `test1` VALUES (1573, 'РК-379', '12', '17');
INSERT INTO `test1` VALUES (1574, 'РК-380', '12', '19');
INSERT INTO `test1` VALUES (1575, 'РК-381', '12', '7');
INSERT INTO `test1` VALUES (1576, 'РК-382', '12', '8');
INSERT INTO `test1` VALUES (1577, 'РК-383', '12', '14');
INSERT INTO `test1` VALUES (1578, 'РК-384', '12', '23');
INSERT INTO `test1` VALUES (1579, 'РК-385', '12', '24');
INSERT INTO `test1` VALUES (1580, 'РК-386', '12', '7');
INSERT INTO `test1` VALUES (1581, 'РК-387', '12', '9');
INSERT INTO `test1` VALUES (1582, 'РК-388', '12', '15');
INSERT INTO `test1` VALUES (1583, 'РК-389', '12', '21');
INSERT INTO `test1` VALUES (1584, 'РК-390', '12', '25');
INSERT INTO `test1` VALUES (1585, 'РК-391', '12', '4');
INSERT INTO `test1` VALUES (1586, 'РК-392', '12', '5');
INSERT INTO `test1` VALUES (1587, 'РК-393', '12', '6');
INSERT INTO `test1` VALUES (1588, 'РК-394', '12', '11');
INSERT INTO `test1` VALUES (1589, 'РК-395', '12', '15');
INSERT INTO `test1` VALUES (1590, 'РК-396', '12', '21');
INSERT INTO `test1` VALUES (1591, 'РК-397', '12', '4');
INSERT INTO `test1` VALUES (1592, 'РК-398', '12', '5');
INSERT INTO `test1` VALUES (1593, 'РК-399', '12', '6');
INSERT INTO `test1` VALUES (1594, 'РК-400', '12', '11');
INSERT INTO `test1` VALUES (1595, 'РК-401', '12', '15');
INSERT INTO `test1` VALUES (1596, 'РК-402', '12', '19');
INSERT INTO `test1` VALUES (1597, 'РК-403', '12', '4');
INSERT INTO `test1` VALUES (1598, 'РК-404', '12', '5');
INSERT INTO `test1` VALUES (1599, 'РК-405', '12', '7');
INSERT INTO `test1` VALUES (1600, 'РК-406', '12', '16');
INSERT INTO `test1` VALUES (1601, 'РК-407', '12', '18');
INSERT INTO `test1` VALUES (1602, 'РК-408', '12', '23');
INSERT INTO `test1` VALUES (1603, 'РК-409', '12', '4');
INSERT INTO `test1` VALUES (1604, 'РК-410', '12', '7');
INSERT INTO `test1` VALUES (1605, 'РК-411', '12', '8');
INSERT INTO `test1` VALUES (1606, 'РК-412', '12', '14');
INSERT INTO `test1` VALUES (1607, 'РК-413', '12', '20');
INSERT INTO `test1` VALUES (1608, 'РК-414', '12', '24');
INSERT INTO `test1` VALUES (1609, 'РК-421', '12', '8');
INSERT INTO `test1` VALUES (1610, 'РК-415', '12', '5');
INSERT INTO `test1` VALUES (1611, 'РК-416', '12', '7');
INSERT INTO `test1` VALUES (1612, 'РК-417', '12', '5');
INSERT INTO `test1` VALUES (1613, 'РК-418', '12', '4');
INSERT INTO `test1` VALUES (1614, 'РК-419', '12', '5');
INSERT INTO `test1` VALUES (1615, 'РК-420', '12', '5');
INSERT INTO `test1` VALUES (1616, 'РК-193', '12', '22');
INSERT INTO `test1` VALUES (1617, 'РК-194', '12', '24');
INSERT INTO `test1` VALUES (1618, 'РК-195', '12', '24');
INSERT INTO `test1` VALUES (1619, 'РК-196', '12', '33');
INSERT INTO `test1` VALUES (1620, 'РК-197', '12', '19');
INSERT INTO `test1` VALUES (1621, 'РК-198', '12', '17');
INSERT INTO `test1` VALUES (1622, 'РК-199', '12', '19');
INSERT INTO `test1` VALUES (1623, 'РК-200', '12', '17');
INSERT INTO `test1` VALUES (1624, 'РК-201', '12', '13');
INSERT INTO `test1` VALUES (1625, 'РК-202', '12', '15');
INSERT INTO `test1` VALUES (1626, 'РК-203', '12', '18');
INSERT INTO `test1` VALUES (1627, 'РК-204', '12', '22');
INSERT INTO `test1` VALUES (1628, 'РК-205', '12', '31');
INSERT INTO `test1` VALUES (1629, 'РК-206', '12', '13');
INSERT INTO `test1` VALUES (1630, 'РК-207', '12', '15');
INSERT INTO `test1` VALUES (1631, 'РК-208', '12', '18');
INSERT INTO `test1` VALUES (1632, 'РК-209', '12', '22');
INSERT INTO `test1` VALUES (1633, 'РК-367', '12', '53');
INSERT INTO `test1` VALUES (1634, 'РК-368', '12', '24');
INSERT INTO `test1` VALUES (1635, 'РК-210', '12', '25');
INSERT INTO `test1` VALUES (1636, 'РК-211', '12', '48');
INSERT INTO `test1` VALUES (1637, 'РК-212', '12', '23');
INSERT INTO `test1` VALUES (1638, 'РК-213', '12', '45');
INSERT INTO `test1` VALUES (1639, 'РК-214', '12', '31');
INSERT INTO `test1` VALUES (1640, 'РК-215', '12', '49');
INSERT INTO `test1` VALUES (1641, 'РК-216', '12', '28');
INSERT INTO `test1` VALUES (1642, 'РК-217', '12', '52');
INSERT INTO `test1` VALUES (1643, 'РК-258', '12', '19');
INSERT INTO `test1` VALUES (1644, 'РК-259', '12', '19');
INSERT INTO `test1` VALUES (1645, 'РК-260', '12', '20');
INSERT INTO `test1` VALUES (1646, 'РК-261', '12', '20');
INSERT INTO `test1` VALUES (1647, 'РК-262', '12', '25');
INSERT INTO `test1` VALUES (1648, 'РК-11', '12', '17');
INSERT INTO `test1` VALUES (1649, 'РК-12', '12', '19');
INSERT INTO `test1` VALUES (1650, 'РК-13', '12', '19');
INSERT INTO `test1` VALUES (1651, 'РК-14', '12', '23');
INSERT INTO `test1` VALUES (1652, 'РК-336', '12', '70');
INSERT INTO `test1` VALUES (1653, 'РК-337', '12', '109');
INSERT INTO `test1` VALUES (1654, 'РК-474', '12', '41');
INSERT INTO `test1` VALUES (1655, 'РК-475', '12', '39');
INSERT INTO `test1` VALUES (1656, 'РК-476', '12', '40');
INSERT INTO `test1` VALUES (1657, 'РК-477', '12', '40');
INSERT INTO `test1` VALUES (1658, 'РК-266', '12', '17');
INSERT INTO `test1` VALUES (1659, 'РК-267', '12', '28');
INSERT INTO `test1` VALUES (1660, 'РК-108', '12', '36');
INSERT INTO `test1` VALUES (1661, 'РК-109', '12', '8');
INSERT INTO `test1` VALUES (1662, 'РК-3', '12', '18');
INSERT INTO `test1` VALUES (1663, 'РК-83', '12', '53');
INSERT INTO `test1` VALUES (1664, 'РК-84', '12', '56');
INSERT INTO `test1` VALUES (1665, 'РК-98', '12', '62');
INSERT INTO `test1` VALUES (1666, 'РК-99', '12', '62');
INSERT INTO `test1` VALUES (1667, 'РК-110', '12', '20');
INSERT INTO `test1` VALUES (1668, 'РК-30', '12', '24');
INSERT INTO `test1` VALUES (1669, 'РК-31', '12', '23');
INSERT INTO `test1` VALUES (1670, 'РК-32', '12', '21');
INSERT INTO `test1` VALUES (1671, 'РК-91', '12', '41');
INSERT INTO `test1` VALUES (1672, 'РК-92', '12', '40');
INSERT INTO `test1` VALUES (1673, 'РК-53', '12', '11');
INSERT INTO `test1` VALUES (1674, 'РК-54', '12', '14');
INSERT INTO `test1` VALUES (1675, 'РК-97', '12', '46');
INSERT INTO `test1` VALUES (1676, 'РК-111', '12', '17');
INSERT INTO `test1` VALUES (1677, 'РК-112', '12', '22');
INSERT INTO `test1` VALUES (1678, 'РК-50', '12', '45');
INSERT INTO `test1` VALUES (1679, 'РК-51', '12', '44');
INSERT INTO `test1` VALUES (1680, 'РК-52', '12', '37');
INSERT INTO `test1` VALUES (1681, 'РК-59', '12', '25');
INSERT INTO `test1` VALUES (1682, 'РК-60', '12', '28');
INSERT INTO `test1` VALUES (1683, 'РК-55', '12', '9');
INSERT INTO `test1` VALUES (1684, 'РК-56', '12', '11');
INSERT INTO `test1` VALUES (1685, 'РК-57', '12', '13');
INSERT INTO `test1` VALUES (1686, 'РК-58', '12', '17');
INSERT INTO `test1` VALUES (1687, 'РК-102', '12', '49');
INSERT INTO `test1` VALUES (1688, 'РК-103', '12', '41');
INSERT INTO `test1` VALUES (1689, 'РК-7', '12', '14');
INSERT INTO `test1` VALUES (1690, 'РК-8', '12', '17');
INSERT INTO `test1` VALUES (1691, 'РК-9', '12', '6');
INSERT INTO `test1` VALUES (1692, 'РК-33', '12', '12');
INSERT INTO `test1` VALUES (1693, 'РК-10', '12', '19');
INSERT INTO `test1` VALUES (1694, 'РК-34', '12', '28');
INSERT INTO `test1` VALUES (1695, 'РК-35', '12', '22');
INSERT INTO `test1` VALUES (1696, 'РК-36', '12', '22');
INSERT INTO `test1` VALUES (1697, 'РК-37', '12', '24');
INSERT INTO `test1` VALUES (1698, 'РК-44', '12', '6');
INSERT INTO `test1` VALUES (1699, 'РК-45', '12', '5');
INSERT INTO `test1` VALUES (1700, 'РК-46', '12', '11');
INSERT INTO `test1` VALUES (1701, 'РК-47', '12', '10');
INSERT INTO `test1` VALUES (1702, 'РК-48', '12', '19');
INSERT INTO `test1` VALUES (1703, 'РК-49', '12', '18');
INSERT INTO `test1` VALUES (1704, 'РК-106', '12', '20');
INSERT INTO `test1` VALUES (1705, 'РК-107', '12', '17');
INSERT INTO `test1` VALUES (1706, 'РК-218', '12', '24');
INSERT INTO `test1` VALUES (1707, 'РК-219', '12', '35');
INSERT INTO `test1` VALUES (1708, 'РК-220', '12', '18');
INSERT INTO `test1` VALUES (1709, 'РК-221', '12', '18');
INSERT INTO `test1` VALUES (1710, 'РК-222', '12', '15');
INSERT INTO `test1` VALUES (1711, 'РК-223', '12', '18');
INSERT INTO `test1` VALUES (1712, 'РК-224', '12', '19');
INSERT INTO `test1` VALUES (1713, 'РК-225', '12', '20');
INSERT INTO `test1` VALUES (1714, 'РК-63', '12', '26');
INSERT INTO `test1` VALUES (1715, 'РК-64', '12', '24');
INSERT INTO `test1` VALUES (1716, 'РК-65', '12', '27');
INSERT INTO `test1` VALUES (1717, 'РК-4', '12', '18');
INSERT INTO `test1` VALUES (1718, 'РК-5', '12', '16');
INSERT INTO `test1` VALUES (1719, 'РК-6', '12', '23');
INSERT INTO `test1` VALUES (1720, 'РК-148', '12', '46');
INSERT INTO `test1` VALUES (1721, 'РК-149', '12', '25');
INSERT INTO `test1` VALUES (1722, 'РК-66', '12', '14');
INSERT INTO `test1` VALUES (1723, 'РК-67', '12', '11');
INSERT INTO `test1` VALUES (1724, 'РК-68', '12', '12');
INSERT INTO `test1` VALUES (1725, 'РК-89', '12', '15');
INSERT INTO `test1` VALUES (1726, 'РК-90', '12', '14');
INSERT INTO `test1` VALUES (1727, 'РК-114', '12', '5');
INSERT INTO `test1` VALUES (1728, 'РК-115', '12', '5');
INSERT INTO `test1` VALUES (1729, 'РК-116', '12', '14');
INSERT INTO `test1` VALUES (1730, 'РК-117', '12', '10');
INSERT INTO `test1` VALUES (1731, 'РК-268', '12', '6');
INSERT INTO `test1` VALUES (1732, 'РК-119', '12', '5');
INSERT INTO `test1` VALUES (1733, 'РК-120', '12', '7');
INSERT INTO `test1` VALUES (1734, 'РК-121', '12', '11');
INSERT INTO `test1` VALUES (1735, 'РК-122', '12', '9');
INSERT INTO `test1` VALUES (1736, 'РК-272', '12', '11');
INSERT INTO `test1` VALUES (1737, 'РК-123', '12', '8');
INSERT INTO `test1` VALUES (1738, 'РК-124', '12', '11');
INSERT INTO `test1` VALUES (1739, 'РК-269', '12', '12');
INSERT INTO `test1` VALUES (1740, 'РК-270', '12', '24');
INSERT INTO `test1` VALUES (1741, 'РК-271', '12', '9');
INSERT INTO `test1` VALUES (1742, 'РК-113', '12', '69');
INSERT INTO `test1` VALUES (1743, 'РК-104', '12', '10');
INSERT INTO `test1` VALUES (1744, 'РК-105', '12', '11');
INSERT INTO `test1` VALUES (1745, 'РК-118', '12', '8');
INSERT INTO `test1` VALUES (1746, 'РК-125', '12', '18');
INSERT INTO `test1` VALUES (1747, 'РК-126', '12', '24');
INSERT INTO `test1` VALUES (1748, 'РК-127', '12', '15');
INSERT INTO `test1` VALUES (1749, 'РК-128', '12', '24');
INSERT INTO `test1` VALUES (1750, 'РК-137 ', '12', '151');
INSERT INTO `test1` VALUES (1751, '', '12', '15');
INSERT INTO `test1` VALUES (1752, 'РК-356', '12', '112');
INSERT INTO `test1` VALUES (1753, '', '12', '4');
INSERT INTO `test1` VALUES (1754, '', '12', '5');
INSERT INTO `test1` VALUES (1755, 'РК-493', '12', '4');
INSERT INTO `test1` VALUES (1756, 'РК-494', '12', '4');
INSERT INTO `test1` VALUES (1757, 'РК-495', '12', '4');
INSERT INTO `test1` VALUES (1758, 'РК-496', '12', '4');
INSERT INTO `test1` VALUES (1759, 'РК-497', '12', '6');
INSERT INTO `test1` VALUES (1760, 'РК-498', '12', '6');
INSERT INTO `test1` VALUES (1761, 'РК-499', '12', '6');
INSERT INTO `test1` VALUES (1762, 'РК-500', '12', '6');
INSERT INTO `test1` VALUES (1763, 'РК-501', '12', '8');
INSERT INTO `test1` VALUES (1764, 'РК-502', '12', '8');
INSERT INTO `test1` VALUES (1765, 'РК-503', '12', '9');
INSERT INTO `test1` VALUES (1766, 'РК-504', '12', '8');
INSERT INTO `test1` VALUES (1767, 'РК-505', '12', '5');
INSERT INTO `test1` VALUES (1768, 'РК-506', '12', '5');
INSERT INTO `test1` VALUES (1769, 'РК-507', '12', '6');
INSERT INTO `test1` VALUES (1770, 'РК-508', '12', '5');
INSERT INTO `test1` VALUES (1771, 'РК-509', '12', '6');
INSERT INTO `test1` VALUES (1772, 'РК-510', '12', '7');
INSERT INTO `test1` VALUES (1773, 'РК-511', '12', '7');
INSERT INTO `test1` VALUES (1774, 'РК-512', '12', '7');
INSERT INTO `test1` VALUES (1775, 'РК-513', '12', '8');
INSERT INTO `test1` VALUES (1776, 'РК-514', '12', '9');
INSERT INTO `test1` VALUES (1777, 'РК-515', '12', '10');
INSERT INTO `test1` VALUES (1778, 'РК-516', '12', '9');
INSERT INTO `test1` VALUES (1779, 'РК-633', '12', '42');
INSERT INTO `test1` VALUES (1780, 'РК-634', '12', '43');
INSERT INTO `test1` VALUES (1781, 'РК-635', '12', '69');
INSERT INTO `test1` VALUES (1782, 'РК-636', '12', '37');
INSERT INTO `test1` VALUES (1783, 'РК-637', '12', '28');
INSERT INTO `test1` VALUES (1784, 'РК-638', '12', '19');
INSERT INTO `test1` VALUES (1785, 'РК-639', '12', '73');
INSERT INTO `test1` VALUES (1786, 'РК-640', '12', '39');
INSERT INTO `test1` VALUES (1787, 'РК-641', '12', '29');
INSERT INTO `test1` VALUES (1788, 'РК-642', '12', '20');
INSERT INTO `test1` VALUES (1789, 'РК-643', '12', '35');
INSERT INTO `test1` VALUES (1790, 'РК-644', '12', '27');
INSERT INTO `test1` VALUES (1791, 'РК-645', '12', '20');
INSERT INTO `test1` VALUES (1792, 'РК-646', '12', '20');
INSERT INTO `test1` VALUES (1793, 'РК-647', '12', '39');
INSERT INTO `test1` VALUES (1794, 'РК-648', '12', '29');
INSERT INTO `test1` VALUES (1795, 'РК-649', '12', '22');
INSERT INTO `test1` VALUES (1796, 'РК-650', '12', '21');
INSERT INTO `test1` VALUES (1797, 'РК-651', '12', '41');
INSERT INTO `test1` VALUES (1798, 'РК-652', '12', '32');
INSERT INTO `test1` VALUES (1799, 'РК-653', '12', '25');
INSERT INTO `test1` VALUES (1800, 'РК-654', '12', '24');
INSERT INTO `test1` VALUES (1801, 'РК-655', '12', '34');
INSERT INTO `test1` VALUES (1802, 'РК-656', '12', '35');
INSERT INTO `test1` VALUES (1803, 'РК-657', '12', '34');
INSERT INTO `test1` VALUES (1804, 'РК-658', '12', '32');
INSERT INTO `test1` VALUES (1805, 'РК-659', '12', '39');
INSERT INTO `test1` VALUES (1806, 'РК-660', '12', '22');
INSERT INTO `test1` VALUES (1807, 'РК-661', '12', '23');
INSERT INTO `test1` VALUES (1808, 'РК-662', '12', '27');
INSERT INTO `test1` VALUES (1809, 'РК-663', '12', '29');
INSERT INTO `test1` VALUES (1810, 'РК-664', '12', '34');
INSERT INTO `test1` VALUES (1811, 'РК-665', '12', '49');
INSERT INTO `test1` VALUES (1812, 'РК-666', '12', '54');
INSERT INTO `test1` VALUES (1813, 'РК-129', '12', '14');
INSERT INTO `test1` VALUES (1814, 'РК-130', '12', '17');
INSERT INTO `test1` VALUES (1815, 'РК-131', '12', '18');
INSERT INTO `test1` VALUES (1816, 'РК-132', '12', '16');
INSERT INTO `test1` VALUES (1817, 'РК-133', '12', '18');
INSERT INTO `test1` VALUES (1818, 'РК-134', '12', '18');
INSERT INTO `test1` VALUES (1819, 'РК-135', '12', '18');
INSERT INTO `test1` VALUES (1820, 'РК-136', '12', '13');
INSERT INTO `test1` VALUES (1821, 'РК-138', '12', '29');
INSERT INTO `test1` VALUES (1822, 'РК-139', '12', '32');
INSERT INTO `test1` VALUES (1823, 'РК-140', '12', '26');
INSERT INTO `test1` VALUES (1824, 'РК-246', '12', '13');
INSERT INTO `test1` VALUES (1825, 'РК-247', '12', '11');
INSERT INTO `test1` VALUES (1826, 'РК-248', '12', '14');
INSERT INTO `test1` VALUES (1827, 'РК-249', '12', '16');
INSERT INTO `test1` VALUES (1828, 'РК-250', '12', '17');
INSERT INTO `test1` VALUES (1829, 'РК-251', '12', '17');
INSERT INTO `test1` VALUES (1830, 'РК-252', '12', '21');
INSERT INTO `test1` VALUES (1831, 'РК-253', '12', '23');
INSERT INTO `test1` VALUES (1832, 'РК-254', '12', '23');
INSERT INTO `test1` VALUES (1833, 'РК-340', '12', '42');
INSERT INTO `test1` VALUES (1834, 'РК-341', '12', '43');
INSERT INTO `test1` VALUES (1835, 'РК-342', '12', '43');
INSERT INTO `test1` VALUES (1836, 'РК-343', '12', '20');
INSERT INTO `test1` VALUES (1837, 'РК-344', '12', '22');
INSERT INTO `test1` VALUES (1838, 'РК-345', '12', '17');
INSERT INTO `test1` VALUES (1839, 'РК-349', '12', '7');
INSERT INTO `test1` VALUES (1840, 'РК-351', '12', '9');
INSERT INTO `test1` VALUES (1841, 'РК-350', '12', '12');
INSERT INTO `test1` VALUES (1842, 'РК-438', '12', '31');
INSERT INTO `test1` VALUES (1843, 'РК-439', '12', '67');
INSERT INTO `test1` VALUES (1844, 'РК-440', '12', '33');
INSERT INTO `test1` VALUES (1845, 'РК-441', '12', '85');
INSERT INTO `test1` VALUES (1846, 'РК-442', '12', '31');
INSERT INTO `test1` VALUES (1847, 'РК-443', '12', '57');
INSERT INTO `test1` VALUES (1848, 'РК-444', '12', '30');
INSERT INTO `test1` VALUES (1849, 'РК-445', '12', '69');
INSERT INTO `test1` VALUES (1850, 'РК-446', '12', '38');
INSERT INTO `test1` VALUES (1851, 'РК-447', '12', '92');
INSERT INTO `test1` VALUES (1852, 'РК-448', '12', '32');
INSERT INTO `test1` VALUES (1853, 'РК-449', '12', '73');
INSERT INTO `test1` VALUES (1854, 'РК-450', '12', '35');
INSERT INTO `test1` VALUES (1855, 'РК-451', '12', '57');
INSERT INTO `test1` VALUES (1856, 'РК-452', '12', '37');
INSERT INTO `test1` VALUES (1857, 'РК-453', '12', '60');
INSERT INTO `test1` VALUES (1858, 'РК-61', '12', '12');
INSERT INTO `test1` VALUES (1859, 'РК-62', '12', '11');
INSERT INTO `test1` VALUES (1860, 'РК-454', '12', '31');
INSERT INTO `test1` VALUES (1861, 'РК-520', '12', '45');
INSERT INTO `test1` VALUES (1862, 'РК-455', '12', '103');
INSERT INTO `test1` VALUES (1863, 'РК-521', '12', '62');
INSERT INTO `test1` VALUES (1864, 'РК-456', '12', '35');
INSERT INTO `test1` VALUES (1865, 'РК-457', '12', '128');
INSERT INTO `test1` VALUES (1866, 'РК-458', '12', '39');
INSERT INTO `test1` VALUES (1867, 'РК-459', '12', '53');
INSERT INTO `test1` VALUES (1868, 'РК-460', '12', '27');
INSERT INTO `test1` VALUES (1869, 'РК-461', '12', '37');
INSERT INTO `test1` VALUES (1870, 'РК-462', '12', '25');
INSERT INTO `test1` VALUES (1871, 'РК-463', '12', '14');
INSERT INTO `test1` VALUES (1872, 'РК-464', '12', '70');
INSERT INTO `test1` VALUES (1873, 'РК-465', '12', '39');
INSERT INTO `test1` VALUES (1874, 'РК-466', '12', '27');
INSERT INTO `test1` VALUES (1875, 'РК-467', '12', '38');
INSERT INTO `test1` VALUES (1876, 'РК-468', '12', '26');
INSERT INTO `test1` VALUES (1877, 'РК-469', '12', '17');
INSERT INTO `test1` VALUES (1878, 'РК-470', '12', '13');
INSERT INTO `test1` VALUES (1879, 'РК-471', '12', '14');
INSERT INTO `test1` VALUES (1880, 'РК-472', '12', '25');
INSERT INTO `test1` VALUES (1881, 'РК-473', '12', '29');
INSERT INTO `test1` VALUES (1882, 'РК-478', '12', '31');
INSERT INTO `test1` VALUES (1883, 'РК-480', '12', '23');
INSERT INTO `test1` VALUES (1884, 'РК-479', '12', '31');
INSERT INTO `test1` VALUES (1885, 'РК-481', '12', '23');
INSERT INTO `test1` VALUES (1886, 'РК-542', '12', '60');
INSERT INTO `test1` VALUES (1887, 'РК-543', '12', '42');
INSERT INTO `test1` VALUES (1888, 'РК-544', '12', '31');
INSERT INTO `test1` VALUES (1889, 'РК-545', '12', '19');
INSERT INTO `test1` VALUES (1890, 'РК-546', '12', '65');
INSERT INTO `test1` VALUES (1891, 'РК-547', '12', '46');
INSERT INTO `test1` VALUES (1892, 'РК-548', '12', '33');
INSERT INTO `test1` VALUES (1893, 'РК-549', '12', '20');
INSERT INTO `test1` VALUES (1894, 'РК-550', '12', '71');
INSERT INTO `test1` VALUES (1895, 'РК-551', '12', '49');
INSERT INTO `test1` VALUES (1896, 'РК-552', '12', '35');
INSERT INTO `test1` VALUES (1897, 'РК-553', '12', '20');
INSERT INTO `test1` VALUES (1898, 'РК-554', '12', '57');
INSERT INTO `test1` VALUES (1899, 'РК-555', '12', '39');
INSERT INTO `test1` VALUES (1900, 'РК-556', '12', '29');
INSERT INTO `test1` VALUES (1901, 'РК-557', '12', '17');
INSERT INTO `test1` VALUES (1902, 'РК-558', '12', '79');
INSERT INTO `test1` VALUES (1903, 'РК-559', '12', '69');
INSERT INTO `test1` VALUES (1904, 'РК-560', '12', '40');
INSERT INTO `test1` VALUES (1905, 'РК-561', '12', '19');
INSERT INTO `test1` VALUES (1906, 'РК-562', '12', '74');
INSERT INTO `test1` VALUES (1907, 'РК-563', '12', '65');
INSERT INTO `test1` VALUES (1908, 'РК-564', '12', '37');
INSERT INTO `test1` VALUES (1909, 'РК-565', '12', '17');
INSERT INTO `test1` VALUES (1910, 'РК-566', '12', '76');
INSERT INTO `test1` VALUES (1911, 'РК-567', '12', '51');
INSERT INTO `test1` VALUES (1912, 'РК-568', '12', '28');
INSERT INTO `test1` VALUES (1913, 'РК-569', '12', '20');
INSERT INTO `test1` VALUES (1914, 'РК-570', '12', '16');
INSERT INTO `test1` VALUES (1915, 'РК-571', '12', '78');
INSERT INTO `test1` VALUES (1916, 'РК-572', '12', '55');
INSERT INTO `test1` VALUES (1917, 'РК-573', '12', '30');
INSERT INTO `test1` VALUES (1918, 'РК-574', '12', '22');
INSERT INTO `test1` VALUES (1919, 'РК-575', '12', '17');
INSERT INTO `test1` VALUES (1920, 'РК-576', '12', '69');
INSERT INTO `test1` VALUES (1921, 'РК-577', '12', '47');
INSERT INTO `test1` VALUES (1922, 'РК-578', '12', '25');
INSERT INTO `test1` VALUES (1923, 'РК-579', '12', '18');
INSERT INTO `test1` VALUES (1924, 'РК-580', '12', '14');
INSERT INTO `test1` VALUES (1925, 'Стільниця планка  кутова 28мм/38мм', '13', '20');
INSERT INTO `test1` VALUES (1926, 'Стільниця Планка з+B2014єднувальна 28мм/38мм', '13', '15');
INSERT INTO `test1` VALUES (1927, 'Стільниця Планка від плити 28мм/38мм', '13', '10');
INSERT INTO `test1` VALUES (1928, 'Стільниця стяжка для стільниці 60мм/150мм', '13', '6');
INSERT INTO `test1` VALUES (1929, 'Ніжка SCILM монтажна Н=80,100,130,150мм', '13', '4');
INSERT INTO `test1` VALUES (1930, 'Ніжка SCILM монтажна універсальна Н=80,100,130,150мм', '13', '5');
INSERT INTO `test1` VALUES (1931, 'НІжка монтажна Н=100мм', '13', '1');
INSERT INTO `test1` VALUES (1932, 'Кліпса до цоколя ДСП, пластикового', '13', '1');
INSERT INTO `test1` VALUES (1933, 'Ніжка пластикова Н=27мм', '13', '3');
INSERT INTO `test1` VALUES (1934, 'Ніжка декоративна (алюм) Н=100мм', '13', '15');
INSERT INTO `test1` VALUES (1935, 'Ніжка декоративна (ст.золото) Н=100мм', '13', '40');
INSERT INTO `test1` VALUES (1936, 'Ніжка нерегулюєма врізна (хром, ст.золото) Н=60мм', '13', '20');
INSERT INTO `test1` VALUES (1937, 'Ніжка нерегулюєма врізна (хром) Н=100мм', '13', '25');
INSERT INTO `test1` VALUES (1938, 'Нога для відкидного столу (сіра) Н=720мм', '13', '100');
INSERT INTO `test1` VALUES (1939, 'Нога для столу (хром) Н=720мм', '13', '60');
INSERT INTO `test1` VALUES (1940, 'Нога для столу (хром) Н=850мм', '13', '85');
INSERT INTO `test1` VALUES (1941, 'Механізм для відкидного столу', '13', '85');
INSERT INTO `test1` VALUES (1942, 'Ролік меблевий резиновий з площадкою', '13', '10');
INSERT INTO `test1` VALUES (1943, 'Планка монтажна  2м', '13', '25');
INSERT INTO `test1` VALUES (1944, 'Командорики малі (1 отвір)', '13', '1');
INSERT INTO `test1` VALUES (1945, 'Консоль з накладкою 77*120мм', '13', '10');
INSERT INTO `test1` VALUES (1946, 'Кут з накладкою регул.', '13', '3');
INSERT INTO `test1` VALUES (1947, 'Кутник металевий великий (хром, сатин, алюм) 222*222мм', '13', '60');
INSERT INTO `test1` VALUES (1948, 'Кутник металевий 30*20мм', '13', '1');
INSERT INTO `test1` VALUES (1949, 'Стяжка міжсекційна хром, мідь, золото', '13', '0');
INSERT INTO `test1` VALUES (1950, 'Планка пряма зєднювальна для деталей ДСП', '13', '2');
INSERT INTO `test1` VALUES (1951, 'Підвіска шафи регульована', '13', '2');
INSERT INTO `test1` VALUES (1952, 'Підвіска шафи нерегульована  (вухо)', '13', '2');
INSERT INTO `test1` VALUES (1953, 'Підвіска нижньої тумби (ліва+права)', '13', '50');
INSERT INTO `test1` VALUES (1954, 'Планка монтажна  для кріплення нижньої тумби', '13', '10');
INSERT INTO `test1` VALUES (1955, 'полицетримач L240мм на дюбелі скритого монтажу)', '13', '10');
INSERT INTO `test1` VALUES (1956, 'Тримач полки ДСП, скло, середній (пелікан), сатин, ст.золото, золото, хром', '13', '10');
INSERT INTO `test1` VALUES (1957, 'Тримач полки ДСП, скло, великий (пелікан), сатин, ст.золото, золото, хром', '13', '20');
INSERT INTO `test1` VALUES (1958, 'Підйомник пневматичний N60, N80, N100', '13', '15');
INSERT INTO `test1` VALUES (1959, 'Підйомник механічний ', '13', '30');
INSERT INTO `test1` VALUES (1960, 'Барна Труба барна  хром ?50 мм (3м)', '13', '130');
INSERT INTO `test1` VALUES (1961, 'Барні фланці на трубу барну верхній, нижній', '13', '16');
INSERT INTO `test1` VALUES (1962, 'Барна Корзина центральна (хром) D=360мм', '13', '130');
INSERT INTO `test1` VALUES (1963, 'Барна Корзина центральна висока (хром) D=360мм', '13', '190');
INSERT INTO `test1` VALUES (1964, 'Барний Бокалотримач (хром)', '13', '120');
INSERT INTO `test1` VALUES (1965, 'Барний Пляшкотримач (хром)', '13', '120');
INSERT INTO `test1` VALUES (1966, 'Барна склополиця центральна хром', '13', '150');
INSERT INTO `test1` VALUES (1967, 'Барна Труба барна ст.золото/сатин ?50 мм (3м)', '13', '500');
INSERT INTO `test1` VALUES (1968, 'Барні фланці на трубу барну ст.золото/сатин верхній, нижній', '13', '25');
INSERT INTO `test1` VALUES (1969, 'Барна склополиця центральна ст.золото/сатин', '13', '200');
INSERT INTO `test1` VALUES (1970, 'Барна Корзина центральна (хром)', '13', '170');
INSERT INTO `test1` VALUES (1971, 'Труба  хром  ?25 мм, 3м ', '13', '50');
INSERT INTO `test1` VALUES (1972, 'Тримачі на трубу  ?25 мм', '13', '3');
INSERT INTO `test1` VALUES (1973, 'Штанга  на одяг овальна, 3м ', '13', '40');
INSERT INTO `test1` VALUES (1974, 'Штанга  на одяг кріплення', '13', '2');
INSERT INTO `test1` VALUES (1975, 'Труба 25мм Муфта хрестоподібна', '13', '35');
INSERT INTO `test1` VALUES (1976, 'Труба 25мм Муфта Т-подібна', '13', '30');
INSERT INTO `test1` VALUES (1977, 'Барна  труба коса', '13', '110');
INSERT INTO `test1` VALUES (1978, 'Підсвітка галогенна нижня трикутна 1 лампа з вим / без вим, матов хром/ст.золото', '13', '40');
INSERT INTO `test1` VALUES (1979, 'Підсвітка галогенна верхня 1 лампа', '13', '25');
INSERT INTO `test1` VALUES (1980, 'Підсвітка  галогенна верхня 3 лампи', '13', '110');
INSERT INTO `test1` VALUES (1981, 'Підсвітка  галогенна верхня 5 ламп', '13', '140');
INSERT INTO `test1` VALUES (1982, 'Підсвітка кольорова для склополиць на 2 полиці+трансформатор', '13', '150');
INSERT INTO `test1` VALUES (1983, 'Підсвітка світодіодна верхня накладна, хром/алюм, тепле/холодне біле світло', '13', '60');
INSERT INTO `test1` VALUES (1984, 'Підсвітка верхня світодіодна накладна, хром/алюм, тепле/холодне біле світло (компл. 3шт+трансформатор)', '13', '240');
INSERT INTO `test1` VALUES (1985, 'Підсвітка нижня світодіодна (трикутник) хром, алюм', '13', '70');
INSERT INTO `test1` VALUES (1986, 'Профіль накладний для стрічки світодіодної+ пластик прозорий (розсіювач світла) (2м)', '13', '80');
INSERT INTO `test1` VALUES (1987, 'Стрічка світодіодна120 діодів (10 Вт), тепле/холодне біле світло', '13', '80');
INSERT INTO `test1` VALUES (1988, 'Трансформатор 30 Вт для світодіодних підсвіток (пост.струму)', '13', '80');
INSERT INTO `test1` VALUES (1989, 'Трансформатор 60 Вт для світодіодних підсвіток (пост.струму)', '13', '135');
INSERT INTO `test1` VALUES (1990, 'Люмінісцентна лампа 34см', '13', '40');
INSERT INTO `test1` VALUES (1991, 'Люмінісцентна лампа 54см', '13', '60');
INSERT INTO `test1` VALUES (1992, 'Вимикач ланцюжковий хром/золото', '13', '15');
INSERT INTO `test1` VALUES (1993, 'Вимикач кнопковий врізний HAFELE', '13', '15');
INSERT INTO `test1` VALUES (1994, 'Вилка електрична', '13', '8');
INSERT INTO `test1` VALUES (1995, 'Трансформатор  60 W', '13', '35');
INSERT INTO `test1` VALUES (1996, 'Трансформатор  105 W', '13', '50');
INSERT INTO `test1` VALUES (1997, 'Трансформатор 150  W', '13', '100');
INSERT INTO `test1` VALUES (1998, 'Релінг  100 см сатин, т.золото, золото, хром', '13', '25');
INSERT INTO `test1` VALUES (1999, 'Релінг Гачок до релінга', '13', '15');
INSERT INTO `test1` VALUES (2000, 'Релінг Кут релінгу', '13', '15');
INSERT INTO `test1` VALUES (2001, 'Релінг Тримач на релінг', '13', '12');
INSERT INTO `test1` VALUES (2002, 'Релінг Закінчення на релінг', '13', '10');
INSERT INTO `test1` VALUES (2003, 'Лоток д/ложок 30 см, сірий', '13', '40');
INSERT INTO `test1` VALUES (2004, 'Лоток д/ложок 35 см, сірий', '13', '40');
INSERT INTO `test1` VALUES (2005, 'р', '13', '40');
INSERT INTO `test1` VALUES (2006, 'страда', '13', '40');
INSERT INTO `test1` VALUES (2007, 'Лоток д/ложок 50 см, сірий', '13', '50');
INSERT INTO `test1` VALUES (2008, 'Лоток д/ложок 55 см, сірий', '13', '50');
INSERT INTO `test1` VALUES (2009, 'Лоток д/ложок 60 см, сірий', '13', '60');
INSERT INTO `test1` VALUES (2010, 'Лоток д/ложок 70 см, сірий', '13', '70');
INSERT INTO `test1` VALUES (2011, 'Лоток д/ложок 80 см, сірий', '13', '80');
INSERT INTO `test1` VALUES (2012, 'Лоток д/ложок 90 см, сірий', '13', '90');
INSERT INTO `test1` VALUES (2013, 'Дно металізоване (алюм) 600мм', '13', '90');
INSERT INTO `test1` VALUES (2014, 'Дно металізоване (алюм) 800мм', '13', '110');
INSERT INTO `test1` VALUES (2015, 'Дно металізоване (алюм) 900мм', '13', '120');
INSERT INTO `test1` VALUES (2016, 'Дно металізоване (алюм) 1200мм', '13', '140');
INSERT INTO `test1` VALUES (2017, 'Контейнер під сміття VIBO (2 секції) на направляючих', '13', '450');
INSERT INTO `test1` VALUES (2018, 'Декор Решітка фасадна (кратка) дерево', '13', '500');
INSERT INTO `test1` VALUES (2019, 'Декор Балюстрада пряма з балясинами дерево', '13', '300');
INSERT INTO `test1` VALUES (2020, 'Декор Балюстрада радіусна з балясинами', '13', '400');
INSERT INTO `test1` VALUES (2021, 'Декор Закінчення радіусне на стільницю (клюшка)', '13', '400');
INSERT INTO `test1` VALUES (2022, 'Декор Балюстрада пряма з балясинами МДФ', '13', '125');
INSERT INTO `test1` VALUES (2023, 'Декор Бал гнута з балясинами МДФ', '13', '160');
INSERT INTO `test1` VALUES (2024, 'Шафа Пантограф GTV 450-600мм', '13', '210');
INSERT INTO `test1` VALUES (2025, 'Шафа Пантограф GTV 600-830мм', '13', '210');
INSERT INTO `test1` VALUES (2026, 'Шафа Пантограф GTV 30-1150мм', '13', '210');
INSERT INTO `test1` VALUES (2027, 'Шафа Штанга під одяг з підсвіткою LED та сенсорним вимикачем 600мм (522-567мм)', '13', '160');
INSERT INTO `test1` VALUES (2028, 'Шафа Штанга під одяг з підсвіткою LED та сенсорним вимикачем 800мм (722-767мм)', '13', '175');
INSERT INTO `test1` VALUES (2029, 'Шафа Вішак висувний (тромбон) 300мм,410мм,450мм,500мм', '13', '30');
INSERT INTO `test1` VALUES (2030, 'Шафа Фіксатор до дверей шафи-купе', '13', '5');
INSERT INTO `test1` VALUES (2031, 'Шафа Щітка до дверей шафи-купе', '13', '2');
INSERT INTO `test1` VALUES (2032, 'Світополка (алюм, золото, золото полироване, чорний браш 450мм', '13', '220');
INSERT INTO `test1` VALUES (2033, 'UA-AA-А3 алюм 128 / 160 mm', '9', '13');
INSERT INTO `test1` VALUES (2034, 'UA-AA-А3 алюм 160 / 192 mm', '9', '15');
INSERT INTO `test1` VALUES (2035, 'UA-AA-А3 алюм 224 / 256 mm', '9', '18');
INSERT INTO `test1` VALUES (2036, 'RE - AL / SAT 96 / 176 mm', '9', '10');
INSERT INTO `test1` VALUES (2037, 'RE - AL / SAT 128 / 188 mm', '9', '10');
INSERT INTO `test1` VALUES (2038, 'RE - AL / SAT 160 / 220 mm', '9', '10');
INSERT INTO `test1` VALUES (2039, 'RE - AL / SAT 192 / 272 mm', '9', '15');
INSERT INTO `test1` VALUES (2040, 'RE - AL / SAT 224 / 304 mm', '9', '20');
INSERT INTO `test1` VALUES (2041, 'RE - AL / SAT 352 / 432 mm', '9', '20');
INSERT INTO `test1` VALUES (2042, 'RE - AL / SAT 384 / 464 mm', '9', '20');
INSERT INTO `test1` VALUES (2043, 'RE - AL / SAT 416 / 496 mm', '9', '24');
INSERT INTO `test1` VALUES (2044, 'RE - AL / SAT 448 / 528 mm', '9', '24');
INSERT INTO `test1` VALUES (2045, 'RE - AL / SAT 448 / 528 mm', '9', '24');
INSERT INTO `test1` VALUES (2046, 'RE - AL / SAT 512 / 592 mm', '9', '30');
INSERT INTO `test1` VALUES (2047, 'UA-AA18(алюм) 512 / 592 mm', '9', '12');
INSERT INTO `test1` VALUES (2048, 'UA-AA18(алюм) 512 / 592 mm', '9', '15');
INSERT INTO `test1` VALUES (2049, 'UA-AA18(алюм) 192 / 210 mm', '9', '15');
INSERT INTO `test1` VALUES (2050, 'UA-AA18(алюм) 192 / 210 mm', '9', '15');
INSERT INTO `test1` VALUES (2051, 'UA-AA18(алюм) 192 / 210 mm', '9', '17');
INSERT INTO `test1` VALUES (2052, 'UA-AA18(іnox) 192 / 210 mm', '9', '18');
INSERT INTO `test1` VALUES (2053, 'UA-AA18(іnox) 192 / 210 mm', '9', '19');
INSERT INTO `test1` VALUES (2054, 'UA-B0-311 алюм 96 / 116 mm', '9', '10');
INSERT INTO `test1` VALUES (2055, 'UA-B0-311 алюм 128 / 148 mm', '9', '10');
INSERT INTO `test1` VALUES (2056, 'UA-B0-311 алюм 160 / 180 mm', '9', '11');
INSERT INTO `test1` VALUES (2057, 'UA-B0-311 алюм 160 / 180 mm', '9', '12');
INSERT INTO `test1` VALUES (2058, 'UA-B0-311 алюм 224 / 244 mm', '9', '13');
INSERT INTO `test1` VALUES (2059, 'UA-B0-311 алюм 256 / 276 mm', '9', '14');
INSERT INTO `test1` VALUES (2060, 'UA-B0-311 алюм 320 / 340 mm', '9', '16');
INSERT INTO `test1` VALUES (2061, 'UA-00-315 алюм 320 / 340 mm', '9', '21');
INSERT INTO `test1` VALUES (2062, 'UA-00-315 алюм 320 / 340 mm', '9', '24');
INSERT INTO `test1` VALUES (2063, 'UA-00-315 алюм 320 / 340 mm', '9', '25');
INSERT INTO `test1` VALUES (2064, 'UA-00-315 алюм 224 / 263 mm', '9', '27');
INSERT INTO `test1` VALUES (2065, 'UA-00-315 алюм 256 / 295 mm', '9', '30');
INSERT INTO `test1` VALUES (2066, 'UA-00-315 алюм 320 / 359 mm', '9', '33');
INSERT INTO `test1` VALUES (2067, 'UA-00-319 алюм 320 / 359 mm', '9', '10');
INSERT INTO `test1` VALUES (2068, 'UA-00-319 алюм 128 / 168 mm', '9', '12');
INSERT INTO `test1` VALUES (2069, 'UA-00-319 алюм 160 / 200 mm', '9', '13');
INSERT INTO `test1` VALUES (2070, 'UA-A0-330 алюм 128 / 168 mm', '9', '17');
INSERT INTO `test1` VALUES (2071, 'UA-00-333 алюм 96 mm', '9', '7');
INSERT INTO `test1` VALUES (2072, 'UA-00-333 алюм 128 mm', '9', '8');
INSERT INTO `test1` VALUES (2073, 'UA-B0-337 алюм 128 / 168 mm', '9', '16');
INSERT INTO `test1` VALUES (2074, 'UA-B0-337 алюм 128 / 168 mm', '9', '18');
INSERT INTO `test1` VALUES (2075, 'UA-00-340 алюм 96 / 136 mm', '9', '10');
INSERT INTO `test1` VALUES (2076, 'UA-00-340 алюм 128 / 168 mm', '9', '14');
INSERT INTO `test1` VALUES (2077, 'UA-00-340 алюм 128 / 168 mm', '9', '16');
INSERT INTO `test1` VALUES (2078, 'UA-00-340 алюм 192 / 232 mm', '9', '18');
INSERT INTO `test1` VALUES (2079, 'UA-00-340 алюм 224 / 264 mm', '9', '18');
INSERT INTO `test1` VALUES (2080, 'UA-00-340 алюм 256 / 296 mm', '9', '20');
INSERT INTO `test1` VALUES (2081, 'UA-00-340 алюм 320 / 360 mm', '9', '20');
INSERT INTO `test1` VALUES (2082, 'UA-00-358 алюм 128 / 168 mm', '9', '12');
INSERT INTO `test1` VALUES (2083, 'UA-00-358 алюм 160 / 200 mm', '9', '15');
INSERT INTO `test1` VALUES (2084, 'UA-00-338 алюм 140мм', '9', '11');
INSERT INTO `test1` VALUES (2085, 'UA-00-338 алюм 160мм', '9', '12');
INSERT INTO `test1` VALUES (2086, 'RR0804 сатин 160 / 200 mm', '9', '12');
INSERT INTO `test1` VALUES (2088, 'RG0806', '9', '8');
INSERT INTO `test1` VALUES (2089, 'РК-11', '9', '20');
INSERT INTO `test1` VALUES (2090, 'РК-12', '9', '20');
INSERT INTO `test1` VALUES (2091, 'РК-13', '9', '22');
INSERT INTO `test1` VALUES (2092, 'РК-14', '9', '22');
INSERT INTO `test1` VALUES (2093, 'UN7108 сат/алюм', '9', '22');
INSERT INTO `test1` VALUES (2094, 'UN7106сат/алюм', '9', '25');
INSERT INTO `test1` VALUES (2095, 'UN7106 сатин', '9', '11');
INSERT INTO `test1` VALUES (2096, 'UN4907 сатин', '9', '15');
INSERT INTO `test1` VALUES (2097, 'GAL 1', '9', '61');
INSERT INTO `test1` VALUES (2098, 'GAL 2', '9', '61');
INSERT INTO `test1` VALUES (2099, 'VETRO', '9', '33');
INSERT INTO `test1` VALUES (2100, 'MODENA хром/сатин', '9', '21');
INSERT INTO `test1` VALUES (2101, 'LOTOS', '9', '26');
INSERT INTO `test1` VALUES (2102, 'LOTOS малий', '9', '22');
INSERT INTO `test1` VALUES (2103, 'TULIPAN', '9', '30');
INSERT INTO `test1` VALUES (2104, 'TULIPAN малий', '9', '22');
INSERT INTO `test1` VALUES (2105, 'FLOWER', '9', '33');
INSERT INTO `test1` VALUES (2106, 'FLOWER малий', '9', '22');
INSERT INTO `test1` VALUES (2107, 'NARCYZ 96 / - mm', '9', '30');
INSERT INTO `test1` VALUES (2108, 'NARCYZ', '9', '19');
INSERT INTO `test1` VALUES (2109, 'DP 211', '9', '15');
INSERT INTO `test1` VALUES (2110, 'DP 212', '9', '15');
INSERT INTO `test1` VALUES (2111, 'DP 213', '9', '15');
INSERT INTO `test1` VALUES (2112, 'DP 215', '9', '15');
INSERT INTO `test1` VALUES (2113, 'DP 217', '9', '15');
INSERT INTO `test1` VALUES (2114, 'DP 191', '9', '15');
