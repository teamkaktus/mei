<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ls_admin\models\Vacancy;

/**
 * VacancySearch represents the model behind the search form about `app\modules\ls_admin\models\Vacancy`.
 */
class VacancySearch extends Vacancy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type', 'count_m', 'results_sobesedovanyya', 'training_company', 'compensation_lunches', 'corporate_dining', 'lca', 'sick_pay', 'paid_holiday', 'corporate_transportation', 'fitness_club', 'mobile_payment', 'corporate_mobile_communication', 'status_admin', 'ps_internet', 'ps_office', 'ps_photoshop', 'ps_autocad', 'ps_corel', 'ps_1c', 'auto_cat_a', 'auto_cat_b', 'auto_cat_c', 'auto_cat_d', 'auto_cat_e', 'created_at', 'updated_at'], 'integer'],
            [['name', 'kod', 'duties', 'education', 'experience', 'faculty', 'other', 'additional', 'premiums', 'daily_work', 'probation', 'payment_probation', 'business_trips', 'status', 'additional_terms'], 'safe'],
            [['salary_b', 'salary_e'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacancy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'count_m' => $this->count_m,
            'salary_b' => $this->salary_b,
            'salary_e' => $this->salary_e,
            'results_sobesedovanyya' => $this->results_sobesedovanyya,
            'training_company' => $this->training_company,
            'compensation_lunches' => $this->compensation_lunches,
            'corporate_dining' => $this->corporate_dining,
            'lca' => $this->lca,
            'sick_pay' => $this->sick_pay,
            'paid_holiday' => $this->paid_holiday,
            'corporate_transportation' => $this->corporate_transportation,
            'fitness_club' => $this->fitness_club,
            'mobile_payment' => $this->mobile_payment,
            'corporate_mobile_communication' => $this->corporate_mobile_communication,
            'status_admin' => $this->status_admin,
            'ps_internet' => $this->ps_internet,
            'ps_office' => $this->ps_office,
            'ps_photoshop' => $this->ps_photoshop,
            'ps_autocad' => $this->ps_autocad,
            'ps_corel' => $this->ps_corel,
            'ps_1c' => $this->ps_1c,
            'auto_cat_a' => $this->auto_cat_a,
            'auto_cat_b' => $this->auto_cat_b,
            'auto_cat_c' => $this->auto_cat_c,
            'auto_cat_d' => $this->auto_cat_d,
            'auto_cat_e' => $this->auto_cat_e,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'kod', $this->kod])
            ->andFilterWhere(['like', 'duties', $this->duties])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'faculty', $this->faculty])
            ->andFilterWhere(['like', 'other', $this->other])
            ->andFilterWhere(['like', 'additional', $this->additional])
            ->andFilterWhere(['like', 'premiums', $this->premiums])
            ->andFilterWhere(['like', 'daily_work', $this->daily_work])
            ->andFilterWhere(['like', 'probation', $this->probation])
            ->andFilterWhere(['like', 'payment_probation', $this->payment_probation])
            ->andFilterWhere(['like', 'business_trips', $this->business_trips])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'additional_terms', $this->additional_terms]);

        return $dataProvider;
    }
}
