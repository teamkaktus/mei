<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "vacancy".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $kod
 * @property integer $type
 * @property integer $count_m
 * @property string $duties
 * @property string $education
 * @property string $experience
 * @property string $faculty
 * @property string $other
 * @property string $small_desc
 * @property string $additional
 * @property double $salary_b
 * @property double $salary_e
 * @property string $premiums
 * @property integer $results_sobesedovanyya
 * @property string $daily_work
 * @property string $probation
 * @property string $payment_probation
 * @property integer $training_company
 * @property integer $compensation_lunches
 * @property integer $corporate_dining
 * @property integer $lca
 * @property integer $sick_pay
 * @property integer $paid_holiday
 * @property integer $corporate_transportation
 * @property integer $fitness_club
 * @property integer $mobile_payment
 * @property integer $corporate_mobile_communication
 * @property string $business_trips
 * @property string $status
 * @property integer $status_admin
 * @property string $additional_terms
 * @property integer $ps_internet
 * @property integer $ps_office
 * @property integer $ps_photoshop
 * @property integer $ps_autocad
 * @property integer $ps_corel
 * @property integer $ps_1c
 * @property integer $auto_cat_a
 * @property integer $auto_cat_b
 * @property integer $auto_cat_c
 * @property integer $auto_cat_d
 * @property integer $auto_cat_e
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property LangVac[] $langVacs
 * @property Users $user
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'count_m', 'results_sobesedovanyya', 'training_company', 'compensation_lunches', 'corporate_dining', 'lca', 'sick_pay', 'paid_holiday', 'corporate_transportation', 'fitness_club', 'mobile_payment', 'corporate_mobile_communication', 'status_admin', 'ps_internet', 'ps_office', 'ps_photoshop', 'ps_autocad', 'ps_corel', 'ps_1c', 'auto_cat_a', 'auto_cat_b', 'auto_cat_c', 'auto_cat_d', 'auto_cat_e', 'created_at', 'updated_at'], 'integer'],
            [['duties', 'other', 'small_desc', 'additional', 'additional_terms'], 'string'],
            [['salary_b', 'salary_e'], 'number'],
            [['name', 'kod', 'education', 'experience', 'faculty', 'premiums', 'daily_work', 'probation', 'payment_probation', 'business_trips', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Работодатель',
            'name' => 'Название вакансии',
            'kod' => 'Код вакансии',
            'type' => 'Горящие вакансии',
            'count_m' => 'Count M',
            'duties' => 'Duties',
            'education' => 'Education',
            'experience' => 'Experience',
            'faculty' => 'Faculty',
            'other' => 'Other',
            'additional' => 'Additional',
            'salary_b' => 'Salary B',
            'salary_e' => 'Salary E',
            'premiums' => 'Premiums',
            'results_sobesedovanyya' => 'Results Sobesedovanyya',
            'daily_work' => 'Daily Work',
            'probation' => 'Probation',
            'payment_probation' => 'Payment Probation',
            'training_company' => 'Training Company',
            'compensation_lunches' => 'Compensation Lunches',
            'corporate_dining' => 'Corporate Dining',
            'lca' => 'Lca',
            'sick_pay' => 'Sick Pay',
            'paid_holiday' => 'Paid Holiday',
            'corporate_transportation' => 'Corporate Transportation',
            'fitness_club' => 'Fitness Club',
            'mobile_payment' => 'Mobile Payment',
            'corporate_mobile_communication' => 'Corporate Mobile Communication',
            'business_trips' => 'Business Trips',
            'status' => 'Status',
            'status_admin' => 'Модерация вакансии',
            'additional_terms' => 'Additional Terms',
            'small_desc' => 'Краткое описание',
            'ps_internet' => 'Ps Internet',
            'ps_office' => 'Ps Office',
            'ps_photoshop' => 'Ps Photoshop',
            'ps_autocad' => 'Ps Autocad',
            'ps_corel' => 'Ps Corel',
            'ps_1c' => 'Ps 1c',
            'auto_cat_a' => 'Auto Cat A',
            'auto_cat_b' => 'Auto Cat B',
            'auto_cat_c' => 'Auto Cat C',
            'auto_cat_d' => 'Auto Cat D',
            'auto_cat_e' => 'Auto Cat E',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangVacs()
    {
        return $this->hasMany(LangVac::className(), ['vac_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
