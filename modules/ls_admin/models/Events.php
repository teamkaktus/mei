<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $type
 * @property string $file
 * @property string $file_name
 * @property string $small_desc
 * @property string $desc
 * @property string $img
 * @property string $img_title
 * @property string $img_alt
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_key
 * @property integer $vizible_now
 * @property integer $date_for_time
 * @property string $date_for
 * @property integer $created_at
 * @property integer $updated_at
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'small_desc','desc','img','date_for'], 'required'],
            [['small_desc', 'desc', 'seo_desc'], 'string'],
            [['vizible_now','date_for_time', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'type', 'file', 'file_name', 'img', 'img_title', 'img_alt', 'seo_title', 'seo_key', 'date_for'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Url',
            'small_desc' => 'Краткое описание',
            'desc' => 'Описание',
            'img' => 'Картинка',
            'file' => 'Файл',
            'type' => 'Тип',
            'img_title' => 'Img Title',
            'img_alt' => 'Img Alt',
            'seo_title' => 'Seo Title',
            'seo_desc' => 'Seo Desc',
            'seo_key' => 'Seo Key',
            'vizible_now' => 'Отображать сичас',
            'date_for' => 'Когда состоится',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'file_name' => 'Назва файла',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => 'app\commands\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url',
                'translit' => true
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->date_for){
                $this->date_for_time = Yii::$app->formatter->asTimestamp($this->date_for);
            }
            return true;
        } else {
            return false;
        }
    }
}
