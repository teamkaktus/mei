<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "partners".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $site
 * @property integer $create_page
 * @property string $desc
 * @property string $img
 * @property string $img_title
 * @property string $img_alt
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_key
 * @property integer $created_at
 * @property integer $updated_at
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['create_page', 'created_at', 'updated_at'], 'integer'],
            [['desc', 'seo_desc'], 'string'],
            [['name', 'url', 'site', 'img', 'img_title', 'img_alt', 'seo_title', 'seo_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва партнера',
            'url' => 'Url',
            'site' => 'Сайт',
            'create_page' => 'Создать персональную страницу',
            'desc' => 'Содержымое страницы',
            'img' => 'Картинка',
            'img_title' => 'Img Title',
            'img_alt' => 'Img Alt',
            'seo_title' => 'Seo Title',
            'seo_desc' => 'Seo Desc',
            'seo_key' => 'Seo Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => 'app\commands\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url',
                'translit' => true
            ]
        ];
    }
}
