<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "worker_vacamcies".
 *
 * @property integer $id
 * @property integer $vac_id
 * @property integer $user_id
 * @property integer $to_respond
 * @property integer $created_at
 * @property integer $updated_at
 */
class WorkerVacamcies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker_vacamcies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vac_id', 'user_id', 'to_respond', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vac_id' => 'Vac ID',
            'user_id' => 'User ID',
            'to_respond' => 'To Respond',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}
