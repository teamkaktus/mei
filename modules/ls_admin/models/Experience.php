<?php

namespace app\modules\ls_admin\models;

use Yii;

/**
 * This is the model class for table "experience".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $experience
 * @property string $sity_work
 * @property string $situation
 * @property string $year_work_b
 * @property string $month_work_b
 * @property string $year_work_e
 * @property string $month_work_e
 * @property integer $created_at
 * @property integer $updated_at
 */
class Experience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['experience', 'sity_work', 'situation', 'year_work_b', 'month_work_b', 'year_work_e', 'month_work_e'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'experience' => 'Experience',
            'sity_work' => 'Sity Work',
            'situation' => 'Situation',
            'year_work_b' => 'Year Work B',
            'month_work_b' => 'Month Work B',
            'year_work_e' => 'Year Work E',
            'month_work_e' => 'Month Work E',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
