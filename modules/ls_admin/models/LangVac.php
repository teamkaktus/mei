<?php

namespace app\modules\ls_admin\models;

use Yii;

/**
 * This is the model class for table "lang_vac".
 *
 * @property integer $id
 * @property string $name
 * @property string $level
 * @property integer $vac_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Vacancy $vac
 */
class LangVac extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang_vac';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vac_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'level'], 'string', 'max' => 255],
            [['vac_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vacancy::className(), 'targetAttribute' => ['vac_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'level' => 'Level',
            'vac_id' => 'Vac ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVac()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'vac_id']);
    }
}
