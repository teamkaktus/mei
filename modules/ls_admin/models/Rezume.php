<?php

namespace app\modules\ls_admin\models;

use Yii;

/**
 * This is the model class for table "rezume".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $sity
 * @property string $st_metro
 * @property string $citizenship
 * @property string $crossing
 * @property string $native_lang
 * @property string $sector_job
 * @property string $education
 * @property string $additional_education
 * @property string $courses
 * @property string $recommendations
 * @property string $hobbies
 * @property string $information
 * @property integer $ps_internet
 * @property integer $ps_office
 * @property integer $ps_photoshop
 * @property integer $ps_autocad
 * @property integer $ps_corel
 * @property integer $ps_1c
 * @property integer $auto_cat_a
 * @property integer $auto_cat_b
 * @property integer $auto_cat_c
 * @property integer $auto_cat_d
 * @property integer $auto_cat_e
 * @property integer $created_at
 * @property integer $updated_at
 */
class Rezume extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rezume';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ps_internet', 'ps_office', 'ps_photoshop', 'ps_autocad', 'ps_corel', 'ps_1c', 'auto_cat_a', 'auto_cat_b', 'auto_cat_c', 'auto_cat_d', 'auto_cat_e', 'created_at', 'updated_at'], 'integer'],
            [['sity', 'st_metro', 'citizenship', 'crossing', 'native_lang', 'sector_job', 'education', 'additional_education', 'courses', 'recommendations', 'hobbies', 'information'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sity' => 'Sity',
            'st_metro' => 'St Metro',
            'citizenship' => 'Citizenship',
            'crossing' => 'Crossing',
            'native_lang' => 'Native Lang',
            'sector_job' => 'Sector Job',
            'education' => 'Education',
            'additional_education' => 'Additional Education',
            'courses' => 'Courses',
            'recommendations' => 'Recommendations',
            'hobbies' => 'Hobbies',
            'information' => 'Information',
            'ps_internet' => 'Ps Internet',
            'ps_office' => 'Ps Office',
            'ps_photoshop' => 'Ps Photoshop',
            'ps_autocad' => 'Ps Autocad',
            'ps_corel' => 'Ps Corel',
            'ps_1c' => 'Ps 1c',
            'auto_cat_a' => 'Auto Cat A',
            'auto_cat_b' => 'Auto Cat B',
            'auto_cat_c' => 'Auto Cat C',
            'auto_cat_d' => 'Auto Cat D',
            'auto_cat_e' => 'Auto Cat E',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
