<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $small_desc
 * @property string $desc
 * @property string $img
 * @property string $img_title
 * @property string $img_alt
 * @property string $seo_title
 * @property string $seo_desc
 * @property string $seo_key
 * @property integer $vizible_now
 * @property integer $date_for
 * @property integer $created_at
 * @property integer $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','img','desc','small_desc'], 'required'],
            [['small_desc','date_for', 'desc', 'seo_desc'], 'string'],
            [['vizible_now',  'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'img', 'img_title', 'img_alt', 'seo_title', 'seo_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Url',
            'small_desc' => 'Краткое описание',
            'desc' => 'Описание',
            'img' => 'Картинка',
            'img_title' => 'Title картинкы',
            'img_alt' => 'Alt картинкы',
            'seo_title' => 'Seo Title',
            'seo_desc' => 'Seo Desc',
            'seo_key' => 'Seo Key',
            'vizible_now' => 'Отображать сичас',
            'date_for' => 'Отображать с',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => 'app\commands\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url',
                'translit' => true
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$this->vizible_now &&  $this->date_for){
                $this->updated_at = Yii::$app->formatter->asTimestamp($this->date_for);
            }
            return true;
        } else {
            return false;
        }
    }
}
