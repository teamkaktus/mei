<?php

namespace app\modules\ls_admin\models;

use Yii;

/**
 * This is the model class for table "vuz".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $faculty
 * @property string $specialty
 * @property string $year_entrance_m
 * @property string $year_ending_m
 * @property string $year_entrance_b
 * @property string $year_ending_b
 * @property string $month_entrance_m
 * @property string $month_ending_m
 * @property string $month_entrance_b
 * @property string $month_ending_b
 * @property integer $created_at
 * @property integer $updated_at
 */
class Vuz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vuz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'faculty', 'specialty', 'year_entrance_m', 'year_ending_m', 'year_entrance_b', 'year_ending_b', 'month_entrance_m', 'month_ending_m', 'month_entrance_b', 'month_ending_b'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'faculty' => 'Faculty',
            'specialty' => 'Specialty',
            'year_entrance_m' => 'Year Entrance M',
            'year_ending_m' => 'Year Ending M',
            'year_entrance_b' => 'Year Entrance B',
            'year_ending_b' => 'Year Ending B',
            'month_entrance_m' => 'Month Entrance M',
            'month_ending_m' => 'Month Ending M',
            'month_entrance_b' => 'Month Entrance B',
            'month_ending_b' => 'Month Ending B',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
