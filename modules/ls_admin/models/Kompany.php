<?php

namespace app\modules\ls_admin\models;

use Yii;

/**
 * This is the model class for table "kompany".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $site
 * @property string $pravovaya_form
 * @property string $user_post
 * @property string $industry
 * @property string $type_activity
 * @property string $full_name
 * @property string $fio_podpysanta
 * @property string $industry_podpysanta
 * @property string $u_adres
 * @property string $f_adres
 * @property string $inn
 * @property string $kpp
 * @property string $osnovanie
 * @property string $country
 * @property string $name_bank
 * @property string $country_bank
 * @property string $bik
 * @property string $kod
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Users $user
 */
class Kompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kompany';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['u_adres', 'f_adres'], 'string'],
            [['name', 'site', 'pravovaya_form', 'user_post', 'industry', 'type_activity', 'full_name', 'fio_podpysanta', 'industry_podpysanta', 'inn', 'kpp', 'osnovanie', 'country', 'name_bank', 'country_bank', 'bik', 'kod'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'site' => 'Site',
            'pravovaya_form' => 'Pravovaya Form',
            'user_post' => 'User Post',
            'industry' => 'Industry',
            'type_activity' => 'Type Activity',
            'full_name' => 'Full Name',
            'fio_podpysanta' => 'Fio Podpysanta',
            'industry_podpysanta' => 'Industry Podpysanta',
            'u_adres' => 'U Adres',
            'f_adres' => 'F Adres',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'osnovanie' => 'Osnovanie',
            'country' => 'Country',
            'name_bank' => 'Name Bank',
            'country_bank' => 'Country Bank',
            'bik' => 'Bik',
            'kod' => 'Kod',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
