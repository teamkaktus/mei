<?php

namespace app\modules\ls_admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "my_events".
 *
 * @property integer $id
 * @property integer $events_id
 * @property integer $user_id
 * @property integer $to_respond
 * @property integer $created_at
 * @property integer $updated_at
 */
class MyEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'my_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'user_id', 'to_respond', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events ID',
            'user_id' => 'User ID',
            'to_respond' => 'To Respond',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }
}
