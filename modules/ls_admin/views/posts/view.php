<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\News */

$this->title = $model->name;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту елемент?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab_1" aria-expanded="true">Основные</a></li>
<!--            <li class=""><a data-toggle="tab" href="#tab_2" aria-expanded="false">Картинка</a></li>-->
            <li class=""><a data-toggle="tab" href="#tab_3" aria-expanded="false">Seo оптимизация</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab_1" class="tab-pane active">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'id',
                        'name',
                        'type',
                        //'url:url',
                        'small_desc',
                        'desc:html',
                        'vizible_now:boolean',
                        'date_for',
                        //'updated_at:datetime'
                    ],
                ]) ?>
            </div><!-- /.tab-pane -->
<!--            <div id="tab_2" class="tab-pane">-->
<!--                --><?php //echo DetailView::widget([
//                    'model' => $model,
//                    'attributes' => [
//                        'img:image',
//                        'img_title',
//                        'img_alt',
//                    ],
//                ]) ?>
<!--            </div>-->
            <div id="tab_3" class="tab-pane">
                <?php echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'seo_title',
                        'seo_desc:ntext',
                        'seo_key',

                    ],
                ]) ?>
            </div>
        </div><!-- /.tab-content -->
    </div>

</div>
