<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\ls_admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            //'url:url',
//            'small_desc:ntext',
//            'desc:ntext',
            // 'img',
            // 'img_title',
            // 'img_alt',
            // 'seo_title',
            // 'seo_desc:ntext',
            // 'seo_key',

            [
                'attribute'=>'vizible_now',
                'format'=>'boolean',
                'filter' => array("0"=>"Нет","1"=>"Да"),
            ],
            [
                'attribute'=>'date_for',
                'filter' => \kartik\widgets\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_for',
                    'type' => \kartik\widgets\DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ]),
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
