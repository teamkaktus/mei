<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\VacancySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'kod') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'count_m') ?>

    <?php // echo $form->field($model, 'duties') ?>

    <?php // echo $form->field($model, 'education') ?>

    <?php // echo $form->field($model, 'experience') ?>

    <?php // echo $form->field($model, 'faculty') ?>

    <?php // echo $form->field($model, 'other') ?>

    <?php // echo $form->field($model, 'additional') ?>

    <?php // echo $form->field($model, 'salary_b') ?>

    <?php // echo $form->field($model, 'salary_e') ?>

    <?php // echo $form->field($model, 'premiums') ?>

    <?php // echo $form->field($model, 'results_sobesedovanyya') ?>

    <?php // echo $form->field($model, 'daily_work') ?>

    <?php // echo $form->field($model, 'probation') ?>

    <?php // echo $form->field($model, 'payment_probation') ?>

    <?php // echo $form->field($model, 'training_company') ?>

    <?php // echo $form->field($model, 'compensation_lunches') ?>

    <?php // echo $form->field($model, 'corporate_dining') ?>

    <?php // echo $form->field($model, 'lca') ?>

    <?php // echo $form->field($model, 'sick_pay') ?>

    <?php // echo $form->field($model, 'paid_holiday') ?>

    <?php // echo $form->field($model, 'corporate_transportation') ?>

    <?php // echo $form->field($model, 'fitness_club') ?>

    <?php // echo $form->field($model, 'mobile_payment') ?>

    <?php // echo $form->field($model, 'corporate_mobile_communication') ?>

    <?php // echo $form->field($model, 'business_trips') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'status_admin') ?>

    <?php // echo $form->field($model, 'additional_terms') ?>

    <?php // echo $form->field($model, 'ps_internet') ?>

    <?php // echo $form->field($model, 'ps_office') ?>

    <?php // echo $form->field($model, 'ps_photoshop') ?>

    <?php // echo $form->field($model, 'ps_autocad') ?>

    <?php // echo $form->field($model, 'ps_corel') ?>

    <?php // echo $form->field($model, 'ps_1c') ?>

    <?php // echo $form->field($model, 'auto_cat_a') ?>

    <?php // echo $form->field($model, 'auto_cat_b') ?>

    <?php // echo $form->field($model, 'auto_cat_c') ?>

    <?php // echo $form->field($model, 'auto_cat_d') ?>

    <?php // echo $form->field($model, 'auto_cat_e') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
