<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\Vacancy */

$this->title = $model->name;
?>
<div class="vacancy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту вакансию?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'user_id',
            [
                'label'=>'Работодатель',
                'value'=>  $model->user->surname.' '.$model->user->username.' '.$model->user->lastname,
            ],
            'name',
            'kod',
            'type',
            'count_m',
            'duties:ntext',
            'education',
            'experience',
            'faculty',
            'other:ntext',
            'additional:ntext',
            'salary_b',
            'salary_e',
            'premiums',
            'results_sobesedovanyya',
            'daily_work',
            'probation',
            'payment_probation',
            'training_company',
            'compensation_lunches',
            'corporate_dining',
            'lca',
            'sick_pay',
            'paid_holiday',
            'corporate_transportation',
            'fitness_club',
            'mobile_payment',
            'corporate_mobile_communication',
            'business_trips',
            'status',
            'status_admin',
            'additional_terms:ntext',
            'ps_internet',
            'ps_office',
            'ps_photoshop',
            'ps_autocad',
            'ps_corel',
            'ps_1c',
            'auto_cat_a',
            'auto_cat_b',
            'auto_cat_c',
            'auto_cat_d',
            'auto_cat_e',
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>

</div>
