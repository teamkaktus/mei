<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\ls_admin\models\VacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вакансии';
?>
<div class="vacancy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'user_id',
            [
                //'attribute'=>'user_id',
                'label'=>'Работодатель',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>function($data){
                    return $data->user->surname.' '.$data->user->username.' '.$data->user->lastname;
                },
            ],
            'name',
            'kod',
            //'type',
            [
                'attribute'=>'type',
                //'label'=>'Родительская категория',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>function($data){
                    return ($data->type == null) ? 'нет': 'добавлено';
                },
                'filter' => array(""=>"нет","1"=>"добавлено"),
            ],
            // 'count_m',
            // 'duties:ntext',
            // 'education',
            // 'experience',
            // 'faculty',
            // 'other:ntext',
            // 'additional:ntext',
            // 'salary_b',
            // 'salary_e',
            // 'premiums',
            // 'results_sobesedovanyya',
            // 'daily_work',
            // 'probation',
            // 'payment_probation',
            // 'training_company',
            // 'compensation_lunches',
            // 'corporate_dining',
            // 'lca',
            // 'sick_pay',
            // 'paid_holiday',
            // 'corporate_transportation',
            // 'fitness_club',
            // 'mobile_payment',
            // 'corporate_mobile_communication',
            // 'business_trips',
             'status',
            [
                'attribute'=>'status_admin',
                //'label'=>'Родительская категория',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>function($data){
                    return ($data->status_admin == null) ? 'ждет разрешения': 'одобрено';
                },
                'filter' => array(""=>"ждет разрешения","1"=>"одобрено"),
            ],
            // 'additional_terms:ntext',
            // 'ps_internet',
            // 'ps_office',
            // 'ps_photoshop',
            // 'ps_autocad',
            // 'ps_corel',
            // 'ps_1c',
            // 'auto_cat_a',
            // 'auto_cat_b',
            // 'auto_cat_c',
            // 'auto_cat_d',
            // 'auto_cat_e',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
