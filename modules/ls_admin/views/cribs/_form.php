<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model app\modules\ls_admin\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab_1" aria-expanded="true">Основные</a></li>
            <li class=""><a data-toggle="tab" href="#tab_2" aria-expanded="false">Картинка</a></li>
<!--            <li class=""><a data-toggle="tab" href="#tab_3" aria-expanded="false">Seo оптимизация</a></li>-->
        </ul>
        <div class="tab-content">
            <div id="tab_1" class="tab-pane active">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'vizible_now')->checkbox() ?>

                <?=
                 $form->field($model, 'date_for')->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter event time ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'mm/dd/yyyy hh:ii:ss',
                    ]
                ]);
                ?>

                <?php echo $form->field($model, 'small_desc')->textarea() ?>

                <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'full',
                    'clientOptions' => ElFinder::ckeditorOptions(['elfinder','path' => 'pages'], ['language'=>'ru']),
                ]) ?>
            </div><!-- /.tab-pane -->
            <div id="tab_2" class="tab-pane">
                <?= $form->field($model, 'img')->widget(InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                    'path'          => 'news',
                    'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false       // возможность выбора нескольких файлов
                ]);?>


                <?= $form->field($model, 'img_alt')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'img_title')->textInput(['maxlength' => true]) ?>
            </div><!-- /.tab-pane -->
<!--            <div id="tab_3" class="tab-pane">-->
                <?php //echo $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

                <?php //echo $form->field($model, 'seo_desc')->textarea(['rows' => 6]) ?>

                <?php //echo $form->field($model, 'seo_key')->textInput(['maxlength' => true]) ?>
<!--            </div>-->
        </div><!-- /.tab-content -->
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
