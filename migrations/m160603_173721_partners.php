<?php

use yii\db\Migration;
use yii\db\Schema;

class m160603_173721_partners extends Migration
{
    public function up()
    {
        $this->createTable('partners',[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL',
            'url' => Schema::TYPE_STRING,
            'site' => Schema::TYPE_STRING,
            'create_page' => Schema::TYPE_BOOLEAN,
            'desc' => Schema::TYPE_TEXT,
            'img' => Schema::TYPE_STRING,
            'img_title' => Schema::TYPE_STRING,
            'img_alt' => Schema::TYPE_STRING,
            'seo_title' => Schema::TYPE_STRING,
            'seo_desc' => Schema::TYPE_TEXT,
            'seo_key' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        echo "m160603_173721_partners cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
