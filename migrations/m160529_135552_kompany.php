<?php

use yii\db\Migration;
use yii\db\Schema;

class m160529_135552_kompany extends Migration
{
    public function up()
    {
        $this->createTable('kompany',[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'name'=> Schema::TYPE_STRING,
            'site'=> Schema::TYPE_STRING,
            'pravovaya_form'=> Schema::TYPE_STRING,
            'user_post'=> Schema::TYPE_STRING,
            'industry'=> Schema::TYPE_STRING,
            'type_activity'=> Schema::TYPE_STRING,
            'full_name'=> Schema::TYPE_STRING,
            'fio_podpysanta'=> Schema::TYPE_STRING,
            'industry_podpysanta'=> Schema::TYPE_STRING,
            'u_adres'=> Schema::TYPE_TEXT,
            'f_adres'=> Schema::TYPE_TEXT,
            'inn'=> Schema::TYPE_STRING,
            'kpp'=> Schema::TYPE_STRING,
            'osnovanie'=> Schema::TYPE_STRING,
            'country'=> Schema::TYPE_STRING,
            'name_bank'=> Schema::TYPE_STRING,
            'country_bank'=> Schema::TYPE_STRING,
            'bik'=> Schema::TYPE_STRING,
            'kod'=> Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('user', 'kompany', 'user_id');
        $this->addForeignKey(
            'FK_kompany', 'kompany', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('kompany');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
