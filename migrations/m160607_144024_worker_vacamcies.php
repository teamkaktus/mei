<?php

use yii\db\Migration;
use yii\db\Schema;

class m160607_144024_worker_vacamcies extends Migration
{
    public function up()
    {
        $this->createTable('worker_vacamcies',[
            'id' => Schema::TYPE_PK,
            'vac_id' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'to_respond' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        echo "m160607_144024_worker_vacamcies cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
