<?php

use yii\db\Migration;
use yii\db\Schema;

class m160521_125003_experience extends Migration
{
    public function up()
    {
        $this->createTable('experience',[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'experience' => Schema::TYPE_STRING,
            'sity_work' => Schema::TYPE_TEXT,
            'situation' => Schema::TYPE_TEXT,
            'year_work_b' => Schema::TYPE_STRING,
            'month_work_b' => Schema::TYPE_STRING,
            'year_work_e' => Schema::TYPE_STRING,
            'month_work_e' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('user', 'experience', 'user_id');
        $this->addForeignKey(
            'FK_experience', 'experience', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('experience');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
