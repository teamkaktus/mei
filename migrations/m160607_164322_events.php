<?php

use yii\db\Migration;
use yii\db\Schema;

class m160607_164322_events extends Migration
{
    public function up()
    {
        $this->createTable('events',[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL',
            'url' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_STRING,
            'file' => Schema::TYPE_STRING,
            'file_name' => Schema::TYPE_STRING,
            'small_desc' => Schema::TYPE_TEXT,
            'desc' => Schema::TYPE_TEXT,
            'img' => Schema::TYPE_STRING,
            'img_title' => Schema::TYPE_STRING,
            'img_alt' => Schema::TYPE_STRING,
            'seo_title' => Schema::TYPE_STRING,
            'seo_desc' => Schema::TYPE_TEXT,
            'seo_key' => Schema::TYPE_STRING,
            'vizible_now' => Schema::TYPE_BOOLEAN,
            'date_for' => Schema::TYPE_STRING,
            'date_for_time' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        echo "m160607_164322_events cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
