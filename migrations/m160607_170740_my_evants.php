<?php

use yii\db\Migration;
use yii\db\Schema;

class m160607_170740_my_evants extends Migration
{
    public function up()
    {
        $this->createTable('my_events',[
            'id' => Schema::TYPE_PK,
            'events_id' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'to_respond' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        echo "m160607_170740_my_evants cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
