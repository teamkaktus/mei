<?php

use yii\db\Migration;
use yii\db\Schema;

class m160521_124909_vuz extends Migration
{
    public function up()
    {
        $this->createTable('vuz',[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'faculty' => Schema::TYPE_STRING,
            'specialty' => Schema::TYPE_STRING,
            'year_entrance_m' => Schema::TYPE_STRING,
            'year_ending_m' => Schema::TYPE_STRING,
            'year_entrance_b' => Schema::TYPE_STRING,
            'year_ending_b' => Schema::TYPE_STRING,
            'month_entrance_m' => Schema::TYPE_STRING,
            'month_ending_m' => Schema::TYPE_STRING,
            'month_entrance_b' => Schema::TYPE_STRING,
            'month_ending_b' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('user', 'vuz', 'user_id');
        $this->addForeignKey(
            'FK_vuz', 'vuz', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('vuz');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
