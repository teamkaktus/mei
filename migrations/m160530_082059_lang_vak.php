<?php

use yii\db\Migration;
use yii\db\Schema;

class m160530_082059_lang_vak extends Migration
{
    public function up()
    {
        $this->createTable('lang_vac',[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'level' => Schema::TYPE_STRING,
            'vac_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('vac', 'lang_vac', 'vac_id');
        $this->addForeignKey(
            'FK_lang_vac', 'lang_vac', 'vac_id', 'vacancy', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('lang_vac');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
