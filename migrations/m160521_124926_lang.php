<?php

use yii\db\Migration;
use yii\db\Schema;

class m160521_124926_lang extends Migration
{
    public function up()
    {
        $this->createTable('lang',[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'level' => Schema::TYPE_STRING,
            'user_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('user', 'lang', 'user_id');
        $this->addForeignKey(
            'FK_lang', 'lang', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('lang');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
