<?php

use yii\db\Migration;
use yii\db\Schema;

class m160521_120921_rezume extends Migration
{
    public function up()
    {
        $this->createTable('rezume',[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'sity' => Schema::TYPE_STRING,
            'st_metro' => Schema::TYPE_STRING,
            'citizenship' => Schema::TYPE_STRING,
            'crossing' => Schema::TYPE_STRING,
            'native_lang' => Schema::TYPE_STRING,
            'sector_job' =>Schema::TYPE_STRING,
            'education' =>Schema::TYPE_STRING,
            'salary'=>Schema::TYPE_FLOAT,
            'experience'=>Schema::TYPE_STRING,
            'additional_education' =>Schema::TYPE_STRING,
            'courses' =>Schema::TYPE_STRING,
            'recommendations' =>Schema::TYPE_STRING,
            'hobbies' =>Schema::TYPE_STRING,
            'information' =>Schema::TYPE_STRING,
            'ps_internet' =>Schema::TYPE_BOOLEAN,
            'ps_office' =>Schema::TYPE_BOOLEAN,
            'ps_photoshop' =>Schema::TYPE_BOOLEAN,
            'ps_autocad' =>Schema::TYPE_BOOLEAN,
            'ps_corel' =>Schema::TYPE_BOOLEAN,
            'ps_1c' =>Schema::TYPE_BOOLEAN,
            'auto_cat_a' =>Schema::TYPE_BOOLEAN,
            'auto_cat_b' =>Schema::TYPE_BOOLEAN,
            'auto_cat_c' =>Schema::TYPE_BOOLEAN,
            'auto_cat_d' =>Schema::TYPE_BOOLEAN,
            'auto_cat_e' =>Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->createIndex('user', 'rezume', 'user_id');
        $this->addForeignKey(
            'FK_rezume', 'rezume', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('rezume');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
