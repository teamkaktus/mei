<?php

use yii\db\Migration;
use yii\db\Schema;

class m160530_065918_vacancy extends Migration
{
    public function up()
    {
        $this->createTable('vacancy',[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'name'=> Schema::TYPE_STRING,
            'kod'=> Schema::TYPE_STRING,
            'type'=> Schema::TYPE_BOOLEAN,
            'count_m'=> Schema::TYPE_INTEGER,
            'duties'=> Schema::TYPE_TEXT,
            'education'=> Schema::TYPE_STRING,
            'experience'=> Schema::TYPE_STRING,
            'faculty'=> Schema::TYPE_STRING,
            'other'=> Schema::TYPE_TEXT,
            'additional'=> Schema::TYPE_TEXT,
            'salary_b'=> Schema::TYPE_FLOAT,
            'salary_e'=> Schema::TYPE_FLOAT,
            'premiums'=> Schema::TYPE_STRING,
            'results_sobesedovanyya'=> Schema::TYPE_BOOLEAN,
            'daily_work'=> Schema::TYPE_STRING,
            'probation'=> Schema::TYPE_STRING,
            'payment_probation'=> Schema::TYPE_STRING,
            'training_company'=> Schema::TYPE_BOOLEAN,
            'compensation_lunches'=> Schema::TYPE_BOOLEAN,
            'corporate_dining'=> Schema::TYPE_BOOLEAN,
            'lca'=> Schema::TYPE_BOOLEAN,
            'sick_pay'=> Schema::TYPE_BOOLEAN,
            'paid_holiday'=> Schema::TYPE_BOOLEAN,
            'corporate_transportation'=> Schema::TYPE_BOOLEAN,
            'fitness_club'=> Schema::TYPE_BOOLEAN,
            'mobile_payment'=> Schema::TYPE_BOOLEAN,
            'corporate_mobile_communication'=> Schema::TYPE_BOOLEAN,
            'business_trips'=> Schema::TYPE_STRING,
            'status'=> Schema::TYPE_STRING,
            'status_admin'=> Schema::TYPE_BOOLEAN,
            'additional_terms'=> Schema::TYPE_TEXT,
            'small_desc'=> Schema::TYPE_TEXT,
            'ps_internet' =>Schema::TYPE_BOOLEAN,
            'ps_office' =>Schema::TYPE_BOOLEAN,
            'ps_photoshop' =>Schema::TYPE_BOOLEAN,
            'ps_autocad' =>Schema::TYPE_BOOLEAN,
            'ps_corel' =>Schema::TYPE_BOOLEAN,
            'ps_1c' =>Schema::TYPE_BOOLEAN,
            'auto_cat_a' =>Schema::TYPE_BOOLEAN,
            'auto_cat_b' =>Schema::TYPE_BOOLEAN,
            'auto_cat_c' =>Schema::TYPE_BOOLEAN,
            'auto_cat_d' =>Schema::TYPE_BOOLEAN,
            'auto_cat_e' =>Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('user', 'vacancy', 'user_id');
        $this->addForeignKey(
            'FK_vacancy', 'vacancy', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('vacancy');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
