<?php

namespace app\controllers;

use app\modules\ls_admin\models\Cribs;
use app\modules\ls_admin\models\Events;
use app\modules\ls_admin\models\Experience;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\Lang;
use app\modules\ls_admin\models\News;
use app\modules\ls_admin\models\Partners;
use app\modules\ls_admin\models\Posts;
use app\modules\ls_admin\models\RegForm;
use app\modules\ls_admin\models\Rezume;
use app\modules\ls_admin\models\User;
use app\modules\ls_admin\models\Vacancy;
use app\modules\ls_admin\models\Vuz;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionLogout()
    {
        Yii::$app->session['user']=null;
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    public function actionOut()
    {
        Yii::$app->session['user']=null;
        Yii::$app->user->logout();

        return $this->redirect('/signin');
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignin()
    {
        $error=false;
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->session['user']['role']=='соискатель')
                return $this->redirect(['/worker']);
            else
                return $this->redirect(['/employer']);
        }

        $model = new \app\modules\ls_admin\models\LoginForm();

        if (Yii::$app->request->post()){

            $model->user_login = Yii::$app->request->post('login');
            $model->password = Yii::$app->request->post('password');
            $model->rememberMe = Yii::$app->request->post('rememberMe');
            if ($model->login()) {

                if (Yii::$app->session['user']['role']=='соискатель')
                    return $this->redirect(['/worker']);
                else
                    return $this->redirect(['/employer']);
            }
            if ($model->validate()){
                $error=false;
            }else {
                $error =$model->errors;
            }
        }
        return $this->render('signin',[
            'model' => $model,
            'masseg' => $error,
        ]);
    }

    public function actionSignup()
    {
        $this->layout = 'main';
        $error = false;

        $model = new User();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post('worker')==1){
                $model->role='соискатель';
            }else{
                $model->role='работодатель';
            }
            $model->username = Yii::$app->request->post('username');
            $model->password = Yii::$app->request->post('password');
            $model->login = Yii::$app->request->post('login');
            $model->email = Yii::$app->request->post('email');
            $model->status = 10;
            $model->setPassword($model->password);
            $model->generateAuthKey();
            if ( $model->validate()){

                $model->save();
                if (Yii::$app->getUser()->login($model)):
                    if (Yii::$app->session['user']['role']=='соискатель'){
                        $rezume= new Rezume();
                        $rezume->user_id = $model->id;
                        $rezume->save();

                        $lang = new Lang();
                        $lang->user_id = $model->id;
                        $lang->save();

                        $vuz = new Vuz();
                        $vuz->user_id = $model->id;
                        $vuz->save();

                        $experience = new Experience();
                        $experience->user_id = $model->id;
                        $experience->save();

                        return $this->redirect(['/updaterezume']);
                    }
                    else{
                        $kompany= new Kompany();
                        $kompany->user_id = $model->id;
                        $kompany->save();
                        return $this->redirect(['/employer']);
                    }

                endif;
            }

            if ($model->validate()) {
                $error = false;
            } else {
                $error = $model->errors;
            }
        }
        return $this->render(
            'signup',
            [
                'model' => $model,
                'masseg' => $error,
            ]
        );
    }

    public function actionWorker()
    {
        $model = User::findOne(Yii::$app->user->id);
        $rezume = Rezume::find()->where(['user_id'=> Yii::$app->user->id])->one();
        $langs = Lang::find()->where(['user_id'=> Yii::$app->user->id])->all();
        $vuzs = Vuz::find()->where(['user_id'=>Yii::$app->user->id])->all();
        $experiences = Experience::find()->where(['user_id'=>Yii::$app->user->id])->all();
        return $this->render('worker',[
            'user'=>$model,
            'rezume'=>$rezume,
            'langs' => $langs,
            'vuzs' => $vuzs,
            'experiences' => $experiences,
        ]);
    }

    public function actionUpdaterezume()
    {
        $model = Rezume::find()->where(['user_id'=> Yii::$app->user->id])->one();
        $user = User::findOne(Yii::$app->user->id);
        $langs = Lang::find()->where(['user_id'=>Yii::$app->user->id])->all();
        $vuzs = Vuz::find()->where(['user_id'=>Yii::$app->user->id])->all();
        $experiences = Experience::find()->where(['user_id'=>Yii::$app->user->id])->all();
        //var_dump(Yii::$app->request->post());

        if (Yii::$app->request->post()){
            //var_dump(Yii::$app->request->post());
            $model_post=Yii::$app->request->post();
            $user->surname= $model_post['surname'];
            $user->lastname= $model_post['lastname'];
            $user->username= $model_post['username'];
            $user->login= $model_post['login'];
            $user->password= $model_post['password'];
            $user->email= $model_post['email'];
            $user->sex= $model_post['sex'];
            $user->tel_m= $model_post['tel_m'];
            $user->date=$model_post['date'];
            $user->setPassword($user->password);
            $user->save();

            $model->sity=$model_post['sity'];
            $model->st_metro=$model_post['st_metro'];
            $model->citizenship=$model_post['citizenship'];
            $model->crossing=$model_post['crossing'];
            $model->native_lang=$model_post['native_lang'];
            $model->sector_job=$model_post['sector_job'];
            $model->education=$model_post['education'];
            $model->sector_job=$model_post['sector_job'];
            $model->courses=$model_post['courses'];
            $model->hobbies=$model_post['hobbies'];
            $model->information=$model_post['information'];
            $model->sector_job=$model_post['sector_job'];
            $model->sector_job=$model_post['sector_job'];
            $model->salary=$model_post['salary'];
            $model->additional_education=$model_post['additional_education'];
            $model->recommendations=$model_post['recommendations'];
            $model->experience=$model_post['experience'];

            if (array_key_exists('ps_internet',$model_post))
                $model->ps_internet=$model_post['ps_internet'];
            else
                $model->ps_internet=null;

            if (array_key_exists('ps_office',$model_post))
                $model->ps_office=$model_post['ps_office'];
            else
                $model->ps_internet=null;
            if (array_key_exists('ps_photoshop',$model_post))
                $model->ps_photoshop=$model_post['ps_photoshop'];
            else
                $model->ps_photoshop=null;
            if (array_key_exists('ps_autocad',$model_post))
                $model->ps_autocad=$model_post['ps_autocad'];
            else
                $model->ps_autocad=null;
            if (array_key_exists('ps_corel',$model_post))
                $model->ps_corel=$model_post['ps_corel'];
            else
                $model->ps_corel=null;
            if (array_key_exists('ps_1c',$model_post))
                $model->ps_1c=$model_post['ps_1c'];
            else
                $model->ps_1c=null;


            if (array_key_exists('auto_cat_a',$model_post))
                $model->auto_cat_a=$model_post['auto_cat_a'];
            else
                $model->auto_cat_a=null;

            if (array_key_exists('auto_cat_b',$model_post))
                $model->auto_cat_b=$model_post['auto_cat_b'];
            else
                $model->auto_cat_b=null;

            if (array_key_exists('auto_cat_c',$model_post))
                $model->auto_cat_c=$model_post['auto_cat_c'];
            else
                $model->auto_cat_c=null;

            if (array_key_exists('auto_cat_d',$model_post))
                $model->auto_cat_d=$model_post['auto_cat_d'];
            else
                $model->auto_cat_d=null;
             if (array_key_exists('auto_cat_e',$model_post))
                $model->auto_cat_e=$model_post['auto_cat_e'];
            else
                $model->auto_cat_e=null;

            $model->save();

            $k=0;
            foreach($langs as $lang){
                $k++;
                if (($model_post['lang'.$k] !='')){
                    $lang->name=$model_post['lang'.$k];
                    $lang->level=$model_post['level'.$k];
                    $lang->save();
                } else {
                    if ($k==1){
                        $lang->name='';
                        $lang->level='';
                        $lang->save();
                    }

                    if ($k>1)
                        $lang->delete();
                }

            }

            if ($k<($model_post['count_lang']-1)){
                $i=$k+1;
                for($i; $i < ($model_post['count_lang']); $i++){
                    $item_lang= new Lang();
                    $item_lang->name=$model_post['lang'.$i];
                    $item_lang->level=$model_post['level'.$i];
                    $item_lang->user_id=Yii::$app->user->id;
                    $item_lang->save();
                }
            }

            $k=0;
            foreach($vuzs as $vuz){
                $k++;
                if (($model_post['vuz_name'.$k] !='')){
                    $vuz->name=$model_post['vuz_name'.$k];
                    $vuz->faculty=$model_post['vuz_faculty'.$k];
                    $vuz->specialty=$model_post['vuz_specialty'.$k];
                    $vuz->month_entrance_m=$model_post['m_b_m'.$k];
                    $vuz->month_entrance_b=$model_post['m_b_b'.$k];
                    $vuz->month_ending_m=$model_post['m_e_m'.$k];
                    $vuz->month_ending_b=$model_post['m_e_b'.$k];
                    $vuz->year_entrance_m=$model_post['y_b_m'.$k];
                    $vuz->year_entrance_b=$model_post['y_b_m'.$k];
                    $vuz->year_ending_m=$model_post['y_b_b'.$k];
                    $vuz->year_ending_b=$model_post['y_e_b'.$k];
                    $vuz->save();
                } else {
                    if ($k==1){
                        $vuz->name='';
                        $vuz->save();
                    }

                    if ($k>1)
                        $vuz->delete();
                }

            }
            if ($k<($model_post['count_vuz']-1)){
                $i=$k+1;
                for($i; $i < ($model_post['count_vuz']); $i++){
                    $item_vuz= new Vuz();
                    $item_vuz->name=$model_post['vuz_name'.$i];
                    $item_vuz->faculty=$model_post['vuz_faculty'.$i];
                    $item_vuz->specialty=$model_post['vuz_specialty'.$i];
                    $item_vuz->month_entrance_m=$model_post['m_b_m'.$i];
                    $item_vuz->month_entrance_b=$model_post['m_b_b'.$i];
                    $item_vuz->month_ending_m=$model_post['m_e_m'.$i];
                    $item_vuz->month_ending_b=$model_post['m_e_b'.$i];
                    $item_vuz->year_entrance_m=$model_post['y_b_m'.$i];
                    $item_vuz->year_entrance_b=$model_post['y_b_m'.$i];
                    $item_vuz->year_ending_m=$model_post['y_b_b'.$i];
                    $item_vuz->year_ending_b=$model_post['y_e_b'.$i];
                    $item_vuz->user_id=Yii::$app->user->id;
                    $item_vuz->save();
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            $k=0;
            foreach($experiences as $experience){
                $k++;
                if (($model_post['sity_work'.$k] !='')){
                    $experience->sity_work=$model_post['sity_work'.$k];
                    $experience->situation=$model_post['situation'.$k];
                    $experience->month_work_b=$model_post['m_w_b'.$k];
                    $experience->month_work_e=$model_post['m_w_e'.$k];
                    $experience->year_work_b=$model_post['y_w_b'.$k];
                    $experience->year_work_e=$model_post['y_w_e'.$k];
                    $experience->save();
                } else {
                    if ($k==1){
                        $experience->sity_work='';
                        $experience->situation='';
                        $experience->month_work_b='';
                        $experience->month_work_e='';
                        $experience->year_work_b='';
                        $experience->year_work_e='';
                        $experience->save();
                    }

                    if ($k>1)
                        $experience->delete();
                }

            }

            if ($k<($model_post['count_experience']-1)){
                $i=$k+1;
                for($i; $i < ($model_post['count_experience']); $i++){
                    $item_experience= new Experience();
                    $item_experience->sity_work=$model_post['sity_work'.$i];
                    $item_experience->situation=$model_post['situation'.$i];
                    $item_experience->month_work_b=$model_post['m_w_b'.$i];
                    $item_experience->month_work_e=$model_post['m_w_e'.$i];
                    $item_experience->year_work_b=$model_post['y_w_b'.$i];
                    $item_experience->year_work_e=$model_post['y_w_e'.$i];
                    $item_experience->user_id=Yii::$app->user->id;
                    $item_experience->save();
                }
            }


            return $this->redirect(['/worker']);

        }


        return $this->render('up_rezume',[
            'rezume'=>$model,
            'user'=>$user,
            'langs'=>$langs,
            'vuzs'=>$vuzs,
            'experiences'=>$experiences,
        ]);
    }

    public function actionEmployer()
    {
        $user = User::findOne(Yii::$app->user->id);

        $kompany = Kompany::find()->where(['user_id'=>Yii::$app->user->id])->one();
        return $this->render('employer',[
            'user'=>$user,
            'kompany'=>$kompany,
        ]);
    }

    public function actionEmployerupdate()
    {

        $user = User::findOne(Yii::$app->user->id);

        $kompany = Kompany::find()->where(['user_id'=>Yii::$app->user->id])->one();



        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->setPassword($user->password);
            $user->generateAuthKey();
            $user->save();
            if ($kompany->load(Yii::$app->request->post()) && $kompany->validate()) {
                $kompany->save();
                //var_dump(Yii::$app->request->post());
                return $this->redirect(['employer']);
            }else{
                return $this->render('employer_update',[
                    'user'=>$user,
                    'kompany'=>$kompany,
                ]);
            }


        } else {
            return $this->render('employer_update',[
                'user'=>$user,
                'kompany'=>$kompany,
            ]);
        }

    }

    public function actionNews()
    {
        if (Yii::$app->request->get('slug')) {
            $model = News::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('new', [
                'model' => $model,
            ]);
        } else {
            $model = News::find()->where(['<', 'updated_at', time()])->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('news', [
                'news' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionCribs()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Posts::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('crib', [
                'model' => $model,
            ]);
        } else {
            $model = Posts::find()->where(['<', 'updated_at', time()])->andWhere(['type'=>'Шпаргалка'])->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('cribs', [
                'models' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionPosts()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Posts::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('post', [
                'model' => $model,
            ]);
        } else {
            $model = Posts::find()->where(['<', 'updated_at', time()])->andWhere(['type'=>'Статья'])->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('posts', [
                'models' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionInformation()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Posts::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('post', [
                'model' => $model,
            ]);
        } else {
            $model = Posts::find()->where(['<', 'updated_at', time()])->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('information', [
                'models' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionEvents()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Events::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('event', [
                'model' => $model,
            ]);
        } else {
            $model = Events::find()->where(['>', 'date_for_time', time()])->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('events', [
                'models' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionArchivevents()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Events::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('event', [
                'model' => $model,
            ]);
        } else {
            $model = Events::find()->orderBy(['updated_at'=>SORT_DESC]);
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
            // приводим параметры в ссылке к ЧПУ
            $pages->pageSizeParam = false;
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('archivevents', [
                'models' => $models,
                'pages' => $pages,
            ]);
        }
    }

    public function actionPartner()
    {
        if (Yii::$app->request->get('slug')) {
            $model = Partners::find()->where(['url' => Yii::$app->request->get('slug')])->one();
            if (!isset($model)) {
                throw new NotFoundHttpException("The page was not found.");
            }
            return $this->render('post', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException("The page was not found.");
        }
    }

    public function actionVacancies(){

        var_dump(Yii::$app->request->post());
        $post=null;
        if (Yii::$app->request->post()){
            $post=Yii::$app->request->post();
            if(Yii::$app->request->post('name2')){
                $name=Yii::$app->request->post('name2');
                $post['name']=$name;
            }
            else {
                $name=Yii::$app->request->post('name');
            }


            $vacanc= Vacancy::find()
                ->where(['status'=>'1', 'status_admin'=>'1'])
                ->andFilterWhere(['like','education', Yii::$app->request->post('education')])
                ->andFilterWhere(['like', 'name', $name])
                ->andFilterWhere(['like', 'faculty', Yii::$app->request->post('facultet')])
                ->andFilterWhere(['<', 'salary_e', Yii::$app->request->post('salary_e')])
                ->andFilterWhere(['>', 'salary_b', Yii::$app->request->post('salary_b')])
                ->orderBy(['updated_at'=>SORT_DESC]);

        }else
            $vacanc= Vacancy::find()->where(['status'=>'1', 'status_admin'=>'1'])->orderBy(['updated_at'=>SORT_DESC]);

        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 10]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('vacancies', [
            'vacancies'=>$models,
            'pages' => $pages,
            'post' => $post,
            'count'=>$vacanc->count(),
        ]);
    }
    public function actionHotvacancies(){
        $vacanc= Vacancy::find()->where(['status'=>'1', 'status_admin'=>'1', 'type'=>1])->orderBy(['updated_at'=>SORT_DESC]);

        $pages = new Pagination(['totalCount' => $vacanc->count(), 'pageSize' => 10]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $vacanc->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('hot_vacancies', [
            'vacancies'=>$models,
            'pages' => $pages,
            'count' =>$vacanc->count()
        ]);
    }
}
