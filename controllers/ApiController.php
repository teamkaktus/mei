<?php

namespace app\controllers;
use app\modules\ls_admin\models\Experience;
use app\modules\ls_admin\models\Kompany;
use app\modules\ls_admin\models\Lang;
use app\modules\ls_admin\models\MyEvents;
use app\modules\ls_admin\models\Rezume;
use app\modules\ls_admin\models\Vacancy;
use app\modules\ls_admin\models\Vuz;
use app\modules\ls_admin\models\WorkerVacamcies;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\ls_admin\models\User;
use yii\helpers\Json;
use yii\imagine\Image;


class ApiController extends \yii\web\Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionPdf()
    {
        return $this->render('pdf');
    }

    public function actionRegister(){
        $model = new User();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post('worker')==1){
                $model->role='соискатель';
            }else{
                $model->role='работодатель';
            }
            $model->username = Yii::$app->request->post('username');
            $model->password = Yii::$app->request->post('password');
            $model->login = Yii::$app->request->post('login');
            $model->email = Yii::$app->request->post('email');
            $model->status = 10;
            $model->setPassword($model->password);
            $model->generateAuthKey();
            if ( $model->validate()){
                $model->save();
                if (Yii::$app->getUser()->login($model)):
                    Yii::$app->session['user']=$model;
                    if (Yii::$app->session['user']['role']=='соискатель'){
                        $rezume= new Rezume();
                        $rezume->user_id = $model->id;
                        $rezume->save();

                        $lang = new Lang();
                        $lang->user_id = $model->id;
                        $lang->save();

                        $vuz = new Vuz();
                        $vuz->user_id = $model->id;
                        $vuz->save();

                        $experience = new Experience();
                        $experience->user_id = $model->id;
                        $experience->save();
                        return $this->redirect('/worker');
                    } else{
                        $kompany= new Kompany();
                        $kompany->user_id = $model->id;
                        $kompany->save();
                        return $this->redirect(['/employer']);
                    }

                endif;
            }

            if ($model->validate()) {
                $error = false;
            } else {
                 return json_encode($model->errors);
            }
        }
    }

    public function actionUnicemail(){
        $email = $_POST['email'];
        if (User::find()->where(['email'=>$email])->one())
            return true;
        else
            return false;
    }

    public function actionUnicelogin(){
        $login = $_POST['login'];
        if (User::find()->where(['login'=>$login])->one())
            return true;
        else
            return false;
    }

    public function actionAvatar(){
        $user = User::findOne(Yii::$app->user->id);
        $file = \yii\web\UploadedFile::getInstanceByName('image');
        //var_dump($file);
        $file->extension;
        echo Yii::getAlias('@webroot');
        $file->saveAs(Yii::getAlias('@webroot').'/img/avatars/avatar_'.$user->id.'.'.$file->extension);
        $user->avar='/img/avatars/avatar_'.$user->id.'.'.$file->extension;
        $user->save();
        Image::thumbnail('@webroot'.$user->avar, 150, 150)
            ->save(Yii::getAlias('@webroot'.$user->avar), ['quality' => 50]);
        if ($user->role == 'соискатель')
            $this->redirect('/worker');
        else
            $this->redirect('/employer');

    }

    public function actionReport() {
        // get your HTML raw content without any layouts or scripts
        $rezume= Rezume::find()->where(['user_id'=>Yii::$app->user->id])->one();
        $user= User::findOne(Yii::$app->user->id);
        $content = $this->renderPartial('pdf',[
            'rezume'=>$rezume,
            'user'=>$user,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            //'cssFile' => '@web/frontend/css/main.css',
            // any css to be embedded if required
            'cssInline' => '.sum_param{color: #a9a9a9;
    float: left;
    font-size: 15px;
    line-height: 15px;
    margin: 0 2% 20px 0;
    width: 30%;}',
            // set mPDF properties on the fly
            //'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>['Krajee Report Header'],
//                'SetFooter'=>['{PAGENO}'],
//            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionUpvac() {
        $data = $_POST['data'];
        foreach($data as $value){
            $vac= Vacancy::findOne($value);
            $vac->status = '0';
            $vac->save();
            $vac->status = '1';
            $vac->save();
        }
        return true;
    }

    public function actionClosevac() {
        $data = $_POST['data'];
        foreach($data as $value){
            $vac= Vacancy::findOne($value);
            $vac->status = '0';
            $vac->save();
        }
        return true;
    }

    public function actionRespond() {
        $data = $_POST['data'];
        $user= User::findOne(Yii::$app->user->id);
        $admins = User::find()->where(['role'=>'admin'])->all();

        $bogy_admin= '<table><tr><td>код</td><td>Название компании</td><td>Название вакансии</td></tr>';

        foreach($data as $value){
            $my_vac = WorkerVacamcies::findOne($value);
            $my_vac->to_respond = 1;
            $my_vac->save();

            $vac = Vacancy::findOne($my_vac->vac_id);
            $kompany=Kompany::find()->where(['user_id'=>$vac->user_id])->one();
            $user_k= User::findOne($my_vac->user_id);

            $bogy_admin = $bogy_admin.'<tr><td>'.$vac->kod.'</td><td>'.$kompany->name.'</td><td>'.$vac->name.'</td></tr>';
            Yii::$app->mailer->compose('respond',['user'=>$user_k])
                ->setTo($user_k->email)
                ->setFrom('mei@gmail.com')
                ->setSubject('Отклик на вакансию от студента МЭИ')
                ->send();

        }

        $bogy_admin.='</table>';

        foreach($admins as $admin) {
            Yii::$app->mailer->compose()
                ->setTo($admin->email)
                ->setFrom('mei@gmail.com')
                ->setSubject($user->surname . ' ' . $user->username . ' ' . $user->lastname . ' ' . $user->date)
                ->setTextBody($bogy_admin)
                ->send();
        }
        return true;
    }

    public function actionVacaddworker() {
        $vac_id = $_POST['vac_id'];
        $item = WorkerVacamcies::find()->where(['vac_id'=>$vac_id,'user_id'=>Yii::$app->user->id])->one();
        if (!$item){
            $my_vac = new WorkerVacamcies();
            $my_vac->vac_id = $vac_id;
            $my_vac->user_id = Yii::$app->user->id;
            $my_vac->to_respond=0;
            $my_vac->save();
            return true;
        }else{
            return false;
        }

    }

    public function actionEventsreg() {
        $events_id = $_POST['events_id'];
        $item = MyEvents::find()->where(['events_id'=>$events_id])->one();
        if (!$item){
            $my_events = new MyEvents();
            $my_events->events_id = $events_id;
            $my_events->user_id = Yii::$app->user->id;
            $my_events->to_respond=0;
            $my_events->save();
            return true;
        }else{
            return false;
        }

    }

    public function actionVacancyname($q = null) {
        $vac = Vacancy::find()->select('name')->filterWhere(['like', 'name', $q])->distinct()->all();
        $out = [];
        foreach ($vac as $d) {
            $out[] =  $d['name'];
        }
        echo Json::encode($out);
        //echo $q;
    }

}
