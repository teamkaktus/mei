<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/css/main.css',
        'frontend/css/sweetalert2.css',
        'template_admin/assets/css/Form.css'
    ];
    public $js = [
        //'frontend/js/jquery-1.11.3.min.js',
        'frontend/js/owl.carousel.js',
        'frontend/js/jquery.formstyler.js',
        'frontend/js/jquery.arcticmodal-0.3.min.js',
        'frontend/js/sweetalert2.min.js',
        'frontend/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
